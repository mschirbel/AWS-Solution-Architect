# Thumbnail Generator

Vamos criar um serviço que cria thumbnails a partir de imagens em um bucket.

Vamos usar:

1. S3 Events
2. Timeouts
3. Memory
4. IAM Permissions
5. Plugins to deploy python
6. Custom Variables
7. Env Variables

Todo o código está em *resources/python-s3-thumbnail*.

No arquivo handler.py temos uma função chamada *s3_thumbnail_generator*:

```python
def s3_thumbnail_generator(event, context):
    # parse event
    print(event)
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = event['Records'][0]['s3']['object']['key']
    # only create a thumbnail on non thumbnail pictures
    if (not key.endswith("_thumbnail.png")):
        # get the image
        image = get_s3_image(bucket, key)
        # resize the image
        thumbnail = image_to_thumbnail(image)
        # get the new filename
        thumbnail_key = new_filename(key)
        # upload the file
        url = upload_to_s3(bucket, thumbnail_key, thumbnail)
        return url
```

Essa função tem um nome diferente do que vimos até agora, para ela ser acessível pelo Serverless, é preciso declará-la:

```yaml
functions:
  s3-thumbnail-generator:
    handler: handler.s3_thumbnail_generator
    events:
      - s3:
          bucket: ${self:custom.bucket}
          event: s3:ObjectCreated:*
          rules:
            - suffix: .png
```

É aqui que declaramos nosso trigger. 

Caso aconteça uma criação de objeto, e ele tenha por sufixo *.png*, aí sim ativamos a função *s3_thumbnail_generator* dentro do arquivo handler.

## s3_thumbnail_generator

Essa é a nossa função lambda. Podemos ter no nosso script python outras funções auxiliares. Mas essa será a função controlada pelo serviço Lambda.

Uma boa prática é sempre printar o *event* recebido pela função Lambda. Isso server como um debug, pois nunca sabemos quais eventos será trigger para nossas funções(nem sempre é o esperado).

Primeiro, vamos pegar as informações do nosso evento:

```python
bucket = event['Records'][0]['s3']['bucket']['name']
key = event['Records'][0]['s3']['object']['key']
```

Sendo bucket **ONDE** a imagem foi guardada. E **KEY** o caminho para encontrá-la.

Então, vamos pegar a imagem do bucket:

```python
image = get_s3_image(bucket, key)
```

Agora, vamos mudar o seu tamanho, trocar o nome e fazer upload:

```python
thumbnail = image_to_thumbnail(image)
        # get the new filename
        thumbnail_key = new_filename(key)
        # upload the file
        url = upload_to_s3(bucket, thumbnail_key, thumbnail)
```

Veja que existe uma variável de ambiente para armazenar o tamanho da imagem:

```python
size = int(os.environ['THUMBNAIL_SIZE'])
```

Temos um import muito importante, o **boto3**, ele é usado para interagir com recursos AWS. 

Temos funções do **boto3** como por exemplo:

```python
# criar um client
s3 = boto3.client('s3')
# pegar objetos do bucket
s3.get_object(Bucket=bucket, Key=key)
# colocar objetos no bucket
s3.put_object(
        ACL='public-read',
        Body=out_thumbnail,
        Bucket=bucket,
        ContentType='image/png',
        Key=key
    )
```

## Serverless.yml

Aqui, temos que declarar as permissões necessárias para nossa função acessar o S3 e pegar a image:

```yaml
iamRoleStatements:
   - Effect: "Allow"
     Action:
       - "s3:*"
     Resource: "*"
  environment:
     THUMBNAIL_SIZE: "128"
```

Também aumentamos a memória e o timeout, pois não sabemos quanto tempo nossa função demora para processar uma imagem:

```yaml
timeout: 10
memorySize: 128
```

Criamos também variáveis de ambiente dentro da tag *provider*:

```yaml
environment:
     THUMBNAIL_SIZE: "128"
```

Fora da tag *provider* podemos declarar variáveis custom:

```yaml
custom:
  bucket: mschirbel-s3-thumbnail-generator
  pythonRequirements:
   dockerizePip: true
```

E para acessar uma variável custom:

```yaml
events:
      - s3:
          bucket: ${self:custom.bucket}
```

O formato é esse:

```
${ARQUIVO:custom.NOMEDAVARIAVEL}
```

O bom de usar variáveis é que são reutilizáveis.

Outro ponto importante é o plugin:

```yaml
plugins:
  - serverless-python-requirements
```

Esse plugin vai instalar tudo que estiver no arquivo *requirements.txt*, para o Python são packages externas que devem ser instaladas.

## Deploy

Agora, vamos fazer o deploy:

```
sls deploy -v
```

O primeiro passo é instalar as packages do Python listadas no requirements.txt. E tudo isso vai ser enviado para o Lambda.

Depois, qualquer arquivo *png* que sobe para o bucket, vira uma thumbnail.