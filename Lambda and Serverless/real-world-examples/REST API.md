# Rest API with Serverless

Vamos ter uma CRUD Rest API com as seguintes funções:

1. create
2. list
3. update
4. delete
5. get

Nossa API vai se comunicar com um API Gateway que vai ser o trigger de nossa função Lambda.
Por fim, tudo será armazenado num DynamoDB.

O código foi retirado [dos exemplos oficiais](https://github.com/serverless/examples/tree/master/aws-node-rest-api-with-dynamodb).

## Package.json

É um arquivo, como o requirements.txt no Python. Serve para dizer quais são as dependências necessárias. No nosso caso, temos:

```json
{
  "name": "aws-rest-with-dynamodb",
  "version": "1.0.0",
  "description": "Serverless CRUD service exposing a REST HTTP interface",
  "author": "",
  "license": "MIT",
  "dependencies": {
    "uuid": "^2.0.3"
  }
}
```

Nossa dependência é *uuid*.

Para suprir essa dependência, vá até a pasta *resources/aws-node-rest-api-with-dynamodb/* e instale as dependências.

```
cd resources/aws-node-rest-api-with-dynamodb/
npm install
```

Isso vai criar uma pasta chamada *node-modules*, isso é necessário para o deploy da nossa função.

## Serverless.yaml

Nosso *provider* é bem comum para uma API node:

```yml
provider:
  name: aws
  runtime: nodejs6.10
```

### DynamoDB Table

E agora temos uma variável de ambiente:

```yaml
environment:
    DYNAMODB_TABLE: ${self:service}-${opt:stage, self:provider.stage}
```

Nossa tabela vai ter o mesmo nome do nosso serviço, declarado no começo do arquivo:

```yaml
service: serverless-rest-api-with-dynamodb
```

Seguido por uma option. Esse parâmetro pode ser passado ao realizar o comando de deploy. Caso não seja passado, vai receber o *stage* default, que é **dev**.

### IAM Role

```yaml
  iamRoleStatements:
    - Effect: Allow
      Action:
        - dynamodb:Query
        - dynamodb:Scan
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource: "arn:aws:dynamodb:${opt:region, self:provider.region}:*:table/${self:provider.environment.DYNAMODB_TABLE}"
```

Aqui temos uma IAM bem restritiva para o dynamodb, ou seja, não passamos um wildcard, mas setamos somente aquilo que será necessário.

O resource que teremos acesso é definido por:

```yaml
Resource: "arn:aws:dynamodb:${opt:region, self:provider.region}:*:table/${self:provider.environment.DYNAMODB_TABLE}"
```

Será sempre nesse formato, e assim podemos usar variáveis para remanejar recursos, caso seja necessário. 

### Functions

Agora, diferente dos exemplos anteriores, temos mais de uma função.

```yaml
functions:
  create:
    handler: todos/create.create
    events:
      - http:
          path: todos
          method: post
          cors: true

  list:
    handler: todos/list.list
    events:
      - http:
          path: todos
          method: get
          cors: true

  get:
    handler: todos/get.get
    events:
      - http:
          path: todos/{id}
          method: get
          cors: true

  update:
    handler: todos/update.update
    events:
      - http:
          path: todos/{id}
          method: put
          cors: true

  delete:
    handler: todos/delete.delete
    events:
      - http:
          path: todos/{id}
          method: delete
          cors: true
```

O handler está especificado para a pasta e o arquivo com a função. Sendo que, *todos/create* é o arquivo. E *create.create* é a função dentro do arquivo.

Temos aqui um *http event*, ou seja, caso seja feito uma request http, usando método post, a função receberá o trigger.

**cors** é para permitir que essa função seja chamada de outros domínios.

### Resources

Algo que podemos fazer com Serverless é especificar um template de CloudFormation. Isso permite que criemos nossa infraestrutura diretamente correspondente a nossa função

```yaml
resources:
  Resources:
    TodosDynamoDbTable:
      Type: 'AWS::DynamoDB::Table'
      DeletionPolicy: Retain
      Properties:
        AttributeDefinitions:
          -
            AttributeName: id
            AttributeType: S
        KeySchema:
          -
            AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        TableName: ${self:provider.environment.DYNAMODB_TABLE}
```

### Response

Quando criamos uma Lambda Function que está atrás de um API Gateway, temos que ter um método de **response**. Que é como a função retorna para o Gateway o que foi produzido, como por exemplo:

```js
// create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Item),
    };
    callback(null, response);
  });
```

Isso **deve** retornar um *JSON*. Esse JSON deve ter:

1. statusCode - http status
2. body - resultado

Essa **response** vem no **callback**. Que é o return do JS.

## Deploy

```
sls deploy -v
```

Isso vai fazer o deploy de toda a stack, inclusive o CloudFormation

Também vai mostrar na tela os Endpoints da nossa REST API.