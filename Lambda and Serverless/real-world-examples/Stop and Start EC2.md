# Stop and Start EC2 Instances

Vamos criar um CRON Job para fazer um stop nas instâncias de noite e um start pela manhã.

Todo dia às 9h, teremos um trigger que iniciará a instância
Todo dia às 5h, teremos um trigger que terminará a instância

Vamos usar:

1. Python
2. CloudWatch
3. IAM
4. Timeouts
5. Memory

Temos os arquivos em *resources//python-ec2-start-stop/*.

## Handler

```python
import boto3

ec2 = boto3.client('ec2')

def start_ec2(event, context):
    ec2_instances = get_all_ec2_ids()
    response = ec2.start_instances(
        InstanceIds=ec2_instances,
        DryRun=False
    )
    return response

def stop_ec2(event, context):
    ec2_instances = get_all_ec2_ids()
    response = ec2.stop_instances(
        InstanceIds=ec2_instances,
        DryRun=False
    )
    return response

# get the list of all the ec2 instances
def get_all_ec2_ids():
    response = ec2.describe_instances(DryRun=False)
    instances = []
    for reservation in response["Reservations"]:
        for instance in reservation["Instances"]:
            # This sample print will output entire Dictionary object
            # This will print will output the value of the Dictionary key 'InstanceId'
            instances.append(instance["InstanceId"])
    return instances
```

Importamos a library *boto3* responsável por administrar recursos na AWS.

Temos uma função para iniciar, pausar e coletar todas as instâncias EC2.

O mais importante, é que nas funções de stop e start, retornamos uma resposta, com os IDs das instância e o estado que ficarão.

## Serverless.yml

Temos no nosso provider as informações básicas bem como a Role para administração de EC2:

```yaml
provider:
  name: aws
  runtime: python2.7
  region: us-east-1
  profile: serverless-admin
  memorySize: 128
  iamRoleStatements:
   - Effect: "Allow"
     Action:
       - "ec2:*"
     Resource: "*"
```

Então temos as duas funções, e como executá-las em CRON Jobs:

```yaml
functions:
  start-ec2:
    handler: handler.start_ec2
    timeout: 60
    events:
      - schedule: cron(0 13 * * ? *) # run at 1 PM UTC every day (9 am EST)
  stop-ec2:
    handler: handler.stop_ec2
    timeout: 60
    events:
      - schedule: cron(0 21 * * ? *) # run at 9 PM UTC every day (5 pm EST)
```

Veja que o horário é sempre em UTC, devemos converter para qual fuso horário desejamos.

## Deploy

```
sls deploy -v
```