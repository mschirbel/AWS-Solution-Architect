# AWS Lambda

AWS é um cloud provider, que oferece serviços segundo a demanda e podemos escalá-los rapidamente e de forma bem fácil.

Agora, se não quisermos mais ter um servidor, basta parar de pagar por ele.

Em 2014 foi lançado o AWS Lambda

## Lambda

Servidores para a AWS são feitos em instâncias EC2, configuradas pelo usuário.

Scaling é adicionar ou remover servidores. Seja automático ou manual.

Mas e se não houvessem servidores para o usuário? Com Lambda somente temos **funções**. Essas funções funcionam por tempo, e não por máquina.

Agora não temos mais que administrar a infraestrutura. Somente as funções.

### Pros

1. São baratas
2. Pagamos por request
3. Se integra com toda a AWS
4. Tem várias linguagens de programações
5. Muito fácil de monitorar
6. Fácil de escalar

### Languages

1. nodejs
2. python
3. java
4. groovy
5. scala
6. c#

### Main Integrations

- API Gateway - fornece um API para as funções lambdas
- Kinesis - streaming de dados, assim que os dados chegam, são processados
- DynamoDB - nosql db.
- S3 - storage de objetos
- IoT
- CloudWatch
- SNS
- Cognito

#### Example: Thumbnail creation

Vamos supor que tenha uma imagem no S3. Queremos criar uma thumbnail da imagem que está no S3. Isso vai ter um trigger para uma Lambda Function que vai criar a thumbnail.
Assim que terminar, vai salvar no S3 de novo e vai guardar os metadados no DynamoDB.

Esse é um exemplo que faremos mais adiante.

## Pricing

Podemos ver o preço, **sempre** [nesse link](https://aws.amazon.con/lambda/pricing/)

O preço é dividido em dois:

1. Preço de chamadas:
   1. O preço das 1.000.000 primeiras requisições são grátis.
   2. Depois, $0.20 por cada 1.000.000 milhão de requisições.

2. Preço de duração:
   1. 400.000GB/sec por mês, são grátis
   2. Depois, $1.00 por 600.000 GB/sec