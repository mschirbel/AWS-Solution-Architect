# First Function

Entre na sua conta AWS e vá no serviço do Lambda.

Clique em *Get Started Now*.

Aqui temos alguns blueprints, que são exemplos que a AWS coloca para você escolher. Você também pode criar a sua do zero.

Filtre por *Hello*

Escolha uma *hello-world-python*.

![](../images/Anotação-2019-04-16-231528.jpg)

Isso vai criar uma função de Hello World para nós.

Agora, devemos escolher um trigger(event) para nossa função. Como nossa função não dependerá de nenhum outro serviço, podemos deixar em branco e clicar em *Next*

![](../images/Anotação-2019-04-16-233437.jpg)

Agora, daremos um nome para nossa função, bem como uma descrição.

*Runtime* é qual a linguagem que escolheremos:

![](../images/Anotação-2019-04-16-233543.jpg)

O código deixaremos como definido para Python2.7

Para a parte de *Handlers*:

![](../images/Anotação-2019-04-16-233825.jpg)

O handler deve corresponder ao nome dado na sua função. E a role deve possuir o necessário para alterar os serviços que fora utilizados.

De resto, podemos deixar como *default*.

Clicamos em *Next* e depois em *Create Function*.

Agora, podemos testar nossa função. Para isso, criaremos um *input test event*.

Clique em *Actions* -> *Input Test Event*.

Aqui, temos um arquivo JSON que será a entrada de nossa função

![](../images/Anotação-2019-04-16-234051.jpg)

Clique em Save and Test.

Receberemos o retorno da função e o *log* da função.

Podemos ver também a monitoração de nossa função

![](../images/Anotação-2019-04-16-234324.jpg)

## Using Serverless

Antes de tudo, veja o arquivo *Install Serverless.md*.

Vamos usar esse framework para fazer o *deploy*.

Para usar o comando, usamos:

```
serverless
sls
```

Dos dois jeitos obteremos o mesmo resultado.

Para criar uma nova função:

```
sls create --template aws-python --path hello-world-python
```

Isso vai gerar um diretório chamado *hello-world-python*

Temos aqui dois arquivos:

- handler.py
- serverless.yml

Handler é o arquivo que contém nossa função.

Podemos apagar o que tem nele e deixar do seguinte jeito:

```python
def hello(event, context):
    print("hi")
    return "hello-world"
```

Agora, abrimos o arquivo serverless.yml e encontramos a parte de provider, ficará assim:

```yaml
provider:
    name: aws
    runtime: python3
    profile: serverless-admin
    region: us-east-1
```

Adicionamos o profile que criamos e qual region estamos usando na nossa função Lambda.

Para fazer o deploy dessa função:

```
sls deploy -v
```

No final, teremos um *Stack Output*, contendo

- ARN da Lambda Function
- S3 Bucket do deploy

Para verificar se isso funcionou, podemos ir no console da AWS, no serviço Lambda e checamos se a função está lá.