# Serverless Framework

É um framework que auxilia a criar, controlar, aplicar e debuggar as funções Lambda.

Pode ser acessado [aqui](https://serverless.com/)

Tem suporte a CloudFormation e é integrável com CI/CD.

## Instalar

É necessário ter o Node e a AWSCLI.

Passos:

1. Instalar node
2. Instalar awscli
3. Instalar o serverless framework
4. criar um user na aws para o framework
5. download de credenciais

## Node

Para instalar o node, podemos ver o download [aqui](https://nodejs.org/en/download/).

## awscli

Podemos instalar via pip:

```
pip install awscli
```

Ou via node:

```
npm install aws-cli -g
```

## Serverless

Agora, vamos instalar o serverless:

```
npm install serverless -g
```

## User

Agora vamos no serviço de IAM na AWS e adicione um novo usuário chamado *serverless-admin* com Programatic access.

Dê full admin acess para esse usuário, visto que ainda não temos uma policy específica.

Baixe o *.csv* com as Keys do usuário.

## Fazer o Serverless usar essas credenciais

```
serverless config credentials --provider aws --key [KEY] --secret [SECRET] --profile serverless-admin
```

Sendo [KEY] e [SECRET] o conteúdo do csv.