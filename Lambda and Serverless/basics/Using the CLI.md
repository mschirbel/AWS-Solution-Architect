# Using the Serverless CLI

```
sls invoke -f hello -l
```

Esse comando nos permite invocar uma função que está criada em nossa conta AWS.
O parâmetro *-l* é para registrar logs.

Passamos a função hello que criamos no arquivo "Hello World Lambda.md"

Ele vai printar o resultado da função.
Juntamente com o começo, inicio, duração e memória da função.

## Update function

Vamos modificar a nossa função, de:

```python
def hello(event, context):
    print("hi")
    return "hello-world"
```

para:

```python
def hello(event, context):
    print("first update")
    return "hello-world"
```

Para fazer o deploy:

```
sls deploy -v
```

Isso vai criar toda a stack necessária para rodar a função.

Para conferir se está tudo correto e atualizado:

```
sls invoke -f hello -l
```

Agora, mudaremos o conteúdo da função para:

```python
def hello(event, context):
    print("second update")
    return "hello-world"
```

E ao invés de fazer o deploy de toda a stack, vamos fazer somente da função:

```
sls deploy function -f hello
```

Veja que o processo foi muito mais rápido.

Se invocarmos a função, o resultado é a função atualizada:

```
sls invoke -f hello -l
```

A principal diferença é que, temos que fazer o deploy de toda a stack ou quando temos uma mudança muito grande, usaremos o deploy total.

Mas se alterarmos pequenas partes da função, usaremos somente o deploy function, que é bem mais rápido.

## Logs de funções

As logs existem? São guardadas em algum lugar?

Sim e sim.

Podemos ver elas pelo console, no CloudWatch.
Na aba de *Monitoring* do serviço de Lambda tem um link para o cloudwatch:

![](../images/Anotação-2019-04-17-204737.jpg)

Assim podemos ver todos os eventos de logs das nossas funções.

Para ver na cli, podemos ver usando o comando:

```
sls logs -f hello -t
```

-t é para fazer um *tail* das logs.
Esse é um comando de stream, ou seja, é atualizado conforme as logs vão acontecendo.

## Destruir uma função

Problemas:

1. Remover a função
2. Remover as dependências da função
3. Remover as logs do CloudWatch
4. Remover as Roles
5. Remover quaisquer outras coisas que o Serverless tenha criado

Serverless faz isso para nós com o comando:

```
sls remove
```

Isso vai remover todas as funções.

Para remover somente uma delas, você deve editar o arquivo serverless.yml, removendo de lá a função.

Depois, rode o comando:

```
sls deploy
```