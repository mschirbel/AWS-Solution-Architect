# VPC in AWS

Usamos VPC's para proteger os recursos que subiremos.

As funções Lambdas podem receber security groups também. E assim poderão acessar com segurança seus recursos.

Podemos colocar uma Lambda Function dentro de uma subnet, seja ela privada ou pública. Assim vai ter um IP de acordo com a subnet.

Para setar uma VPC, podemos fazer isso no *provider*:

```yaml
provider:
  name: aws
  runtime: python2.7
  profile: serverless-admin
  region: us-east-1
  vpc:
    securityGroupIds:
      - sg-8a73a7fb
    subnetIds:
      - subnet-c69251b1
      - subnet-07816c5e
```

Podemos fazer isso também para cada uma das funções.

Após isso, basta fazer o deploy.

```
sls deploy -v
```

Automaticamente é adicionado uma policy ao usuário que roda as funções Lambda, chamada *AWSLambdaVPCAccessExecutionRole*. Essa policy permite executar todos os comandos sobre os recursos de uma VPC.