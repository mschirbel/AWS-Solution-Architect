# Serverless Runtime

Agora veremos como funciona o runtime de uma função Lambda usando o Serverless.

Vamos ver também cada um dos arquivos criados.

## Create with any runtime

Se rodarmos o comando:

```
sls create
```

Veremos que isso não funciona, pois precisamos de mais alguns parâmetros. Um deles é o *--template*. Isso informa para o Serverless qual runtime usar para rodar a função.

```
sls create --template aws-csharp --path csharp-example
```

Isso criou uma pasta chamada csharp-example.

Isso cria três arquivos dentro dessa pasta:

1. .gitignore - para não subir alguns arquivos para versionamento
2. handler - onde vai ser escrita a função
3. serverless.yml - metadados dessa função

Para cada runtime(linguagem) teremos variações nesses arquivos. Mas em geral seguem essa ideia.

Podemos ver alguns exemplos dentro da pasta *resources*:

1. nodejs-example
2. java-maven-example
3. csharp-example
4. hello-world-python

## Timeout and Memory

Para isso, vamos usar a função de hello-world python.

Temos o arquivo serverless.yml. Vamos inserir mais uma função nele:

```yaml
functions:
  hello-short-timeout:
    handler: handler.hello
    memorySize: 128
    timeout: 3
  hello-long-timeout:
    handler: handler.hello
    memorySize: 256
    timeout: 6
```

E no handler.py:

```python
import time

def hello(event, context):
    print("second update")
    time.sleep(4)
    return "hello-world"
```

Agora temos um sleep na função. Como o sleep dura 4 segundos, ele é maior do que o timeout da primeira função. Logo, a função vai entrar no timeout.

Mas não na segunda, pois o timeout é maior do que o sleep. Logo, ela vai nos retornar algo.

Agora, vamos aplicar:

``` 
sls deploy -v
```

Veja no console que temos as duas funções. Entre nas funções e execute um teste em cada uma delas.

Veja que na *hello-short-timeout* houve um erro.

É possível declarar o timeout e o tamanho da memória dentro do provider. Assim será feito uma herança sobre quaisquer valores declarados abaixo, como por exemplo:

```yaml
provider:
  name: aws
  runtime: python2.7
  profile: serverless-admin
  region: us-east-1
  memorySize: 512
  timeout: 2
```

Mas se o valor estiver declarado na função, ele vai sobrescrever o do *provider*