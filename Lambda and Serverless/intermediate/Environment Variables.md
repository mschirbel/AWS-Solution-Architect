# Environment Variables

Variáveis de ambiente são usadas para trazer configurações externas as funções.

Podemos alterar o retorno de uma função sem trocar o código.

Para isso, temos na pasta *resources/python-example-environment-variable/*

Para pegar uma variável de ambiente do SO, declaramos do seguinte jeito:

```python
import os

print(os.environ['ENV_NAME'])
```

Em nossa função, teremos algo parecido:

```python
import os


def hello(event, context):
    return os.environ['FIRST_NAME']
```

E em nosso arquivo serverless.yml:

```yaml
 environment:
    variable1: value1
    variable2: value2
    FIRST_NAME: "John"
```

Assim, teremos variáveis globais, dentro da tag *provider*, a todas as nossas funções.

Caso seja necessário somente em uma função:

```yaml
functions:
  hello-env-john:
    handler: handler.hello
  hello-env-marc:
    handler: handler.hello
    environment:
      FIRST_NAME: "Marc"
```

Assim, a função *hello-env-John* vai puxar a variável global. Mas a função *hello-env-marc* vai sobrepor com a variável "Marc".

Agora basta fazer o deploy:

```
sls deploy -v
```