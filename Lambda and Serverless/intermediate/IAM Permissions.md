# IAM Permissions

Nossas funções, muito provavelmente, precisarão acessar outros serviços.

Por default, elas não são liberadas para acessar nada.
Precisamos, explicitamente, declarar isso. Usamos uma IAM policy para fazer isso.

Lembre-se que criamos um usuário com Full Admin Access, mas as nossas funções só podem ter aquilo que **realmente** necessitam.

Na pasta *resources/python-example-iam/* temos alguns arquivos:

handler.py:

```python
import boto3

# Note: It's always preferable to define your boto3 outside of your functions
client = boto3.client('lambda')

def hello(event, context):
    response = client.list_functions()
    return response
```

Se fizermos o deploy dessa função, com o serverless.yml default, ocorrerá um erro. Isso acontece porque tentamos acessar uma API que não temos permissão.

Para adicionar uma policy, precisamos declarar um *iamRoleStatements*:

```yaml
provider:
  name: aws
  runtime: python2.7
  profile: serverless-admin
  region: us-east-1
# you can add statements to the Lambda function's IAM Role here
  iamRoleStatements:
    - Effect: "Allow"
      Action:
        - "lambda:*"
      Resource:
        - "*"
```

É um bloco que está dentro do *provider* e isso dá acesso a qualquer função do Lambda em qualquer recurso da AWS.

Perceba também que foi criada uma Policy no IAM com o que foi declarado no Serverless:

![](../images/Anotação-2019-04-17-234004.jpg)