# Biling Model

Para controlar nossos gastos, podemos usar um novo widget dentro do serviço do Billing.

![](../media/billing.png)

Se entrarmos nessa parte, podemos clicar em *Preferences* e colocar um e-mail para recer os alertas:

![](../media/billing2.png)