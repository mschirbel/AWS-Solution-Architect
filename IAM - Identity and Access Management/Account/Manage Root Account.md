# Root Account

Nossa conta root é uma das peças mais importantes da AWS. É o foco de segurança quando criamos uma nova conta.
O padrão de segurança é trancar o acesso root, e nunca mais usar. Entretanto, podemos ainda precisar dele, como por exemplo:
- Habilitar o MFA
- Criar/Deletar Keys
- Trocar a senha

Para todos os outros serviços, podemos usar usuários e chaves de acesso.

## Best Practices
- Usar MFA
- Deletar as Access Keys do root
- Deletar, após uso, as access keys de outros usuários
- Trocar a senha do root para uma mais segura.
- Criar usuários para uso de serviços

### MFA

A primeira coisa que precisamos fazer é habilitar o MFA na conta root da AWS. Além de poder habilitar para a root, podemos habilitar para usuários.

Existem dois tipos de MFA:
1. Hardware MFA
2. Virtual MFA

Hardware é o que usamos com devices físicos, por exemplo, quando usamos um código que troca a cada minuto, quando plugamos no computador o device.

Virtual é quando usamos um aplicativo no nosso celular, por exemplo, para obtermos os códigos que rotacionam. O site fica com uma ligação com o celular usado.

Para habilitar o MFA(virtual) devemos:
0. Fazer o download de algum aplicativo de autenticação
1. Fazer o login na conta root
2. Entre no serviço do IAM
3. clique em `Activate MFA` e depois em `Manage MFA`
4. Faça a leitura do QR Code dentro do aplicativo.
5. Confirme os códigos

Depois disso verá uma mensagem de sucesso. E na próxima vez que logar na AWS, na conta root, teremos que usar os códigos do app.

## Quais tarefas precisam de root?

Existem algumas tarefas que sim, precisam acesso de root. E depois de fazê-las, podemos *trancar* esse usuário:

- trocar a forma de pagamento
- encerrar a conta
- se inscrever para aws gov
- transferir domínio de R53 para outra conta
- trocar o tipo de suporte
- encontrar o ID canônico da conta(dentro de security credentials)
- remover a porta 25 das EC2
- requisitar teste de penetração na infra
- solicitar ec2 para ter ids duradouros
- criar um certificado X.509 criado pela AWS. Isso serve para chamadas SOAP
- criar um key pair do cloudfront