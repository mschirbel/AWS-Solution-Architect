# IAM Roles

Roles são identidades, e assim como usuários, possuem autorizações para os serviços da AWS. Mas as Roles podem ser associadas com outras identidades, e assim podem se passar como essa role para obter outras autorizações.

O principal fator de uma role é que é **temporário**. Então podemos definir acessos para grupos ou serviços ou até programas, facilitamos a comunicação usando Roles.

![](../media/role.png)

Podemos ter usuários de outras contas, ou até mesmo de fontes externas para assumir Roles. Para isso, existe uma *trust policy*, que controla quem pode assumir essa Role. Ou seja, são duas partes importanets para assumir essa Role. O nome disso é *Federation*, que é criar uma confiança entre a AWS e outro domínio de usuários.