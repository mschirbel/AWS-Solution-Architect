# EC2 Permissions with Roles

![](../media/ec2_role.png)

Quando usamos uma role, não temos que usar credenciais, como access_keys e secret_keys.
Também não precisamos controlar essas credenciais, como rotatividade. E nem ao menos nos preocupar se usarmos a mesma role em várias máquinas, basta alterar a role que as máquinas já terão as permissões.

```json
{
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Allow",
        "Action":  "s3::*",
        "Resource": "*",
    }
}
```

Com essa policy, podemos ter total controle sobre o S3, de dentro de uma EC2. Assim podemos usar comandos como `aws s3 ls`.