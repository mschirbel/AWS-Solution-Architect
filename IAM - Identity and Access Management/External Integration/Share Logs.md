# Share CloudTrail Logs between Accounts

Vamos ter um bucket S3 para todas as logs de nossas contas AWS.

![](../media/ctrail.png)

Primeiramente destinamos um *trail* para nossos logs a um bucket específico. Agora, criamos uma policy nesse bucket para receber objetos de outras contas:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
                "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "<ARN BUCKET>"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
                "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": [
                "<arn:aws:s3::<bucketname>/<PrefixName>/<AccountID>/*",
                "<arn:aws:s3::<bucketname>/<PrefixName>/<AccountID>/*",
                "<arn:aws:s3::<bucketname>/<PrefixName>/<AccountID>/*"
            ],
            "Contition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
```

E quando formos criar outro *trail* em outra conta, basta colocar o nome do bucket no destino, como os buckets são globais, não tem problema.