# Access to Third Parties

Para isso, precisamos criar uma IAM Role para acesso aos recursos. E na parte de *Trust Policy*, garantimos que o externo tem acesso a nossa role. Depois mandamos o *arn* da Role para o externo, assim ele pode chamar o *assumeRole* usando essa Role.

Para criar a Role com outra conta AWS use:

![](../media/thirdpt.png)

