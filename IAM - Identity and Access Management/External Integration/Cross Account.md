# Cross Account

Para fazer o acesso de uma conta para outra, podemos criar uma Role. Para isso, selecionamos a opção `Another AWS account`, na tela de criação:

![](../media/across_role.png)

Depois, especificamos o ID da outra conta, e podemos usar até MFA para login.

Para a conta, precisamos permitir que seja feito o *assume role*:

```json
{
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Allow",
        "Action": "sts:AssumeRole",
        "Resource": "<arn da role>"
    }
}
```