# Confused Deputy

O problema é quando criamos um *trust* com um third party mas não especificamos um **ExternalID**. Sem isso, o externo pode adivinhar nosso ARN e acessar nossos recursos.

Para isso, podemos usar o ExternalID na *trust policy*, assim somente a conta que tiver o ExternalID pode assumir a role. Para isso, ao criarmos a Role usamos do seguinte jeito:

![](../media/confdept.png)

A trust policy seria mais ou menos assim:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<EXTERNAL ID>:root"
            },
            "Action": "sts:AssumeRole",
            "Condition": {
                "StringEquals": {
                    "sts:ExternalId": "<EXTERNALID>"
                }
            }
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<YOURID>:role/<YOUR ROLE TO ACCESS>"
            },
            "Action": "sts:AssumeRole"    
        }
    ]
}
```

Para testar use os comandos:

```
aws sts assume-role --role-arn <arn> --role-session-name <name> --external-id <externalid>
```