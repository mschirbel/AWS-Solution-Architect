# Federation

Podemos usar o playground de Federation da AWS para simular o acesso a um bucket. E podemos ver a comunicação, bem como os dados, sendo transitados entre as federações.

Podemos entrar no link abaixo:
https://web-identity-federation-playground.s3.amazonaws.com/index.html

Primeiro fazemos a autenticação no Identity Provider externo. Com isso, recebemos um ID Token, com esse token podemos assumir uma Role na AWS, graças a verificação do Token. Depois, recebemos autorização, via as policies que estão na role. Todas as credenciais, então passadas ao usuário, são temporárias para o acesso as serviços da AWS.

![](../media/federation.png)

