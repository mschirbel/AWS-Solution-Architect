# IAM - Identity and Access Management Course

Linux Academy Course

## Grade

1. Account
    - Biling and Free Tier
    - Root User
2. IAM Objects
    - Users and Groups
    - Employee Account
    - MFA
3. Policies
    - Policies for Users
    - Temporary Credentials
    - Limited Permissions
    - Inline Policies
    - S3 Bucket
4. Roles
    - Strategies
    - EC2 Roles
5. External Integration
    - Cross Account & Federation
    - Third Party
    - Confused Deputy
    - Share Logs
6. Services Integration
    - EC2 Profiles
    - AssumeRole with Python
    - Cloudformation
    - Best Practices
    - Troubleshoot