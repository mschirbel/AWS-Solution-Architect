# Inline Policies

Inline Policies são úteis quando queremos controlar granularmente nossos usuários, grupos ou roles. Podemos criar policies de recursos que aindão não são gerenciados, tanto para adicionar quanto para remover.

Para fazer debug também é bom.

Para criar uma, vamos no IAM, depois em Users e selecionamos o usuários que vamos adicionar a policy.
Clicamos em 'Add Inline Policy' e colamos o JSON, ou usamos o editor visual.

Por exemplo, podemos usar uma policy para o ACM:

```json
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Action": "acm:ListCertificates",
    "Resource": "*"
  }]
}
```