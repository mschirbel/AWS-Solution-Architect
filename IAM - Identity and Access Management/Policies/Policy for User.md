# Policy

## Overview

Os conceitos principais que devemos ter em mente quando construimos uma policy são:
1. todas as requisições são negadas por padrão
2. temos que ter um *permitir explícito* para que tenhamos a permissão
3. se houver um *negar explícito* ele supera o *permitir explícito*.
4. somente o root tem todo o acesso sem precisar de policies
5. existem dois tipos de policies: Identity Based e Resourced Based

![](../media/policy-types.png)

Uma policy está determinada por um *Principal* que é o alvo dos *Effects* sobre os *Resources* especificados no documento.

## Identity Policies 

Podem ser de dois tipos: Managed Policies e Inline Policies. As Managed podem ser controladas pela AWS ou por algum *customer*.

As Inline Policies são criadas por um usuários e podem ser attachadas em um user, group ou role.

Por boas práticas, devemos sempre usar as Managed Policies, mas podemos usar a Inline para um propósito específico.

## Policies for All Users

Temos que considerar algumas políticas que seriam aplicadas a todos os funcionários de uma empresa. Como uma que nega acesso de fora do CIDR da empresa. Por exemplo:

```json
{
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Deny",
        "Action": "*",
        "Resource": "*",
        "Condition": {
            "NotIpAddress": {
                "aws:SourceIp": [
                    "192.103.10.0/24",
                    "192.103.20.0/24"
                ]
            }
        }
    }
}
```

Assim negamos o acesso a nossa AWS de pessoas externas.
A *Version* é sempre a mesma.
O elemento de *Statement* é o principal, que pode incluir vários outros statements, inclusive.
O *Effect* só pode ser allow ou deny.
*Action* descreve as ações específicas sobre o effect.
*Resource* são os objetos que o statement cobre
*Conditions* podemos especificar quando a policy é válida. É opicional.

Sempre que um *principal* faz uma request para AWS, o IAM verifica se ele está autenticado e autorizado. Para checar a autorização, o IAM vê se a policy está no contexto da requisição, se apenas uma das ações foi negada, todo o acesso será negado.

## Policies for User to use MFA

Dentro de um grupo, que contém todos os usuários, podemos incluir os usuários individuais controlarem o seu próprio MFA.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowListActions",
            "Effect": "Allow",
            "Action": [
                "iam:ListUsers",
                "iam:ListVirtualMFADevices"
            ],
            "Resource": "*"
        },
        {
            "Sid": "AllowIndividualUserToListOnlyTheirOwnMFA",
            "Effect": "Allow",
            "Action": [
                "iam:ListMFADevices"
            ],
            "Resource": [
                "arn:aws:iam::*:mfa/*",
                "arn:aws:iam::*:user/${aws:username}"
            ]
        },
        {
            "Sid": "AllowIndividualUserToManageTheirOwnMFA",
            "Effect": "Allow",
            "Action": [
                "iam:CreateVirtualMFADevice",
                "iam:DeleteVirtualMFADevice",
                "iam:EnableMFADevice",
                "iam:ResyncMFADevice"
            ],
            "Resource": [
                "arn:aws:iam::*:mfa/${aws:username}",
                "arn:aws:iam::*:user/${aws:username}"
            ]
        },
        {
            "Sid": "AllowIndividualUserToDeactivateOnlyTheirOwnMFAOnlyWhenUsingMFA",
            "Effect": "Allow",
            "Action": [
                "iam:DeactivateMFADevice"
            ],
            "Resource": [
                "arn:aws:iam::*:mfa/${aws:username}",
                "arn:aws:iam::*:user/${aws:username}"
            ],
            "Condition": {
                "Bool": {
                    "aws:MultiFactorAuthPresent": "true"
                }
            }
        },
        {
            "Sid": "BlockMostAccessUnlessSignedInWithMFA",
            "Effect": "Deny",
            "NotAction": [
                "iam:CreateVirtualMFADevice",
                "iam:EnableMFADevice",
                "iam:ListMFADevices",
                "iam:ListUsers",
                "iam:ListVirtualMFADevices",
                "iam:ResyncMFADevice"
            ],
            "Resource": "*",
            "Condition": {
                "BoolIfExists": {
                    "aws:MultiFactorAuthPresent": "false"
                }
            }
        }
    ]
}
```

## Create Limited Administrator

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "ManageUsersPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:ChangePasword",
        "iam:CreateAccessKey",
        "iam:CreateLoginProfile",
        "iam:CreateUser",
        "iam:DeleteAccessKey",
        "iam:DeleteLoginProfile",
        "iam:DeleteUser",
        "iam:UpdateAccessKey",
        "iam:ListAttachedUserPolicies",
        "iam:ListPolicies",
        "iam:ListUserPolicies",
        "iam:ListGroups",
        "iam:ListGroupsForUser",
        "iam:GetPolicy",
        "iam:GetAccountSummary"
      ],
      "Resource": "*"
    },
    {
      "Sid": "LimitedAttachmentPermissions",
      "Effect": "Allow",
      "Action": [
        "iam:AttachUserPolicy",
        "iam:DetachUserPolicy"
      ],
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "iam:PolicyArn": [
            "arn:aws:iam::AWS-ACCOUNT-ID:policy/MyProjectS3Access",
            "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
          ]
        }
      }
    }
  ]
}
```

Podemos usar isso para consultores, por exemplo, que devem trabalhar com certos privilégios, mas com acesso restrito. Podemos salvar isso num arquivos e criar via CLI:

```
aws iam create-policy --policy-name MyAdminLtd --description "grant admin" --policy-document file://policy.json
```

Depois um usuário:

```
aws iam create-user --user-name ladmin
```

Criamos as access keys:

```
aws iam create-access-key --user-name ladmmin
```

Criamos um perfil de login:

```
aws iam create-login-profile --user-name ladmin --password mudar@123
```

Agora fazer um attach da policy no usuário

```
aws iam attach-user-policy --user-name ladmin --policy-arn <ARN POLICY>
```

Esse usuário pode criar outros usuários, mas não tem todos os privilégios.