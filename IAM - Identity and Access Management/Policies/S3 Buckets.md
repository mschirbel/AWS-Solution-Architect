# S3 Buckets

As policies podem ser colocadas diretamente em buckets. Ao invés de permitimos usuários, grupos ou roles, podemos fazer o controle individual dentro do bucket. Isso nos permite fazer a gestao de acesso até mesmo em objetos individuais.

O bucket tem uma hierarquia de *flat-objects*, mas podemos enxergar isso com pastas e subpastas, usando o *KeyPrefix*.

E podemos usar uma combinação, de bucket policies, user policies e groups policies para fazer nossa gestão de acesso.

Entretanto, existem situações que podemos escolher qual usar. Mas para, por exemplo, acesso entre contas, devemos usar *bucket policies*, pois é algo restrito do bucket.

## Configuration of Folder Structure

Podemos configurar uma pasta dentro do bucket para cada um dos times. E colocar permissões por pastas para grupos determinados. Para isso usamos a policy `ListBucketAndRootLevel`.

É interessante também adicionar o versionamento para possíveis usos parecidos com um FTP.

## Across Accounts

Podemos estabelecer uma relação de confiança entre duas contas para que os recursos de uma sejam acessíveis pela outra.

```json
{
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Allow",
        "Principal": {
            "AWS": "arn:aws:iam::334234334:user"
        },
        "Action":  [
            "s3:GetBucketLocation",
            "s3:ListBuckets"
        ],
        "Resource": "ar:aws:s3:::nossobucket",
    }
}
```

Podemos colocar essa policy num bucket para que outra conta acesse os dados de um bucket. Podemos adicionar permissões no Action.