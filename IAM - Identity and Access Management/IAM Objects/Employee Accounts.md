# Employee Account

Principalmente quando cuidamos de uma empresa, precisamos definir os acessos de cada um dos empregados, e para facilitar isso, podemos fazer isso com grupos de times.

Para boas práticas, sempre devemos considerar se o acesso do usuário vai necessidar de acesos a CLI da AWS. Caso não precise, **com certeza** devemos restringir isso.
Se o acesso for feito na somente na console web, podemos também usar uma senha gerada automaticamente e com reset de senha.

Com isso, nosso usuário já tem uma policy atrelada: `IAMUserChangePassword`. Depois disso, podemos enviar e-mails para mostrar como fazer o login e a troca de senha.

## Access Key Management

Por boas práticas, devemos usar *roles* para adquirir access keys temporárias. Mas quando tratamos de usuários, podemos tomar alguns cuidados para que nossa segurança seja estabelecida.

Na tabela de usuários do IAM, podemos remover a coluna de AccessKey. Isso é bom, pois um usuário pode ter mais de uma access key.

O primeiro, é deletar as chaves de acesso de usuários que não participam mais da empresa. O segundo, é fazer a rotação de access keys. Nunca devemos usar as chaves de acesso diretamente no código, isso é algo muito perigoso.

Para fazer a rotação, devemos: criar uma nova access key, e fazer a antiga ser inativa. Para depois remover.