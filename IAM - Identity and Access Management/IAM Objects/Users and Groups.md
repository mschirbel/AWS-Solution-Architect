# Users and Groups

## Admin Group and User

Isso é uma das boas práticas para fazer um *lock* da conta root, que não devemos usar. Criar um usuário admin, mesmo com todos os privilégios, é uma ótima prática de segurança.

Primeiramente, vamos criar um grupo de administrator, pois o grupo em si receberá uma policy de administrator, logo, os usuários podem ser removidos facilmente.

Poderíamos adicionar a policy diretamente ao usuário, mas é mais fácil criar um grupo para isso.

Um grupo é um conjunto de usuários sob um mesmo conjunto de policies. Caso um usuário tenha suas próprias policies, é feito uma união dos conjuntos.
E caso haja uma divergência, o *deny* prevalece.

## Users

Para um usuário, podemos ter dois tipos de acessos:
    - acessos programáticos, para API, CLI, SDK e ferramentas de desenvolvimento
    - acesso a Console web

Os dois podem ser usados ao mesmo tempo, e mesmo usar uma senha gerada automaticamente e que tem data de expiração.

Ao criar um usuário, recebemos uma access key, que pode ser acessível sempre. E uma secret key, que só pode ser vista na última tela de criação de usuário, depois nunca mais.
Essas chaves serão usadas para o acesso programático, ou seja, via cli, ou sdk's.

#### Using the CLI

Para instalar a cli use o comando `pip install awscli --upgrade --user`. Veja se tudo está ok usando o comando `aws --version`.

Assim que criarmos nosso usuário e tivermos a suas chaves de acesso, usamos o comando `aws configure` para configurar nossa cli para uso do nosso usuário.

Com a cli podes fazer scripts para automatizar nossa infraestrutura.

Agora, temos alguns comandos para scriptar a criação de grupos/usuários:

```shell
# criar grupo
aws iam create-group --group-name 'nome-do-grupo'

# listar grupos
aws iam list-groups

# incluir uma policy num grupo
aws iam attach-group-policy --group-name 'nome-do-grupo' --policy-arn 'arn'

# listar policies de um grupo
aws iam list-attached-group-policies --group-name 'nome-do-grupo'

# criar usuário
aws iam create-user --user-name 'username'

# criar perfil de login
aws iam create-login-profile --user-name 'username' --password 'pass'

# criar acesso programático
aws iam create-access-key --user-name 'username'

# adicionar usuário a um grupo
aws iam add-user-to-group --group-name 'nome-do-grupo' --user-name 'username'
```

## Groups

Com o uso de grupos, podemos controlar os acessos dos usuários de forma mais fácil. Isso é útil quando temos um número grande de usuários, e principalmente quando tem funções diferentes.

Grupos não são uma identidade, como usuários, pois não podem ser identificados como *Principal* em uma Policy. Dentro dessa entidade, grupo, podemos ter vários usuários, mas nunca podem ser *aninhados* ou *nested*.

Usuários podem estar vários grupos, mas não existe um grupo inicial para agrupar todos os usuários quando criamos a conta, isso pode ser interessantes, principalmente para controle individual de MFA.

Em um grupo, podemos adicionar policies, que é o principal objetivo de um grupo: compartilhar policies entre vários usuários.

## Configuring MFA for Users

Dentro de *Users* e ao selecionar um usuário, vamos em *Security Credentials*.
Podemos trocar a opção *Assigned MFA Device*. Podemos aqui autenticar um device para fazer o MFA, mas isso não é tão prático, pois conforme o número de funcionários crescer, assim será o trabalho manual.