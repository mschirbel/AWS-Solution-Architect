# Best Practices

Existem certas preocupações que devem existir quando tratamos do IAM em empresas.

- Devemos deletar as chaves de acesso root, bem como fechar esse acesso para os usuários. Para isso, devemos criar usuários individuais, com grupos e policies definidas para cada um dos grupos.

- Sempre devemos partir do princípio de pouco privilégio. Nunca dar acesso total a um recurso, sempre na medida da utilização.

- As senhas dos usuários devem expirar, e sempre ser rotacionadas, bem como as credenciais deles. Outro ponto importante é usar MFA e deixar que o usuário controle seu acesso a MFA.

- Monitorar as atividades do IAM, usando CloudTrail, na qual podemos exportar para um bucket e depois podemos usar alguma ferramenta para análise.

- Sempre usar Roles para os recursos, pois as credenciais temporárias são sempre mais seguras.

- Remova credenciais não utilizadas, grupos não utilizadas, usuários de pessoas que já deixaram a empresa.