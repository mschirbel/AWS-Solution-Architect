# EC2 Profiles

Um instance profile é um detentor de uma Role, quando passamos para uma instância quando ela inicia. Então, quando nossas aplicações precisam de acesso a AWS, podemos usar os Instances Profiles para associar uma Role com uma instância.

Depois de criada a role, podemos associar via console aqui:

![](../media/ec2role.png)

Ou quando a Instância já existe, podemos selecionar(uma por vez) e ir em *Actions* -> *Instance Settings* -> *Attach IAM Role*.

Podemos fazer também pela CLI:

```
aws iam create-instance-profile --instance-profile-name dummy
aws iam add-role-to-instance-profile --instance-profile-name dummy --role-name roleName
aws iam list-instance-profile
```