# Troubleshooting

Geralmente envolve problemas de credenciais, ou acesso negado, reset de senha/MFA ou até mesmo quando as mudanças não são visíveis.

## General

### Lost Access Keys

Dentro do User, vá para *Security Credentials*, e crie uma Access Key para ele. Caso já exista uma, faça o rotacionamento.

### Need Access to Account

No menu de login, clique em *Forgot Password*.

### Access Denied

- Verifique se há credenciais temporárias e se elas não expiraram
- Verifique se a role/user tem as permissões corretas
- Verifique se existe *Resource Based Policy*, tem que dar permissão para a Role/User.

### Policy variables not working

Uma coisa que é importante é a *Version*, tem que ser a *"2012-10-17"*.

### Changes are not visible

IAM tem *eventual consistency* para alguns serviços, ou seja, demora um pouco para todos os endpoints conceberem a mudança.

---

## Policies

Um dos melhores jeitos para fazer o debug de policies é usando o Visual Editor. Geralmente vemos warnings ou erros, que ficam muito mais evidentes no Visual Editor.

Bem como podemos fazer o *switch* para Deny em uma policy, assim o allow vira deny.

## EC2

Nas instâncias podemos ter problemas como:
1. não pode fazer o *assumeRole* - verifique se o serviço pode asumir a role no *trust relationships*

2. nao pode editar a role - não podemos deletar as que são gerenciadas pela AWS

3. uma nova role apareceu - acontece por causa do *Service Link Role*

4. nao vemos a role quando tentamos criar a instancia - porque a role não pode ser assumida pelo serviço ou o usuário não tem permissão para *ListInstanceProfile*. Isso acontece muito quando criamos uma role usando a AWSCLI, pois lá são coisas distintas, roles e instances profiles. Temos que adicionar a role ao instance profile.

5. as credenciais sao da role errada - pode ser *eventual consistency*.

6. access denied para Icriar uma instancia com uma role - temos que saber se temos permissão para os *InstanceProfile* e checar se o instance profile tem uma role.
