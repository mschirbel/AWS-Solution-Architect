# Access to Billing Data

Isso primeiramente deve ser feito como root. Logo, podemos conceder permissões para outros usuários. Em um usuário que não tem acesso, ao clicar em *Billing Dashboard* não terá acesso. Mas o *root* pode clicar em *My Account*, pode selecionar a opção de habilitar esse acesso:

![](../media/billing3.png)

Depois, criamos uma Policy. Podemos criar duas, uma para controle e outra para visualização. No visual editor, escolha a opção de *Billing* para criar a policy.