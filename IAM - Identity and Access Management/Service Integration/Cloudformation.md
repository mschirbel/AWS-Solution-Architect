# Cloudformation

Podemos usar um template no CF para o caso de usuários temporários em nosso sistema.
Podemos criar um usuário e grupo do seguinte jeito:

```json
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "Temporary user and group",
    "Resource":{
        "User1": {
            "Type": "AWS::IAM::User",
            "Properties": {
                "Path": "/",
                "LoginProfile": {
                    "Password": "myPass"
                },
                "UserName": "User1"
            }
        },
        "DevOps": {
            "Type": "AWS::IAM::Group",
            "Properties": {
                "Path" : "/",
                "GroupName": "DevOps"
            }
        },
        "AdminUserMembership": {
            "Type": "AWS::IAM::UserToGroupAddition",
            "Properties": {
                "GroupName": {
                    "Ref": "DevOps"
                },
                "Users": [
                    {
                        "Ref": "User1",
                    }
                ]
            }
        },
        "myaccesskey": {
            "Type": "AWS::IAM::AccessKey",
            "Properties": {
                "UserName": {
                    "Ref": "User1"
                }
            }
        },
        "ExplicitAdmin":{
            "Type": "AWS::IAM::Policy",
            "Properties": {
                "Groups": [
                    {
                        "Ref": "DevOps"
                    }
                ],
                "PolicyName": "devops-policy",
                "PolicyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "NotAction": [
                                "iam:*",
                                "organizations:*"
                            ],
                            "Resource": "*"
                        },
                        {
                            "Effect": "Allow",
                            "Action": [
                                "iam:CreateServiceLinkedRole",
                                "iam:DeleteServiceLinkedRole",
                                "iam:CListRoles",
                                "organizations:DescribeOrganization"
                            ],
                            "Resource": "*"
                        }
                    ]
                }
            }
        }
    }
}

```

Usando o CF podemos subir e descer essas configurações na AWS em pouco tempo e sem *overhead*. Se a stack for deletada, os users e groups também.