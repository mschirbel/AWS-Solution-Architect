# Assume Role with Python

Vamos fazer o seguinte:
1. criar uma role para acesso no s3
2. dar essa role para um ec2
3. conectar em um bucket usando python de dentro da instância

A policy:

```json
{
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Allow",
        "Action":  "s3::*",
        "Resource": "*",
    }
}
```

A trust relationship:
```json
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Sid": "",
        "Effect": "Allow",
        "Principal": {
            "Service": "ec2.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
    },
    {
        "Sid": "",
        "Effect": "Allow",
        "Principal": {
            "AWS": "arn:aws:iam::<ACCOUNTID>:role/<Role Name>"
        },
        "Action": "sts:AssumeRole"
    }
    ]
}
```

O código em Python3

```python
import boto3
s3 = boto3.resource('s3')

my_bucket = s3.Bucket('bucket_name')

for file in my_bucket.objects.all():
    print(file.key)
```

Para rodar o código use:

```bash
yum install python3 -y
pip3 install boto3 -y
python3 get_s3.py
```