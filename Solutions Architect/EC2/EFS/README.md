# Elastic File System

EFS é um sistema de storage para EC2. É fácil de usar e permite criare configurar file systems rapidamente.
É possível crescer ou diminuir automaticamnete.

Quando criamos com EBS - precisamos dar os detalhes e é tudo travado - e nao pode ser usado para mais de uma instância

Com EFS temos suporte NFSv4 e só pagamos pela quantidade que usamos.
Pode ser escalável até petabytes
E os arquivos são distribuídos por várias AZ

Read After Write Consistency - com block based storage.

## Acesso ao EFS

Para acessar o EFS, temos que usar algumas da regiões suportadas:

!(images/01.png)[]

Ou seja, escolha uma das regiões acima e crie o EFS:

!(images/02.png)[]

O primeiro passo é configurar o FS. O EFS é ligado a alguma VPC, podemos usar a default de nossa conta.

Depois precisamos criar mount targets - são as AZ que distribuiram nossos arquivos. E ainda podemos escolher nossas subnets.

!(images/03.png)[]

Agora podemos escolher tags para nosso EFS:

!(images/04.png)[]

E agora só revisamos as informações. Podemos criar.

Agora, vamos criar 3 instancias de EC2 - com uma subnet diferente para cada uma.

Depois das instâncias, vamos criar um load balancer.

Agora, se voltarmos a página do EFS, podemos ver que está disponível:

!(images/05.png)[]

Vemos também as AZ que comporta:

!(images/06.png)[]

Para que o EFS funcione, tanto o EFS quanto as instâncias, DEVEM estar dentro do mesmo Security Group.

Logue nas instâncias que foram criadas:

```
ssh ec2-user@[ip] -i [key.pem]
```

Vire root e instale o Apache:

```
sudo su
yum install httpd
```

Inicie o Apache

```
service httpd start
```

Ao abrir seu EFS, há um comando para fazer mount no EC2:

!(images/07.png)[]

Para instalar:

!(images/08.png)[]

Mas se estiver usando a AMI da AWS, não precisa.

Agora, para fazer o mount:

!(images/09.png)[]

Apeas mude o final do comando, onde está "efs", mude para o caminho do seu Apache:

```
/var/www/html/
```

Agora todo arquivo criado nessa página, vai ser replicado na outra instância -  tipo um DFS.

Isso salva muito tempo, pois não precisa fazer deploy em múltiplas instâncias.
