# Autoscaling Lab

Primeiramente vamos criar um texto de healcheck. Coloquei o meu em anexo.

Agora vamos para S3 e vamos acessar o bucket que usamos na aula de bootstrap. Caso não tenha, crie um novo.

No bucket deve conter o arquivo index.html.

Em algumas aulas atrás, criamos um Load Balancer:

![](images/01.png)

Agora, vamos para a aba de Launch Configurations, e vamos criar um Auto Scaling Group:

![](images/02.png)

Antes de criar um Auto Scaling Group, vc deve criar uma Launch Configuration. E os passos são os mesmos de se criar um EC2.

Para as configurações:

![](images/03.png)

O script usado foi o mesmo de bootstrap:

```
#!/bin/bash
yum install httpd -y
yum update -y
aws s3 cp s3://marceloschirbel-website/index.html /var/www/html/ --recursive
service httpd start
chkconfig httpd on
```

Os demais passos são default.

Agora clicamos para criar um Auto Scaling Group:

![](images/04.png)

Para as configurações:

![](images/05.png)

Perceba que, as Subnets são as AZ que terão as instancias balanceadas. O algoritmo distribui igualmente entre elas as instâncias, então é sempre interessante ter o máximo de Subnets possíveis.

Para a Scaling Policy:

Podemos aumentar o diminuir o número de instâncias, dependendo de algum trigger:

![](images/06.png)

Podemos colocar como trigger, CPU, memória, tráfego, network.
E podemos eliminar as instâncias, também.

É possível inserir notificações para determinados grupos, ou pessoas específicas.

![](images/07.png)

Agora basta criar o grupo.

Isso vai iniciar a nossa instância EC2

Podemos voltar na aba de instâncias que veremos o número que permitimos. E elas estão espalhadas por diferentes AZ da AWS.

Ao matar uma instância, podemos acessar de dois modos:
	- Com o IP público de cada instância
	- Pelo DNS name do load balancer.

Ao matar uma instancia, a página específica ficara com Timeout, mas a página do Load Balancer não, pois ainda redireciona tráfego para qualquer instância ativa.

![](images/08.png)
