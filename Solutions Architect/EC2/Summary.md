# Summary and Exam Tips

## Price
Saber a diferença entre os preços:
	- On Demand: paga pelo segundo ou pela hora
	- Spot: vc poe um bid, se ficar dentro do bid, suas instâncias sobem
	- Reserved: reserva uma capacidade por 12 a 36 meses
	- Dedicated Host: máquinas reservadas
Lembrar que com spot: se vc der um terminate, vc paga pela hora. Se a AWS terminar, vc nao paga.

## Types of EC2
Saber as diferenças de tipos de instâncias de EC2.
Não precisa saber todas para o Associate, mas ter uma ideia geral de tipos de instância.
mnemonico - FIGHT DR MCPX

## Types of EBS
Tipos de EBS:
	- GP2 até 10k IOPS - SSD
	- IO1 > 10k IOPS - SSD
	- ST1 para muito acesso - HDD
	- SC1 para pouco acesso - HDD
	- Magnetic - pouquíssimo frequente - HDD
Não pode fazer mount de um EBS para múltiplos EC2, para isso use EFS

Lembre-se que a termination protection é desligada por default.
E quando uma instância terminar o root EBS é deletado.
EBS pode ser criptografado usando ou a AWS ou algum software - usando um snapshot.

## Volumes vs Snapshots
Volumes são para EBS
Snapshots são para S3
Você pode tirar um snapshot de um volume, vai ficar guardado em um S3
Snapshots são incrementais - tipo as layers do docker.
Snapshots de volumes criptografados também são
Só pode compartilhar snapshots se forem descriptografados.
Para tirar um snapshot, primeiro pare a instância.

## Instance Store
São efêmeros - se eles param vc perde os dados.

## Raid Array
Para tirar um snapshot de um RAID Array é preciso parar a aplicação, remover o cache e assim tirar o snapshot - shutdown EC2.

## Amazon Machine Images
São regionais.
Pode ser usado pela AWS CLI

## Monitoring
Standard = 5min
Detailed = 1min

Cloudwatch = logs de performance
Cloudtrail = auditorias

## Cloudwatch
Criar dashboards para ver as instâncias
Criar alarmes/triggers
Criar eventos para responder a mudanças
Logs.

## Roles
São muito mais seguras do que guardar a chave de segurança - e também é mais facil de administrar.
Roles podem ser dadas a um EC2 mesmo depois que ja foi iniciado.
Roles são universais, em qualquer região - tipo IAM.

# Instance Meta-data
Basta usar o comando:

```
curl http://169.254.169.254/latest/meta-data/
```
de dentro da instância que vc quer saber.
Ou até mesmo

```
curl http://169.254.16.254/latest/user-data/
```
Para saber informações do seu usuário.

## EFS
Suporta NFSv4
Só paga pelo que for usar de storage - podendo ir até petabytes.
E pode suportar milhares de conexões ao mesmo tempo - em várias regions diferentes.

## Lambda
Serviço de computação - nao precisa preocupar com SO, Patches ou scaling.
É dirigido a eventos, respondendo ao que for programado.

## Placement Group
Clustered - sempre vai estar em 1 AZ - big data, ML - low latency e high network - Se não for dito qual o tipo, assuma que é Clustered.
Spread - pode estar em várias AZ

# Quiz

1. EBS volumes são deletados quando a instância é terminada, MAS, pode continuar - caso seja programado para(detalhe importante)
2. não posso deletar um snapshot que é usado como root device de uma AMI registrada. Primeiro antes DEREGISTRAR a AMI.
3. para criar snapshots via aws-cli: ec2-create-snapshot
4. placement group é bom para baixa latencia dentro de 1 AZ.
