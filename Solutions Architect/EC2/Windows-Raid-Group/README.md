# Raids, Volumes & Snapshots

RAID = Redundant Array of Independent Disks
É basicamente colocar um monte de discos juntos e eles funcionam como 1 só para o SO

	RAID 0 - sem redundância, boa performance
	RAID 1 - mirrored, redundância
	RAID 5 - bom para leitura, mas a AWS nao recomenda que coloque RAID5 para EBS *** ISSO É IMPORTANTE ***
	RAID 10 - combinação de RAID1 com RAID0 - tem redundancia e boa performance

Vamos para o serviço do EC2 e vamos na parte de Security Groups:

![](images/01.png)

Vamos editar algumas inbound rules:

![](images/02.png)

Agora voltamos para o Dashboard do EC2
E vamos iniciar uma nova instância:

![](images/03.png)

Vamos escolher uma imagem de Windows:

![](images/04.png)

Para as configurações:

![](images/05.png)

Para a parte de Storage:

![](images/06.png)

Para as Tags:

![](images/07.png)

Para os Securities Groups:

![](images/08.png)

Cuidado, as configurações desta aula não são grátis, os prints a seguir são da aula.

Vamos criar uma nova chave para nossa instância:

![](images/09.png)

E usando essa senha, vamos gerar a senha de administrador.
Essa VM do Windows demora um pouco mais do que as de Linux.

O próximo passo seria logar na VM, para fazer o RDP para sua instância, precisaremos de um user e uma senha.
Para Windows, o username *administrator* e a senha nós vamos pegar da plataforma AWS.

Ao clicarmos em nossa instância de Windows, podemos ir no menu Actions e veremos o campo para obtermos a senha:

![](images/10.png)

E para obter a senha do Windows, devemos copiar e colar nossa senha privada.

![](images/11.png)

Clique em Decrypt, e assim obteremos a senha.

Agora, vamos conectar na instância. No Windows, basta usar o

```
mstsc
```

![](images/12.png)

Pode demorar um pouco para estabelecer a conexão.

Assim, como criamos vários discos para nossa instância, podemos ver que estão dispostos:

![](images/13.png)

Para olharmos melhor nas configurações dos discos:

Clique com o botão direito no ícone do Windows e vá em Disk Management:

![](images/14.png)

Podemos ver nossas partições de EBS:

![](images/15.png)

Assim como mencionado, como podemos melhorar o IO de nossas máquinas?
Fazendo as RAIDS, assim como visto acima.

Vamos deletar todos os volumes:

![](images/16.png)

Ao clicarmos com o botão direito em um volume não alocado:

![](images/17.png)

Vamos criar um Stripped Volume:

Ao criar, adicione todos os discos desejados para a RAID, lembrando que somente discos não alocados podem ser usados:

![](images/18.png)

Vamos designar a letra que quisermos para o novo volume.

![](images/19.png)

Agora basta finalizar a criação. E clicar em Sim para os avisos.
Para adicionar mais IO aos discos, basta adicionar mais volumes EBS

![](images/20.png)

### Como podemos tirar um Snapshot de um RAID?

Temos um problema, pois quando tiramos um snapshot, ele não salva os dados em cache de aplicações e nem do SO.
Isso não importa se tivermos somente um disco, mas como temos um Array de discos, isso pode ser ruim para a independência deles.

Para solucionarmos isso, precisamos tirar *um snapshot consistente com a aplicação*
Devemos fazer algumas coisas antes:

1. Temos que pausar a aplicação de escrever no disco.
2. Alinhar(flush) do cache no disco

Como podemos fazer isso?

1. Freeze no FS
2. Unmount da RAID
3. Parar a instância do EC2(mais comum)
