# CloudWatch

Vamos para o serviço de EC2 e tenha alguma instância de EC2 rodando.

Para cada instância, temos uma aba de Monitoring.

![](images/01.png)

Temos algumas funções básicas do nosso EC2, rodando a cada 5min

Podemos diminuir isso para cada 1 minuto, mas isso custa além do Free Tier.

Vamos agora para o serviço do CloudWatch

![](images/02.png)

Vemos que no menu da esquerda, temos 5 opções importantes:
1. Dashboard
2. Alarms
3. Events
4. Logs
5. Metrics

## Dashboard

Vamos começar com Dashboard:

![](images/03.png)

Vamos criar um novo Dashboard

![](images/04.png)

Temos 4 diferentes tipos de widgets para colocarmos no nosso dashboard:

![](images/05.png)

### Vamos começar com um widget de texto:

![](images/06.png)

Aqui ele somente vai mostrar como criar um markdown.

Após adicionarmos as informações que queremos:

![](images/07.png)

### Vamos adicionar mais widgets, vamos adicionar um de linhas:
![](images/08.png)

Veja que é possível fazer a monitoração de diversos serviços, não somente EC2, mas sim, BD, S3, ELB e EBS

Vamos configurar um de EC2.

E isso é uma informação importante para o teste, as métricas default do CloudWatch para EC2 são relacionadas a:
1. CPU
2. Disk
3. Network
4. Status

Adicionamos ao gráfico a métrica de CPU Utilization e salvamos.

![](images/09.png)

### Vamos adicionar um de Stacked Area

E assim como o gráfico, vamos em EC2.

E selecione CPU Utilization e CPU Credit Usage

Clique em salvar

![](images/10.png)

### Por último, vamos adicionar um número:

Assim como nas anteriores, vamos para EC2.

E adicione a CPU Utilization

![](images/11.png)

## Alarms

Agora, para Alarms:

![](images/12.png)

Clique em criar um alarme.

![](images/13.png)

Vamos em EC2 Metrics

![](images/14.png)

Selecione CPU Utilization, abaixo podemos ver o gráfico de nossa métrica.
Clique em Next.

Crie o alarme definindo a porcentagem de CPU, o intervalo de coleta e a lista de e-mails para quem deve mandar o alarme.
Note que, os e-mails devem ser verificados.

## Events

![](images/16.png)

Só precisamos entender num nível mais superficial.
O event nos mostra quando algum recurso sofreu alteração.
Quando algum EC2 começou a funcionar, quando vc tira um snapshot.

## Logs

Nos habilita a monitorar nosso EC2 a nível de aplicação ou de kernel.
Instalamos um client em nosso EC2 e esse client vai enviar as informações para a AWS.

## Metrics

Ao invés de criar dashboards, podemos ver as métricas diretamente aqui, sem ter que usar widgets.

## Exam Tips

1. tempo default de monitoração: 5min
2. tempo pago de monitoração: 1min
3. dashboard - agilizar a visualizaçã
4. alarms para tresholds
5. events para monitorar recursos na AWS
6. logs - agrega, monitora e guarda as logs com um client instalado no ec2
7. cloudwatch é para logging e monitoring
8. cloudtrail é para auditorias - todo o ambiente AWS(Roles, IAM, S3, EC2)
