# AMI Types - EBS vs Instance Store

Root devices = aqueles onde vc instala o sistema operacional da sua imagem

Existem dois tipos de Root devices:
1. EBS - backed
2. Instance Store - efêmero 

Podemos escolher nossa AMI baseado na região, no sistema operacional, arquitetura(32 ou 64 bits), launch permissions

Podemos ver isso quando iniciamos uma instância:

Ao clicarmos em Launch Instance:

![](images/01.png)

Vemos que o tipo de root device é EBS, por exemplo.

Vamos notar que a maioria é EBS

Vamos iniciar uma instância default.

![](images/02.png)

Agora vamos iniciar outra, mas dessa vez, ao escolhermos nossa AMI, vamos em Community AMIs:

![](images/03.png)

E podemos filtrar por tipos de root device:

![](images/04.png)

Vamos escolher uma imagem de 64bits com HVM

![](images/05.png)

E vemos que os tipos de instâncias são reduzidos:

![](images/06.png)

Para a parte de Storage temos:
![](images/07.png)

O que nos diz que podemos adicionar mais de um Instance Store antes de iniciarmos, porém, depois de iniciado, SOMENTE EBS

![](images/08.png)

#### Diferenças entre as duas

1. Na instância do EBS eu posso pausar a instância e tirar um snapshot(assim como as boas práticas)
Mas com a Intance Store, eu não posso pausar, posso dar reboot ou terminate

E por que podemos querer pausar a instância?
Para providenciar um novo Hypervisor. Isso pode ajudar no desempenho. Caso sua instance store estiver em um host com problemas, vc não pode parar.

2. Outra diferença é que podemos dar detach nos volumes do EBS, o que não é possível com as Instance Stores
