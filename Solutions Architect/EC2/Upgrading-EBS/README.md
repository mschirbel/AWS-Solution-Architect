# Upgrading EBS Volume Types

Vamos lançar uma nova instância.
Pode ser tudo default, mas na parte de Storage:

![](images/01.png)

GP2 - General Purpose SSD: onde fica o SO de nosso EC2
E terá outros 3 discos, assim como na imagem.

Vamos adicionar uma Tag:

![](images/02.png)

Podemos usar o Security Group que criamos antes. Launch.

Pode demorar um pouco pra ficar ativa.

![](images/03.png)

Podemos ver o Volumes associados:

![](images/04.png)

![](images/05.png)

Veja que a AZ para nossos Volumes são as mesmas.
EBS e o EC2 devem estar na mesma AZ, porque a latência seria grande - TEM QUE ESTAR NA MESMA AZ

Snapshot é uma AMI que selecionamos - temos aqui o ID da AMI.

Podemos modificar o Volume:

![](images/06.png)

Veja que podemos mudar para Provisioned IOPS - para aumentar o desempenho. Podemos trocar até o tamanho o volume sem haver indisponibilidade.

Pode ter algum conflito na aplicação, pois vc está trocando de Storage, mas não tem indisponibilidade.

![](images/07.png)

Lembrando que se o tipo for Standard(Magnetic), não é possível modificar o Volume.

---

Para criar um outro Volume em outra AZ, como fazer?
1. tire um snapshot:
![](images/08.png)

Se eu for na área de Snapshots:
![](images/09.png)

2. Clique em Actions no Snapshot e Create a Volume:
![](images/10.png)

E podemos trocar a AZ:
![](images/11.png)

Assim, podemos trocar a AZ de um Volume.

Também é possível mudar uma EC2 de uma região para outra usando esse método de snapshots. Depois basta criar uma imagem desse snapshot e fazer um boot.

---

Também podemos copiar esse Snapshot para qualquer outra região. E partir dessa cópia, faremos uma AMI e assim bootaremos num EC2.

## Criando uma Imagem a partir de um Snapshot

![](images/12.png)

Clique em Create Image:

![](images/13.png)

No menu da esquerda, entre em AMI, e selecione a imagem que vc criou a partir do seu Snapshot

![](images/14.png)

Podemos trocar de region de duas formas:

![](images/15.png)

![](images/16.png)

Ou damos Launch e selecionamos a AZ ou copiamos para outra Region.

Para apagar uma AMI:

![](images/17.png)

Apague também o Snapshot e a sua instância.
Lembre que, ao apagar a instância, os Volumes não são apagados juntos. Voce deve apagar manualmente.

## Exam Tips

1. Volumes existem nos EBS - são virtual hard disks - onde o SO está instalado
2. Snapshots existem no S3
3. Snapshots são copias pontuais do Volume
4. Snapshots são incrementais, ou seja, somente os blocos que mudaram vao ser movidos para o S3, somente as mudanças que vc fez que são salvas
5. Pode mudar o tamanho do Volume enquanto ele está online(on the fly) - mas é recomendado que fique funcionando
6. Volumes SEMPRE estão na mesma AZ do EC2
7. Para mover um Volume para outra AZ/Region, vc tira um snap e roda uma imagem ou copia para outra regiao.
8. Snapshots de Volumes criptografados também são criptografados
9. Vc pode compartilhar snapshots se forem publicos - criptografados não
10. Vc pode compartilhar snapshots até mesmo com outras contas AWS.
