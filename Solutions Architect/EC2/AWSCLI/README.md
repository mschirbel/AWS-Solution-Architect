# AWS Command Line

Para instalar, vc deve ter no mínimo python 2.6.5

O comando é:

```
pip install awscli
```

Antes de começar, exclua a instância de EC2 que está online.

![](images/01.png)

Vamos subir outra instância com a imagem que inclui a AWS CLI

![](images/02.png)

Para o restante das configurações, podemos deixar como default

Somente selecione o Security Group já criado

Agora vamos para o serviço de IAM

![](images/03.png)

Vamos criar um novo usuário.

![](images/04.png)

Vamos adicionar policies diretamente ao usuário.

![](images/05.png)

Crie o usuário. Copie as informações de acesso para um notepad.

Volte para o EC2 e veja se sua instância já subiu.
Quando subir, pegue o IP público e vamos logar na instância:

```
chmod 400 key.pem
ssh ec2-user@[ip publico] -i [key].pem
```

Podemos usar a AWS CLI dentro da instância, por exemplo:

```
aws s3 ls
```

Isso nos dará um erro, dizendo que não foi possível localizar as credenciais.

Precisamos configurar nossas credenciais:

```
aws configure
```

E siga as instruções, colocando as informações do user que criamos.

E agora podemos ver todos os s3:

```
aws s3 ls
```

Vamos ver onde as credenciais estão guardadas:

```
cd ~
ls -la
cd .aws/
ls -la
nano credentials
```

Fica guardada localmente em nosso EC2. Se alguem entrar em nossa instância, ele poderia logar em qualquer coisa que ele quisesse. Isso é muito prejudicial.

Vamos fazer a instância de EC2 se auto destruir.

Para pegar o ID:
```
aws ec2 describe-instances
```
Assim podemos ver todas as informações das instâncias.

![](images/06.png)

Com o ID em mãos:

```
aws ec2 terminate-instances --intances-ids [id]
```
![](images/07.png)

Vemos que a instância foi terminada.

Se voltarmos ao console

![](images/08.png)

E agora, vamos voltar ao IAM e deletar o usuário que criamos. Para evitar riscos de segurança.
