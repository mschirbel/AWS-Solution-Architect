# Linha de comando S3

Isso não é algo que cai muito no exame, mas ajuda a fazer um troubleshooting, eventualmente.

Crie uma nova instância EC2 com a Role de AdminS3

Enquanto a instância é criada. Vamos criar alguns buckets

![](images/01.png)
![](images/02.png)
![](images/03.png)

Adicione aleatoriamente alguns arquivos a cada um dos buckets. De preferência que ejam diferentes.

Agora temos que ir para o IAM

E vamos criar uma Role(provavelmente ja existe) para S3 Admin.

Coloque a Role na instância EC2:

![](images/04.png)

Ao invés de rodarmos um aws configure, configuramos a instância para aceitar a role, assim, nao precisamos deixar as credenciais dentro da instância, o que pode ser ruim para segurança.

Vá para a instância:

```
aws s3 ls
```

Veja que retornou os buckets da sua conta, e você nao rodou um aws configure.

Vamos copiar os arquivos. Para nossa instância:

```
aws s3 cp --recursive s3://marceloschirbel-nv /home/ec2-user
cd /home/ec2-user
ls
```

Agora vamos tentar com outra região:

```
aws s3 ls
aws s3 cp --recursive s3://marceloschirbel-euwest2 /home/ec2-user/
```

Pode acontecer de sua region não aceitar o pool de outra region, pra isso, usamos o parametro:

```
aws s3 cp --recursive s3://marceloschirbel-euwest2 /home/ec2-user/ --region eu-west-2
```

Depende muito de qual region seu bucket está. Então é sempre bom usar com a region origem.
