# Bootstrap Scripts

Bash Script para AWS seria uma série de comandos para a sua instância EC2.

É possível instalar o packages, dar update de segurança e outros diversos comandos, assim como se estivéssemos no terminal.

### Primeiros Passos

Antes, vamos trocar nossa região para North Virginia.
Agora vamos criar um Bucket para guardar nosso arquivo.

![](images/01.png)

E faremos o upload de nosso arquivo index.html

![](images/02.png)

Não se esqueça de tornar o arquivo público antes de tentar acessá-lo.

Basta clicar no arquivo, e na aba Overview, torná-lo público.

Agora vá para IAM e cria uma nova Role com permissões de admin de S3 dentro do EC2. Provavelmente ela já está criada, então segue o jogo.

Agora vamos criar uma instância de EC2, pode ser tudo default, mas com a IAM Role de S3-Admin.

Na Aba 3 - Configure Instance Details, colocamos a IAM Role e podemos também inserir comandos diretamente aqui:

![](images/03.png)

Estamos fazendo tudo isso em Nothern Virginia, pois nessa region não há problemas de compatibilidade, que pode acontecer em outras.

Para deixar tudo mais simples, vamos dar upload no script que criamos.

![](images/04.png)

Inicie a instância com alguma chave conhecida.

```
ssh ec2-user@[IP PUBLICO] -i [key]
sudo su
yum update #vai ver que está com update
curl http://localhost:80
```

Assim você confere que os updates estão todos feitos
E que a sua página está com o arquivo do s3