# IAM roles with EC2

O que aconteceria caso alguem entrasse no seu EC2 sem ter as permissões. Poderia pegar la de dentro as credentials e entrar em qualquer outra máquina.

Para criar uma nova camada de segurança:

Clique em IAM

![](images/01.png)

Vamos em Roles:

![](images/02.png)

Vamos criar uma nova Role.

Veja que existem vários tipos de Roles diferentes:

![](images/03.png)

A primeira é a AWS Service Role, com ela você pode adicionar uma Role específica para cada serviço da AWS

Nesse exemplo vamos utilizar essa com EC2. Selecione e clique em next.

Vamos filtrar por S3 e procurar aquela de dá todo acesso.

![](images/04.png)

Dê o nome e termine a criação.

Agora vamos para o dashboard do EC2

Vamos criar uma instância, com tudo default, exceto por:

![](images/05.png)

Ao criarmos, podemos ver na parte de baixo do dashboard que a IAM role está lá:

![](images/06.png)

Ao entrarmos na Role podemos ver o JSON:

![](images/07.png)

Perceba que é possível colocar ou retirar IAM Roles dentro da dashboard do EC2:

![](images/08.png)

Agora logue na instância EC2.

```
aws s3 ls
```

Veja que assim foi listado todos os S3 da sua conta. Não precisa das credentials para ver, posso usar tudo de dentro do EC2.

```
cd ~
aws configure
cd .aws
ls
nano config
```

Veja que assim podemos ter um controle maior sobre os operadores de nossas instâncias.
