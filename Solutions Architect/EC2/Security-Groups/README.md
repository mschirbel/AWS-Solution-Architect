# Security Groups

Security Group são arquivos virtuais que controlam o tráfego das instancias
Quando vc lança umas instancia de EC2 pela primeira vez vc associa com 1 ou mais Security Groups
E vc adiciona regras a cada grupo que permite tráfego entre as instancias

Security Group é a primeira linha de defesa contra hackers.
Por exemplo, no último lab nós habilitamos SSH na máquina, se nao tivéssemos, algum hacker não conseguiria entrar.

Vamos criar uma instância do EC2(essa usaremos até o fim do curso)

Clique em Launch Instance

![](images/01.png)

Usamos a AMI do Linux da Amazon, com as configurações default. Somente adicionei algumas tags:

![](images/02.png)

Agora, para a parte de Security Groups

![](images/03.png)

Vamos usar o grupo que criamos para o lab anterior. Basta selecionar um grupo existente e marcar o que usamos.

Review and Launch.

Crie uma nova Key e faça o download.

Inicie sua instância.

![](images/04.png)

Agora, vamos dar ssh na ec2, instalar as dependencias de segurança, instalar um apache, subir uma pagina web.
Do mesmo jeito que fizemos na aula passada.

![](images/05.png)

Agora vamos deixar que o serviço do Apache inicie todas as vezes que bootarmos o EC2, para isso:

```
chkconfig httpd on
```

Quando abrimos o browser, vemos que a aplicação está acessível.

Voltando ao Dashboard, entramos em Security Groups

![](images/06.png)

Clique em detalhes no Security Group que está usando

![](images/07.png)

Perceba que em Inboud, temos regras para HTTP, HTTPS e SSH
Mas em Outbound só temos uma geral.

Clicamos botão EDIT e vamos remover a regra de HTTP.
Será que ainda vai conectar?

![](images/08.png)

Podemos ver que não.
E perceba que isso acontece imediatamente, isso é importante para a prova.

Para o Outbound, nao importa se vc tem não. TUdo que vc permita que entre, Vc permite que saia também.

Para VPC não é assim, vc tem que adicionar tudo pra entrar e pra entrar.
Essa diferença é importante

Outro ponto importante é que por default, tudo é bloqueado, vc só pode permitir nas regras de Inbound.

Agora, vamos ver como adicionar outro Security Group.

Selecione o default Security Group:

![](images/09.png)

Vamos adicionar 2 novas regras de Inbound:

![](images/10.png)

Para adicionar um grupo aos já existentes, volte no dashboard da sua instancia.

Clique em Actions-> Networking-> Change Security Groups

![](images/11.png)

Selecione quais grupos vc quer. Clique em Assign.

![](images/12.png)

Vemos agora que temos dois security groups associados na nossa instancia:

![](images/13.png)

Se clicar em view inbound rules:

![](images/14.png)

Ele adiciona todas as regras, e não pode haver conflito.
