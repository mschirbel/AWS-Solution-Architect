# Polly

## Arquitetura
Um website hospedado em um S3 vai se comunicar por API Gateway com Lambda Functions. Essas, por sua vez, vao inserir/retirar informações de um DynamoDB.
Uma das funções vai enviar o texto que for escrito no site para um SNS Queue, que vai ser um gatilho para outra Lambda function, esta vai acionar a Polly para converter o texto em audio e depois salvar no DynamoDB.

## Setup

Primeiramente, troque a region para N.Virginia, pois queremos todas as funções disponíveis.

## Polly Service

Vamos dar uma olhada no serviço Polly antes de começar:

![](images/01.png)

A função mais básica do Polly é ler textos, assim como na imagem:

![](images/02.png)

É possível alterar o idioma, e até mesmo o sotaque.

## DynamoDB

Agora vamos criar nosso banco de dados. Não vamos entrar em muitos detalhes, pois teremos uma seção própria para isso:

![](images/03.png)

## Bucket

Agora que temos a tabela criada, vamos criar nosso bucket que vai sustentar nosso site.
Tenha dois buckets: uma para o site, outro para guardar os mp3s que serão criados pela Polly:

![](images/04.png)

Agora vamos adicionar uma Policy para nosso bucket.
O arquivo está junto com essa aula - bucketpolicy.json
Vá no bucket que sustentará o website e em Permissions, cole a policy:

![](images/08.png)

## Role para Lambda Function.

Essa role vai permitir que as Lambdas Function converse com todos os serviços.
Vá no serviço de IAM e cria uma nova Role:

![](images/05.png)

Vai ser uma Lambda Role. Ao invés de usar uma policy já preparada, vamos criar a nossa própria.
Podemos editar como um JSON, que está nos arquivos desta aula - lambdapolicy.json

E podemos terminar de editar a função:

![](images/06.png)

Agora que criamos a policy, termine de criar a Role.

![](images/07.png)

## SNS

Agora vamos para o serviço do SNS - Simple Notification Service:

![](images/09.png)

Vamos criar um tópico:

![](images/10.png)

Agora que temos tudo criado, precisamos criar nossas Lambda Functions

## Lambda

Agora crie uma nova função:

![](images/11.png)

Vemos quais serviços a nossa função tem acesso:

![](images/12.png)

E para o nosso código:

![](images/13.png)

E agora precisamos incluir algumas variáveis de ambiente.

Para a variável de SNS, podemos pegar no topic que criamos:

![](images/14.png)

E para as configurações básicas de nossa lambda:

![](images/15.png)

Agora salve. E vamos testar nossa função:

![](images/16.png)

Vamos usar o Hello World mesmo.

![](images/17.png)

Agora basta criar e clicar em teste novamente.

Agora podemos ver o texto que foi salvo em nosso DynamoDB.
