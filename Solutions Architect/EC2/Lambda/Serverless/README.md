# Serverless Website

## Vamos usar Route53, API Gateway, Lambda e S3.

Para isso vamos precisar de um domínio registrado. Todos os códigos estão adicionados aqui.

Para registrar um domínio, precisamos verificar se:
- o domínio está disponível - isso é pago
- o namespace para o S3 está disponível - isso é grátis

Vamos para o bucket que criamos e vamo fazer dele um website.
Coloque os códigos no bucket.

Isso é agora um site estático dentro de um S3.

Vamos para Route53 e vamos registrar um domínio. É importante registrar que o domínio deve ser o mesmo do bucket name.
Não precisa ter o .com no bucket, mas o namespace sim, é obrigatório.

Podemos ver na dashboard quando o domínio estiver disponível - pode demorar até 3 dias(mas é muito raro)

Agora precisamos configurar nossa Lambda Funcion.

Crie uma função from scratch.
Dê um nome, como por exemplo ServerlessWebsite
Usaremos o Python 3.6
E vamos criar uma role com a policy template de Simple Microservices Template.

Assim, podemos criar a função.

A IDE abaixo das função vieram do c9, que a Amazon comprou. Nessa IDE vamos colocar o código python disponível.
Basta trocar o seu nome, dentro da tag do body. E salve.

Agora só precisamos adicionar os triggers. É importante decorar quais são para a prova.
Vamos adicionar API Gateway.
E configure para criar uma nova API.
Dê um nome. 
E Deployment stage, podemos dizer se é homol, QA ou Prod.
E podemos deixar Open, só para teste, em Segurança.

Clique em add, e salve a função.

Podemos clicar no nome da API Gateway, para configurar.
Delete o método que veio por default. E crie um novo método GET.

Primeiro vamos conectar a integração do método, isso será ligado com uma função Lambda
E vamos permitir um proxy
E selecione a função lambda que criou.

Depois, clique em Action e faça o deploy no deployment stage que vc digitou.
Isso dará uma InvokeURL, com ela o acesso é possível.

Dentro do arquivo index, troque pela InvokeURL o que vc recebeu do API Gateway.
Adicione os arquivos html dentro do bucket e faça-os públicos.

Vá para Route53 e crie um Record Set dentro de Hosted Zones.
Em alias, selecione o bucket.

Agora basta chamar a URL de seu domínio no browser e esperar abrir.
