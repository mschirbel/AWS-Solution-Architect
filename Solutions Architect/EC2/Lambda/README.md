# Lambda

Antes, precisamos entender a história de Cloud Computing.

A parte de hardware pode ser sempre confusa, por isso existem API que traduzem isso.

Mas acaba demorando muito tempo para subir um servidor, seja de aplicação ou de banco de dados. Isso mudou quando a AWS lançou a AWS. Porque agora ficou muito mais rápido - Infra as a Code.

O problema da AWS é que ainda roda em alguma máquina, seja física ou virtual. Para isso saiu a plataforma como  serviço. Elastic Beans Talk - que avalia o seu código e deixa que a AWS estabeleça a infra - mas ainda continua tendo que administrar SO.

Depois lançaram containers, como Docker, mais ainda fica em algum servidor, ainda tem algo que precisa ser administrado

Em 2014, lançaram o Serverless. Literalmente só temos que nos preocupar com o código. A Lambda cuida desde o Data Center até a camada de Aplicação.

É um serviço computacional com o qual vc pode fazer upload do seu código e criar uma função Lambda - como no Python. Lambda cuida dos servidores que vao rodar o código.

Para a Lambda funcionar, deve ser causado por triggers - como um task scheduler - que pode ser feito com usuário-lambda ou lambda-lambda e tudo isso escala automaticamente

## Hands On

Para isso, logamos em nossa conta AWS e selecionamos N. Virginia como region, isso se deve porque a maior parte dos serviços são primeiramente disponibilizados nessa região.

Vamos testar o serviço do Lambda:

![](images/01.png)

Vamos criar uma nova função. Temos 3 formas de criar uma função:
- De própria autoria
- Usando algum template
- Serverless repo

Vamos criar uma bem simples, de próprio punho:

![](images/02.png)[]

Veja que em Runtime, podemos escolher qual linguagem está escrito nosso código.

Clique em Criar Função.

Um recado assim deve aparecer:

![](images/03.png)[]

Podemos adicionar os triggers possíveis para as Lambda Function - é importante saber todos os triggers.

Agora vamos explorar o API Gateway.
Caso um usuário envie uma requisição HTTP para o API Gateway, ele vai ativar uma Lambda Function que vai enviar a resposta para o API e de volta para o usuário.
E se mais de um usuário fizer a requisição, mais Lambda Funcion serão ativadas, ou seja, isso vai escalar automaticamente.

As linguagens suportadas:
- Java
- C#
- Python
- Nodejs

Quanto custa?
Primeiras 1 milhão de requisições são grátis. Depois é $0.20/milhão

A função não pode executar mais de 5min - se não o Lambda não aceita.

### Exam Tips

1. Lambda tem scale out automático, o que é diferente de scale up
2. As funções são independentes - 1 requisição = 1 função
3. É serverless
4. Lambda podem ativar outras Lambdas
