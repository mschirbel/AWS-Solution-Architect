# EC2
EC2 = Elastic Compute Cloud.

É um serviço que providencia capacidade computacional na nuvem de forma mutável - ou seja
é mt facil fazer um scale da sua app pois pra bootar um novo server demora mt pouco.

São VM na nuvem.

Mudou a economia da nuvem, pois permite pagar somente pelo que usa.
EC2 permite construir apps que são isoladas.

## EC2 Options

On Demand: paga uma taxa fixa pela hora ou pelo segundo.
Reserved: contrato com a amazon que te da disconto, dura de 1 a 3 anos.
Spot: vc paga um "bid" para uma instancia e usa sob horários flexíveis
Dedicated Host: EC2 físicos. vc pode colocar as suas licenças nesses hosts.

### On Demand

Perfeito para users que querem low cost e flexibilidade.
Apps de short-term ou com workloads que nao podem parar
Aplicações de teste para Amazon.

### Reserved Instances

Aplicações que tem um uso predizível
Ou que usam uma capacidade reservada
Vc pode pagar uma entrada, assim tem um disconto maior nas horas futuras
custa mais ou menos uns 75% do On Demand
	Convertible RIs = vc pode mudar os atributos da sua RI, para maior ou igual valor(nunca menor)
	Schedule RIs = vc pode lançar dentro de uma janela - vc paga pelo que usou

### Spot Instances

Aplicações que tem start/end flexíveis
Aplicações viáveis para low compute prices - ou seja, só quer pagar por um tempo certo
Vc pode usar para fazer um scale rapido da sua aplicação
Não cobra por horas quebras, só por horas inteiras.

### Dedicated Hosts
Muito usado quando multi-tenant virtualization nao é suportado(varios clientes numa mesma máquina física)
Bom para colocar as licenças que vc ja tem
Vc paga pela hora e custa 70% do On Demand

## Tipos de EC2:

#### F1 - Field Programmable Gate Array

Usado para pesquisas de genoma, analise financeira, big data

#### I3 - High Speed Storage

NoSQL, DBs, Data Warehousing

#### G3 - Graphics Intensive

Streaming de app 3D

#### H1 - High Disk Throughput

MapReduce, HDFS

#### T2 - Lowest Cost, General Purpose

Web Serves, Small DBs

#### D2 - Dense Storage

Fileservers, Hadoop

#### R4 - Memory Optimized

Apps/DBs de intenso uso de memória

#### M5 - General Purpose

Application Servers

#### C5 - Compute Optimized

Apps/DBs de uso intenso de CPU

#### P3 - Graphics

Machine Learning, Bit Coin Mining

#### X1 - Memory Optmized

SAP HANA/Apache Spark

## Como lembrar de todos os tipos?

FIGHT DR. MC PIX

![](images/fight.jpg)

## O que é EBS?

EBS permite que vc cria volumes de storage e os junte com EC2. Uma vez unidos, vc pode criar um FS em cima desses volumes, rodar qualquer tipode de Block Device
EBS é colocado sob uma AZ onde são replicados para evitar falhas.

EBS = Elastic Block Storage

Tipo o Windows, que é instalado no C, mas vc pode ter o disco D,F,L e etc.

### Tipos de EBS
1. GP2 = General Purpose SSD
	3 IOPS/GB - pode ir até 10.000 IOPS
2. IO1 = Provisioned IOPS SSD
	pode ir alem de 10.000 IOPS
3. ST1 = Throughput Optimized HDD
	usado para big data, processamento de log - Não pode ser um volume de BOOT(tem que ser um volume adcional)
4. SC1 = Cold HDD
	file server, usado para coisas nao tao acessadas - não pode ser volume de boot
5. Magnetic(Standard)
	custo mais baixo. são usados para coisas não acessadas mas necessitam ser guardadas

### Instance Metadata

Para vermos a Metadata de nossas instâncias,basta logar com ssh na instância

```
ssh ec2-user@[public ip] -i key
```

Para ver os dados, basta:

```
curl http://169.254.16.254/latest/meta-data/
```

Essa URL é muito importante, precisa lembrar ela para o exame.

Assim vamos receber diferentes formas de dados, basta:

```
curl http://169.254.169.254/meta-data/latest/[dados]
```

## Exam Tips
* Options do EC2
* Tipos do EC2
* Preços
* EBS
* Meta-data
