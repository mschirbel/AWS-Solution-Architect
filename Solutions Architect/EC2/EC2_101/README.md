# Primeira Instancia do EC2

Primeiramente, escolha uma região próxima a vc.
Pode ser que não esteja todas os tipos de EC2 na região que vc escolher.

Dentro de Services, escolha o EC2:

![](images/01.png)

Aqui podemos ver um resumo de tudo que temos no nosso EC2:

![](images/02.png)

Vamos clicar em Lauch Instance:

![](images/03.png)

Aqui podemos ver todas as AMI - que são Snapshots de VMs e podes fazer boot dessas para nossos EC2

![](images/04.png)

Existem 2 tipos de virtualização
1. PV - Paravirtual
2. HVM - Hyper Virtual Machine

Para nossos guias, usaremos a Amazon Linux AMI - porque vem com muitas libs intaladas - junto com a AWS Cli

Podemos filtrar por Free-Tier.

Selecione a AMI e clique em Select.

![](images/05.png)

Agora podemos ver os tipos de EC2, que vimos na parte de Overview, assim como algumas configurações de nossa VM

![](images/06.png)

Clique em Next:Configure Instance Details

Aqui podemos configurar nosso EC2 em detalhes:

![](images/07.png)

Podemos definir o scalling da aplicação.
Purchasing Option é onde definimos o preço que pagaramos.
Request Valid From/To é o tempo que o EC2 ficará online
Persistent Request: colocar algum horário específico
Para mudar a opção do EC2, clique em Tenancy.

Em Network, temos a Virtual Private Cloud default - que é a default da sua region
Subnet é qual AZ vc quer colocar seu EC2
1 Subnet é igual a 1 AZ, 1 pra 1, sempre.

Shutdown behavior - é quando eu paro o EC2, ele deleta td ou somente para? podemos definir aqui.
Enable Termination Protection é pra prevenir que pessoas desliguem seu servidor
Monitoring: custa, e vc pode definir um tempo de monitoração

Clique em Next.

![](images/08.png)

Aqui é o volume root. Podemos acrescentar novos discos e em Types temos os que vimos em Overview
O que acontece se vc deletar uma instancia do EC2, o que acontece com o EBS?
O EBS vai sumir se a opção Delete on Termination for marcada.

Clique em Next.

![](images/09.png)

Uma tag é como chamaremos esse EC2.
As tags ajudam a controlar o preço, de onde ele vem
O cenário ideal é taggear o máximo possível.

Next.

![](images/10.png)

Security Groups são Virtual Firewalls
Vamos criar um novo:

![](images/11.png)

Review and Launch.

![](images/12.png)

Podemos ver todas as configs.
Launch.

![](images/13.png)

Aqui mostra que precisamos de um par de chaves para aumentar a segurançado do nosso EC2. Crie uma nova e clique em Download Key Pair.
Salve esse arquivo, será necessário depois.
Agora clique em Launch.

![](images/14.png)

Aqui vemos o status do nosso EC2.

Clique em View Instances.

![](images/15.png)

Podemos ver todas as nossas intancias.

Se clicarmos na aba Description:

![](images/16.png)

Podemos ver várias infos, dentre elas nosso IP Público.
É esse que usaremos para SSH.

Para isso, vamos ao diretório que está nossa key

```
cd /home/schirbel/Downloads/
```

E vamos trocar as permissões de nossa chave
```
chmod 400 key.pem
```

Agora só precisamos:

```
ssh ec2-user@[ip publico] -i [key].pem
```

![](images/17.png)

Agora vamos virar root

```
sudo su
```
Vemos que o user mudou.

Agora vamos dar update de segurança

```
yum update -y
```

Agora vamos instalar o Apache.

```
yum install httpd -y
```

Agora entramos no diretório padrao do Apache
```
cd /var/www/html/
```

Vamos criar um index.html
```
vi index.html
```

Inserimos qualquer conteúdo nele.

Agora só precisamos iniciar o Apache
```
service httpd start
```

Agora vamos no browser e colamos nosso IP Público:

![](images/18.png)

## Agora vamos ver algumas configs diferentes e Terminar nossa EC2

Se entrarmos na DashBoard do EC2, vemos que temos uma Instancia rodando agora:

![](images/19.png)

Vemos que temos um Volume, que é nosso disco
Uma key pair, que utilizamos pra dar ssh
E mais um security group que criamos ao iniciar a vpc

Entre na instancia rodando

![](images/20.png)

Se usarmos o DNS público, podemos ver nosso EC2:

![](images/21.png)

Para fazermos qualquer ação em nosso EC2, vamos em Actions -> Instance State

![](images/22.png)

Vamos clicar em Terminate.

![](images/23.png)

Não é possível terminar a instancia, isso se deve porque precisamos encerrar o volume associado antes.
Ou podemos desabilitar a Termination Protection:

![](images/24.png)

Clique em Yes, Disable

![](images/25.png)

Antes de dar Terminate, vemos nosso Status Check:

![](images/26.png)

Aqui podemos ver se nossa app é acessível -no System Status Check ver se o hypervisor está acessível
E vemos se o Instance Status Check se nossa app está acessível por url

Agora vamos Terminate nossa instância.

Veja o status ao desligar:

![](images/27.png)

## Como criar Reserved Instances.

No menu da esquerda, podemos ver:

![](images/28.png)

Assim podemos comprar nossa instância reservada, por um contrato de 1 a 3 anos.

## Encrypted Boot Volumes

Ao darmos launch numa instancia:

![](images/29.png)

Não podemos criptografar o root volume, pois eles são providenciados pela AWS, mas podemos criptografar os outros.

## EC2 Placement Groups

Os nomes devem ser únicos, assim como os buckets do S3.
Não pode haver merge entre Placement Groups. E nada pode ser movido para um Placement Group.
Mas é possível criar uma AMI e instanciá-la no grupo

#### Clustered Placement Group - mais cobrado

Grupo de instâncias dentro de uma AZ. São recomendados para aplicações que necessitam de baixa latência na network, tipo cluster de Cassandra DB. Isso acontece justamente pela proximidade geográfica. Somente algumas instâncias podem ser feitas usando esse grupo, não são todas que podem.

#### Spread Placement Group

Cada instância está em um hardware diferente. Isso é bom para aplicações com um baixo número de instâncias que precisam estar em diversas AZ.

## Summary

1. Termination Protection é OFF por default, precisamos habilitar para que ninguem desligue a VM
2. Por default, quando a instancia é deletada, o EBS também é
3. EBS Root Volume nao pode ser criptografado
4. Mas podemos criptografar o root volume usando programas de terceiros - fazendo uma cópia e criptografando a cópia
5. Placement Group - muito importante
