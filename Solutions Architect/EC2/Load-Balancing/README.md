# Load Balancer & Healthcheck

Vamos para EC2 - e vamos voltar a aplicação que tínhamos parado.

![](images/01.png)

Agora faça um SSH para sua instância:

```
ssh ec2-user@[ip publico] -i [chave.pem]
```

Agora cheque se o serviço de HTTPD está rodando:

```
service httpd status
```

Se não estiver:
```
service httpd start
```

Caso queira que inicie sempre ao boot da máquina:
```
chkconfig httpd on
```

Vá para o diretório da aplicação:

```
cd /var/www/html/
```

Veja se tem um arquivo index.html. Se não tiver, crie um com algum texto.

Agora vamos criar um novo arquivo para healthcheck:

```
nano healthcheck.html
```

Escreva qualquer outra coisa dentro desse arquivo.

Agora volte para a console da AWS.

Na parte de EC2, no menu da esquerda:

![](images/02.png)

Clique em Load Balancer

Ao clicar em Create Load Balancer

![](images/03.png)

Os três tipos, que vimos na aula teórica, estarão lá.

Vamos criar um clássico:

![](images/04.png)

Para os grupos de segurança:

![](images/05.png)

Ignore, por enquanto, as configurações de segurança.
E para a configuração do Healthcheck:

Vamos testar usando HTTP na porta 80 usando a página que criamos.
Response Timeout é o tempo que vai esperar pela resposta do Healthcheck, pode ir de 2 a 60 segundos
Interval é o tempo entre Healthcheck
Unhealthy treshold são quantos se falhas consecutivas até declarar uma instância não saudável
Healthy treshold é o oposto

![](images/06.png)

E agora para adicionar quais instâncias do EC2, apenas marque a instância que deseja e clique em next.

E para as tags(essa é a parte mais importante):

![](images/07.png)

Agora podemos criar.

![](images/08.png)

Podemos ver a aba de Healthcheck ao selecionarmos nosso ELB.

![](images/09.png)

Agora volte ao seu EC2

```
cd /var/www/html/
rm healthcheck.html
```

Vamos simular uma falha

Vemos que a instância ficou Out of Service:

![](images/10.png)

Agora criamos novamente o arquivo do healthcheck:

```
echo "I'm healthy again" >> healthcheck.html"
```
Veja que a instância voltou:
![](images/11.png)

Uma vez que a instância está off, o load balancer não enviará requests para sua instância do EC2.

Ao colar o DNS no browser:

http://myclassicelb-114650537.sa-east-1.elb.amazonaws.com/

Vemos que recebemos a nossa página html. isso mostra também que NÃO recebemos um IP público para nosso load balancer, somente um DNS
A razão é que os ips publicos mudam e o DNS não.

Agora vamos criar mais um Load Balancer, só que um Application.

![](images/12.png)

Lembrando que, cada AZ é uma SubnetID. Isso é importante.

Vamos selecionar nosso Security Group

E para as configurações de routing:

![](images/13.png)

Para os Register Targets, selecione, na aba de Instances, a instância e clique em Add to registered.
Selecione em Registered Instances a instância e cliquei em Next.

![](images/14.png)

Pode criar nosso Application Load Balancer.

Ao criar, o status será de Provisioning:

![](images/15.png)

Assim como o clássico, não usaremos um ip publico, mas somente um dns.
Em Monitoring podemos ver os acessos:

![](images/16.png)

### Exam Tips:

1. temos 3 tipos
2. erro 504
3. se precisamos pegar o ip de quem fez a request, temos que usar o X-Fowarded-For Header
4. instancias são monitoradas pelo ELB usando InService ou OutOfService
5. healthcheck ve se a instancia ta ON simplemente conversando com ela
6. ELB tem sempre um DNS e nunca um IP publico
7. leia o FAQ de ELB
