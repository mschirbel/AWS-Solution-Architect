# Snapshots e Encrypting Root Devices.

Primeiramente devemos tirar um snapshot.
E como vimos na aula passada, para que um snapshot seja consistente, temos que parar a instância do EC2.

Para isso, logue na AWS, vá em EC2 e veja as instâncias.
Em seguir, pare a instância:

![](images/01.png)

O aviso nos indica que o cache vai ser perdido, mas tudo bem, não precisamos dele.

A instância deverá ficar assim:

![](images/02.png)

Parar a instância antes de tirar um snapshot é boa prática, pois mantém consistente o snapshot tirado entre múltiplas instâncias.

Assim que o status ficar em Stopped, vá no menu da esquerda, na parte de Volumes:

![](images/03.png)

Clique em Actions -> Create Snapshot.

![](images/04.png)

No menu da esqueda, acesso Snapshots e veja o status do snapshot que foi tirado:

![](images/05.png)

Espere o snapshot ser concluído.

Podemos agora encriptar esse snapshot de várias maneiras. Uma delas é copiando para outra região.

Actions -> Copy Snapshot.

![](images/06.png)

Como já visto, podemos copiar snapshots para outras regiões. E criptografá-los no processo.
Nesse caso, vamos copiar para US East e criptografar.

Para visualizarmos essa cópia, devemos trocar de região:

![](images/07.png)

E vemos que o status de criptografia foi aplicado.
Agora podemos criar uma imagem desse snapshot.

Actions -> Create Image

![](images/08.png)

E podemos usar essa imagem para bootar instâncias EC2 com o boot device criptografado, o que aumenta a segurança.

Apenas um detalhe: ao criar a imagem, vc deve usar a HAV(Hardware Assisted Virtualization) - assim o Free Tier fica disponível.

No menu da esquerda, vamos em AMIs:

![](images/09.png)

Clicamos em Launch e deixamos as configurações default.

Perceba que nosso device está criptografado:

![](images/10.png)

Basta clicar em Launch.

Vemos nossa instância criada:

![](images/11.png)

Podemos terminar essa instância. Deregistrar nossa AMI e excluir nosso Snapshot.

### Aprendemos

Aprendemos que sempre devemos parar a instância EC2 antes de tirar um Snapshot

Vimos que snapshots de volumes criptografados também são criptografados

Volumes que são restaurados a partir de snapshots, são criptografados

Só podemos compartilhar snaphosts se forem PÚBLICO - assim como na AWS AMI Marketplace
