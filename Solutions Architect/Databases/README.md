# Databases 101

## Relational Database

São os mais usados, parecem muito uma planilha, onde existem colunas, indices e relacionamento entre colunas.

### Tipos de BD Relacional

1. SQL Server
2. Oracle
3. MySQL
4. Postgres
5. Amazon Aurora
6. MariaDB

Vamos usar MySQL. No teste vai ser perguntado por RDS como um todo e também sobre Amazon Aurora

## Non-Relation Database

Temos um databse, dentro do collection, dentro do documento temos um documento, e dentro do documento temos keys e values.

Traduzindo isso para um mundo relacional:
1. Collection = Table
2. Document = Row
3. Key/Value = Data

A difrença é que os documentos podem ter quantas keys forem necessárias, vc não fica restrito igual nos banco relacionais.
É completamente flexível.

Um dos mais famosos tipos de documentos, são os JSON files.
São compostos de keys e values:

```json
{
"_id": "92189284082F09JWCW1WCNC9",
"firstname": "John",
"surname": "Smith",
"age": "23",
"address:[
	{"street": "Baker Street 221b",
	 "suburb": "Central London"}
	]
}
```

Como vemos acima, temos um array dentro do documento, assim como um dictionary, em Python

## Data Warehouse

Usado para BI, usando ferramentas como Cognos, Jaspersoft, SQL Server, SAP NetWeaver.

Armazena grandes quantidades de dados, muitas vezes complexos.

### OLTP vs OLAP

OLTP = Online Transaction Processing
OLAP = Online Analytics Processing

#### OLTP

Vai pegar uma row de alguma table, contendo dados que foram solicitados.
Assim como quando é inserido algo num carrinho de compras.

#### OLAP

Quando vc quer uma análise sobre um conjunto de dados. Por exemplo:
* Custo por unidade em uma região
* Taxa de venda por região
* Soma das vendas no Pacífico.

Isso não é bom ser feito em produção, pois pode causar lentidão. Por isso vc exporta seus dados e roda em um Warehouse separado.

## Elasticache

É um webservice que deixa mais fácil o deploy, operation e scaling da nuvem. Porque ele permite recuperar informações que ficam guardadas em memória ao contrário de ficar guardado em discos, como nos bancos tradicionais - tipo o SAP4HANA - BD em RAM.

Exemplo: guardar os top 10 vendedores de todos os tempos - coisas que não mudam tão facilmente.

Suporte do Elasticache

1. Memcached
2. Redis

## AWS Database Types

1. RDS - OLTP
 * SQL
 * MySQL
 * PostgreSQL
 * Oracle
 * Aurora
 * MariaDB
2. DynamoDB - NoSQL - cai muito no Developer
3. Redshift - Warehouse - OLAP
4. Elasticache - In memory caching

## Quiz

1. É gratis replicar os dados de uma instância primária para uma secundária.
2. 16TB é o volume maximo que podemos ter, por default, quando usamos RDS Provisioned IOPS Storage,com MySQL e Oracle
3. Operações I/O são suspensas durante o snapshot
4. Com Multi-AZ não é possível ter uma instância só para leitura. Todas são Write/Read
5. O limte de IOPS para MySQL e Oracle RDS mudam com o tempo.
