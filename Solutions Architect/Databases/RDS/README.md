# RDS

Vamos subir um webserver e um banco de dados bem simples usando os scripts que estão nessa pasta.
Vamos usar Apache, PHP e MySQL.

## Banco de dados

Primeiro passo, vamos logar na conta AWS e ir para o serviço do RDS:

![](images/01.png)

Vamos para nossa Dashboard e vamos criar uma instância:

![](images/02.png)

Nessa aula, vamos usar o MySQL com todas as opções default:

![](images/03.png)

É possível marcar uma opção para deixar somente as opções gratuitas do RDS.

Todas as configurações ficaram em default. Somente vamos trocar as opções de login e senha:

![](images/04.png)

O ID da instância do banco é somente um Alias para a sua conta AWS na região que o banco foi criado.

Em Advanced Settings é possível marcar a sua instância como privada, sendo assim mais protegida.

![](images/05.png)

E na ultima parte setamos a porta e as conexões com o banco de dados:

![](images/06.png)

Agora basta criar o DB. Isso pode levar alguns minutos.
Podemos ver o status da instância no menu Instances

![](images/07.png)

## Instância EC2

Agora vamos criar uma instância EC2 com todas as características necessárias para conectarmos ao banco de dados.

Pode ser todas as configurações default. Mas vamos usar nosso boot script em Advanced Details:

![](images/08.png)

Todas as outras configurações serão default. Vamos esperar uns 5min até estar tudo instalado.
Enquanto isso, podemos voltar ao dashboard do RDS e ver o status de nossa instância:

![](images/09.png)

Perceba que, para RDS nunca recebemos um IP público, mas sim um Endpoint:

![](images/10.png)

Para saber se a intância de EC2 já subiu, copie o IP público e abra no navegador:

![](images/11.png)

Se a versão do PHP apareceu, é porque deu certo.

Agora abra um terminar, e conecte na instância EC2:

```
ssh ec2-user@ip -i Key.pem
```

Eleve os privilégios:

```
sudo su -
```

Entre na pasta do Apache e veja que temos dois arquivos lá. Caso o arquivo do connect não esteja, talvez vc deva colocar em algum local publico para que o wget funcione. Poder ser até mesmo um bucket na AWS.

```
cd /var/www/html/
ls -la
```

Agora vamos editar o arquivo connect.txt para virar um arquivo php com as variáveis corretas.

```
mv connect.txt connect.php
vi connect.php
```

Para saber o hostname, usamos o Endpoint do RDS.

Com o arquivo editado, voltamos ao browser e ao invés de ir para o IP de nosso EC2, vamos para 

http://52.67.151.139/connect.php

A requisição vai terminar com um erro, pois ainda precisamos adicionar o security group em igualdade, tanto no EC2 como no RDS.

![](images/12.png)

Para isso: 

Clicamos em Security Group na instância RDS:

![](images/13.png)

Removemos o filtro dos grupos:

![](images/14.png)

Vamos criar uma Inbound Rule para nosso grupo do RDS:

![](images/15.png)

![](images/16.png)

Agora basta clicar em Save e dar refresh na página do browser:

![](images/17.png)
