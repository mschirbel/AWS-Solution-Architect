# Automated Backups

Existem dois tipos de backups:

1. Automated Backups - permite recuperar o DB em qualquer ponto no tempo dentro do tempo de retençaõ, que vai de 1 até 35 dias. O esquema de recovery funciona semelhantemente ao de layers gravadas em logs, sendo possivel o retorno com até segundos contados. Vai ter um snapshot diário e logs de mudanças no DB. São marcados por default e ficam gravados no S3 - e é gratis.
2. Database Snapshots - são feitos manualmente - e ficam gravados mesmo após excluir a instâcia.

Caso apague a instância de DB, os automated backups também serão apagados.

## Restore Backup

A versão restaurada vai ser uma nova instância, ou seja, o endpoint vai mudar. Isso é importante.

## Encryption

Todos os relacionais são passíveis de criptografia. É feita utilizando a KMS - AWS Key Management Service.
Todos os dados são criptografados - assim como os automated backups, replicas e snapshots.

Para criptografar uma instância é necessário criar um snapshot, fazer uma cópia e depois criptografar a cópia.

Para fazer isso, basta entrar na página das suas instâncias do banco de dados:

![](images/18.png)

Se clicarmos em snapshots, podemos ver os automated snapshots:

![](images/19.png)

Para fazer um restore em algum ponto no tempo, basta voltarmos na instância e clicar em Instance Action

![](images/20.png)

Isso nos levará para uma outra tela onde podemos selecionar qual tempo, seja o último, ou custom:

![](images/21.png)

Vamos tirar um snapshot:

![](images/22.png)

![](images/23.png)

Assim podemos ver que foi criado. Pode demorar cerca de 2-3min.

Agora podes ir nas opções do snapshot e criar uma cópia

![](images/24.png)

E aqui podemos fazer a criptografia desse snap, para aí sim, criar uma instância RDS criptografada

![](images/25.png)

## Multi-AZ

Suponha que existam 3 instâncias EC2 conectadas a algum banco em SA-east-1. Podemos ter uma cópia exata deste banco em alguma outra region, como US-east-1b.
Isso só deve ser feito para DR, pois os dados serão mantidos em regions diferentes.

E o DR ia fazer um failover diretamente para a outra region.

E isso é seguro, pois quando tratamos de databases, nunca usamos IPs, e sim DNS como Endpoints.

Se quiser algo para performance, devemos usar Read Replicas. Multi-AZ é somente para DR.

Para ativar Multi-AZ, clique em Modify:

![](images/27.png)

E aqui temos a opção:

![](images/28.png)

## Read Replicas

Do mesmo exemplo do Multi-AZ, quando temos read replicas, os dados serão levados para cópias exatadas da instância RDS. Em até 5 cópias, por default. Mas por que seria feito?

Mas isso é somente para READ-ONLY. SOMENTE READ-ONLY as replicas.

Isso pode ser feito para que as instâncias EC2 leiam diretamente das cópias, o que aumenta a performance em cada uma das instâncias do banco de dados. Scaling Out do banco principal e levando para replicas.

E podemos ter replicas de replicas - mas isso pode trazer latência.
Cada replica vai ter o próprio endpoint.
Podemos ter replicar em outras AZ ou até mesmo outras Regions.

Isso é usado para scaling, não para DR.

As replicas podem ter Multi-AZ
E podemos ter replicas de Multi-AZ databases.

E as replicas podem ser promovidas para databases originais. Mas isso quebra a replicação.

Para criar uma read replica:

![](images/26.png)

Podemos selecionar para onde queremos mandar a replica, se queremos criptografia e até mesmo logs exportadas para CloudWatch
