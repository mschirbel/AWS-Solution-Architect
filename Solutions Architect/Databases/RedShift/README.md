# Redshift - AWS Warehouse

Petabyte-scalable. Sendo o custo $0.25 per hour.
O custo é quase um décimo da maioria dos outros warehouses.

## OLAP

Warehouses são bons para OLAP, pois como os dados não estão em produção(write and read) podemos processar queries longas, mesmo que demore um tempo.

## RedShift Configuration

1. Single Node - até 160GB
2. Multi Node - mais que 160GB
	* Leader Node: controla as conexões e recebe as queries
	* Compute Node: guarda os dados e realiza as queries - pode ir até 128 compute nodes

## Columnar Data Storage

Redshift organiza os dados por colunas. 
Ao contrário da maioria dos bancos, que coloca os dados em linhas.

Organizar por colunas é bom para analytics e queries sobre grandes data sets.
Isso garante menos I/Os, ou seja, mais performance.

## Advanced Compression

Redshift tem multiplos algoritmos de compressão, em relação aos bancos de dados.

E ao contrário dos bancos de dados, ele não precisa de indexes ou materializar views - isso ocupa menos espaço no disco.

Quando vc quer dados em alguma tabela vazia, o Redshift faz o select e apresenta na melhor compressão.

## MPP - Massively Parallel Processing

Distribui os dados e as queries entre todos os nodes. Isso faz com que os nodes rodem a query mais rapidamente.
Para isso, deve estar em Multi Node Architecture.

## Cobrança

- Pelas horas de Compute Nodes - Leader Node não é cobrado
- Backups
- Data Transfer

## Avaliability

Somente em 1 AZ - o que faz sentido, porque não precisa de low latency.
O maior uso é para analytics mesmo.

E é possível restaurar um snapshot em outra AZ, caso seja necessário.
