# Elasticache

É um web service que permite:
1. Fazer deploy
2. Operar
3. Scaling in memory cache

Aumenta a performance de uma aplicação pois permite retirar informações que ficam guardadas em cache na memória - isso agiliza muito.

Ao contrário dos DB convencionais que usam discos para gravar informações.

Diminui a latênaica para heavy-application:
- Gaming
- Media Sharing
- Portals

Cached information pode ser usado para guardar dados críticos ou intensos calculos.

## Types of Elasticache

### Memcached

Sistema de memoria cache. E tem usabilidade com a maioria das ferramentas existentes hoje no mercado.

### Redis

Open-source que guarda key-value.
Guarda os dados ordenados e em listas.

Pode usar com Multi-AZ e redundância.

## Exam Tips

Elasticache é bom se o seu database usa muito reading e não muda muito.

Redshift é bom se vc precisa rodar muitas OLAP.
