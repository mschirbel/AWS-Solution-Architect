# DynamoDB

É o banco não relacional da AWS

É rápido e low latency - coisa de millisegundos.
É facilmente escalável.

Bom para aplicações que usam app mobile, games, IoT.

## Facts

1. Storagem em SSD - o que fica muito rápido
2. Espalhado em 3 datacenters diferentes
3. Eventual Consistent Reads - geralmente demora 1 segundo para ler - Best Read Performance
4. Strongly Consistent Reads - responde que recebeu oq foi escrito antes de conseguir ler.

## Princing

* Provisioned
1. Write Throughput = $0.0065/hour for every 10 units
2. Read Throughput  = $0.0065/hour for every 50 units

* Storage
1. $0.25/GB for every month

Ex:
1kk writes and 1kk reads per day - 3GB.
1kk/day = 11.6/sec writes or reads
0.0065/10 x 12 x 24 = $0.1872 -> Write
0.0065/50 x 12 x 24 = $0.0374 -> Read

DynamoDB é caro pra writes mas é barato pra reads.

Para criar uma tabela:

![](images/01.png)

Geralmente é rápido para criar.
Veja que podemos selecionar qual será a primary key da nossa tabela.

![](images/02.png)

Se formos para reserved capacity:

![](images/03.png)

Podemos estabelecer um contrato por um determinado tempo, o que abaixa os preços.

Para adicionar algum item:

![](images/04.png)

Um item é uma entrada na tabela.

![](images/05.png)

Podemos dar append em nossos itens, com os atributos.

![](images/06.png)

Aqui podemos ver como ficou após a inserção.

Para adicionar ainda mais campos:

![](images/07.png)

E assim podemos adicionar dinamicamente, mesmo que não exista as colunas. Em um DB relacional precisaríamos adicionar as colunas antes.

Para pesquisar algo no DB:

![](images/08.png)

Diferente do RDS, onde precisamos de um snapshot e criar réplicas, no DynamoDB o scaling é automático:

![](images/09.png)

Enquanto é feito o scaling, não tem downtime.
RDS tem downtime no scaling, porque preciso tirar o snapshot e copiar ainda.
