# Aurora

Somente funciona na AWS. Não é possível fazer um download e usar localmente.

Mas é um MySQL-compatible que combina a velocidade e disponibilidade com simplicidade e custo-benefício de bancos open source.

Tem 5x melhor performance que um MySQL e custa um décimo da maioria deles.

## Scaling

Começa com 10GB e vai de 10GB até 64TB

Compute pode ir até 32 CPUs e 244GB de RAM.

Mantem 2 cópias dos dados em cada AZ com no mínimo 3 AZ.

Aurora pode suportar perda de 2 copias sem afetar o DB.

Aurora é self-healing, isso significa que os discos são scaneados para verificar erros e reparados automaticamente.

## Replicas

1. Aurora Replicas = são replicas identicas do DB, pode ter até 15.
2. MySQL Read Replicas = replicas só para leitura de dados.

A diferença é o failover, o failover acontece automaticamente para Aurora Replica, mas não para o MySQL Read Replica.

## Lab

*Não é de graça, e é bem caro*

Aurora está somente em algumas regions.

Para subir uma instância Aurora, logue na AWS e vá para o serviço RDS. E clique em Lauch a Database:

![](images/01.png)

Seleciona Aurora e preencha as informações:

![](images/02.png)

Tiers determinam quais instâncias viram master em failover.

![](images/03.png)

Podemos habilitar criptografia, usando o KMS da AWS.

Na dashboard do RDS podemos ver a Aurora:

![](images/04.png)

Pode demorar até 10min para iniciar.

Assim que ficar disponível

Note que a instância criada é para Write, e não é Multi-AZ, isso acontece porque desmarcamos a opção. Essa instância fica dentro de uma VM e é passível de falha. Para que isso não ocorra, devemos criar uma réplica

Para criar uma Aurora Replica:

![](images/05.png)

![](images/06.png)

Podemos escolher a publicidade e até qual AZ colocar nossa réplica.

Os tier escolhem para qual instância será direcionado o tráfego em caso de failover.

E perceba que a replica vai ser um Reader e não um writer:

![](images/07.png)

Ao clicarmos sobre uma instância, podemos ver os detalhamentos dela:

![](images/08.png)

Assim como o Endpoint. Lembre que, para escrever no banco, usaremos o Endpoint da instância Write.

Podemos ter até todas as AZ da region.

Se o cluster endpoint não resolver, haverá um failover para a instância de menor Tier. Isso evita configurar a connection string toda vez que acontecer alguma queda no sistema.
