# Elastic Container Service - ECS

## Typical Application Stack

Existem algumas camadas dentro da sua aplicação:

1. Guest OS - como Windows, RHEL, CentOS
2. Dependencies - como Apache, IIS, PHP, MySQL
3. Application
4. VM

![](images/01.png)

Um dos problemas, são as versões das dependencias instaladas. Porque em todas as máquinas, as versões devem ser as mesmas.

E se tudo estivesse empacotado, como em algum container?

Para isso foi criado um Container, todos eles seguem um padrão, ou seja, podem ser aplicados em qualquer máquina.

O Docker empacota todas as necessidades do ambiente e roda em qualquer plataforma.

## Docker

É um Software Open Source.

É altamente confiável pois garante que o código funcione em qualquer sistema distribuído

É altamente escalável, pois trata o ambiente como código.

Os containers de docker garantem que as versões são controladas.

E a stack de aplicação fica diferente:

![](images/02.png)

## Virtualisation vs Containerisation

Quando usando uma VM, a aplicação está sobre o hypervisor. E os recursos do SO da VM podem usar muito, cerca de 80%, dos recursos da VM. Isso é uma perda enorme!

Com containers, somente usa a aplicação e suas dependências.

Docker é um orquestrador de containers, isso facilita muito o uso de containers em cima de um mesmo SO.

E algo muito bom, é que as aplicações em contianers, não tomam nada de uma aplicação em um segundo container.

Isso é o princípio de MicroServices.

## Docker Components

* Docker Images

Contém somente os arquivos necessários para rodar um container

* Docker Container

É um isolado e seguro ambiente para a aplicação

* Layers

São camadas que são unidas pelo Union File Systems para formar um único FS.
Quando mudar uma docker image, uma nova camada será escrita. Isso é menos custoso do que sobrescrever toda a image.

* DockerFile

Arquivo que contém instruções para construções de camadas em cima de uma image base.
Cada instrução é uma nova layer.

* Docker Daemon/Engine

Se coordena com o Docker client para executar comandos no SO.

* Docker Client

Uma interface entre o usuário e o docker daemon e permite a manipulação de containers

* Docker Registries

Stores privados ou publicos que contém uma coleção de images. Essas images podem ser feitas por alguem, ou feitas pelo próprio usuário.

## ECS

É um serviço que administra containers docker em um cluster de EC2 instances.

É regional e podemos usar em uma ou mais AZs.

Administra um cluster e evita a preocupação com scaling.

Também pode ser usado para criar um deploy consistente e criar aplicações com microservices.

Na AWS, temos o AWS ECR que funciona como um Docker Hub.

## ECR

Amazon Elastic Container Registry.

Suporta repositórios privados com permissões de IAM ou uso específicos.

Um EC2 pode acessar esses repositórios e as images. É possível também usar a Docker CLI para fazer isso.

## ECS Task Definition

É algo necessário para rodar Docker Containers na AWS.

São textos, em formato JSON que descrevem um ou mais containers na sua aplicação.

### ECS Tasks parameters

1. Docker Image
2. CPU/Memory para cada container
3. Links entre containers
4. Networking
5. Portas mapeadas
6. Tasks quando algum container falhar
7. IAM roles

Entre outros. É como se fosse um compose, mas feito pra AWS

Isso funciona como um AutoScaling para o ECS.

### ECS Clusters

* Podem conter multiplos tipos de containers
* Regional
* Um container só pode fazer parte de 1 cluster por vez
* Pode ser criado policies para restringir o acesso

### Scheduling

1. Service Scheduler

Assegura que um número específico de tasks vai funcionar, em caso de failover.

Pode ser registrado em um ELB

2. Custom Scheduler

Criar as proprias schedulers de acordo com o negócio.

Existem softwares de terceiros que podem fazer isso, como Blox.

## ECS Agent

É um agente que permite concetar com o cluster via linha de comando.

**Não funciona em Windows**

Já vem instalado nas AMIs do ECS e pode ser instalado em um EC2

## ECS Security

Funciona com IAM para acessar os recursos.

SG também funcionam a nivel da instância, não a nivel da Task.

## Limites

1000 Clusters por Region
1000 Instancias por Cluster
500 Services por Instances

## Exam Tips

1. ECS cuida de um cluster de docker containers em EC2 Instances
2. Images são read-only templates
3. AWS ECR é o registry da AWS
4. ECS Agent não funciona em Windows. Funciona em EC2.
5. Task é necessário para ECS - tipo um CloudFormation
6. Services são como AutoScaling para ECS
7. Cluster são regionals
8. IAM pode ser integrado com ECS
9. SG funcionam a nivel da instancia EC2, não a nivel da task nem a nivel do container.