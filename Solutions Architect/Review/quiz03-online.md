# Quiz Online 3

## Cloud Formation

You have five CloudFormation templates; each template is for a different application architecture. This architecture varies between your blog apps and your gaming apps. What determines the cost of using the CloudFormation templates?

* The time it takes to build the architecture with Cloud Formation.
* Cloud Formation does not have any additional cost but you are charged for the underlying resources it builds.
* 0.10$ per template per month
* 0.1$ per template per month

Explanation:
Answer: (B) There is no additional charges for AWS CloudFormation templates. You only pay for the AWS resources it builds.

---

## Databases and Subnets

Which of the following correctly applies to changing the DB subnet group of your DB instance?

* An existing DB Subnet group can be updated to add more subnets for existing Availability Zones.
* An existing DB group cannot be updated to add more subnets for new Availability Zones.
* Removing subnets from an existing DB subnet group can cause unavailability.
* Updating an existing DB subnet group of a deployed DB instance is not currently allowed.
* Explicitly changing the DB Subnet group of a deployed DB instance is not currently allowed.

Explanation:
Answer: (A), (C), (D), and (E) An existing DB subnet group can be updated to add more subnets, either for existing Availability Zones, or for new Availability Zones added since the creation of the DB instance. Removing subnets from an existing DB subnet group can cause unavailability for instances.

---

## SSL and Load Balancer

If you want to use an SSL protocol but do not want to terminate the connection on your load balancer, you can use a __________ protocol for connection from the client to your load balancer.

* HTTP
* TSL
* HTTPS
* TCP

Explanation:
Answer: (D) If you want to use an SSL protocol but do not want to terminate the connection on your load balancer, you can use a TCP protocol for connection from the client to your load balancer. Use the SSL protocol for connection from the load balancer to your back-end application, and install certificates on all the back-end instances handling requests.

---

## Payment on AWS

If you want to build your own payments application, then you should take advantage of the richness and flexibility of _____________.
* PayPal Payment service
* EBay Payment service
* Amazon AWS DevPay
* Amazon AWS FPS

Explanation:
Answer: (C) and (D) Amazon DevPay and Amazon FPS both leverage the Amazon Payments infrastructure to process payments from customers.

---

## Storage

You are building an automated transcription service in which "Amazon EC2 worker" instances process an uploaded audio file and generate a text file. You must store both of these files in the same durable storage until the text file is retrieved, but you do not know what the storage capacity requirements are. Which storage option is both cost-efficient and scalable?

* Multiple Amazon EBS volume with snapshots
* A single Amazon Glacier Vault
* A single Amazon S3 bucket
* Multiple instance stores

Explanation:
Answer: (C) Amazon S3 provides a cost effective, durable, and scalable storage option. It provides the developers the same highly scalable, reliable, fast, inexpensive data storage infrastructure that Amazon uses to run its own global network of websites.

---

## CloudFormation

Which of the following should be referred to if you want to map Amazon Elastic Block Store to an Amazon EC2 instance for AWS CloudFormation resources?

* The logical IDs of the instance
* Reference the logical IDs of both the block stores and the instance
* Reference the physical IDs of the instance
* Reference the physical IDs of the both the block stores and the instance

Explanation:
Answer: (B) As part of the CloudFormation template, you need to build the JSON with all the required attributes.

---

## Databases

In the event of a planned or an unplanned outage of your primary DB instance, Amazon RDS automatically switches to a standby replica in another Availability Zone if you have enabled_________.

* More than one read replica
* More than one write replica
* Multiple Availability Zones
* Multi Region Deployment

Explanation:
Answer: (C) In the event of a planned or unplanned outage of your primary DB instance, Amazon RDS automatically switches to a standby replica in another Availability Zone if you have enabled multi-Availability Zones.

---

## Snapshots

Which of the following approaches provides the lowest cost for Amazon elastic block store snapshots while giving you the ability to fully restore data?

* Maintain two snapshots: the original snapshot and the latest incremental snapshot.
* Maintain a volume snapshot; subsequent snapshots will overwrite one another.
* Maintain a single snapshot; the latest snapshot is both incremental and complete.
* Maintain the most current snapshot; archive the original and increment to Amazon Glacier.

Explanation:
Answer: (A) After writing data to an EBS volume, you can periodically create a snapshot of the volume to use as a baseline for new volumes or for data backup. If you make periodic snapshots of a volume, the snapshots are incremental which means only the blocks on the device that have changed after your last snapshot are saved in the new snapshot. Even though snapshots are saved incrementally, the snapshot deletion process is designed such that you need to retain only the most recent snapshot in order to restore the volume.

---

## SSH Connection

You try to connect via SSH to a newly created Amazon EC2 instance and get one of the following error messages: 'Network error: connection timed out" or "Error connecting to [instance], reason :-> Connection timed out: connect,' you have confirmed that the network and security group rules are configured correctly and the instance is passing status checks. What steps should you take to identify the source of the behavior? (Select all that apply)

* Verify that the private key file corresponds to the Amazon EC2 key pair assigned at launch.
* Verify that your IAM user policy has permission to launch Amazon EC2 instances.
* Verify that you are connecting with the appropriate user name for your AMI.
* Verify that the Amazon EC2 instance was launched with the proper IAM role.
* Verify that your federation trust to AWS has been established.

Explanation:
Answer: (A) and (C) For any EC2 instance, you need the correct key pair and the user account to log into the instance. Without these even AWS support team cannot access that instance.

---

## VPC

In a VPC network, access control lists (ACLs) act as a firewall for associated subnets, controlling both inbound and outbound traffic at the __________ level.

* Full VPC
* Customer Gateway
* EC2 instance
* Subnet

Explanation:
Answer: (D) Amazon VPC provides two features that you can use to increase security for your VPC: security groups and ACL. Security groups act as a firewall for associated Amazon EC2 instances, controlling both inbound and outbound traffic at the subnet level.

---

## Secondary index

Which of the following is NOT true about the local secondary index?

* The key of a local secondary index consists of a hash key and a range key.
* For each hash key, the total size of all indexed items must be 10 GB or less.
* The local secondary index allows you query over the entire table, across all partitions.
* When you query a local secondary index, you can choose either eventual consistency or strong consistency.

Explanation:
Answer: (C) A local secondary index lets you query over a single partition, as specified by the hash key value in the query. A global secondary index lets you query over the entire table, across all partitions.

---

## CloudWatch

A user has created multiple data points for the CloudWatch metrics with the dimensions, Box=UAT, App = Document and Box = UAT, App = Notes. If the user queries CloudWatch with the dimensions parameter as, Server=prod, what data will he get?

* The last value of the email and sms metric
* It will not return any data as the dimension for Box=UAT does not exist
* All values specified for the dimension Box=UAT, App=Document
* All values specified for the dimension Box=UAT, App=Notes

Explanation:
Answer: (B) A dimension is a key value pair used to uniquely identify a metric; the user cannot get the CloudWatch metrics statistics if he has not defined the right combination of dimensions for it. In this case the dimension combination is either Box=UAT, App=Document, or Box=UAT, App= Notes.

---

## DynamoDB

For Dynamo DB, which of the following statements are correct? (Select all that apply)

* By using proxy, it is not possible for a developer to achieve item level access control.
* By using FGAC, it is possible for a developer to achieve item level access control.
* By using Per-Client Embedded Token, it is possible for a developer to achieve item level access control.
* By using secret key, it is possible for a developer to achieve item level access control.

Explanation:
Answer: (A), (B), and (C) Fine Grained Access Control (FGAC) gives a DynamoDB table owner a high degree of control over data in the table. Specifically, the table owner can indicate who (caller) can access which items or attributes of the table and perform what actions.

---

## S3

You try to enable lifecycle policies on one of the S3 buckets created by you, but you are not able to do so on that particular bucket. What could be the reason?

* Bucket is corrupted.
* Versioning is not enabled on that bucket.
* Bucket type is not correct.
* Versioning is enabled on the bucket.

Explanation:
Answer: (B) You can manage an object's lifecycle by enabling lifecycle policies, which define how Amazon S3 manages objects during their lifetime. You need to enable bucket versioning to manage S3 lifecycle policies.

---

## EC2 and VPC

Each EC2 instance has a default network interface that is assigned a primary private IP address on your Amazon VPC network. What is the name given to the additional network interfaces that can be created and attached to any Amazon EC2 instance in your VPC?

* Elastic IP
* Elastic Network Interface
* AWS Elastic Interface
* AWS Network ACL

Explanation:
Answer: (B) An Elastic Network Interface (ENI) is a virtual network interface that you can attach to an instance in a VPC. An ENI can include a primary private IP address.

---

## IAM

Which IAM policy condition key should be used if you want to check whether the request was sent using SSL?
* AWS: secure transport
* AWS: secure IP
* AWS: source IP
* AWS: user agent

Explanation:
Answer: (A) AWS provides the following predefined keys for all AWS services that support IAM for access control: AWS: current time and AWS: secure transport.

---

## EC2 Policy

What does the following policy for Amazon EC2 do?

```json
 { Statement: [{ Effect:Allow, Action:ec2: Describe*, Resource:* }
```

* Allow users to use all actions on an EC2 instance.
* Allow users to use actions that start with 'Describe' across all the EC2 resources.
* Allow users to use actions that does not have the keyword "Describe' across all the EC2 resources.
* Allow a group to be able to Describe with run, stop, start, and terminate instances.

Explanation:
Answer: (B) If you want to assign permissions to a user, group, role, or resource, you can create a policy, which explicitly lists the permissions. As per the above policy, the action attribute says, provide permissions to access any "Describe *" calls on every EC2 resources.

---

## AWS CLI

For what purpose is the string
```
create image
```
API action used?

* To create an Amazon EBS-backed AMI from an Amazon EBS-backed instance that is either running or has stopped
* To initiate the copy of an AMI from the specified source region to the current region
* To deregister the specified AMI. After you deregister an AMI, It can't be used to launch new instances.
* To describes one or more of the images (AMIS, AKIS, and ARIS) available to you

Explanation:
Answer: (A) It creates an Amazon EBS-backed AMI from an Amazon EBS-backed instance that is either running or has stopped. If you customize your instance with instance store volumes or EBS volumes in addition to the root device volume, the new AMI contains block device mapping information for those volumes.

---

## Instances

If you launch an instance into a VPC that has an instance tenancy of a ______________, your instance is automatically a Dedicated Instance, regardless of the tenancy of the instance.

* secured instance
* dedicated instance
* default instance
* new instance

Explanation:
Answer: (B) Each VPC has a related instance tenancy attribute. You can't change the instance tenancy of a VPC after you create it. Any instance that you launch in a VPC with a specific tenancy will inherit the same tenancy of the VPC.

---

## DynamoDB

In DynamoDB you can issue a scan request. By default, the scan operation processes data sequentially. DynamoDB returns data to the application in _________ increments, and an application performs additional scan operations to retrieve the next ___________ of data.

* 0, 1 MB
* 1, 10 MB
* 1, 1 MB
* 5, 5 MB

Explanation:
Answer: (C) By default, the scan operation processes data sequentially. DynamoDB returns data to the application in 1 MB increments, and an application performs additional scan operations to retrieve the next 1 MB of data.

---

21. AWS requires ____________ when you need to specify a resource uniquely across all of AWS, such as in IAM policies, Amazon Relational Database Service (Amazon RDS) tags, and API calls.
 IAM Used Id
 Account Id
 IAM policy
 Amazon Resource Names
Explanation:
Answer: (D) Amazon Resource Names (ARNs) uniquely identify AWS resources. An ARN is required when you need to specify a resource unambiguously across all of AWS, such as in IAM policies, Amazon Relational Database Service (Amazon RDS) tags, and API calls.

22. ___________ is a task coordinator and state management service for cloud applications.
 Amazon SWF
 Amazon SNS
 Amazon SQS
 Amazon SES
Explanation:
Answer: (A) Amazon Simple Workflow (Amazon SWF) is a task coordinator and state management service for cloud applications. With Amazon SWF, you can stop writing complex codes or invest in state machinery and business logic that makes your applications unique.

23. Which of the following IP address mechanisms are supported by ELB?
IPv4
IPv5
IPv6
IPv3
Explanation:
Answer: (A) and (C) ELB supports both IPv4 and IPv6. IPv4 is the most widely used form of address. But with the boom of the Internet and connected devices IPv4 is running out of IP addresses; IPv6 is slowly replacing it as it has more IP addresses available.

24. A ___________ is a physical device or software application on your side of the VPN connection.
 Customer gateway
 Gateway level
 Gateway table
 Virtual private gateway
Explanation:
Answer: (A) When you create a VPN connection, the VPN tunnel comes up when traffic is generated from your side of the VPN connection. The virtual private gateway is not the initiator; your customer gateway initiates the tunnels.

25. You are currently hosting multiple applications in a VPC and have logged numerous port scans coming in from a specific IP address block. Your security team has requested that all access to the offending IP address block be denied for the next 24 hours. Which of the following is the best method to quickly and temporarily deny access to the specified IP address block?
 Create an AD policy to modify Windows Firewall settings on all hosts in the VPC to deny access to the IP address block.
 Modify the Network ACLs (NACLs) associated with all public subnets in the VPC to deny access from the IP address block.
 Modify the Windows Firewall settings on all Amazon Machine Images (AMIs) which your organization uses in that VPC to deny access from the IP address block.
Explanation:
Answer: (B) AWS has implemented security layers at every level. As per OSI layers, you can restrict access at network level using NACL rules at VPC and below subnet levels. You can configure NACL rules to allow and deny the traffic. After crossing the network layer, if you still want to configure at the instance or resource level, you can configure it using security groups. Per the above context, you need to do it at the network level for a specific period and roll back the changes. You can do this at the network layer by altering allow/deny NACL rules.

26. Which ELB component is responsible for monitoring the Load Balancers?
 Controller service
 Load Balancer
 Auto Scaling
 Load Manager
Explanation:
Answer: (A) Elastic Load Balancing (ELB) consists of two components: the load balancers and the controller service. The load balancers monitor the traffic and handle requests that come in through the Internet. The controller service monitors the load balancers, adding and removing load balancers as needed and verifying that the load balancers are functioning properly.

27. Which disaster recovery method involves running your site in AWS and on your existing on-site infrastructure in an active-active configuration?
 Multi-site solution
 Active-passive solution
 Pilot light
 Warm standby solution
Explanation:
Answer: (A) In multi-site solution both the infrastructures on AWS and on your external data center are always active and you can use one of them in case of a disaster.

28. An application hosted at the EC2 instances receives HTTP requests through the ELB. Each request has an X-Forwarded-For request header, having three IP addresses. Which of the following IP address will be a part of this header?
 IP address of ELB
 IP address of Forward Request
 IP address of client
 IP address of CloudWatch
Explanation:
Answer: (A) The X-Forwarded-For request header helps you identify the IP address of a client when you use HTTP/HTTPS load balancer. Because load balancers intercept traffic between clients and servers, your server access logs contain only the IP address of the load balancer. Elastic Load Balancing stores the IP address of the client in the X-Forwarded-For request header and passes the header along to your server.

29. You have launched an instance in EC2-Classic and you want to make some change to the security group rule. How will these changes be effective?
 Security group rules cannot be changed.
 Changes are automatically applied to all instances that are associated with the security group.
 Changes will be effective after rebooting the instances in that security group.
 Changes will be effective after 24-hours.
Explanation:
Answer: (B) If you're using EC2-Classic, you must use security groups created specifically for EC2-Classic. When you launch an instance in EC2-Classic, you must specify a security group in the same region as the instance. If you make any changes, they will be automatically applied to all instances that are associated with the security group.

30. You have an application running on Amazon Web Services. The application has 4 EC2 instances in Availability Zone us-east-1c. You're using Elastic Load Balancer to load balance traffic across your four instances. What changes would you make to create a fault tolerant architecture?
 Create EBS backups to ensure data is not lost.
 Move all four instances to a different Availability Zone.
 Move two instances to another Availability Zone.
 Use CloudWatch to distribute the load evenly.
Explanation:
Answer: (C) Elastic Load Balancer automatically distributes incoming application traffic across multiple Amazon Elastic Compute Cloud (Amazon EC2) instances. You can set up an elastic load balancer to load balance incoming application traffic across Amazon EC2 instances in a single Availability Zone or multiple Availability Zones. Elastic Load Balancing enables you to achieve greater fault tolerance in your applications and it also seamlessly provides the amount of load balancing capacity that is needed in response to incoming application traffic.

31. The load balancer does not distribute traffic across ________.
 One Availability Zone
 Domains
 Availability Zones within a region
 Regions
Explanation:
Answer: (D) You can set up your Elastic Load Balancing to distribute incoming requests across EC2 instances in a single Availability Zone or multiple Availability Zones within a region. Your load balancer does not distribute traffic across regions.

32. In context of CloudFormation, which of the following information do you get from the AWS Cloud Formation list-stacks Command?
 A list of any of the stacks you have created.
 A list of any of the stacks you have created or have deleted up to 90 days ago.
 A list of any of the stacks that have been created or deleted up to 60 days ago.
 A 90 days history list of all your activity on stacks.
Explanation:
Answer: (B) The AWS CloudFormation list-stacks command enables you to get a list of any of the stacks you have created (even those which have been deleted up to 90 days). You can use an option to filter results by stack status, such as CREATE_COMPLETE and DELETE_COMPLETE. The AWS CloudFormation list-stacks command returns summary information about any of the running or deleted stacks, including the name, stack identifier, template, and status.

33. When you use the wizard in the console to create a VPC with a gateway, the wizard automatically __________ to use the gateway.
 updates the route tables
 updates the IP tables
 updates the protocol tables
 updates the IP tables and the protocol tables
Explanation:
Answer: (A) When you use the wizard in the console to create a VPC with a gateway, the wizard automatically updates the route tables to use the gateway. If you�re using the command line tools or the API to set up your VPC, then you have to update the route tables yourself.

34. You've created production architecture on AWS. It consists of one load balancer, one route53 domain, two Amazon S3 buckets, Auto Scaling policy, and Amazon CloudFront for content delivery. Your manager asks you to duplicate this architecture by using a JSON based template. Which of the following AWS service would you use to achieve this?
 Amazon DynamoDB
 Amazon Simple DB
 Amazon CloudFormation
 Amazon Bootstrap
Explanation:
Answer: (C) AWS CloudFormation gives developers and system administrators an easy way to create and manage a collection of related AWS resources; provisioning and updating them in an orderly and predictable fashion.

35. You have configured a website www.abc.com and hosted it on WebLogic Server and you are using ELB with the EC2 instances for load balance. Which of the following would you configure to ensure that the EC2 instances accept requests only from ELB?
 Configure the security group of EC2, which allows access to the ELB source security group.
 Configure the EC2 instance so that it only listens on the ELB port.
 Configure the security group of EC2, which allows access only to the ELB listener.
 Open the port for an ELB static IP in the EC2 security group.
Explanation:
Answer: (A) A security group acts as a firewall that controls the traffic allowed into a group of instances. When you launch an Amazon EC2 instance, you can assign it to one or more security groups. For each security group, you can add rules that govern the allowed inbound traffic to instances in the group. By configuring the security group of EC2 you can ensure that the EC2 instances accept requests only from ELB.

36. You have written a CloudFormation template that creates one Elastic Load Balancer fronting two EC2 instances. Which section of the template should you edit so that the DNS of the load balancer is returned upon creation of the stack?
 Outputs
 Resources
 Parameters
 Mappings
Explanation:
Answer: (A) The Outputs section defines custom values that are returned by the AWS CloudFormation describe-stacks command and in the AWS CloudFormation console Outputs tab after the stack is created. You can use output values to return information from the resources in the stack such as the URL for a website that was created in the template or the Domain Name Server (DNS).

37. What does a 'Domain" refer to in Amazon SWF?
 Set of predefined fixed IP addresses
 A Security group in which internal tasks can communicate with each other
 A collection of related Workflows
 A collection of related topics
Explanation:
Answer: (C) Domains provide a way of scoping Amazon SWF resources within your AWS account. All the components of a workflow, such as the workflow type and activity types, must be specified in a domain. It is possible to have more than one workflow in a domain; however, workflows in different domains cannot interact with each other.

38. A customer has a website which is accessible over the Internet and he wants to secure the communication and decides to implement HTTPS instead of HTTP. He has configured EC2 instance behind an ELB. Where should you configure the SSL certificate?
 Not possible in AWS
 SSL certificate will be installed at ELB and the listener port should be changed from 80 to 443 to allow the traffic to reach EC2
 SSL certificate will be installed at EC2 and listener port should be changed from 80 to 443
 SSL certificate will be installed at EC2 and listener port can remain at 443
Explanation:
Answer: (B) If you secure the communication, you will configure SSL certificates to allow HTTPS secure communication. You can configure and install SSL certificate on ELB in order to enable HTTPS traffic.

39. Once you've successfully created a Microsoft windows stack on AWS CloudFormation, you can log in to your instance with _______ to configure it manually.
 AWS Command Line Interface
 Remote Desktop
 Power shell
 Windows Command prompt
Explanation:
Answer: (B) Once you've successfully created a Microsoft Windows stack on AWS Cloud Formation, you can log in to your instance with Remote Desktop to configure it manually.

40. You have created a custom configured Amazon instance using Linux, containing all your software and applications. If you want to use the same setup again, what is the best way to do it?
 Create a back up copy of the EBS service
 Create a backup of the EC2 instances only
 Create a snapshot of the AMI only
 Create an EBS Image (AMI)
Explanation:
Answer: (D) The Amazon Linux AMI is a supported and maintained Linux image provided by Amazon Web services for use on Amazon Elastic Compute Cloud (Amazon EC2). It is designed to provide a stable, secure, and high performance execution environment for applications running on Amazon EC2. It also includes packages that enable easy integration with AWS, including launch configuration tools and many popular AWS libraries and tools.

41. With regards to VPC, what is the default maximum number of virtual private gateways allowed per region?
 10
 15
 5
 1
Explanation:
Answer: (C) The default number of VPCs per region is 5. This limit can be increased upon request. The default number of subnets per VPC is 200. This limit can be increased upon request. The default number of Internet gateways per region is 5 and you can create as many internet gateways as your VPCs per region limit. Only one Internet gateway can be attached to a VPC at time.

42. Elasticity is a fundamental property of the cloud. Which of the following best describes elasticity?
 The power to scale computing resources up and down easily with minimal friction
 Ability to create services without having to administer resources
 Process by which scripts notify you of resource so you can fix them manually.
 Power to scale computing resources up easily but not scale down.
Explanation:
Answer: (A) Elasticity can best be described as the power to scale computing resources up and down easily with minimal friction. Amazon Elastic Compute Cloud (Amazon EC2) is a web service that provides resizable compute capacity in the cloud. It is designed to make web-scale computing easier for developers.

43. With regards to RDS, the standby should be in the same ______________ as the primary instance.
 Availability Zone
 Region
 VPC
 Subnet
Explanation:
Answer: (B) Your standby is automatically provisioned in a different Availability Zone of the same Region as your primary DB instance.

44. AWS Identity and Access Management is available through which of the following interfaces?
AWS Management Console
Command line interface (CLI)
IAM QUERY API
Elastic Load Balancer
Cloud Formation
Explanation:
Answer: (A), (B), and (C) You can configure the IAM by using AWS console and command line interface using AWS CLI, AWS SDK, and IAM Query API calls.

45. Scalability is a fundamental property of a good AWS system. Which of the following best describes scalability on AWS?
 Scalability is the concept of planning ahead for what maximum resources will be required and building your infrastructure based on that capacity plan.
 The law of diminishing returns will apply to resources as they are increased with workload.
 Increasing resources result in a proportional increase in performance.
 Scalability is not a fundamental property of the cloud.
Explanation:
Answer: (C) Auto Scaling allows you to scale your Amazon EC2 capacity up or down automatically according to conditions you define. With Auto Scaling, you can ensure that the number of Amazon EC2 instances you're using increases seamlessly during demand spikes to maintain performance.

46. Which technique can be used to integrate AWS IAM (Identity and Access Management) with an on-premises LDAP (Light Weight Directory Access Protocol) directory service?
 Use an IAM policy that references the LDAP account identifiers and the AWS credentials.
 Use SAML (Security Assertion Markup Language) to enable single sign-on between AWS and LDAP.
 Use AWS security Token Service from an identity broker to issue short-lived AWS credentials.
 Use IAM roles to automatically rotate the IAM credentials when LDAP credentials are updated.
 Use the LDAP credentials to restrict a group of users from launching specific EC2 instance types.
Explanation:
Answer: (B) You can use SAML Identity Providers in order to integrate IAM between AWS and on premise LDAP or federated SSO implementation. For example, you want to provide a way for users to copy data from their computers to a backup folder, in your organization. You build an application that users can run on their computers. On the back end, the application reads and writes objects in an S3 bucket but the users don't have direct access to AWS. Instead, the application can communicate with an identity provider (IdP) to authenticate the user. The IdP gets the user information from your LDAP which is the organization's identity store and then generates a SAML assertion that includes authentication and authorization information about that user. The application then uses that assertion to make a call to the AssumeRoleWithSAML API to get temporary security credentials. The app can then use those credentials to access a folder in the S3 bucket that's specific to the user.

47. If you are using a non-transactional engine such as My ISAM, which of the following steps need to be performed to successfully set up your Read Replica so it has a consistent copy of your data?
Stop all DML and DDL operations on non-transactional tables and wait for them to complete
Flush and lock those tables
Create the Read Replica using the Create DB instance Read Replica API
Check the progress of the Replica creation using the describe DB instances API
Set AWS IAM and KMS
Explanation:
Answer: (A), (B), (C),and (D) If you are using a non-transactional engine such as My ISAM, you will need to perform the following steps to successfully set up your Read Replica. These steps are required in order to ensure that the Read Replica has a consistent copy of your data. 1. Stop all DML and DDL operations on non-transactional tables and wait for them to complete. 2. Flush and lock those tables. 3. Create the Read Replica using the Create DB instance Read Replica API. 4. Check the progress of the Replica creation using the describe DB instances API.

48. In CloudFront, if you add a CNAME for www.abc.com to your distribution, you also need to create (or update) a CNAME record with your DNS service to route queries for ___________.
 www.abc.comto d111111abcdef8.cloudfront.com
 d111111abcdef8.cloudfront.com to www.abc.com
 www.abc.com to d111111abcdef8.cloudfront.net
 d111111abcdef8.cloudfront.net to www.abc.com
Explanation:
Answer: (C) You can specify one or more domain names that you want to use for URLs for objects instead of the domain name that CloudFront assigns when you create your distribution.

49. Your manager has asked you to build a MongoDB replica set in the Cloud. Amazon Web Services does not provide a MongoDB service. How would you go about setting up the MongoDB replica set?
 You have to build it on another data center.
 Request AWS to add a Mongo DB service.
 Build the replica set using EC2 instances and manage the Mongo DB instances yourself.
 It is not possible to do it.
Explanation:
Answer: (C) Mongo DB runs well on Amazon EC2. To deploy Mongo DB on EC2 you can either set up a new instance manually or deploy a pre-configured AMI from the AWS Marketplace.

50. Your company has an application that requires access to a NoSQL database. Your IT departments have no desire to manage the NoSQL servers. Which Amazon service provides a fully managed and highly available NoSQL service?
 Elastic Map Reduce
 Amazon RDS
 Simple DB
 DynamoDB
Explanation:
Answer: (D) DynamoDB is a fast, fully managed NoSQL database service that makes it simple and cost-effective to store and retrieve any amount of data, and serve any level of request traffic. Its guaranteed throughput and single-digit millisecond latency make it a great fit for gaming, advertising technology, mobile, and many other applications.

51. How many requests per second can Amazon CloudFront handle?
 10,000
 100
 1000
 500
Explanation:
Answer: (A) Amazon CloudFront can handle up to 10, 000 requests per second

52. When you need to use CloudFront to distribute your content you need to create a distribution. You also need to specify the configuration settings. Which of the following configuration settings would you specify?
 You can configure the environment variables.
 You can specify the number of files that you can serve per distribution.
 You can specify whether you want the files to be available to everyone or you want to restrict access to selected users.
 You can specify your origin Amazon S3 bucket or HTTP server.
Explanation:
Answer: (D) When you want to use CloudFront to distribute your content, you create a distribution and specify configuration settings such as: Your origin, which is the Amazon S3 bucket or HTTP server from which CloudFront gets the files that it distributes. You can specify any combination of up to 10 Amazon S3 buckets and/or HTTP servers as your origins.

53. You currently operate a web application in the AWS US-East region. The application runs on an auto-scaled layer of EC2 instances and an RDS Multi-AZ database. Your IT security compliance officer has tasked you to develop a reliable and durable logging solution to track changes made to your EC2, IAM, and RDS resources. The solution must ensure the integrity and confidentiality of your log data. Which of these solutions would you recommend?
 Create a new Cloud Trail with one new S3 bucket to store the logs and with the global services option selected. Use IAM roles S3 bucket policies and Multi Factor Authentication (MFA) delete on the S3 bucket that stores your logs.
 Create a new Cloud Trail with one new S3 bucket to store the logs. Configure SNS to send log file delivery notifications to your management system. Use IAM roles and S3 bucket policies on the S3 bucket that stores your logs.
 Create a new Cloud Trail with an existing S3 bucket to store the logs and with the global services option selected. Use S3 ACLs and Multi Factor Authentication (MFA) delete on the S3 bucket that stores your logs.
 Create three new Cloud trails with three new S3 buckets to store the logs-one for the AWS management console, one for AWS SDKs ,and one for command line tools. Use IAM roles and S3 bucket policies on the S3 buckets that store your logs.
Explanation:
Answer: (A) As Cloud Trail will be stored in S3, to avoid any delete you should enable IAM Role as well as MFA. If you decide to use an existing bucket, when you turn on Cloud Trail for a new region, you might receive the error that there is a problem with the bucket policy. CloudTrail allows you to track changes made to your EC2, IAM, and RDS resources.

54. Which of the following metrics can have a CloudWatch Alarm?
RRS lost object
EC2 instance status Check Failed
EC2 CPU utilization
Auto Scaling group CPU utilization
Explanation:
Answer: (B), (C), and (D) Amazon CloudWatch provides monitoring for AWS cloud resources and the applications customers run on AWS. Developers and system administrators can use it to collect and track metrics, gain insight, and react immediately to keep their applications and businesses running smoothly. Amazon CloudWatch monitors AWS resources such as Amazon EC2 and Amazon RDS DB instances, and can also monitor custom metrics generated by a customer's applications and services.

55. Which of the following payment options are associated with Reserved Instances?
Partial Upfront
No Upfront
Annual Upfront
All Upfront
Explanation:
Answer: (A), (B) and (D) Amazon EC2 Reserved Instances allow you to reserve Amazon EC2 computing capacity for 1 or 3 years, in exchange for a significant discount (up to 75%) compared to On-Demand instance pricing. You can choose between three payment options: All Upfront, Partial Upfront, and No Upfront. If you choose the Partial or No Upfront payment option, the remaining balance will be due in monthly increments over the term.

56. You have a website www.abc.com which is used quite frequently. Therefore, you decide to use 50 EC2 instances, with two availability zones in two regions, each with 25 instances. However, while starting the servers, you are able to start only 20 servers and then the requests start failing. Why?
 There is a limit of 20 EC2 instances in each region; you can request to increase the limit.
 There is a limit of 20 EC2 instances in each availability zone, you can request to increase the limit.
 You might have exhausted the free space available and need to select paid version of storage.
 You cannot have more than one availability zone in a region.
Explanation:
Answer: (A) Unless otherwise noted, there is a limit per region. You are limited to: running up to 20 on-demand instances, purchasing 20 reserved instances, and requesting 5 spot instances per region. New AWS accounts may start with limits that are lower than the limits described here. Certain instance types are further limited per region.

57. www.picsee.com website has millions of photos and also thumbnails for each photo. Thumbnails can easily be reproduced from the actual photo. However, a thumbnail takes less space than actual photo. Which of the following is the best solution to store thumbnails?
 S3
 Reduced Redundancy Storage
 DynamoDB
 Elastic Cache
 Amazon Glacier
Explanation:
Answer: (A) Reduced Redundancy Storage(RRS) is an Amazon S3 storage option that enables customers to reduce their costs by storing noncritical, reproducible data at lower levels of redundancy that Amazon S3's standard storage. It provides a cost effective, highly available solution for distributing or sharing content that is durably stored elsewhere, or for storing thumbnails, transcoded media, or the processed data that can be easily reproduced. The RRS option stores objects on multiple devices across multiple facilities, providing 400 times the durability of a typical disk drive, but does not replicate objects as many times as a standard Amazon S3 storage.

58. You want your Hadoop job to be triggered based on the event notification of a file upload action. Which of the following components can help you implement this in AWS?
S3
SQS
SNS
EC2
IAM
Explanation:
Answer: (A), (B), and (C) Amazon S3 can send event notifications when objects are uploaded to Amazon S3. Amazon S3 event notifications can be delivered using Amazon SQS or Amazon SNS, or sent directly to AWS Lambda, enabling you to trigger workflows, alerts, or other processing.

59. www.dropbag.com is a website where you have file sharing and storing services like Google Drive and Google Dropbox. During the sync up from desktop you accidently deleted an important file. Which of the simple storage service will help you retrieve the deleted file?
 Versioning in S3
 Secured signed URLs for S3 data access
 Don't allow delete objects from S3 (only soft delete is permitted)
 S3 Reduced Redundancy Storage.
Explanation:
Answer: (A) Amazon S3 provides further protection with versioning capability. You can use versioning to preserve, retrieve, and restore every version of every object stored in your Amazon S3 bucket. This allows you to easily recover from both unintended user actions and application failures. By default, requests will retrieve the most recently written version.

60. www.picnic.com is a photo and video hosting website and they have millions of users. Which of the following is a good solution for storing big data object, by reducing costs, scaling to meet demand, and increasing the speed of innovation?
 AWS S3
 AWS RDS
 AWS Glaciers
 AWS Redshift
Explanation:
Answer: (A) Whether you're storing multimedia files such as photos and videos or pharmaceutical files, or financial data Amazon S3 can be used as your big data object store. Amazon Web services offers a comprehensive portfolio of services to help you manage big data by reducing costs, scaling to meet demand, and increasing the speed of innovation.