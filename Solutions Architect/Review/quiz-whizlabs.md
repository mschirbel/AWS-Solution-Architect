# Quiz WhizLabs

Pode ser encontrado ![aqui](https://www.whizlabs.com/learn/course/quiz-result/8123)

1) Uma aplicação precisa que uma camada de storage guarde documentos importates. Quais dos seguintes serviços é ideal?

A. S3
B. EBS
C. EFS
D. Storage Gateway VTL

Answer: A - S3 é a camada perfeita para guardar documentos como objetos. Também tem uma opção de versionamento, que pode ser usado para recuperar documentos posteriormente.

---

2) Uma companhia deseja construir um e-commerce na AWS. Quais dos serviços abaixos podem ser usados para manter sessões dos usuários?

A. CloudWatch
B. DynamoDB
C. ELB
D. ElastiCache
E. Storage Gateway

Answer: B,C e D - DyanamoDB e ElastiCache são perfeitos para guardar sessões de dados. DynamoDB é rápido e flexível NoSQL DB para aplicações que precisam de: consistência, latência de milisegundos e escalabilidade. É totalmente manipulável na nuvem e guarda documentos e key-value objetos.
ElastiCache é um serviço que deixa fácil administrar dados distribuídos em memória na nuvem.

---

3) Uma companhia precisa guardar dados na nuvem. A previsão inicial é de 80GB podendo ir até 80TB. A solução precisa ser durável.

A. DynamoDB
B. S3
C. Aurora
D. Redshift

Answer: B - S3 permite que sejam guardados dados em formato de objetos com uma durabilidade de 99.999999999% e disponibilidade de 99.99%.
Também é possível fazer analytics dos dados em S3 usando query-in-place.

---

4) Uma empresa deseja usar um MongoDB em uma instância EC2. Precisa-se de muitas leituras e escritas no banco. Qual EBS devemos usar?

A. EBS Provisioned IOPS SSD
B. EBS Throughput Optimized HDD
C. EBS General Purpose SSD
D. EBS Cold HDD

Answer: A - visto que precisamos de alta performance. EBS Provisioned IOPS SSD é o mais recomendado para isso, podemos ver uma comparação ![aqui](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumeTypes.html)

---

5) Uma empresa deseja mover os arquivos, com certa data de criação, de um bucket para Glacier. Como isso deve ser feito?

A. Bucket Policy
B. Lifecycle rule
C. IAM policy
D. CORS on bucket

Answer: B - com regras de lifecycle é possível mover arquivos de forma totalmente automatizada e isso pode ser aplicado para um grupo de objetos.

---

6) Uma empresa precisa usar um NGINX como web server, como podemos fazer isso de forma rápida?

A. EC2
B. Elastic Beanstalk
C. SQS
D. ELB

Answer: A e B - EC2 é onde podemos instalar o web server e configurá-lo manualmente. Elastic Beanstalk é um serviço que permite manipular a infraestrutura com simples cliques.

---

7) Uma empresa precisa fazer leituras extremamente rápidas no bucket do S3. Como podemos fazer isso?

A. ID sequencial no prefixo
B. Hexadecimal hash no prefixo
C. Hexadecimal hash no sufixo
D. ID sequencial no sufixo

Answer: B - segundo a documentação da AWS que pode ser vista ![aqui](https://docs.aws.amazon.com/AmazonS3/latest/dev/request-rate-perf-considerations.html)

---

8) Uma companhia quer monitorar todos os IPs que estão nas interfaces de network. Qual serviço pode ser usado para isso?

A. Trusted Advisor
B. Flow Logs
C. CloudWatch
D. CloudTrail

Answer: B - Flow Logs permitem métricas para monitorar esse tráfego de IP. Depois esses dados são armazenados em CloudWatch Logs.

---

9) Uma empresa quer guardar dados em blocos. Cada device vai ter 100GB. Qual pode ser usado?

A. EBS Volumes
B. S3
C. Glacier
D. EFS

Answer: A - Para block storage use EBS.

---

10) Uma aplicação foi coloca em EC2 distribuídos por diversas AZ. As instâncias estão atrás de um ELB. E são administradas via Auto Scaling Group. Há uma NAT Instance que é usada para fazer updates das instâncias pela internet.
Qual das seguintes é um bottleneck para a aplicação:

A. EC2
B. ELB
C. Nat Instance
D. Scaling Group

Answer: C - sendo que só há uma instância NAT, ela é o bottleneck. Para uma alta disponibilidade é bom colocar essa instância em diversas AZ e que faça parte do Scaling Group.

---

11) Um time quer fazer um deploy na AWS de forma serverless. Precisam de serviços que possam ser invocados atráves de todo o globo. Quais devem usar?

A. Lambda
B. API Gateway
C. RDS
D. EC2

Answer: A e B - Lambda e API Gateway são serviços globais e não dependem de regions como os outros dois.

---

12) Uma companhia guarda todos os documentos antigos em um Glacier. Precisa-se retirar um documento em 20min para uma auditoria. Qual tipo de retrieval deve ser usado?

A. Vault Lock
B. Expedited Retrieval
C. Bulk Retrieval
D. Standard Retrieval

Answer: B - Com expedited é possível acessar rapidamente os dados quando há alguma urgência.

---

13) Uma empresa precisa configurar o Scaling Group para uma campanha promocial dentro de algumas semanas. Como isso pode ser feito no Scaling Group?

A. Step Scaling
B. Dynamic Scaling
C. Scheduled Scaling
D. Static Scaling

Answer: B - Se a métrica usada for para aumentar ou diminuir proporcialmente o número de instâncias recomenda-se usar uma policy de target tracking.
Em target tracking scaling policies vc define uma métrica ou configura uma customizada. O Auto Scaling cria e administra os alarmes de CloudWatch que ativam essas policies e calcula o ajuste necessário. Nesse caso, é melhor que Scheduled porque não sabemos o tempo que vai terminar.

---

14) Uma empresa deseja construir sua aplicação na AWS. Vai ter uma camada de web server e uma de db server. Ambas em uma VPC. O web server vai estar na public subnet e o db na privada. Todas as subnets são criadas com as configurações default de ACL. Os webservers devem ser acessíveis somente por SSL connection e os DB devem ser acessíveis pelos webservers.

A. Crie uma NACL na subnet do web server e permita 443 HTTPS nos inbounds e source como 0.0.0.0/0
B. Crie um SG para os webservers que permite 443 inbound de qualquer lugar e aplique nos Webservers
C. Crie um SG para o DB que permite 3306 inbound e especifique a source como o SG do web server
D. Cri um NACL na subnet do DB e permita 3306 inbound para os web servers e negue todo o outbound

Answer: B e C

Para que todos o tráfego seja seguro e vá para os webservers de qualquer lugar, precisa-se permitir 443 nas inbound rules. Para permitir que o DB se comunique com os webservers temos que ter um SG.

---

15) Uma empresa quer que um EC2 acesse um DynamoDB, qual o jeito mais seguro de isso ser feito?

A. API credentials usando a user-data da instância
B. API credentials em um S3
C. API credentials nos arquivos .JAR da aplicação
D. IAM roles nas instâncias

Answer: D - segundo a documentação é a forma mais segura de ser feito.

---

16) Uma empresa quer usar infra-as-a-code. Qual o serviço mais recomendado?

A. Beanstalk
B. CloudFormation
C. CodeBuild
D. CodeDeploy

Answer: B - cloudformation é um serviço que controla a infraestrutura com códigos em JSON. É criado um template que será o guia para criar EC2, RDS, VPC e outros serviços. É altamente escalável.

---

17) Uma empresa tem uma série de EC2 com dados críticos em EBS. O time de Security diz que os volumes devem ser criptografados.
Como isso pode ser feito?

A. AWS KMS API
B. AWS Certificate Manager
C. API Gateway + STS
D. IAM Access Key

Answer: A - KMS é usado para criar e controlar keys de criptografia de dados. Ele se integra com outros serviços como EBS, por exemplo, ou S3, Redshift, RDS e outros.

---

18) Uma empresa guarda dados críticos em EBS. Como garantir que os dados podem ser recuperados rapidamente?

A. Tirar Snaps regulares
B. EBS encryption
C. Criar ums script que faz cópias dos dados
D. Mirror Data

Answwe: A - É possível recuperar os dados de snapshots até o segundo. Snapshots são incrementais, ou seja, somente o bloco de mudança que sofreu a alteração vai ser salvo. Isso minimiza o tempo de criação dos snapshots e diminui o custo. Quando um snapshot é deletado, somente os dados únicos são removidos. Cada snapshot possúi informações necesárias para restaurar os dados para um novo volume de EBS.

---

19) Um time quer guardar daods em JSON na AWS. Qual serviço pode ser usado?

A. EFS
B. Redshift
C. DynamoDB
D. CloudFormation

Answer: C - DynamoDB é um NoSQL DB que tem alta escalabilidade. Os dados são salvos em formato JSON.

---

20) Uma empresa quer usar containers para as aplicações de diversos clientes. Esses containers vão usar diversos serviços da AWS e eles não podem se comunicar com containers de outros clientes. Como isso pode ser feito?

A. IAM roles for tasks
B. IAM roles for EC2
C. IAM Instance profile para EC2
D. SG

Answer: A - Com roles podem ser especificados os containers usados em uma task. As aplicações devem usar suas credencias e isso permite que um serviço não interfira no outro.


















































