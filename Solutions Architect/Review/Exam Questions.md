# Exam Questions

## Quiz 1:

1. Ao tirar um snapshot de uma instância, pare a instância, tire o snapshot de cada um independentemente. Depois disso, inicia a instância.
2. Metodologias de criptografia para S3:
  * SSE-S3
  * SSE-C
  * SSE-KMS
  * Amazon S3 Encryption Client
3. Ao criar um usuário no IAM pela primeira vez, essas credenciais são geradas **automaticamente**:
  * Access Key ID
  * Secret Access Key
4. Ao criar um usuário, para que ele faça login, é preciso que você gere a senha
5. S3 oferece Read-after-wirte consistency for **PUTS** and **EVENTUALLY** overwrite PUTS & DELETES
6. NACL é stateless
7. Elastic Map Reduce permite SSH.
8. ELB e RDS não permite SSH.
9. Quando for dar acesso a um bucket para outra conta, deve ser usado o Email da conta OU o User ID.
10. Para abrir portas em uma subnet, deve ser feito diretamente no SG. O efeito é imediato.
11. Não existe Export para Glacier.
12. EBS PIOPS suportam mais de 10.000 IOPS
13. Virtualizações diponiveis em EC2:
  * Para-Virtual
  * Hardware Virtual Machine
14. Gateway-accessed volumes não é uma configuração válida.
15. Quando for necessário importar grandes volumes para AWS usa a AWS Import/Export
16. URL de S3: https://<NOME DO BUCKET>.s3-website-<REGION>.amazonaws.com
17. SWF não é valido para SNS.

---

## Quiz 2:

1. Para diminuir latency entre EC2 - crie um Placement Group.
2. S3 em todas as regiões => Read-after-write Consistency for PUTS & Eventual Consistency for PUTS and DELETES
3. Um placement group de cluster é um agrupamento lógico de instâncias dentro de uma única zona de disponibilidade.
4. É possível criar configurações de RAIDs diferentes com o múltiplos volumes de um EC2
5. Para ter mais IOPS em um EC2 - pode-se colocar múltiplos volumes com provisioned IOPS em RAID0
6. Temos root em: EC2 e Elastic MapReduce
7. Usando SAML é possível conceder SSO(Single Sign On) para usuários acessarem a console AWS.
8. 1 Subnet = 1 AZ.
9. Ao criar subnets dentro de uma VPC, por default elas podem se comunicar umas com as outras
10. É possível transferir uma Reserved Instance de uma AZ para outra. Mas não é possível entre Regions
11. Para saber os dados da instância, use os metadados em http://169.254.169.254/latest/meta-data
12. S3 oferece storage **ILIMITADO** 
13. Não é possível selecionar uma AZ para DynamoDB, mas é possível com RDS.
14. RedShift usa blocos de 1024KB ou 1MB
15. SQS não coloca prioridades em objetos. Caso queira prioridade no poll, deve ser feito a nivel de app.
16. HTTP 200 é o código de sucesso de upload para S3.

## Quiz 3:

1. Se uma subnet dentro da VPC custom é privada, ela não pode ser pública.
2. Se em algum cenário, de 3 AZ, 1 é perdida, deve ser configurado um AutoScaling com 50% de peak load.
3. A melhor maneira de manter segurança com API credentials é criar uma Role no IAM
4. Uso de Memória é uma das métricas que **NÃO** temos nos dashboards de EC2.
5. É possível configurar cool-down timers no AutoScaling para saber que horas do dia devem ser feitos.
6. Para aumentar a capacidade do MapReduce, pode-se reduzir o tamanho da entrada e ajustar para que múltiplas tasks funcionem paralelamente.
7. Uma subnet publica é aquela que tem pelo menos uma rota para um IGW
8. S3 tem uma key value store durável.
9. Permitir tráfego em uma porta tem efeito imediato.
10. Stateless: considera cada requisição como uma transição entre requisição e resposta do servidor.
11. Não podemos ter mais que 20 EC2 por **REGIÃO**. Mas pode ser solicitado um aumento.
12. Para conectar o seu DC com a AWS use uma VPC Gateway. Ele faz essa ligação VPC-VPN
13. Abaixar o custo de storage, mesmo que alguns objetos se percam: S3 RRS - Reduced Redundancy Storage.
14. Da minha VPC para acessar a internet: Na route table => 0.0.0.0/0 to IGW
15. EC2 instance in a public subnet is only publicly accessible if it has a public ip address or is behind an elastic load balancer.
16. OpsWork = Chef.

## Final Quiz

1. CPU optimezed EC2 + Placement Groups ajudam a ter mais CPU e menos latência.
2. 5 VPCs uma conta pode ter por região.
3. Workspaces não é associado com IAM.
4. S3 é o serviço de menor custo de Storage
5. OS Patching **para EC2** é responsabilidade do usuário.
6. Decomission e Destruction de Storage não é uma responsabilidade do usuário
7. Antivirus, OS Level Patches, Aplication Level Patches é tudo responsabilidade do usuário.
8. Raid 0 tem mais performance do que Raid 5.
9. A resposta necessário para um usuário logar com o AWS STS é a resposta do SAML.
10. Por default, as subnets dentro de uma VPC podem se comunicar umas com as outras.
11. Para ter encryption at rest em volumes adicionais a um EC2 é mais fácil criptografar quando criar o volume EBS
12. DynamoDB já é replicado para outras AZ automaticamente.
13. RedShift usa 1024 KB por coluna de storage.
14. CloudFront é o serviço que distribui as imagens pelo mundo.
15. Causas para uma página em branco: Não tem a porta 80/443 no SG; Não tem um Alias para o RecordSet na sua URL de domínio; O healtcheck não encontrou a página, logo, o EC2 não está em serviço.
16. Para dar acesso a Internet em uma instância numa subnet privada: faça um NAT para uma subnet publica e adicione na Main Route Table.
17. Auditoria = CloudTrail.
18. Para fazer um teste de penetração na conta AWS é necessário requisitar a Amazon, via ticket, antes.
19. AWS não dá proteção contra ataques de Socia Engineering.
20. Placement Group é restrito a uma AZ.
21. O nome das AZ são randomizados, não significa que é o mesmo Data Center.
22. Você é cobrado, mesmo se o DB ficar parado.
23. AWS Import/Export é deprecado. Agora é usado o AWS Snowball.
24. O AutoScaling termina a instância com a configuração mais antiga. Para mais detalhes, veja ![aqui](https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-instance-termination.html)
25. SQS FIFO tem máxima visibilidade de 12h.
26. Para fazer log de IPs que tentam acessar o servidor: faça logs de OS e mande para CloudWatch ou S3.
27. Storage-Gateway-Cached é o que mantem a menor latency.
28. IAM Owner é o e-mail usado para criar a conta AWS.
29. Para reduzir o tempo de polling de um SQS: ReceiveMessageWaitTimeSeconds > 0.
30. S3 RRS - minimiza o tempo para buscar um arquivo.
31. Se o seu DB já for large, considere usar um Elasticache para diminuir o tempo de query.
32. Uma VPC setada para dedicated hosts não pode ser trocada para default hosting. Tem que recriar a VPC.
33. URL de um bucket: http://<BUCKETNAME>.s3-aws-region.amazonaws.com
34. Resource Groups é uma ferramenta que cria dashboards das instâncias para monitorar simultaneamente.
35. AWS DNS não responde a requisições feitas de fora da VPC.
36. O IP Público não é controlado na instância, é aplicado um alias como network address do IP privado.
37. Para checar sobre data-consistency, o dev deve codar isso. Veja ![mais](http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/HowItWorks.DataConsistency.html)
38. VPC Peering não suporta Edge Routing = ver algo que está conectado com o outro lado e não diretamente.
39. VisibilityTimeOut é esconder uma mensagem de outro consumidor.
40. Para compartilhar uma AMI, use o S3 para trocar ela de Region.
41. DynamoDB tem um máximo de 400KB para Value and Name.


## O que estudar

1. Os whitepapers
2. EBS e Raid Disks
3. VPC
4. Acesso externo a contas AWS.
5. Billing.
6. AutoScaling