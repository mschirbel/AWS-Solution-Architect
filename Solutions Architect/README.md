# Curso para a Certificação AWS Associate

Curso Udemy de preparação.

![Pode ser visto aqui!](https://www.udemy.com/aws-certified-solutions-architect-associate/)

## Follow this order

Antes de tudo, veja a pasta de overview services, lá tem um compilado de todos os serviços AWS.

Se você quiser, pode acompanhar esse repositório. Para ordenar corretamente os assuntos:

1. IAM
  * basico_iam
  * biling_alarm
  * README
  * Security Token
  * Workspaces
  * quiz
2. Storage
  * S3
    * S3_basic
    * README
    * S3_security
    * S3_transfer
    * Gateway
  * Snowball
    * README
    * Snowball_basics
  * Static-Website
    * README
  * Version-Control
    * README
  * CDN
    * overview
    * README
  * Cross-Region Replication
    * README
  * Lifecycle Glacier
    * README
  * Summary
  * Quiz
3. EC2
  * EC2_101
    * README
    * Overview
  * Security-Groups
    * README
  * Upgrading EBS
    * README
  * Windows RAID Group
    * README
    * Supplemental Steps
  * AMI-Types
    * README
  * Load Balancing
    * README
    * load balancing theory
  * AWS CLI
    * README
  * CloudWatch
    * README
  * IAM Roles EC2
    * README
  * S3 CLI
    * README
  * Bootstrap-Scripts
    * Dar uma olhada no bashscript
    * README
  * EFS
    * README
  * Lambda
    * Polly
      * README
    * Serverless
      * README
  * Summary
4. Route53
  * DNS_101
    * README
  * Register a Domain
    * Ler o bootstrap
    * README
  * Simple Routing
    * README
  * Weighted
    * README
  * Latency
    * README
  * Failover
    * README
  * Geolocation
    * README
  * Multivalue
    * README
  * Tips Quiz
5. Databases
  * RDS
    * MultiAZ
    * README
    * connect
  * DynamoDB
    * README
  * RedShift
    * README
  * Elasticache
    * README
  * Aurora
    * README
6. VPC
  * Building VPC
    * bootstrap
    * README
  * NAT
    * README
  * NACL
    * README
  * FlowLogs
    * README
  * VPC End Point
    * README
  * NATs & Bastion
  * Connect Direct
  * VPC Peering
7. Application Services
  * SQS
  * SWF
  * SNS
  * Elastic Transcoder
  * API Gateway
  * Kinesis
  * Summary
  * Ler o template do CloudFormation
8. Fault Tolerant WebSite
  * README
  * Ler o bootstrapscript
  * CloudFormation
    * README
9. Best Practices
  * AWS_Cloud_Best_Practices
  * README
  * Tagging and Resource Groups
10. The Well Architected Framework
  * AWS_Well_Architected_Framework
  * README
  * First Pillar
  * Second Pillar
  * Third Pillar
  * Fourth Pillar
  * Fifth Pillar
11. ECS
  * README