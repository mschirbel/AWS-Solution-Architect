# Kinesis

## Streaming Data

São dados gerados continuamente de várias origens que são enviados simultaneamente em pequenos pacotes.

Por exemplo, compras na Amazon, Ações da Bolsa de Valores, Games, IOT, Uber e etc.

## O que é Kinesis

É um serviço da AWS que permite que dados sejam enviados. Kinesis carrega e analisa os dados e oferece um jeito de construir aplicações baseadas nesses dados.

## Core Services

1. Kinesis Streams
2. Kinesis Firehose
3. Kinesis Analytics

### Kinesis Streams

Data Producers = computadores, mobiles, EC2, hosts físicos

Os data producers enviam os dados para o Kinesis. Este guarda os dados em Shards.

Shards podem guardar os dados de 24h até 7 dias.

Uma vez que os dados estão nos Shards, eles enviam os dados para um conjunto de instâncias EC2, chamados de Consumers, que vão analisar, agregar, predizer em cima dos dados.

Assim que os dados foram analisados, podem ser enviados para S3, RDS, DynamoDB, Redshift, EMR.

Um Shard é a unidade da sua Kinesis Streams. Esses Shards fazem a leitura dos dados.

### Kinesis Firehose

Ainda temos nossos Data Producers, que vão enviar os dados para o Firehose.

No firehose não temos shards, ou seja, é completamente automático.

Para analisar os dados é possível usar Lambda Functions.

Não há retenção de dados, ou seja, assim que os dados entram, ou são analisados, ou são descartados.

### Kinesis Analytics

Permite usar SQL queries dos dados, desde que os dados existam em Streams ou Firehose.

Essa query vai armazenar o resultado em S3, Redshift ou Elasticsearch Cluster.

### Exam Tips

1. Diferença entre Stream e Firehose
2. Saber o que é o Analytics

## Lab

Vamos usar o arquivo cloudformation.template para nos auxiliar na construção do ambiente.

Vamos também usar a região North VIrginia, só para ter acesso aos recursos totais da AWS.

Primeiramente, acessamos o CloudFormation:

![](images/08.png)

Vamos criar uma nova Stack, especificando uma url de AWS S3.

![](images/09.png)

![](images/10.png)

Podemos especificar uma chave para fazer SSH nas instâncias Producer e Consumer. Mas isso é opcional.

![](images/11.png)

![](images/12.png)

Agora basta clicar em Create.

Por um tempo, vai ficar criando os recursos necessários:

![](images/13.png)

O tempo necessário varia conforme os recursos que forem informados. Então senta e espera.

Quando tudo estiver criado, clique em Output, e será dado uma URL:

![](images/14.png)

Ao clicar na URL:

![](images/15.png)

É um Data Producer e um Data Consumer. Agora vamos para EC2 e veremos as instâncias:

![](images/16.png)

Lembrando que, essa instância produz e consome os dados.

Se formos para Kinesis Streams:

![](images/17.png)

Veremos todos os detalhes de nossa aplicação, como quantidade de shards, tempo de retenção, métricas de shards.

Podemos monitorar nossos Shards:

![](images/18.png)

Esse dados são guardados em um DynamoDB:

![](images/19.png)

Podemos ver a quantidade de dados que foram armazenados ao clicar nos itens de uma tabela:

![](images/20.png)

Para deletar tudo o que foi feito, basta voltar em CloudFormation, selecionar nossa Stack:

![](images/21.png)