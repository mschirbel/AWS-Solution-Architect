# Simple Queue Service

É o serviço mais antigo na AWS.
No exame só precisamos entender em high-level.

## O que é?

É um serviço que da acesso a uma message queue. Uma message queue guarda mensagens para que o computador possa processá-las.

SQS é um sistema distribuído que garante a fila de mensagens. Podendo ser consumida até por outro serviço.

Por exemplo, uma instâcia pode consumir essas mensagens. E o bom disso é porque a sua aplicação pode ter uma quantidade enorme de mensagens, e você não pode se dar ao luxo de perder nenhuma delas.

O bom disso, é que se alguma instância EC2 falhar, as mensagens não falham, elas ficam armazenadas no SQS - tipo o Websphere.

SQS é sempre um pull system. Ou seja, nada vai sair do SQS por vontade dele. Outro serviços vao retirar as informações necessárias.

## Características

Mensagens podem ter até 256kB de texto em qualquer formato, seja JSON, XML, YML e outros.
A queue funciona como um buffer, que guarda as informaçoes antes que o consumidor termine na frente do produtor.

Isso pode ser integrado com EC2 AutoScalling, fazendo uma aplicação elastica

## Tipos de Queues

1. Standard Queues
	É o tipo default. Permite um número quase ilimitado de mensagens por segundo. Garante que a maioria das mensagens serão entregas em ordem.
2. FIFO Queues
	Garante que a ordem permanecerá a mesma - isso sempre acontece, diferente do Standard. E não usa duplicatas. Limitadas a 300 transações/sec.

## Key Facts
1. SQS é pull based
2. 256kb de mensages
3. Mensagens podem ser guardadas de 1 minuto até 14 dias.
4. Período de retenção default é 4dias.
5. SQS garante que as mensagens serão processadas, pelo menos, uma vez

## Visibility Timeout

É o tempo que a mensagem fica invisível na SQS depois que um reader pegou a mensagem.
Se alguem processou a mensagem antes do tempo de visibilidade, ela vai sumir. Caso contrário, ela fica visível novamente para que outro possa processá-la.

O tempo defaul é de 30 segundos, podendo ir até 12h.

## Long Polling

É um jeito de buscar mensagens. Basicamente ele evita mandar mensagens até que a mensagem chegue na queue.
Normalmente o SQS fica mandando mensagens "Não tem mensagens" a cada um período curto de tempo. O long polling ao invés de fazer isso toda hora, só avisa quando a mensagem realmente chegar. Isso acaba salvando dinheiro.
