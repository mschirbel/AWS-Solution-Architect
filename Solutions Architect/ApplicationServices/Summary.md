# Summary

## SQS

É um serviço que guarda mensagens até que algum computador a processe.

Funciona como um WebSphere MQ. 

Lembrando que SQS só funciona com Pull, ou seja, só conseguem pegar mensagens dele. SQS não envia nada.

SQS tem um tempo de visibilidade para que as mensagens não se percam depois que são pegas. Se a mensagens não foi processada dentro daquele tempo, a mensagem fica visível novamente. Ou seja, a mensagem pode ser pega por diversas fontes de processamento. Esse tempo pode ir até 12h.

O tempo de fila é de 1 minuto até 14 dias. O default é 4 dias.

SQS garante que as mensagens serão processadas pelo menos uma vez.

SQS tem o long polling que só responde se houver work hábil para ser feito.

As filas podem ser Standard ou FIFO. FIFO as mensagens tem ordem e Standard não é garantido.

## SWF vs SQS

* SWF pode ter tasks com até 1 ano de workflow. SQS tem um período de retenção de 14 dias.
* SWF é orientado a tasks, SQS é a mensagens
* SWF garante que as tasks não são duplicadas.
* SWF guarda eventos na aplicação

## SWF

Workflow Starters => Aplicação que iniciam um workflow.

Deciders => Controlam as atividades de um workflow, decice para onde vai depois que uma task terminou.

Activy Workers => Fazem as tasks

## SNS

Temos subscribers como HTTP, HTTPS, E-mail, JSON, SQS, Application, Lambda

São todas formas de enviar as mensagens.

## SNS vs SQS

Ambos são orientados a mensagens. Mas a diferença é que SNS é Push based, ou seja, temos que enviar dados para o serviço, enquanto no SQS só fazemos pulls os dados.

## Elastic Transcoder

Converte formatos de mídia para outros. Podendo ser salvos em S3 ou até ativar Lambda functions.

## Kinesis

### Kinesis Streams

Guardam os dados em Shards, que ativam consumidores(EC2 Instances) que vão analisar os dados

### Kinesis Firehose

Ativam Lambda Functions que vão analisar os dados na hora, e descartar os dados inúteis. Os dados são enviados para um S3 depois

### Kinesis Analytics

SQL queris para Streams ou Firehose e salva o resultado em S3 ou Redshift.

## Quiz

1. SWF - Simple WorkFlow
2. SES - Simple Email Service
3. Quando vc cria um topic no SNS, automaticamente é criado um Amazon Resource Name
4. SNS = Push; SQS = Poll
5. SQS allows to decouple infrastructure using message based queries
6. Domain in SWF = coleção de workflows relacionados
7. O modo default do SQS é o Standard, mas existe o FIFO também.
8. SQS garante que a mensagem vai ser entregue no mínimo uma vez.
9. SWF assegura que uma task só é entregue uma vez e nunca é duplicada.
10. SWF pode ser escrito em diversas linguagens.