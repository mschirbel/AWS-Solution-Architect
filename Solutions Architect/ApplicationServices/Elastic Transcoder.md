# Elastic Transcoder

É um serviço que permite converter arquivos de mídia para diferentes formatos.

Esses novos formatos podem ser tocados em celulares, tables, PC's.

Já vem com alguns pre-sets, assim não é necessário algum hard work.

## Pricing

É baseado nos minutos que foram convertidos e em qual resolução.

## Como usar

É possível usar Elastic Transcoder com Lambda Functions, convertendo arquivos de mídia e salvando em S3 buckets.