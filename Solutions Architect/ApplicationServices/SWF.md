# Simple Workflow Service

É um serviço que coordena outros sistemas distribuídos.

Vamos supor um cenário:

1. Você tem uma aplicação de e-commerce.
2. Você tem várias máquinas que fazem a verificação da compra, como validar o preço, validar o cartão, a senha e etc.
3. Cada uma dessas validações, está em uma máquina. Mas essas validações não podem ser feitas ao mesmo tempo.

O SWF permite que você organize, como uma lista To-Do as suas máquinas, para que façam o trabalho na ordem correta.

## Workers

São programas que interagem com SWF para pegar tasks, processar tasks e retornam resultados(assim como trabalhadores mesmo).

## Deciders

São programas que controlam a coordenação das tasks, como a sua ordem, concorrência e agendamento.

## Como funciona?

Workers podem rodar em EC2. SWF controla qual worker vai receber qual task e monitora a função dela.
SWF assegura que a task foi dada somente para 1 worker e não foi duplicada.

Workers e Deciders não precisam guardar status da execução, quem guarda é o SWF.

É altamente escalável e rodam independentemente.

## A diferença entre SQS e SWF

1. Com SWF uma task só é dada pra 1 worker e nunca é duplicada. Com SQS temos o tempo de visibilidade, o que faz com que a mensagem possa ser direcionada para vários lugares.
2. SQS é baseado em mensagens. SWF é baseado em tasks.
3. SWF guarda os registros das tasks. SQS não guarda registros das mensagens. É preciso implementar a nível de aplicação.

Para saber se usaríamos SWF ou SQS, pense se a sua aplicação usaria trabalho humano, se sim, provavelmente SWF. Se a sua aplicação precisa de troca de mensagens entre computadores, use SQS.

## Domain

São os tipos de atividades e o workflow de execuções. Domains isolam uma lista de execução, dentro da mesma conta AWS.
É como se fosse um container para uma série de execuções. Ou seja, podemos ter vários Domains dentro de uma conta AWS.

É escrito em JSON.