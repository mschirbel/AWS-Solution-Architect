# Simple Notification Service

É um serviço que envia notificações da cloud. Envia para inscritos ou para outras aplicações.

Altamente escalável, custo-benefício e muito flexível.

Podemos usar isso, com Auto Scalling, recebendo um e-mail ou um SMS.

Podemos receber notificações:
* Apple
* Google
* Fire OS
* Windows
* Android
* Baidu Cloud Push

Alem de enviar notificações via SMS, e-mail ou diretamente para mobiles, podemos enviar para o SQS ou qualquer outro HTTP endpoint.

Podem atividar Lambda functions também, passando parâmentros com o payload que foi enviado na mensagem.

## Como funciona?

Com topics. Um topic é como um ponto de acesso, recebendo as informações e distribuindo para os inscritos.
Um topic pode ter diversos endpoints, e cada mensagem vai ser formatada para cada inscrito.

Para evitar que as mensagens sejam perdidas, SNS guarda elas redundantemente em Multi-AZ.

Podemos entrar em SNS aqui:

![](images/01.png)

Vamos criar um topic:

![](images/02.png)

Veja que após o tópico ser criado, temos formas de inserir subscriptions:

![](images/03.png)

Temos vários formatos para envio de notificações. Vamos usar o e-mail. Após criado, vamos receber um email com confirmação:

![](images/04.png)

Após confirmar o e-mail:

![](images/05.png)

Agora está dando um recurso AWS como ID. Agora se quisermos publicar uma mensagem para os inscritos:

Basta clicar em Publish to Topic:

![](images/06.png)

Sendo TTL o tempo que a mensagem vai ficar na queue. Se formos ao nosso e-mail:

![](images/07.png)

Não precisa ser e-mail, pode ser uma função lambda, que vai ativar outros recursos da AWS. O caso mais comum é para Auto Scalling de EC2.

## Benefícios

1. Não tem polling. Ou seja, uma vez publicado, vai para todos os inscritos
2. É bem simples de usar e é bem flexível
3. Muito barato

## SNS vs SQS

Ambos são serviços de mensagens.
SNS = Push - vc manda para os inscritos
SQS = Pull - vc baixa a mensagem da fila

## Pricing

$0.50 per 1kk SNS Requests para as primeiras.

Após isso, 0.06/100.000 over HTTP

$0.75/100 SMS

$2.00/100.000 over e-mail