# API Gateway

É um serviço que ajuda desenvolvedoreas a publicar, manter, monitorar e melhorar a segurança de API em qualquer escala.

Esse serviço pode acessar EC2, Lambdas e outros serviços.

O usuário vai acessar o API Gateway, que por sua vez, ativa uma Lambda Function ou algum EC2.

## API Caching

Aumenta a velocidade de resposta do Gateway, diminuindo a latência.

Ele faz cache de respostas por um determinado TTL. Quando alguma request chega, ele primeiramente olha para o endpoint dos caches, ao invêsde fazer a solicitação.

## What can it do?

1. Custa pouco
2. Scalling auto
3. Throttle Requests - proteção contra ataques
4. Pode fazer logs no CloudWatch

## Same Origin Policy

É uma policy que permite que um script contido dentro de uma primeira web page acesse dados de uma segunda web page se e somente se ambas as páginas tem a mesma origem = domínio

## CORS

CORS = Cross Origin Resource Sharing

É um jeito de permitir que recursos(por exemplo, fontes) de uma web page serem solicitados de outro domínio.

Erro: Origin policy cannot be read at the remote resource.
Troubleshooting: enable CORS on API Gateway.

## Exam Tips

1. Caching
2. Características
3. CORS
4. Custo e scalling
5. Logs na CloudWatch
6. Se usar .js ou .ajax que usam outros domínios, use CORS