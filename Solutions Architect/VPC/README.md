# Virtual Private Cloud Overview

VPC é como se fosse um Datacenter virtual na Cloud.

Toda region no mundo tem uma default VPC, que é obtida automaticamente quando criada a conta/troca de region

## Definição

Providencia uma seção isolada da AWS onde é possível lançar os recursos.
Tem controle sobre a network, bem como o range de IP, criação de subnets e routing tables e network gateway.

É possível criar uma subnet que está na internet, mas que protege seus servidores de aplicação ou de banco de dados da rede.
Pode criar múltiplas camadas de segurançã, security groups, acessos.

Também é possível criar uma conexão VPN entre o datancenter que já existe para a sua empresa com a AWS.

Para ter uma noção:

![](images/01.png)

*Cada subnet é igual a uma AZ,não é possível ter mais de uma subnet em uma AZ*

Para os prefixos de IPs, temos o range definidos por IPs internos:

![](images/02.png)

Indo do maior alcançe de endereços para o menor. Pois o primeiro é um /8, o segundo é um /12 e ultimo é um /16.

Para saber mais informações sobre range de IPs, pode-se visitar o seguinte ![site](https://cidr.xyz/).

É possível ter mais do que 5 VPCs em uma região, mas deve ser mandado um email para o suporte da AWS.

## Clean Up

Para limpar tudo que será usado nessa seção, faça nessa ordem:

1. EC2 Instances
2. NAT Gateways
3. Endpoint
4. Internet Gateway(primeiro Detach, depois Delete)
5. VPC(não precisa deletar a VPN Connection)

## Summary

### NAT Instances

1. Quando criar uma NAT Instance, lembrar de clicar em Disable Source/Destination Check
2. NAT Instance sempre devem estar em uma public subnet
3. Tem que ter uma route para fora da private subnet da NAT Instance
4. Pode ser usado AutoScaling Groups, multiplas subnets e até mesmo failover
5. Sempre por tras de um Security Group

### NAT Gateways

1. Scaling até 10Gbps
2. Não precisa de patch no SO, diferente de uma instância.
3. Não precisa de Security Groups
4. Sempre tem um IP público.
5. Lembrar de fazer um update na route table
6. Não precisa de Source/Destination Check

### NACL

1. Ao criar uma VPC, sempre vem com um NACL default que permite todo e qualquer tráfego para dentro e fora.
2. Ao criar um NACL ele, por default, bloqueia todo e qualquer tráfego.
3. Cada subnet precisa estar associada com um NACL. Se você não associar uma, vai ser ligada com o que vem por default ao criar a VPC.
4. Pode associar um NACL com múltiplas subnets, mas uma subnet só pode ser associada com um NACL.
5. Tem uma lista de regras em ordem numérica, e essa ordem implica na execução dessas regras, começando da menor.
6. NACL tem inbound e outbound rules separadas, por isso, são stateless.
7. Podemos bloquear IP address específicos usando NACL. Diferentemente de SG.

### ALB

1. Precisamos de no mínimo 2 subnets para termos um Application Load Balancer

### Flow Logs

1. Não pode receber tags.
2. Só pode fazer logs das VPCs que estão na sua conta
3. Não é possível associar IAM rules depois que a Flow Log foi criada.
4. Nem todo o tráfego de IP é monitorado.

### EndPoint

1. Podem ser gateways ou interfaces
2. Conectam serviços da sua subnet privada com serviços da AWS.