# NAT Instances & NAT Gateways

Na última aula, iniciamos uma instância com MySQL o colocando na subnet privada. Entretanto, percebemos que essa subnet não dava acesso com a Internet, logo, não poderíamos acessar repositórios online de dentro da nossa instância. Essa aula vai ensinar como contornar isso.

## NAT Instances

São instâncias EC2. Podem ser criadas a partir de imagens da comunidade. Para isso, crie uma instância usando a imagem da comunidade:

![](images/01.png)

Vamos inserir dentro da nossa VPC e de nossa subnet pública:

![](images/02.png)

Coloque dentro do Security Group com SSH, HTTP e HTTPS:

![](images/03.png)

*Se seu SG não tem acesso a HTTPS, coloque. O MySQL faz updates usando HTTPS*

Não precisaremos logar nessa NAT instância. Mas precisaremos trocar o Check dessa instância. O check é basicamente informando que aquela instância é a origem e destino de todo o tráfego que recebe/envia. Isso pode ser lido com mais detalhes ![aqui](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_NAT_Instance.html#EIP_Disable_SrcDestCheck).

Ou seja, a nossa NAT instância pode não ser a origem ou destino do seu tráfego. Para fazer isso, selecione a NAT Instance, clique em Actions e depois em Disable Source/Destination Checks.

![](images/04.png)

Agora precisamos criar nossa route para o tráfego.
Para isso, entre em Route Table dentro da sua VPC e vamos criar uma route para fora da sua default VPC:

![](images/05.png)

Em destination, coloque o IP local. E em Target, coloque a sua NAT Instance. Isso criou uma regra de saída da NAT Instance para o mundo. Agora, repita o processo de logar na instância pública e depois na privada.

```sh
ssh ec2-user@[ip publico] -i Chave.pem
ssh ec2-user@[ip privado] -i Chave.pem
sudo su -
yum update -y
```

Isso foi possível, porque o tráfego foi configurado para sair pela NAT Instance.
Mas, o que acontece se a NAT Instance morrer?

![](images/06.png)

![](images/07.png)

Perceba que, a NAT Instance é responsável por toda a conexão com a Internet. E isso é um problema, porque se ela crashar, ou morrer, todo o tráfego será perdido. Claro, que pode ser colocado um auto scaling, colocar em multi AZ. Entretanto, fica muito complicado para administrar.

Se você tentar rodar um

```sh
yum install mysql -y
```

Não será possível. Vamos estabelecer outra conexão com a Internet. Para isso, vamos usar um NAT Gateway.
Na AWS temos os NAT Gateways que operam em IPv4 e temos Egress Only Internet Gateway que opera em IPv6.

Vamos criar um NAT Gateway, bem simples.

![](images/08.png)

Seleciona a subnet pública, para que tenha acesso com a internet e clique para criar um Elastic IP.
Demora cerca de 10-15min para que o NAT Gateway seja criado.

Agora preciso ir para route tables e editar a route table default da minha VPC:

Antes de adicionar uma nova route, perceba que existe um Black Hole, isso se da porque a instância de NAT foi terminada:

![](images/09.png)

Delete o Black Hole e crie uma nova rota local para o NAT Gateway:

![](images/10.png)

E agora vamos para nossa instância EC2 privada. Vamos instalar o Apache.

```sh
yum install httpd -y
```

Perceba que foi possível a comunicação pela Internet, sem o uso de uma instância secundária.

Você pode ver a comparação oficial entre NAT Gateways e NAT Instances ![aqui](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-comparison.html)

Só para mais importante:
- NAT Gateway é muito mais confiável. A instância precisaria de scripts para o failover
- NAT Gateway suporta banda maior
- NAT Gateway pode ter um IP elástico, a instância precisaria de um IP fixo, para não ser configurável toda hora.

## Exam Tips
- Nat gateways são preferíveis por empresas
- Escalam até 10Gbps
- Não precisa de patch
- Não precisa de Security Groups
- Não precisa de um IP público