# VPC Peering

É uma conexão entre duas VPCs que **permite o tráfego usando IPs privados**

É possível criar peerings em VPCs dentro da mesma conta, ou até mesmo em contas diferentes.

**Mas sempre na mesma região**

Não é um gateway, nem uma conexão VPN.

Também não depende de hardware específico. Não há ponto de falha ou algum gargalo.

## Condições

Imagine que temos 3 VPC:

1. 10.0.0.0/16
2. 192.168.0.0/24
3. 172.16.0.0/16

A VPC 1 pode comunicar com a 2 e a 2 com a 3 usando somente IPs privados, sem a necessidade de passar pela internet.

Mas, e se a VPC 2 trocasse o address range para 10.0.0.0/24. O peer ainda iria funcionar?

Não. Pois um peer não pode ser feito com VPCs com o mesmo address range. Pois estão sobreponto um bloco de IPs.

## Transitive Peering

Não é suportado.

Isso significa que, instâncias na VPC 1 não podem se comunicar com instâncias na VPC 3. Pois não estão diretamente conectadas.

## VPC Peering Limitations

1. Não pode fazer peer com o mesmo address range
2. Não pode fazer peer com VPCs de regions diferentes
3. Não pode fazer transitive peering