# NATs vs Bastions

Um NAT pode ser feito como instância ou como um Gateway.

Um bastion é como se fosse um jumper server. Uma instância usada para acessar as outras que estão internas na rede.

Um bastion e um NAT Instance tem acesso a Internet.
São usados geralmente com administração.

Mas como as NAT Instances estão caindo em desuso, para o uso de NAT Gateways, os Bastion Servers tem que ser altamente escaláveis e disponíveis.

E podemos ter um Bastion Server dentro de mais de uma subnet e lembrando que, uma subnet é igual a uma AZ diferente.

E com Bastion ainda podemos ter failover, que é cuidado pela Amazon.

As NAT Instances também server para dar saída com a Internet.