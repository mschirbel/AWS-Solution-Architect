# Custom Load Balancers & ELBs

Primeiramente vamos criar um Load Balancer:

![](images/01.png)

Existem 3 tipos de Load Balancer, sendo que, o Classic Load Balancer é deprecado, deve-se parar de usá-lo.
A maioria das vezes vamos usar o Application Balancer, a não ser que demanda-se uma alta performance com IPs estáticos.

Ao criar um ALB, note que é necessário preencher qual VPC esse LB está associado.

E ao associar com uma VPC, é necessário colocá-lo em alguma subnet, para que fique disponível. E é necessário especificar 2 subnets de duas AZ diferentes para manter a estabilidade do LB.

Mas ao indicar uma subnet que não tenha acesso a Internet, o seguinte aviso aparecerá:

![](images/02.png)

Esse aviso nos diz que não há Internet Gateway entre a subnet e o LB. Logo, não é possível colocar nessa.

E ao tentar colocar somente em uma subnet o seguinte aviso aparecerá:

![](images/03.png)