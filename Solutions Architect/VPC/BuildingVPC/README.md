# Building a VPC

Primeiramente, entre no serviço da VPC:

![](images/01.png)

Poderíamos iniciar o Wizard da VPC, mas com isso o aprendizado ficaria reduzido:

![](images/02.png)

Para ver as subnets de nossa region, clicamos na aba de Subnets no menu à esquerda:

![](images/03.png)

Aqui podemos ver os IPs públicos de nossa default VPC, assim quando é criada a conta.

Também podemos ver as Route Tables:

![](images/04.png)

Vamos agora criar uma nova VPC:

![](images/05.png)

Em Tenancy, temos a opção de deixar dedicada, ou seja, algum host específico, somente para você. Caso seja default, o mesmo hardware pode ser dividido com outros AWS customers.

Assim que criada a VPC, outros dados também serão adicionados:

- VPC:

![](images/06.png)

- Route Table:

![](images/07.png)

- Security Group:

![](images/08.png)

Em security group, temos uma nova entrada, quando criamos uma nova VPC

- Network ALC

![](images/09.png)

Basicamente, até agora temos uma requisição, passando por um Router -> Route Table -> Network ACL -> Security Group.
Tudo isso, dentro de nossa VPC.

Agora vamos criar uma subnet:

![](images/10.png)

E apenas por curiosidade, a AWS randomiza as AZ dentro de uma region. A sa-east-1a pode ser algum DC diferente em alguma outra conta AWS.

Veja que temos para os IPv4 10.0.1.0/24 o que nos dá 256 IPs diferentes, entretanto, só poderemos usar 251.

Assim que terminada a criação da subnet:

![](images/11.png)

Veja que a subnet está dentro da VPC que criamos e só temos 251 IPs disponíveis, mas /24 sempre nos dá 256.

Mas se entrarmos ![aqui](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html) veremos que os primeiros 4 e o último IPs que são criados, não estão disponíveis para o uso. No link tem a explicação de cada um dos IPs.

Agora vamos criar nossa segunda subnet. Lembrando que ela deve estar dentro da nossa VPC.

![](images/12.png)

Agora precisamos colocar um Internet Gateway para termos conectividade com a Internet. Para isso, clique em Internet Gateway:

![](images/13.png)

Clique em Criar

![](images/14.png)

Após criar, por default ele fica em detached, para isso podemos adicionar a uma VPC:

![](images/15.png)

Percabe que não é possível haver múltiplos Internet Gateways dentro de uma VPC.

Agora precisamos ir para nossas Route Tables.
Vemos que temos duas, criadas por default com a nossa VPC, assim como mostrado acima.

Essas duas routes permitem nossas subnets conversem uma com a outra.

![](images/16.png)

Toda vez que criamos uma nova subnet, ela estará ligada, por default, com a main route table. Não queremos que nossa main route table tenha acesso a internet, pois isso pode ser perigoso, toda a vez que criarmos uma subnet, estará acessível pela internet.

Assim, devemos criar uma nova route table:

![](images/17.png)

Agora queremos habilitar o acesso a internet.Para isso clique em Routes, depois em Edit:

![](images/18.png)

Isso permite que estiver tudo que estiver nessa route, ou seja, nossa subnet, seja acessível pela internet.

Para consistência, queremos adicionar uma route out para IPv6:

![](images/19.png)

Isso dará acesso a Internet para IPv4 e IPv6.

Agora vamos associar uma subnet com nossa Route Table, apra isso, clique em Subnet Associations, dentro de Route Table:

Essa vai ser nossa subnet publica:

![](images/20.png)

Você pode ver que foi associada com nossa route table:

![](images/21.png)

Agora vamos testar isso. Vamos criar duas instâncias EC2. Uma dentro da subnet publica e outra na privada.
Mas antes, volte para Subnets e perceba que para as subnets que foram criadas para a sua VPC, não existe um IP Publico automatico:

![](images/22.png)

Para nossa subnet publica, nós queremos um IP Publico, caso contrário, nao acessaríamos a instância. Para isso, clique na subnet desejada:

![](images/23.png)

Clique em Modify auto-assign IP settings e habilite o IPv4.

E com isso teremos um IPv4 publico.

Agora lance duas instâncias EC2 e troque as subnets e a VPC.
Para os security group, note que só existe aquele que é default da VPC que vc criou, não aquele da sua conta.

Podemos criar um novo, com acesso http para IPv4 e IPv6 e SSH.

Perceba que, ao criar a instância que ficará na parte privada:

![](images/24.png)

Não temos como usar o auto-assign Public IP porque não habilitamos na subnet.

Para testar, vamos dar SSH para a instância com IP público:

```sh
ssh ec2-user@[ip] -i Chave.pem
```

Se o SSH foi possível, significa que existe acesso pela internet.

Agora podemos fazer um SSH para a instância com IP Privado:

```sh
ssh [ip privado] -y
```

Mas isso ainda não é possível, pois as subnets não se comunicam uma com a outra.

Agora, vamos trocar o nome da instância que está na subnet privada para MySQLServer

![](images/25.png)

E vamos criar um Security Group para essa instância:

![](images/26.png)

Veja que permitimos SSH para conectar na instância, HTTP e HTTPS, a regra do MySQL e permitimos também ICMP para poder pingar a instância.

Mas, essa instância, só pode falar com a instância que é pública. Para isso:

![](images/27.png)

Agora precisamos mover a instância para dentro do SG.

![](images/28.png)

Agora vamos acessar a instância pública e realizar os testes com a instância privada.

```sh
ssh ec2-user@52.67.62.127 -i Chave.pem
sudo su - 
```

Agora vamos pingar a nossa instância, para isso, use o IP privado:

```sh
ping 10.0.2.207
```

Isso nos mostra que elas estão se comunicando

Agora vamos fazer um ssh. Para isso, precisamos de uma Chave. Podemos usar o mesmo conteúdo da chave que usamos para conectar na instância. Isso é uma ideia horrívei, pois é uma falha de segurança horrível. Mas serve para mostrar a ideia da aula.

Agora, basta conectar:

```sh
ssh ec2-user@10.0.2.207 -i Chave.pem
```

Agora estamos na instância privada.

Perceba que, se vc tentar um yum update, não vai funcionar, pois essa instância não tem saída com a internet.
