# VPC Flow Logs

É uma feature que capta as informações sobre os IPs que entram e saem da sua VPC.

FlowLog é armazenado na CloudWatch.

Podem ser criados em 3 níveis:

1. VPC
2. Subnet
3. Network Interface Level

Para criar um, entre na VPC e selecione sua VPC. Após isso, clique em Create Flow Log, dentro de Actions:

![](images/01.png)

Em Filter, podemos selecionar que tipos de tráfegos queremos logar. Podem ser os aceitos, rejeitados e qualquer um.

Podemos enviar essas logs para o CloudWatch ou para algum S3 bucket.

Em IAM Role, podemos criar uma role só para essas logs, basta clicar em Set Up Permissions, isso nos levará para a página do IAM:

![](images/02.png)

Para Destination Log Group, temos que voltar para Cloud Watch e criar a partir de lá:

![](images/03.png)

Clique em Logs:

![](images/04.png)

Clique em Actions e depois em Create Log Group:

![](images/05.png)

Crie com o nome desejado:

![](images/06.png)

Agora basta voltar para a criação do Flow Log:

![](images/07.png)

Também é possível, definir uma stream para alguma Lambda Function tratar alguma ocorrência na log, como algum ataque indevido, alguma falha no sistema.

## Exam Tips

1. Flow Logs não podem ter configurações alteradas
2. Nem todo o tráfego de IP são monitorados, como metadados da instância, DHCP, Windows Licence Ativation.