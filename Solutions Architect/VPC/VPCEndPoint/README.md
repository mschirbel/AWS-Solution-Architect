# VPC End Point

É um ferramente usada para conectar a sua VPC aos serviços da AWS sem exigir um gateway de internet.

Precisamos ir para IAM:

![](images/01.png)

Vamos criar uma Role para EC2. Permitindo que as instâncias usem serviços da AWS.

![](images/02.png)

Vamos dar permissão de admin para o S3:

![](images/03.png)

Dê um nome e crie.

![](images/04.png)

Vamos adicionar essa Role para nossa instância privada:

![](images/05.png)

Basta clicar em Attach/Replace IAM Role:

E inserir a Role que foi criada:

![](images/06.png)

Agora vamos para nossa VPC e clique em NACL:

E com o NACL criado por default com a sua VPC, insira a subnet pública nele:

![](images/07.png)

*Isso foi feito só para ficar mais fácil a explicação*

Agora acesse a instância pública e pule para a privada:

```
ssh ec2-user@[ip publico] -i chave.pem
ssh ec2-user@[ip privado] -i chave.pem
sudo su -
aws s3 ls
```

Se aparecer o conteúdo dos seus buckets, é porque está funcionando.

Apenas para mostrar, veja que, se apagarmos a route que inserimos na Route Table default quando criamos nossa VPC:

![](images/08.png)

Não há mais conexão com os serviços da amazon de dentro da instância privada:

```
aws s3 ls
```

Agora vamos criar um EndPoint para nossa VPC. Antes, devemos escolher qual o serviço que será conectado por esse endpoint:

![](images/09.png)

Agora escolhemos nossa VPC e selecionamos a Main Route Table:

![](images/10.png)

E vamos usar Full Access:

![](images/11.png)

Isso criará nosso EndPoint. Se voltarmos para nossa Main Route Table, veremos que o nosso EndPoint está lá:

![](images/12.png)

E agora se rodarmos o comando de novo:

```
aws s3 ls
```
Perceba que funcionou.
Esse comando está funcionando somente sobre a private network, sem ir pela pública. O que é muito mais seguro.