# Network Access Control List vs Security Groups

Podemos associar uma subnet com 1 NACL. Nunca podemos ter múltiplos NACL com uma subnet.
Mas é possível ter múltiplas subnets dentro de um NACL

Quando criamos nossa VPC, automaticamente foi criado um NACL:

![](images/01.png)

Vamos criar um novo NACL:

![](images/02.png)

Associando-o com a nossa VPC.

Quando criamos um private NACL, todo o tráfego é negado:

![](images/03.png)

Agora volte para a sua instâcia pública, e instale o Apache:

```sh
sudo su -
yum install httpd -y
service httpd start
chkconfig httpd on
cd /var/www/html
```

Edite um arquivo html e chame o IP público no Browser, deve acontecer isso:

![](images/04.png)

Agora voltamos para nosso NACL, e vamos editar algumas Inbound Rules:
A AWS recomenda que sempre usemos uma sequência para as regras, começando em 100 para IPv4 e 101 para IPv6.

![](images/05.png)

100 para HTTP, 200 para HTTPS, 300 para SSH.

Com security groups, ao editar uma regra, ela já vale para inbound e para outbound. Com NACL é necessário editar as duas vias.

![](images/06.png)

Perceba que a regra 300 é para TCP com uma ephemeral port. É um tipo de porta usada para TCP ou UDP ou STCP. É um porta que pega uma request em uma porta específica e devolve em outra totalmente diferente. É possível ler mais sobre esse tipo de portas ![aqui](https://docs.aws.amazon.com/pt_br/vpc/latest/userguide/vpc-network-acls.html#nacl-ephemeral-ports)

Mas, nosso NACL ainda não está associado com nenhuma subnet.
Para isso:

![](images/07.png)

Associe seu NACL somente com a subnet pública. Uma subnet só pode ser associada com uma NACL:

![](images/08.png)

*A VPC default da conta pode ser associada com mais de uma*

Agora vamos para nossa página na web, se após um refresh, a página continuar online, é porque funcionou.

Só para mais um teste, vamos bloquear o nosso acesso á página:

![](images/09.png)

E ao dar refresh na página, nada aconteceu. Mas por quê?

Porque as regras são respeitadas por ordem numérica, e a regra 100 permite todo e qualquer acesso a nossa página, enquanto a regra 101 nos bloqueia.

Agora, se colocarmos a regra de bloqueio como 99:

![](images/10.png)

E tentarmos acessar a página, não conseguiremos.

## Exam Tips

1. Quando criado uma VPC ela vem com uma NACL que permite todo tráfego inbound e outbound.
2. Quando criado uma custom NACL ela nega todo tráfego inbound e outbound
3. Cada subnet tem que estar associado com uma NACL
4. Uma NACL pode ter várias subnets, mas uma subnet só pode ter uma NACL
5. NACL avalia as regras pelo número dela. Quando menor, mais preferências.
6. NACL são stateless. Ou seja, precisa editar as inbounds E as outbunds
7. É possível bloquear IPs específicos usando NACL e não com SG.