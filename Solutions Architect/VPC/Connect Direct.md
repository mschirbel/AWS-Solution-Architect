# Direct Connect

Permite estabelecer uma conexão de rede entre a AWS e o seu DC.

Essa conexão é privada.

Pode ser feita com um DC, ou um escritório, ou qualquer outro ambiente.

Isso reduz a perda de banda, deixa a sua rede mais estável, e aumenta a banda.

## Benefits

1. Reduz o custo quando usa muito tráfego
2. Aumenta a confiabilidade
3. Aumenta a banda

## Qual a diferença entre Direct Connect e uma VPN

* Uma VPN pode ser configurada em minutos e é uma boa solução imediada. Não necessita de muita banda e aguenta variação na conectividade de Internet.

* Direct Connect não involve a internet. Usa uma rede privada para a sua intranet e a AWS VPC.

## Techs

* Pode ser de 10Gbps ou 1Gbps pela AWS
* Abaixo de 1Gbps pode ser comprado pelos parceiros da AWS
* Usa Ethernet VLAN trunking(802.1Q)