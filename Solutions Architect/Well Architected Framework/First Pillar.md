# First Pillar - Security

## Design Principles

1. Aplicar segurança em todas as camadas da infraestrutura - seja no código, seja nas subnets, portas, ELB, instâncias.
2. Habilite traceability - saber quem acessou o sistema e o que foi feito.
3. Automatizar respostas para eventos de segurança, por exemplo, DDoS, Mas in the Middle.
4. Foque na segurança do sistema.
5. Automatizar as melhores práticas de segurança. Usar uma imagem criptografada, por exemplo. Usar uma AMI já protegida.

## Shared Responsibility Model

1. User:
* Dados
* IAM
* OS
* Network
* Firewall

2. AWS:
* Compute
* Storage
* Database
* Networking
* Regions, AZ, Edge Locations

## Definition

*Isso deve ser lembrado*

1. Data Protection
2. Privilege Management
3. Infrastructure Protection
4. Detective Controls

### Data Protection

Deve-se organizar e classificar seus dados quanto à publicidade. Seja entre os funcionários, grupos, público, somente diretores e etc.

Deve-se também implementar um sistema de acesso baseado em privilégios.

E deve-se também fazer a criptografia em TODO o trânsito.

#### Best Practices

1. Sempre manter controle total dos dados
2. AWS faz fácil criptografar seus dados
3. Logs detalhadas das mudanças
4. Sistemas resilientes e duráveis.
5. Versionamento de arquivos.
6. AWS nunca muda os dados de regions, ou seja, dados governamentais ficam no país onde foram criados.

#### Questions

* Como você está criptografando seus dados, os que estão parados?
* Como você está criptografando os dados em movimento?

### Privilege Management

Assegura que somente pessoas autorizadas e autenticadas possam acessar os recursos.

#### Best Practices

* ACLs
* Roles
* Password Management
* Policies

#### Questions

* Como está sendo protegido o acesso aos dados, usando MFA?
* Como está sendo definido as roles de controle humano as APIs da AWS? Está sendo usado grupos?
* Como está sendo limitado o acesso de terceiros?
* Qual o cuidado com keys e credenciais?

### Infrastructure Protection

Na AWS, o DataCenter é protegido pela Amazon, ou seja, a proteção da infraestrutura é somente com a VPC.

#### Best Practices

* Subnets
* NACL
* Security Groups

#### Questions

* Como está a força da network na fronteira?
* Qual a proteção dos serviços da AWS?
* Como estão protegidos os OS dos sistemas operacionais?

### Detective Controls

Pode usar detective controls para identificar brechas.

#### Best Practices

* CloudTrail
* CloudWatch
* Config
* S3
* Glacier

#### Questions

* Como as logs são analisadas?
* Lembre-se que CloudTrail é baseado na region, como você tem coletado as logs?

## AWS Key Services

1. ELB, EBS, S3, RDS - Data Protection
2. IAM, MFA - Privilege Management
3. VPC - Infrastructure Protection
4. CloudTrail, Config, CloudWatch - Detective Controls

## Exam Tips

1. Lembrar das quatro áreas e as questões de cada uma.