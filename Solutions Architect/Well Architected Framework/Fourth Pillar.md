# Fourth Pillar - Cost Optimization

Devemos usar para reduzir o custo para o mínimo e usar para outros fins

O objetivo é atingir o menor custo possível e ainda assim atingir os objetivos da arquitetura.

Um exemplo disso, é usar Serverless na AWS.

## Desing Principles

1. Transparently expenses - o custo é bem visível para o usuário.
2. Usar managed services - como RDS, por exemplo. Isso reduz o custo de transação
3. Trade capital expense for operating expense - Pagar somente aquilo que é usado
4. Economies of scales - saber exatamente o custo de um scaling. Não fazer um over ou underscaling.
5. Parar de gastar com DC.

## Definition

Composto de 4 áreas:

1. Matched supply and demand
2. Cost-effective resource
3. Expenditure awareness
4. Optimizing over time

### Matched Supply and Demand

Alinhar supply com demand, sem usar muito recurso ou faltar recurso

Um exemplo claro disso, é Autoscaling. 
Outro exemplo, é Serverless, que responde somente quando é requisitado.

CloudWatch mostra o quão grande é a demanda.

#### Questions

* Como saber qual a capacidade mas sem exceder a necessidade?
* Como otimizar o uso de serviços na AWS?

### Cost-effective Resources

Em algumas aplicações, usar uma máquina melhor, pode resultar em menos custo

#### Questions

* Você selecionou os melhores tipos de recursos?
* O modelo selecionado se adequa melhor a sua aplicação?
* Existe algum outro serviço que pode melhorar sua infraestrutura?

### Expenditure Awareness

Não precisa mais de servidores físicos, pegar aprovações, coletar recursos, data de entrega e tudo isso.

Na AWS tudo isso é cuidado pela Amazon.

Mas isso também traz problemas, pois cada time pode ter a sua própria conta AWS. E juntar tudo isso e estar atento a cada gasto é crucial para a arquitetura do sistema.

É necessário usar scaling up e scaling down, assim como billing alerts ou tags. Se estiver várias contas AWS na sua empresa, é possível controlar até por ferramentas de terceiros.

#### Questions

* Qual é o seu processo para administrar os custos com AWS?
* Como vc está monitorando seus gastos?
* Como vc pode saber quais recursos não estão sendo usados?
* Como é considerado o tráfego de dados?

### Optimizing Over Time

A AWS evolui muito rápido. E talvez o serviço que iniciou a sua infraestrutura não seja o melhor daqui a alguns anos. E é necessário estar preparado para mudanças.

Um jeito bom de saber essas mudanças é se inscrever no AWS Blog ou assisir o A Cloud Guru Week.

#### Question

* Como você administraria a adoção de novos serviços?

## Key Services

1. AutoScaling
2. EC2, AWS Trusted Adivisor
3. CloudWatch, SNS
4. AWS Blog