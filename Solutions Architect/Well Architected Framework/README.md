# The Well Architected Framework

O PDF pode ser visto, na íntegra, ![aqui](https://aws.amazon.com/pt/architecture/well-architected/)

Foi lançado em AWS re:Invent, em 2017.

## O que é

Foi desenvolvido por Solutions Architects, baseados em suas experiências ao realizar as melhores práticas na AWS.

## 5 Pillars

1. Security
2. Reliability
3. Performance Efficiency
4. Cost Optimisation
5. Operational Excellence

## Structure of a Pillar

* Principles
* Definition
* Best Practices
* Key AWS Services
* Resources

## General Design Principles

1. Para de tentar adivinhar a capacidade do seu sistema
2. Testar o sistema em escala de produção.
3. Automatização para fazer a sua arquitetura experimental.
4. Permitir evolução na sua arquitetura.
5. Data-Driven na infraestrutura. Como tomar decisões baseadas nos dados coletados
6. Game Days. Simular eventos em produção, como uma BlackFriday, um DR.

## Summary

1. Security
2. Reliability
3. Performance Efficiency
4. Cost Optimization
5. Operational Excellence

### Security

* Data Protection
* Privilege Management
* Infrastrucure Protection
* Detective Controls

Lembrar de proteger os dados, parados e em transito.

Como limitar recursos

Como administrar as keys

Como definir as roles e groups

Proteger o SO dos EC2

Como analisar as logs

### Reliability

* Foundation
* Change Management
* Failure Management

Como é a topologia da AWS

Como é o processo de escalation

Como adaptar o sistema

Como monitorar o ambiente

Como executar as changes

Como fazer backup de dados
### Performance Efficiency

* Compute
* Storage
* Database
* Space-Time Trade-off

Como selecionar o melhor tipo de instância

Como monitorar as instancias para saber q estão fazendo o esperado

Como saber a quantidade de instâncias necessárias

Como saber a proximidade e caching necessária para o seu negócio

### Cost Optimization

* Matched Supply and Demand
* Cost-Effetive Resources
* Expenditure Awareness - saber quanto cada time gasta
* Optimizing Over Time

Como saber se a capacidade é o necessário para a demanda

Como saber o tipo certo de recursos

Como selecionar o melhor modelo

Como monitorar os gastos

Como fazer decomission de recursos não usados

### Operational Excellence

* Preparation
* Operation
* Responses

Como configurar o workload

Como minimizar o impacto de changes

Como monitorar o workload

Como responder a eventos inesperados

## Quiz

1. SWF - coordenar tasks, sejam sincronas ou assincronas.
2. O tempo de retenção no RDS é de 35 dias.
3. Backups automatizados são habilitados por default em instancias de DB.
4. RDS não pode ter aumento de storagem em SQL Server.
5. Usar IOPS quando usa muita transação online
6. S3 é object based
7. SE AVAILABILITY = 99.99%
8. EBS é block based
9. Caso queira rodar algum banco numa instância EC2, use EBS.
10. S3 DURABILITY = 99.9999999999%
11. É possível acessar EBS Snapshots, pela AWS CLI ou AWS Console
12. Policy = document.
13. Ao ser criada, uma instância EC2 recebe um IP Público e um IP Privado.
14. Se um EBS for o root device de uma instância, é necessário pará-la antes. Se não for root, pode tirar mesmo rodando.
15. Error RDS node é aquele que gerou um erro.
16. AWS recomanda que se usem roles ao invés de credentials guardadas diretamente nas instâncias.
17. Uma instância reservada não pode ser movida de Region
18. S3 RRS Durability = 99.99%
19. Máximo tamanho para SQL Server com SQL Server Express é 10GB por DB.
20. S3 RRS = reduced redundancy storage
21. É possível forçar um failover para RDS Multi-AZ
23. EBS = Elastic Block Storage
24. É impossível realizar testes de vulnerabilidade sem alertar a AWS antes
25. Reserved instances podem ser usadas para Multi-AZ
26. Mysql = 3306
27. Quando se cria um SG, todo trafego inbound é negado por default, mas todo oubtbound é permitido.
28. Planos de suporte AWS -> Basic, Developer, Business, Enterprise
29. Para que um site guarde credit card details, devem ter creditação da QSA.
30. AZ = areas distintas dentro de uma AWS region que foram desenhadas para serem isoladas e evitar falhas.
31. Instancias individuais são providenciadas em uma AZ.
31. Quando usando uma custom VPC, uma instância EC2, MESMO QUE NUMA SUBNET PUBLICA, precisa de um IP publico.
32. XEN é o hypervisor do EC2.
33. AWS tem PCI DSS 1.0 compliant
34. AWS tem 18 regions em 02/11/2018
35. Aurora guarda 6 cópias dos seus dados. 6.
36. Elastic Beanstalk é um serviço que gerencia deploys, load balancing, autoscaling e healthcheck de instancias, baseado no código que você oferece.
37. CloudTrail permite fazer auditoria de acessos de usuários.

## Exam Tips

###### Kinesis

Amazon Kinesis é um serviço de stream de dados. Por exemplo, feeds de redes social, notícias. É um jeito de **consumir** Big Data.

Amazon Redshift é usado para **BI**

Elastic Map Reduce para **processar** Big Data.

---

###### EBS vs Instance Store

EBS Backed são **persistentes** - continuam mesmo sem o EC2
É possível fazer um detach de um EBS em um EC2 e colocar em outro EC2.

Instance Store Backed Volumes são **efêmeros**
Não é possível fazer um detach de uma EC2 e colocar em outra. Só vale para uma.

EBS podem ser parados. E os dados são persistentes

Instances Stores não podem ser parados. Se parar a instância EC2, os dados são apagados.

---

###### OpsWorks

É um serviço de orquestração que usa Chef.

Chef consiste de receitas para manter um estado consistente.

---

###### Elastic Transcoder

É um serviço que converte mídias para diferentes plataformas.

Com isso, o desenvolvedor não precisa se preocupar em escolher o melhor formato.

Só para pelo tempo que foi usado.

---

###### SWF Actors

1. Starters - algo que vai iniciar um workflow
2. Deciders - controlar o fluxo de uma task. Decide o que fazer se algo falhar, ou se funcionar
3. Workers - realizam as tasks.

---

###### Metadados de um EC2

```bash
curl http://169.254.169.254/latest/meta-data/

wget http://169.254.169.254/latest/meta-data/
```

*As instâncias tem META DADOS, não existem user data*