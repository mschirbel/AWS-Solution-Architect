# Second Pillar - Reliability

Cobre a habilidade do sistema em se recuperar de algum desastre.
E também a habilidade manter os recursos de acordo com a demanda.

## Design Principles

1. Testar o recovery
2. Automaticamente se recuperar de uma falha.
3. Scaling horizontally. Agregando no sistema mundialmente falando
4. Pare de tentar adivinhar a capacidade

## Definition

1. Foundation
2. Change Management
3. Failure Management

### Foundation

Pensar nos pré-requisitos da sua aplicação. AWS cuida de toda a foundation da sua infraestrutura. Mas ela coloca limites para que não seja usado, acidentalmente, muitos recursos.

Pode ser visto com mais detalhes ![aqui](https://docs.aws.amazon.com/pt_br/general/latest/gr/aws_service_limits.html)

#### Questions

* Como estão sendo controlados os limites de serviços?
* Como é a topologia de network na AWS?
* Você tem algum plano de escalation para problemas técnicos?

### Change Management

Saber como as mudanças afetam o sistema para que possam ser controladas proativamente.

Monitoração permite detectar quaisquer mudanças no ambiente,

Na AWS é usado o CloudWatch para monitorar o ambiente e fazer autoscaling.

#### Questions

* Como o sistema se adapta a demanda?
* Como os recursos da AWS são monitorados?
* Como é feito o controle de Changes?

### Failure Management

O sistema deve ser desenhado pensando que falhas vão ocorrer.
Deve-se saber como as falhas podem acontecer e o que fazer para tratar e evitar.

#### Questions

* Como é feito backup de arquivos?
* Como o sistema responde a falhas?
* Qual o seu plano de recuperação?

## AWS Key Services

1. IAM, VPC
2. CloudTrail
3. CloudFormation, RDS Multi-AZ

## Exam Tips

1. 3 áreas
2. Questions