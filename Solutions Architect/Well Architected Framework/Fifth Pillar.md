# Fifth Pillar - Operational Excellence

Foi o pilar mais recente adicionado ao whitepaper.

Inclui práticas operacional e procedimentos. Inclui como changes são executadas e respostas a eventos inesperados.

Tanto as changes quanto as respostas DEVEM ser automatizados.

Todos os processos devem ser documentados, testados e regularmente revisados.

## Design Principles

1. Operations with code.
2. Alinhar os processos com os objetivos do negócio - usando métricas para saber onde devemos nos focar
3. Changes devem ser regulares, pequenas e incrementais.
4. Testar as respostas para eventos regulares.
5. Aprender com as falhas.
6. Os processos devem estar atualizados.

## Definition

1. Preparation
2. Operation
3. Response

### Preparation

É necessário para garantir que os workloads estão prontos para produção.

Workloads devem ter:

* Runbooks => guias para times de operação fazerem as tasks diárias
* Playbooks => guias para eventos inesperados. isso deve incluir respostas, planos de escalation e notificação dos acionistas.

Há um número enorme de serviços que ajudam com isso, como CloudFormation, pois podemos editar o código e testar rapidamente. Temos também AutoScaling e AWS Config(podemos criar mecanismos para responder a mudanças no ambiente).

#### Questions

* Quais práticas você está usando para suas operações na nuvem?
* Como estão as configurações do seu workload?

#### Best Pratices

Documentar e revisar.

Revisar o workload para produção, isso evita que eventos inesperados causem downtime

### Operations

Devem ser padronizadas para uma rotina básica.

O foco deve ser em automação e changes pequenas e regulares.

Changes não devem ser grandes e infrequentes e não devem precisar de downtime.

Várias logs e métricas devem ser os indicadores do workload. Com base nessas logs e métricas, devemos revisar as operações.

Em AWS é possível fazer um CI/CD muito fácil e não causa impacto.

#### Questions

* Como você evolui o seu workload?
* Como você monitora o seu workload para saber que as suas operações estão fazendo o esperado?

#### Best Practices

Automatize tudo.

Changes devem ser pequenas, regulares.

Rollbacks devem ser evitados.

Monitoração ajuda a ajustar o ambiente com a necessidade do negócio.

### Responses

Respostas a eventos inesperados devem ser automatizadas.

Seja para mitigação, remediation ou rollback.

Até mesmo o processo de escalation devem ser evitadas, pois o processo de um novo deploy deve ser testado ao máximo.

Mas, caso haja necessidade de escalation, que tenha um caminho bem definido e automatizado.

Podemos usar AWS SNS para fazer isso, criando um tópico para cada escalation.

#### Questions

* Como você reponde a eventos inesperados?
* Como é o processo de escalation?

## Key AWS Services

1. AWS Config, AWS SQS, AutoScaling, Service Catalog
2. CodeCommit, CodeDeploy, CodePipleline, CloudTrail
3. CloudWatch, SNS.