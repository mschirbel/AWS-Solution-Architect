# Performance Efficiency

Foca em duas coisas:
1. Como usar os recursos computacionais de forma eficiente.
2. Como manter essa eficiência e atualizar sua tecnologia.

## Design Principles

1. Democratizar as tecnologias avançadas - usar os serviços que os times podem usar sem saber o que acontece por trás, tipo DynamoDB.
2. Go global in minutes.
3. User Serverless Architectures
4. Mais experiências e testes com a infraestrutura.

## Definition

1. Compute
2. Storage
3. Database
4. Space-Time Trada-off

### Compute

Quando desenhar o seu ambiente, é importante saber o tipo de servidor que será necessário.

Algumas aplicações dependem muito de CPU, outras de Memória.

Os servidores da AWS são virtualizados com o clique do serviço, e podemos selecionar qual máquina e SO usaremos no ambiente.

Podemos escolher até mesmo a quantidade de memória.

Podemos escolher até não usar servidores, usando Lambda Functions.

#### Questions

* Como escolher o melhor tipo de instância para a minha aplicação?
* Como garantir que a minha instância permanecerá com qualidade, mesmo após novas tendências serem lançadas?
* Como garantir que as instâncias estão fazendo o que é esperado?
* Como garantir que as instâncias estão atendendo minha demanda?

### Storage

Depende de alguns fatores:

1. Access Method
2. Patterns os Access
3. Throughput Required
4. Frequency of Access
5. Frequency of Updates
6. Availablity Constrains
7. Durability Constrains

Com S3 temos muita durabilidade(11 x 9's). Com EBS podemos ter diferentes tipos de Storage Medium. E podemos mover rapidamente os dados de um storage para outro.

#### Questions

* Como escolher o melhor tipo de storage?
* Como saber que o que vc escolheu ainda é bom depois de novas tecnologias serem lançadas?
* Como monitorar o seu storage?
* Como saber se a capacidade/throughput atendem a sua demanda?

### Databases

1. Consistency
2. Availability
3. SQL or No-SQL

Ainda temos Warehouses, para OLTP ou OLAP

#### Questions

* Como escolher o melhor DB para meu sistema?
* Como garantir que tenho o melhor database sendo que algum novo lançou?
* Como monitorar o DB?
* Como garantir que minha capacidade/throughput atendem a demanda?

### Space-Time Trade Off

Saber a latência entre os seus serviços.

Por exemplo, podemos usar Direct Connect para saber a latência entre algum HQ e a AWS.
Podemos usar cópias de nossa infraestrutura em diversas partes do mundo para atender um público global.

Como ElastiCache ou CloudFront - podemo reduzir a latência

#### Question

* Como escolher a melhor proximidade e caching para nosso sistema?
* Como saber que temos a melhor escolha, mesmo com novas tecnologias?
* Como monitorar essa latência?
* Como saber se isso atende minha demanda

## Key Services

1. Compute, Autoscaling
2. EBS, S3, Glacier
3. RDS, DynamoDB, RedShift
4. CloudFront, ElastiCache, Direct Connect, RDS Read Replicas