# CloudFormation

Usar templates, feitos tem JSON ou XML, para subir a sua infraestrutura na AWS.

Qualquer modificação no template, é feita também na infraestrutura. E tudo é feito dentro de minutos.

Como por exemplo, sharepoints, aplicações, banco de dados.

É uma das ferramentas mais importantes de Solutions Architect

## Editando o template

Vamos no serviço de CloudFormation, que está debaixo de Management Tools.

Aqui temos 3 diferentes opções.

1. Design a Template - temos uma ferramenta visual
2. Create a Stack - criamos um template com código
3. Create a StackSet - criamos um template e compartilhamos com outras contas AWS.

Agora, vamos criar uma nova Stack.

Vamos usar um template para WordPress blog:

![](../images/49.png)

Se clicarmos em View/Edit template in Designer:

![](../images/50.png)

Vemos toda a nossa infraestrutura. Para voltar, basta clicar no ícone:

![](../images/51.png)

Clique em Next.

Agora, fazemos as configurações iniciais.

![](../images/52.png)

Colocamos as informações do Banco de Dados, bem como os tipos de máquinas que vamos usar e qual o range de IPs permitidos para fazer SSH.

Também colocamos a VPC que usaremos e as subnets.

Agora colocamos as roles de IAM:

![](../images/53.png)

Dentro de alguns minutos, nossa infraestrutura estará pronta. Podemos clicar na Stack e em Events para ver cada passo da criação.

Mas com isso, vai acontecer um problema de Timeout.

Esse problema pode ser que nosso SG não permite que HTTP seja acessado de todo o mundo. Para isso, edite as inbound rules para 0.0.0.0/0

Outro problema é o SG de nosso Load Balancer, que precisa estar no mesmo das instâncias.

*Se você eliminar uma stack, todos os recursos serão apagados*

## Sugestão de Cursos

https://acloud.guru/learn/intro-aws-cloudformation - Introdução

https://acloud.guru/learn/aws-advanced-cloudformation - Avançado