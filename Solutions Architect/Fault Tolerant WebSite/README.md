# Creating a Fault Tolerant Word Press Site

## Before the show

Antes de começar, pegue o arquivo bootstrapscript.sh que está em anexo.
Juntamente com o arquivo htaccess.txt, que deverá ser renomeado para .htaccess

## Arquitetura

Vamos ter duas instâncias EC2, por trás de um scalling group, atrás de um application load balancer.

Essas instâncias vao se comunicar com um RDS que terá um multiAZ.

As instâncias de aplicação se comunicarão com buckets dentro de nossa VPC. E com o serviço de CloudFront.

O usuário vai acessar o dns, que pelo Route53, redirecionará para a url.

## Set Up

### Security Groups

Vamos realizar todas as ações em N.Virginia, pois queremos ter certeza que todos os serviços estarão disponíveis.

Vamos para a aba de VPC:

![](images/01.png)

Vamos criar um SG:

![](images/02.png)

Esse SG vai ficar em nossa VPC default, é onde vamos abrir as portas 80 e 22.

O outro SG vai ser para o RDS:

![](images/03.png)

Para as Inbound Rules do WebDMZ:

![](images/04.png)

Para as Inbound Rules do RDSSG:

![](images/05.png)

### RDS

Agora vamos criar nossas instâncias de RDS.
Vamos usar MySQL:

![](images/06.png)

Vamos usar o case de Production, pois queremos usar Multi AZ:

![](images/07.png)

Ao marcar essa opção, não estará mais no Free Tier. Multi AZ tem custo.

Nas Settings, vamos usar tudo igual para ficar mais fácil

![](images/08.png)

![](images/09.png)

Todo o restante será default.

### S3

Enquanto as instâncias serão criadas, o que vai demorar uns 5 minutos, vamos criar os Buckets no S3.

O primeiro será onde vamos armazenar nossos códigos:

![](images/10.png)

O segundo será para o repositório de mídias do CloudFront:

![](images/11.png)

### CloudFront

Agora vamos criar uma Distribution para nossos assets.

Vá para o serviço de CloudFront:

![](images/12.png)

Preencha o Domain Name com o link para o seu bucket de assets:

![](images/13.png)

### Route53

Isso pode demorar um pouco, então, vamos criar DNS no Route53.

Se não quiser pagar para usar um domain name, é possível usar o link da instância EC2/Application Balancer.

### IAM

Vamos agora criar nossas roles.

![](images/14.png)

E a policy:

![](images/15.png)

![](images/16.png)

### EC2

Assim que criada vamos para EC2 e criar nossas instância.

Atenção: use Amazon Linux 2 AMI para essa lição.

![](images/17.png)

Vamos trocar a role da nossa instância para a que acabamos de criar. E vamos passar o bootstrap script em anexo para boot da instância.

```bash
#!/bin/bash
yum install httpd php php-mysql -y
cd /var/www/html
echo "healthy" > healthy.html
wget https://wordpress.org/latest.tar.gz
tar -xzf latest.tar.gz
cp -r wordpress/* /var/www/html/
rm -rf wordpress
rm -rf latest.tar.gz
chmod -R 755 wp-content
chown -R apache:apache wp-content
wget https://s3.amazonaws.com/bucketforwordpresslab-donotdelete/htaccess.txt
mv htaccess.txt .htaccess
chkconfig httpd on
```

Para as tags:

![](images/18.png)

Para os secrity groups:

![](images/19.png)

*Não se esqueça de salvar a key*

### Setting Up EC2

Primeiramente, logue na instância EC2.

```bash
chmod 400 [chave].pem
ssh ec2-user@[ip publico] -i [chave].pem -y
```

Para ver se tudo foi instalado:

```bash
cd /var/www/html
ls -la
```

Temos nosso healthy.html que é nossa página web para controle de healthcheck.

Temos que ver se nosso .htaccess está lá, para isso:

```bash
cat .htaccess
```

Se tudo estiver ok, podemos habilitar o apache:

```
service httpd start
```

Agora temos que copiar o endpoint de nosso banco de dados:

![](images/20.png)

Com esse endpoint copiado, vamos para o IP de nossa instância.

Ao digitar o ip no browser, nos deparamos com o instalador do wordpress:

![](images/21.png)

Clique em Let's Go. E preencha as informações:

![](images/22.png)

Aparecerá um erro, dizendo que não pode escrever sobre um arquivo:

![](images/23.png)

Pegue este código e volte ao terminal e cole o código no arquivo designado.

Agora precisamos criar um site, com um nome, username e e-mail. A senha é gerada automaticamente. Basta clicar em instalar Wordpress.

## Setting up Wordpress

Agora vamos logar na dashboard. Podemos ir em Posts e veremos que existe um por default:

![](images/24.png)

Para inserir imagens, basta ir em Media e clicar em Add New:

![](images/25.png)

Podemos adicionar quaisquer imagens que desejarmos. E quando voltarmos ao nosso Post, podemos clicar em Add Media para adicionarmos alguma das fotos que carregamos, em nossa postagem.

![](images/26.png)

Ao terminar de carregar as imagens, ou qualquer modificação, basta clicar em Update:

![](images/27.png)

Agora basta clicar no Permalink, e poderemos ver a postagem.

## URL Rewrite

Se clicarmos na imagem com o botão direito e pegarmos a sua URL, veremos que a imagem está dentro de nossa instância EC2, dentro da pasta wp-content/uploads

Se voltarmos ao terminal:

```bash
cd wp-content/upload
ls -la
```

Aqui temos as nossas imagens. O que queremos é que toda a vez alguma imagem for para nosso EC2, também vá para nosso bucket. Podemos fazer isso manualmente:

```bash
cd /var/www/html
aws s3 ls
aws s3 cp --recursive /var/www/html/wp-content/uploads s3://marceloschirbel-mediaassets
```

Mas também queremos que o nosso código esteja redundante, para isso:

```bash
cd /var/www/html/
aws s3 ls
aws s3 cp --recursive /var/www/html s3://marceloschirbel-wordpresscode
```

Isso pode levar algum tempo, e podemos verficar com

```bash
aws s3 ls marceloschirbel-wordpresscode
```

Se nossa instância morrer, temos nosso código.

Agora queremos forçar o Wordpress a não usar a imagem da instância, queremos usar o CloudFront.

Para fazer isso, usamos URL Rewrite, e isso é feito com o arquivo .htaccess

Temos que apagar a linha do arquivo com a URL setada, e vamos colocar a URL do nosso CloudFront:

![](images/28.png)

Dessas informações, precisamos do Domain Name.

Voltando no arquivo .htaccess, colamos o domain name na URL.

Agora precisamos sincronizar as mudanças com o nosso bucket.

```bash
aws s3 sync /var/www/html s3://marceloschirbel-wordpresscode
```

Agora as imagens vão usar a URL do CloudFront.
Mas ainda precisamos permitir que o Apache faça URL Rewrites:

```bash
cd /etc/httpd
ls
cd conf
vi httpd.conf
```

Dentro do arquivo, as seguintes linhas estarão:

"Allow override controls what directives may be placed in .htaccess. It can be "All", "None", or any combination of keywords:

Após essas linhas, troque a tag AllowOverride para All.

Apenas para ter certeza que vai funcionar:

```bash
service httpd restart
```

Agora voltamos ao browser, e vamos ver a URL das imagens. Basta ver se a URL está no CloudFronte.

## Application Load Balancer

Vamos para o serviço de EC2, e no menu de Load Balancing:

![](images/29.png)

Vamos criar um Application Load Balancer:

![](images/30.png)

![](images/31.png)

Vamos adicionar dentro de nosso SG:

![](images/32.png)

Para as configurações de healthcheck:

![](images/33.png)

Agora basta selecionar a instância EC2 e criar.

Podemos também criar um record set em Route53 para redirecionar nosso DNS para o LoadBalancer:

![](images/34.png)

*Isso somente se vc registrou um domínio*

Caso queira testar, basta pegar a URL do seu LB e jogar no browser.

## AMI de nossa Aplicação

Vamos criar uma AMI de nossa instância para que toda vez que uma nova instância tiver de ser criada, usaremos a instância já configurada.

E essa frota de instâncias, vão estar atrás de um ALB que vão sincronizar com nossos buckets.

Para isso, use os seguintes comandos dentro da instância EC2:

```bash
cd /etc
vi crontab
```

Adicione a seguinte linha no crontab:

```
*/1 * * * * root aws s3 sync --delete s3://marceloschirbel-wordpresscode /var/www/html
```

Esse comando vai ser executado todos os dias da semana, todo os dias do mes, todo o ano. E vai sincronizar, apagando os arquivos diferentes, o meu bucket com o diretório /var/www/html

Agora precisamos ir para nossa instâcia EC2 e criar uma imagem de nossa instância:

![](images/35.png)

Basta dar algum nome e descrição:

![](images/36.png)

Se formos em Images, podemos ver a que criamos:

![](images/37.png)

## Editando a Crontab para fazer upload.

O que queremos é que, a instância que está rodando agora, seja aquela que vai ser usada para fazer as postagens, e não a que vai atender as requisições. Logo, ela deve ser responsável por fazer UPLOAD de arquivos, e não download.

A instância que será responsável por suportar a aplicação, vai ser criada com a AMI.

Agora, vá até a instância, e edite a crontab novamente:

```bash
cd /etc
vi crontab
```

Substitua a linha por:

```
*/1 * * * * root aws s3 sync --delete /var/www/html s3://marceloschirbel-wordpresscode
*/1 * * * * root aws s3 sync --delete /var/www/html/wp-content/uploads s3://marceloschirbel-mediaassets
```

Para testar se isso vai funcionar:

```bash
cd /var/www/html
echo "this is a test" > teste_crontab.txt
ls
```

Volte para seu bucket e veja se funcionou.

## Upload de imagem diretamente no WordPress

Se tentarmos fazer upload de uma imagem no WordPress, vai acontecer o seguinte:

![](images/38.png)

A imagem não vai carregar, isso acontece porque já foi feito o upload para o EC2, mais o rewrite de url fez que fosse para o CloudFront. E a imagem ainda não existe ainda no CloudFront. Isso demora uns 10-15 minutos. Após isso, só dar um F5. Se ainda não funcionar, tente limpar o cache do navegador.

## Auto Scalling Group

Para fazer Auto Scalling Group, é necessário antes editar as Launch Configurations.
Mas podemos clicar diretamente em Auto Scalling, que o console vai nos conduzir.

Ao clicar para criar, seleciona a AMI que foi criada:

![](images/39.png)

Para as configurações:

![](images/40.png)

Para o Security Group:

![](images/41.png)

Basta criar, agora. Finalmente, vamos criar nosso Auto Scalling Group:

Vamos dar um nome, e colocar todas as subnets disponíveis. A AWS tem inteligência suficiente para distribuir igualmente entre as subnets.

![](images/42.png)

Para os Advanced Details:

![](images/43.png)

*Perceba que o healthcheck é feito pelo nosso ELB*

Para as Scalling Policies:

![](images/44.png)

Em Scalling Policies, pode ser feito um escalamento por CPU, memória, disco e etc.

Para as notificações, podemos criar um SNS Topic:

![](images/45.png)

E basta criar. Para ver a history de criação das instâncias:

![](images/46.png)

Se formos em Target Groups, podemos ver que nosso node inicial está dentro. Mas não queremos isso, pois pode ser prejudicial para a aplicação:

![](images/47.png)

Vamos removê-lo, basta clicar em Edit, selecionar, clicar em Remove e Salvar:

![](images/48.png)

Agora, se acessarmos o endereço de nosso LB, veremos o nosso site. Se abrirmos alguma imagem em uma nova aba, veremos que a URL é do CloudFront.