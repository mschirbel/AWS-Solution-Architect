# Tagging and Resource Groups

## O que são Tags

Tags são metadados.

São feitos com Key-Value

Caso queira dar um nome

```json
{
    Key: Name
    Value: João
}
```

Tags são herdadas dentro de Autoscaling, CloudFormation

Isso pode ser muito útil quando instâncias são criadas usando templates. Com as tags sabemos quem criou.

## O que são Resource Groups

É um jeito de reunir recursos usando as tags que foram definidas.

Eles podem dividir uma ou mais tags.

### Tipos de Resources Groups

1. Classic Resource Groups - são globais.
2. AWS Systems Manager - baseados em regions

## Lab

Crie uma instância EC2:

![](images/01.png)

Crie mais uma em outra region.

Note que, não temos nomes para nossas instâncias. Mas podemos colocar um nome para elas:

![](images/02.png)

*Tags são sensitives*

Veja que ainda não apareceu ali em cima:

![](images/03.png)

Para isso, nosso Key deve começar com letra maiúscula:

![](images/04.png)

Não é possível adicionar múltiplas tags ao mesmo tempo. Deve ser feito recurso por recurso. Mas existem outros jeitos de fazer isso.

Agora vamos para Reources Groups:

![](images/05.png)

Clique em Create a Classic Group, pois dão a opção de fazer em todas as regiões.

Em Group Name damos o nome que vai remeter ao filtro.
Em Tags, temos todas as tags já usadas em nossa conta
Podemos filtrar por região e até por tipo de recurso.

![](images/06.png)

Temos vários serviços disponíveis:

![](images/07.png)

Agora, se clicarmos em Resources Grous, e criarmos um grupo, não clássico:

![](images/08.png)

Aqui podemos pesquisar os nossos recursos, mas isso é baseado em Regions:

![](images/09.png)

E ainda podemos colocar uma tag no grupo. Essa tag não vai herdar para os recursos.

Podemos criar um grupo, para que, possamos ver os recursos desse grupo.

E podemos também executar automações no grupo, como inserir policies, roles e outras coisas.
Mas somente na region.