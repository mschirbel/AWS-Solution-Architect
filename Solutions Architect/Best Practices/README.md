# Best Practices Whitepaper

## Business Benefits of Cloud

1. Zero investimento para infraestrutura
2. Infraestrutura feita na hora.
3. Melhor utilização dos recursos
4. Só paga pelo que usa. Paga pelo tempo de uso.
5. Reduz o tempo para entrar no mercado global.

## Technical Benefits of Cloud

1. Automation - Infraestrutura em script
2. Auto Scalling - failover
3. Proactive Scalling - quando precisa de mais, de acordo com consumo.
4. Development Lifecycle - CI/CD
5. Testability - homol separado mas igual a produção
6. DR muito mais fácil de fazer
7. Overflow - não precisa ter TUDO na nuvem, mas só algumas aplicações.

## Design for Failure

Seja um pessimista quando estiver desenhando a arquitetura na nuvem:

* Assuma que o hardware vai falhar
* Assuma que vai ter algum desastre na AZ
* Assuma que vai ter um número absurdo de usuários
* Assuma que o tempo de uso vai ser maior que o esperado

Com isso, a infraestrutura vai ficar mais robusta, mais cara também, mas isso ajuda com o mundo real.

## Decouple Your Components

Pense em SQS.

Ter componentes que não tem dependências entre si. Assim que algum deles falhar, não afetará os outros.

Tratar os componentes como uma caixa preta. Assim como uma API.

Por exemplo, um servidor de aplicação, não precisa saber sobre o servidor web, ou o servidor de banco de dados.

## Elasticity

1. Proactive Cyclic Scaling - Scaling periódico feitos em tempos determinados, por exemplo, no fim do mês, ou em dia de pagamento.
2. Proactive Event Scaling - Scaling feito quando algum evento acontece, por exemplo, natal, ou quando algum produto é lançado
3. Auto Scaling based on Demand - feito com monitoração de ambiente, acionando triggers e ações baseadas em métricas

## Secure your Application

Usar módulos de segurança em diferentes partes da sua aplicação. Por exemplo, permitir porta 80 e 443 nos servidores web, mas somente a porta 22 nos servidores de aplicação. E qualquer outro tráfego seria negado nos servidores de banco de dados.

*Leia o PDF em anexo*