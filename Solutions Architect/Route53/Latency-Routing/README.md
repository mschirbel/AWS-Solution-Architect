# Latency Routing

Permite que roteie o tráfico baseado na menor latência para o usuário final.

Para usar essa policy, deve ser criado um Record Set para um EC2 ou ELB em cada region com seu website.
Quando o Route53 receber uma request, vai enviar para a region com menor latência.

O retorno é o valor associado a cada record set.

![](images/01.png)

Para o exemplo acima, iria ser redirecionado para EU-WEST-2.

Para criar uma policy assim, abra as hosted zones:

![](images/02.png)

Crie um Record Set:

![](images/03.png)

Assim, podemos configurar uma linha para IP de suas instâncias.
