# Exam Tips

1. Simple Routing - só pode ter 1 Record Set com multiplos IPs. Se vc tiver multiplos values, Route53 vai retornar algum deles aleatoriamente
2. Weighted Routing - direciona o tráfego para uma region baseado na porcentagem especificada.
3. Latency Routing - escolhe a rota com menos latencia para se conectar
4. Failover Routing - temos um site primário e um secundário. Se o primeiro cair, temos o segundo ficando online
5. Geolocation Routing - baseado no local inicial da query - nao leva em conta a latência.
6. Multivalue Routing - podemos ter multiplos Record sets com healthcheck.

# Quiz

Route53 tem suporte para MX Records
MX records é um registro de servidor de mensagens - especifica quais servidores de mensagem podem aceitar emails enviados para seu domínio

Route53 suporta zone apex records = naked domain names - ou seja, aqueles domain.com e não www.domain.com
Vc PODE ter naked domains.

Por cada conta, é possível ter 50 domínios, mas isso pode ser aumentado se vc contatar o suporte da AWS.
