# Simple Routing

Se escolher essa policy, só pode haver um record com múltiplos IP.
Se vc colocar mais de um value em um record, Route53 devolve todos os values em ordem aleatória.

Ou seja, caso vc decida que aquele Record vai ter mais que um IP, o IP retornado vai ser aleatório.

Caso seja criado, a URL ficaria assim:

www.teste.[domínio].com
E em Value, vc coloca os IPs das suas instâncias.

![](images/01.png)

Em TTL é o time-to-live.

Agora, basta clicar em Create.

Se tentar resolver pelo domínio criado, pode demorar um pouco, cerca de 5min. O DNS está sendo propagado ao redor do mundo.
Como é Simple Routing, ele vai pegar aleatóriamente um dos valores que foi descrito em Value.

Basicamente, isso que acontece:

![](images/02.png)
