# DNS - 101

DNS é usado para converter domínios em IP
IP é usado para um computador encontrar o outro na internet.
Existem duas formas comuns de IP, que são IPv4 e IPv6.

## IPv4 vs IPv6
É um campo de 32 bits e tem mais de 4 bilhões endereços.
Mas não conseguiu prever o quanto a internet conseguiria crescer.

Por isso foi criado o IPv6 com um campo de 128 bits.
Com isso, é possível cobrir tudo que existe.

## Top Level Domain.

São uma série de caracteres separados por ponto.
Por exemplo:
.com = top level
.com.br = second level, com br sendo o top level
.gov = top level
.gov.uk = com .uk sendo o top level.

Eles níveis são controlados pela IANA.

## Registrars

São organizadoes de domínios para não haver duplicados
Eles forçam a unicidade - em um banco de dados chamado WhoIs
O DNS funciona na porta 53, por isso chama Route53.

## SOA

Start Of Authority Record
Todos os DNS tem isso, garantem algumas informações sobre o domínio:
1. O nome do servidor
2. O administrador da zona
3. A versão dos arquivos
4. O tempo se segundos time-to-live. - tempo para se propagar na internet.

## NS Records
Name Server - saao usados pelos Top Level Domain server para direcionar o tráfego para as autoridades que contém os DNS

Ex: Usuário tentando acessar helloroute53gurus.com
User => .com (vc tem algo sobre helloroute53gurus?) => NS.awsdns.com = SOA

## A Record

Traduz um nome de domínio para um IP.

## TTL

É o tempo que um DNS fica em cache, seja no Resolving Server ou no computador do usuário - quando menor o TTL, mais rápido as mudanças.

## CNames

Canonical Names são usados para resolver um dominio para outro. Um site mobile redirecionando para um site de compras, por exemplo.

## Alias Record

São únicos para Route53. E são usados para mapear os sites dentro de Elastic Load Balancers, CloudFront ou S3.
Funcionam como CNames, mas a diferença é que não pode ser usado para um naked domain.

### Exam Tips

1. ELBs nao tem IPv4 - tem que usar um DNS
2. Diferença entre Alias Record e um CName
3. Sempre escolher um Alias Record
4. SOA
