# Register a Domain Name

Vamos para o serviço da Route53

![](images/01.png)

Perceba que a Region foi alterada, isso se deve porque o Route53 é um serviço global.

Cique em Registro de Domínio:

![](images/02.png)

Clique para Registrar um domínio:

![](images/03.png)

Ao escolher o domínio, clique em Check. A AWS trará outras opções além do domínio escolhido.:

![](images/04.png)

Para dar continuidade, é preciso adicionar ao carrinho.

Não darei continuidade nessa parte, porque os domínios são pagos.

Após clicar em Continue, vc deverá preencher diversas informações. E após confirmação do pagamento, é possível verificar as pendências:

![](images/05.png)

Pode demorar até 3 dias. Quando estiver disponível, podemos acompanhar no Dashboard.

## Hosted Zones

Temos os NS records apontando para vários top level domains. Isso acontece para que as URLs não ficam indisponíveis.
Temos a SOA também.

## Providenciando instâncias.

Vamos escolher três regiões do mundo. Lançe instâncias em cada uma delas. Para essa aula, podemos usar o script que está em anexo para a parte de bootscript.
Todas as demais configurações podem ser default.

## Routing Policies

1. Simple Routing
2. Weighted Routing
3. Latency-based Routing
4. Failover Routing
5. Geolocation Routing
6. Multivalue Answer Routing
