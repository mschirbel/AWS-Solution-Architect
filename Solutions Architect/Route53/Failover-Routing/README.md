# Failover Routing

Usado para quando vc quer um aplicação com um "backup" ativo.

Route53 vai usar healthcheck para monitorar o site primário, se ele cair, o DR inicia.

Funciona mais ou menos assim:

![](images/01.png)

Assim, quando o site ativo cair, ou cair a AZ, ou até mesmo a Region:

![](images/03.png)

Agora vamos colocar um healthcheck em uma de nossas instâncias.

Para criar um healthcheck, acesse o submenu:

![](images/02.png)

Crie um:

![](images/04.png)

Em Name, vc pode especificar qualquer alias, assim fica mais fácil.
Em protocolo, vc pode definir HTTP ou HTTPS.

E a URL reunirá todas as informações.
Depois podemos configurar algum alarme, seja por e-mail, mensagem.

Podemos ver o status aqui:

![](images/05.png)

Agora podemos criar nosso Record Set:

![](images/06.png)

Assim, criamos o Record Set para o primário. Vamos criar para o secundário:

![](images/07.png)

Note que o segundo, não precisa de um healthcheck.

Dica: caso esteja com problemas, faça um hard refresh com limpeza de cache.
