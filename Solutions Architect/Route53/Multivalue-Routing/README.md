# Multivalue Answer

Faz um redirecionamento aleatório para multiplos recursos.

Assim podemos ter múltiplos Record Sets

E ainda podemos ter um healthcheck associado, para que, quando algum deles fique indisponível, saia do DNS.

![](images/01.png)

Se algum deles cair, falhando no healthcheck, Route53 não enviará trafego para aquela rota:

![](images/02.png)

Para criar um novo:

![](images/03.png)

Para fins de teste, podemos criar mais um, com outro IP:

![](images/04.png)

Lembre-se, os IPs são escolhidos aleatóriamente
