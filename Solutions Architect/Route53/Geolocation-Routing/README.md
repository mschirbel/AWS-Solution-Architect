# Geolocation Routing

Permite escolher o tráfego baseado na localidade do usuário.
Por exemplo, os usuários da europa podem ser redirecionados para eu-west1
Com instâncias espeficicamente programadas para usuários europeus.

Um exemplo disso, são e-commerce, onde os preços variam conforme a localidade do usuário.

![](images/01.png)

Para criar um Record Set com Geolocation Routing, basta:

![](images/02.png)

Com essa, todos os usuários da américa, vao para esse IP.

E isso pode ser feito com quantas instâncias diferentes forem possíveis.

Para não ser confundido com latency, a geolocation não depende do tempo de resposta, somente o local de origem da query para o DNS.
