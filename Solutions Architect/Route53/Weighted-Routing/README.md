# Weighted Routing

É como se fosse um balance com peso nas rotas.
Podemos definir uma porcentagem de balanceamente para uma certa region.

![](images/01.png)

Podemos fazer isso para uma aplicação e-commerce, baseado nos usuários, podemos redirecionar para uma region mais próxima.

Para habilitar, vamos em hosted zones:

![](images/02.png)

Em alguma zona, criamos um novo Record Set:

![](images/03.png)

Em weight colocamos o peso da rota. Em Set ID colocamos um Alias para o Value.

E assim, podemos criar mais um Record Set.
O Set ID pode ser diferente.

A AWS soma os valores e calcula a porcentagem.

Perceba que, para o Simple, só podemos ter 1 Record Set.
Com o Weigthed podemos ter mais de uma linha.

![](images/04.png)
