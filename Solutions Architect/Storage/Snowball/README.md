# Snowball

Note que o serviço de Snowball não está sob Storage. E sim sob Migration:

![](images/01.png)

Jobs são as demandas que colocamos para a AWS.
Desde a solicitação do Snowball, até o envio para a AWS e seu upload na nuvem.

Podemos criar um novo Job.

![](images/02.png)

Primeiramente planejamos o nosso Upload.
Nesse caso, vamos dar upload de um arquivo de nosso computador, para um S3.

![](images/03.png)

Preencha com os dados de envio:

![](images/04.png)

Agora, damos o nome ao Job e escolhemos para qual Bucket vamos importar os dados.

![](images/05.png)

No passo 4, damos as configurações de segurança:

![](images/06.png)

Caso seja necessário, o Snowball pedirá acesso a sua conta AWS:

![](images/07.png)

E na última parte, configure as notificações do envio do seu Snowball:

![](images/08.png)

Não criarei um Job, pois não tenho dinheiro pra bancar um Snowball - custa uns 150 US

Devemos usar um client para poder conectar os dados.
Assim que houver um job criado, podemos clicar em Get Client, ou acessar o link:
https://docs.aws.amazon.com/snowball/latest/ug/snowball-transfer-client.html

Instale o cliente e assim poderemos transferir os dados.

Caso queira ver como é um Snowball:
https://www.youtube.com/watch?v=yl25W7LZAMU

Para transferir os dados, precisamos da CLI do Snowball. Precisamos pegar as credenciais do Job que criamos.
Na Dashboard, podemos ver um botão Get Credentials, onde receberemos um código único.

Fazemos o download do manifest.

![](images/09.png)

Agora vamos ao diretório onde está o manifest:

```
./snowball start -i [ip] -m [manifest.bin] -u [codigo]
```

Para carregar os arquivos:

```
./snowball cp [arquivo] s3://[nome do bucket que vc linkou]
```

Depois de terminar:

```
./snowball stop
```
