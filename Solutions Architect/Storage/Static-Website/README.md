# Creating a Static Website with S3

A grande vantagem, é que não precisamos nos precisar sobre infraestrutura:
	- load balancing
	- scaling
	- manutenção
AWS cuida de tudo isso pra vc. Mas somente se for estático.
Isso significa que não podemos usar PHP, .NET e etc.

Vamos criar um novo bucket.

Caso vc queira um domínio apontando para esse website, vc precisa declarar no DNS do Route53.
Pra isso, é necessário ter um domínio IGUAL ao nome do seu bucket para que o Route53 funcionar.

Podemos deixar o bucket com todas as configurações default:

![](images/01.png)

Dentro do bucket, va na aba de propriedades e habilite a função de Static Website Hosting

![](images/02.png)

Note que:
O nome para o seu webiste é SEMPRE:
http://[nome do bucket].s3-website-[region].amazonaws.com

Isso cai bastante nos testes.

Basta clicar em Save.

Agora vamos dar upload em alguns arquivos HTML. Os arquivos utilizados para esse guia estão nesse repo.

Faça o Upload dos dois arquivos:

![](images/03.png)

Clique em Next.

Nas permissões, devemos colocar em modo público:

![](images/04.png)

Nas propriedades, devemos deixar em Standard e sem criptografia:

![](images/05.png)

Clique em Next e em Upload.

Se eu for na aba de Propriedades e pegar o link do meu site:

http://marceloschirbel-website.s3-website-sa-east-1.amazonaws.com

Ao clicar, veja que o site funciona, bem como as páginas de erro:

![](images/06.png)

Para que eu faça esse site sair do ar, basta que vá na aba de Permissions do arquivo index.html:

E desmarque a opção de leitura para todos:

![](images/07.png)

Veja que damos de cara com a página de erro.

Um bom uso seria para algo bombástico, como o lançamento de um filme, a AWS se encarrega de todo o tráfego e o custo é muito baixo.

