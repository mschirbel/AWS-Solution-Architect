Link para documentação Oficial:
https://aws.amazon.com/answers/infrastructure-management/crr-monitor/

Vamos no bucket que criamos.
Vemos que Versioning está ativado e temos nossos arquivos.

Vamos criar um novo Bucket:

![](images/01.png)

A região que selecionamos é a de Sydney.

Agora vamos ativar a Cross Region Replication.
Mas devemos tomar um erro, pois necessitamos de Versioning em ambos os buckets.

Para isso, vamos no bucket que desejamos replicar.
Entramos em Management:

![](images/02.png)

Vamos adicionar uma regra:

![](images/03.png)

Vemos que podemos replicar todo o conteúdo ou somente um prefixo(que é basicamente uma pasta)
Podemos habilitar essa regra, bem como encriptar a replicação.
Clicamos em next:

![](images/04.png)

Aqui percebemos que:
1. podemos replicar de buckets fora da nossa conta AWS
2. temos que ter o versioning ativado em ambos os buckets, e para isso podemos clicar nesse botão.
3. caso o segundo bucket seja um backup, vc pode deixar como Standard - IA, assim economiza uma boa grana.

Clique em Next.

![](images/05.png)

Aqui podemos selecionar alguma IAM, caso haja para o Backup, como o time de Backup, por exemplo.
Podemos também criar uma nova para esse bucket.

Next.

![](images/06.png)

Temos as reviews de origem, destino e permissões. Salvamos a regra.

![](images/07.png)

A regra foi criada e podemos ver as settings.

Agora vem a pergunta:
Será que todos os objetos já foram replicados?

![](images/08.png)

Vemos que o bucket destino está vazio. Isso se dá porque criamos uma regra.
Essa regra vale para os futuros objetos que forem criados, não para os antigos.

###### Legal, e como copiamos os que já existem?

A forma mais fácil de fazer isso é pela AWS-CLI

Para instalar no Unix:

```
pip install awscli
```
Lembrando que vc deve ter um Python > 2.6.5

Com isso feito, podemos digitar

```
aws configure
```

Necessitamos entrar com as credenciais, para isso, precisamos cria-las no IAM:

Vá para a console web, entre em IAM. Vamos criar um novo grupo:
Damos um nome que remete aos Adms
Para as Policies:

![](images/09.png)

Somente queremos AdministratorAccess. Crie o grupo.

Agora vamos criar um usuário.
Coloque um nome e MARQUE a opção de Programmatic Access, para usar a CLI.

![](images/10.png)

Coloque esse usuário no grupo recém criado:

![](images/11.png)

Agora, vc pode ver a AccessKey e a Secret Access Key desse user, use elas para logar na AWS-CLI:

* Não colocarei um print aqui, por questões de segurança *

Voltamos ao terminal:

```
aws configure
```

Inserimos a Key que obtivemos ao criar o usuário:

Depois a Secret Key.

Depois a region que setamos a nossa conta aws

Depois apertamos enter para o formato do output.

Se tudo isso funcionou:

```
aws s3 ls
```

Vemos nossos buckets listados.

Para replicarmos de um bucket para o outro:

```
aws s3 cp --recursive s3://[bucket_origem] s3://[bucket_destino]
```

![](images/12.png)

Podemos ver no bucket que realmente funcionou:

![](images/13.png)

Agora, delete um objeto do bucket origem, como temos o versionamento ativo, somente ganhou uma marcação de Delete.

###### Será que o outro bucket também recebeu?

Sim, pois a partir da regra, todas as ações são replicadas.

Podemos ver que no replicado, a marcação está lá:

![](images/14.png)

Agora, restauramos o arquivo no bucket de backup. Será que foi restaurado no de origem?
Não, porque ao deletar um objeto, vc coloca uma marcação nele, mas ao deletar a marcação, isso NÃO é replicado.

Editamos o arquivo, para mudar a versão e fazemos o upload.

Não se esqueça de deixá-lo público.

Vá até o bucket replicado e veja se o arquivo está up-to-date:

![](images/15.png)

As permissões também foram replicadas.

### Exam Tips:

1. Versioning tem que estar habilitada em ambos 
2. regions devem ser únicas
3. os arquivos que já existem não são replicados, isso deve ser feito por aws-cli
4. marcações 'delete' são replicadas
5. remover marcação 'delete' não é replciada
6. delete de versão não é replicado
7. sempre melhor criar novos buckets e colocar a regra antes de colocar qualquer arquivo. 


































