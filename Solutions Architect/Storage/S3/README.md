# Criando um S3 Bucket

Buckets é um pasta, onde vamos guardar nosso objects

Vamos em Services e na aba de Storage, clicamos em S3

![](images/01.png)

Veja que trocou sua região para Global, isso se da porque o S3 é nivel global. Não ficando preso a uma região.

![](images/02.png)

Vamos criar um Bucket. Clique no botão Create bucket:

![](images/03.png)

O Bucket Name deve ser único, pois usa um DNS Namespace para referenciar.
Apesar do S3 ser global, o bucket deve ser definido por região
E podemos copiar configurações de outros buckets

Clique em Next e vamos para Configure Options:

![](images/04.png)

Versioning funciona como o git, guardando versões do seu arquivo.
Logging serve pra registrar movimentações no seu Bucket
Tags serve para marcar arquivos e projetos.
Podemos também colocar encription nos arquivos

Clicamos em Nex e vamos para as Permissões:

![](images/05.png)

Podemos dar as permissões a nossa conta, naquele Bucket, bem como adicionar novas contas.
Por exemplo, podemos ter um time de Dev, uma conta para Homol, outra pra Prod e por aí vai

Podemos deixar o Bucket Public.

Clicamos em Next e podemos ver o review da criação do Bucket.

Podemos agora criar o Bucket:

![](images/06.png)

## Editando um Bucket

Se clicarmos em cima do nome do bucket:

![](images/07.png)

Podemos editar, apagar, criar pastas e dar upload de arquivos.

Vamos fazer upload de dois arquivos:

![](images/08.png)

Selecione os arquivos.
Assim como fizemos quando criamos o bucket, podemos definir permissões individuais para cada arquivo.
Agora não vamos, clique em Upload:

No fim da tela, verá uma barra com o progresso de cada arquivo.
![](images/09.png)
Embaixo mostra 1 success, isso se da porque foi feito somente 1 upload.

Se isso foi feito pela linha de comando, vc receberia um HTTP200.

## Editando um Object

Quando clicamos em algum dos arquvos, podemos ver suas prorpiedades, e outras funções:

![](images/10.png)

Vemos que tem um link para esse objeto, ao clicarmos:

![](images/11.png)

Isso acontece porque por default, seus buckets são privados e isso é herdado para os objetos dentro dos buckets.
Se voltarmos à página de edição do objeto, podemos torná-lo público:

![](images/12.png)

Ao clicamos no link do objeto novamente, podemos vê-lo.

![](images/13.png)

Agora veremos as permissões do objeto:

![](images/14.png)

Isso mostra quem pode editar o arquivos e como editar.

Agora veremos as propriedades:

![](images/15.png)

Em Storage Class, temos Standard, se clicarmos, podemos mudar:

![](images/16.png)

Podemos usar também de criptografia:

![](images/17.png)

Em AES-256 lemos que é uma criptografia server-side, que significa que não será possivel ver sem estar com a conta da AWS, mesmo que eu tente ver dentro do disco físico que guarda a informação.

Podemos ver os Metadados e as Tags. As Tags feitas a nível de Bucket não se reproduzem para os Objetos, podemos inserir:

![](images/18.png)

O size mínimo para um objeto é de 0 bytes.

## Propriedades do Bucket

Agora voltamos para o Bucket e vamos em suas Proprieties:

![](images/19.png)

Por default, eles vem desabilitados, para habilitar basta ir na aba de permissões.

![](images/20.png)

Basicamente existem duas maneiras de controlar quem acessa seus buckets, uma é pela Access Control List, a outra é Bucket Policy:

![](images/21.png)

Clique em Policy Generator:

![](images/22.png)

Em tipo de Policy, selecione S3 Type,
Podemos agora descrever o tipo de Policy que queremos.
No último campo, é sempre no seguinte formato: arn:aws:s3:::NAME-OF-YOU-BUCKET

Agora, na aba Management:

![](images/23.png)

Podemos replicar, tirar métricas, e criar um lifecycle para nossos buckets.
Ao replicar, mandamos para novas regiões.

Revisão:
1. Buckets tem namespace universao e único
2. HTTP200 é o code quando deu certo o upload
3. Podemos controlar o Bucket pela ACL ou pelas Buckets Policies
4. Por default, tudo é privado, vc precisa deixar algo publico pra ver o conteúdo dos bucktes
5. Server Side Encryption: nao da pra ler o arquivo se nao estiver logado na conta aws. Nem lendo o disco
