# Lifecycle Management and Glacier

###### Por que usariamos isso?
Porque podemos ter alguns dados que só serão utilizados por algum período de tempo, depois disso, queremos gastar o menos possível para armazená-los. E depois podemos mover para um warehouse ou até mesmo deletar.

Para isso, existe o Lifecycle Manager, dentro dos bucktes do S3.

Antes de tudo, iremos para o Glacier.
Na aba de serviços, selecione o Glacier:

![](images/01.png)

Glacier não está disponível para São Paulo - Brasil
Para isso, usaremos a região us-east-2 - Ohio-US

Essa é a cara do Glacier:

![](images/02.png)

Antes de usarmos o Glacier, vamos criar um novo Bucket, no S3

Crie um Bucket somente com Versioning ativado:

![](images/03.png)

Antes de fazer qualquer upload, vamos em Management.
Vamos adicionar uma nova regra de Lifecycle.

![](images/04.png)

Clicamos em Criar uma Nova Regra.

![](images/05.png)
Podemos inserir tags para afetarem alguns objetos especificamente

Next.

![](images/06.png)

Aqui podemos definir as transições que serão feitas, podem ser de versões anteriores ou somente da versão atual.
Depois definimos para onde e qual a quantidade de tempo da transição.

Assim, depois de 30 dias o arquivo irá para um bucket IA, e depois de 60 dias para um Glacier.

Next.

![](images/07.png)

Nessa seção podemos configurar o tempo de expiração do arquivo. Tanto da versão atual, quanto de versões antigas. O valor default é 425 dias.

O seguinte aviso pode aparecer:
" You cannot enable clean up expired object delete markers if you enable Expiration. "
Isso diz que é necessário habilitar Expiration no seu bucket.

Next.

![](images/08.png)

Apenas um review de nossa regra, clique Save.

Agora podemos ver nossa regra:

![](images/09.png)

### Examp Tips:
1. Pode ser usado em conjunto com versionamento, mas não é necessário
2. Pode ser aplicado para versões atuais ou antigas.
3. Para mudar para um S3 Standard-IA é necessário 30 dias da data de criação.
4. Podemos mudar para um Glacier com 30 dias depois de movermos para um Standard-IA
5. Podemos deletar objetos com lifecycle
