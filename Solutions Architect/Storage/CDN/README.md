# CND - Content Delivery Network

Vamos na aba de serviços, usaremos o serviço de CloudFront:

![](images/01.png)

Caso nunca tenha criado uma distribuição antes, clique no botão:

![](images/02.png)

Existem dois tipos diferentes de distribuição:
* Web - para conteúdos web, como html, css, php, imagens e etc.
* RTMP - real time messaging protocol

Para o a distribuição web, devemos ter uma origin, seja um S3 ou algum site.
Para o RTMP, seus arquivos DEVEM estar em um S3

Vamos criar uma distribuição web:

![](images/03.png)

Origin Domain Name: é de onde vem os seus arquivos, ele se autopopula com os buckets que vc tem na sua conta.
Origin Path: mostra que é possível haver mais de uma origin para uma distribuição. É opicional
OriginID: uma descrição para a origin, porque É POSSIVEL HAVER MAIS DE UMA.
Restricted Access Bucket: todas as requests devem vir do cloudfront.

![](images/04.png)

Para o cache behavior:
A CDN usa multiplas expressões regulares para identificar as origins diferentes.
Viwer Protocol Policy: posso traçar um redirect para minha aplicação.
É possível mudar também o TTL, que é o tempo de cache de um objeto. Para isso ative o Customize Object Caching.

![](images/05.png)
![](images/06.png)

Podemos usar o serviço da Microsoft, Smooth Streaming.
É possível, também, restingir o acesso as URLs da sua aplicação, para um determinado grupo.

Para a parte de configurações da distribuição:

![](images/07.png)
![](images/08.png)

Sempre bom marcar todas as Edge Locations para armazenarem o cache do seu bucket, pois traz uma melhor performance.
AWS WAF Web ACL = podemos um firewall de web application para proteger sua CDN.
Alternate Domain Names(CNAMEs) = alias para seu domínio.
A CDN tem um certificado próprio, mas é possivel colocar o seu próprio cert.

Agora podemos criar nossa CDN.

![](images/09.png)

Podemos clicar em Distributions para ver a que criamos:

![](images/10.png)

Pode levar algum tempo até tudo estar pronto:

![](images/11.png)
![](images/12.png)

Podemos pegar o DomainName e verificar nossos objetos
https://domain/[object]

E agora isso está na EdgeLocation mais próxima a nós

Ao clicarmos no ID de nossa CDN:

![](images/13.png)

Clicamos na aba Origins.

![](images/14.png)

Vemos que é possível criar múltiplas origens para nossa CDN, bem como editá-las.
Lembre-se que o bucket precisa de permissão de leitura para funcionar.

Na aba de Behaviors:
![](images/15.png)

é aqui que podemos editar as expressões regulares, por exemplo:
os arquivos pdfs devem vir do bucket01
os arquivos jpegs devem vir do bucket02
e por aí vai.

Na aba de Error Pages:
![](images/16.png)

Podemos editar as páginas de erro para os usuários

Na aba de Restrictions:
![](images/17.png)

Podemos restringir o acesso a nosso site em alguma determinada região.
Marque a Geo Restriction e clique em Edit.

![](images/18.png)

White List: paises para os quais a CDN vai distribuir conteúdo
Black List: paises para os quais a CDN não vai distruibuir conteúdo.

Não podemos colocar um pais na whitelist ou na blacklist ao mesmo tempo.

Na aba Invalidations:

![](images/19.png)

Podemos invalidar objetos em cache em nossas Edge Location, assim podemos removê-lo
Mas isso é pago.

Para deletar uma CDN:
- Volte em Distributions
- Marque a CDN que quer apagar
- Clique em Disable

![](images/20.png)

Depois de disabilitada, podemos removê-la.
