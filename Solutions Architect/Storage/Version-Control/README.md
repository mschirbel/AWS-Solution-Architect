# Versioning

Primeiramente logamos na conta root da AWS. Acessamos o Bucket que criamos.

Vamos nas propriedades desse bucket, e clicamos em Versioning:

![](images/01.png)

Uma vez que Versioning foi habilitado, nao pode ser desabilitado. Somente suspendido.

Lembrando que, caso seus arquivos sejam grandes, custos elevados decorrem.

Agora, pegue algum arquivo de texto, e faça upload para o seu bucket.

Não esqueça de deixar o arquivo publico:

![](images/02.png)

Agora, faça qualquer alteração no arquivo.

Vamos fazer o upload do arquivo para a AWS novamente.

![](images/03.png)

Veja que agora temos a versão disponível do arquivo.
Perceba que, a publicidade do arquivo não se mantém! É necessário deixá-lo público novamente.

Vamos agora deletar o arquivo:

![](images/04.png)

Isso deletou o objeto. Ou será que não?

Clique em Versions: Show

![](images/05.png)

Existe uma marcação 'Delete' no objeto que acabamos de deletar, mas ainda é possível ver o objeto se clicarmos em cima dele.

Se clicarmos com o botão direito em cima de um objeto com a tag 'Delete' vamos deletar a marcação!

![](images/06.png)

Vemos que o arquivo foi restaurado:

![](images/07.png)

Version Control pode ser algo bem custoso.

##### Como fazer pra deletar permanentemente?

Bom, isso pode ser feito por meio do MFA Versioning

Com o MFA Versioning vc pode:
1. mudar a versão do seu bucket
2. permanentemente deletar um objeto

Mas isso requere a autenticação por meio de um device cadastrado.

## Exam Tips:
1. Guarda todas as versões de um objeto
2. Bom para backup
3. Uma vez habilitado, só pode ser suspenso
4. Usando Versioning MFA é adicionado uma nova camada de segurança
