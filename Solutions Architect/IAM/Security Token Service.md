# STS - Security Token Service

Garante a usuários acesso temporario e limitado a recursos AWS.

Usuários podem vir de 2 fontes:

1. Federation(Active Directory)
  * Usa SAML - é tipo um YAML mas para Security.
  * Não precisa ser um usuário do IAM
  * Não precisa de credenciais IAM para logar
2. Federation with Mobile Apps
  * Usam Facebook, Amazon, Google
  * Permite que usuários de uma conta acessem recursos de outra conta AWS.

## Key Terms

* **Federation** é uma combinação de listas de usuários em um domínio, como IAM com a lista de um outro domínio, tipo Facebook.
* **Identity Broker** é um serviço que toma a identidade de um ponto A e a federa no ponto B
* **Identity Store** são serviços como AD, Facebook, Google
* **Identities** são usuários de um serviço como Facebook, Google, Amazon.

## Cenário

Por exemplo, caso a sua aplicação esteja dentro da sua VPC, mas algum usuário possa logar usando o Facebook, ele só pode ter acesso a determinados recursos da AWS.

O que vai determinar se o usuário é valido, seria um Identity Broker, que vai validar o usuário no Facebook e federá-lo na AWS.

Para fazer isso, faz uma call ao AWS STS chamando a função *getFederationToken()*

Isso inclui os recursos e a duração que o usuário ficará logado no sistema.

Com essa função, quatro valores são retornados para a função:

1. Access Key
2. Secret Access Key
3. Token
4. Duration

*Isso é importante lembrar para o exame*

## Passos para verificação

1. Usuário loga com Facebook.
2. Esse user e senha é dado para o Identity Broker
3. Valida com o Active Directory - LDAP
4. Se for validado, contata o STS
5. STS retona a função *getFederationToken()*
6. Isso vai para a aplicação.
7. A aplicação chama o recurso da AWS.
8. Esse recurso da AWS valida se o usuário pode acessar.

## Active Directory

É possível autenticar com AD, usando SAML.

Sua autenticação acontece primeiro no AD e depois recebe uma credencial temporária.

E não é necessário usar qualquer credencial AWS para acessar o console.