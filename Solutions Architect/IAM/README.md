# Lab de IAM

Primeiro logue na sua conta AWS.
No topo, vc verá a aba de Services. Clique.
Procure por Security, Identity & Compliance
Nesse grupo de Serviços, verá o IAM:

![](images/local_iam.png)

Note que IAM não tem uma região:

![](images/iam_is_global.png)

Quando se cria um usuário, uma role, um grupo, eles ficam disponíveis por todo o mundo
Não importa qual região vc esteja

Perceba que existe um IAM user sign-in link:

![](images/iam_link_signin.png)

Esse número não é aletório, é o número da sua conta AWS.
Também pode ser obtido clicando no nome do seu usuário -> My Account

É possível customizar esse número, criando um Alias.
Isso será um novo DNS Namespace

![](images/customize_aws_number.png)

Percebemos agora, o Status da nossa Segurança:

![](images/security_status.png)

Note que somente o primeiro foi marcado, ainda precisamos completar os outros 4.

Para o segundo, devemos saber o que é primeiro uma root account.
Uma root account é o e-mail que foi usado para criar a conta AWS.
Quando logamos com esse email, temos acesso root a AWS
Isso te dá acesso ilimitado ao cloud. E não queremos isso, queremos restringir.
Só os adms podem ter acesso ao root, por exemplo: RH nao deveria ter root.

Para corrigir isso, expandimos essa aba e clicamos em Manage MFA:

![](images/manage_mfa.png)

Vemos que existem dois tipos de MFA
	- virtual
	- hardware

Vamos usar o virtual, clicando em Next Step:

![](images/mfa_virtual_next.png)

Next Step novamente.

Aparecerá um QR-Code na tela, para lê-lo devemos usar o Google Authenticator.
Pode ser encontrado tanto na AppStore quando na PlayStore.

Basta abrir o aplicativo e clicar em Scanner com Código de Barras.
Vai gerar um código:

![](images/google_autenticador.png)

Agora devemos digitar os dois códigos, para isso:
Coloque os 6 dígitos. Espere os números mudarem. Coloque os novos 6 dígitos:

![](images/six_digits_mfa.png)

Aparecerá uma mensagem de confirmação.

Basta dar um refresh na página que aparece o certinho ;)

Agora vamos para a terceira aba: Create Individual IAM users.

Clicamos em Manage Users. Clicamos em Add Users:

![](images/add_users.png)

No campo User Name, podemos adicionar quantos usuários quisermos.

![](images/adicionar_users.png)

Na parte inferior podemos ver: Selec AWS access type.
Podemos acessar a AWS de duas formas diferentes:
- Por meio da console de Gerenciamente, que é basicamente o site que estamos usando.
- Ou Programmatic Access, que é quando suas aplicações acessam a AWS.

Nesse caso, queremos que os usuários tenham os dois tipos de acessos:
Quanto a senha, podemos deixar de forma aleatória, ou customiza-la
E, por boas práticas, devemos deixar marcado que os usuários mudem a senha:

![](images/access_type.png)

Clicamos em Next: Permissions.

Vemos aqui que podemos adicionar users aos grupos
Criar permissões espelho
Adicionar Politicas

![](images/set_permissions.png)

Vamos criar um novo grupo.

Podemos ver que a AWS já tem várias políticas default.
Devemos dar um nome ao nosso grupo.
E selecionar quais políticas queremos. Podemos criar as nossas também clicando em Create Policy:

![](images/create_group.png)

Podemos ver mais sobre como são criadas essa políticas. São arquivos .JSON. Podemos ver seu conteúdo se clicarmos na setinha do lado do nome.

Quando escolher as políticas, basta clicar em Criar Grupo.
Podemos ver agora como ficou o que escolhemos.
Devemos também deixar selecionado, caso queira que os users tenham aquele grupo.

![](images/grupo_criado.png)

Clicamos em Next: Review

Essa tela somente mostra o que fizemos:

![](images/review_users.png)

Agora podemos ver nossos usuários criados:

![](images/users_criados_sucesso.png)

Podemos ver algumas coisas:
- o tickzinho do lado, indicando o status do user
- access key & secret access key: tokens que são usados quando queremos interagir com AWS via aplicação. Para executar comandos direto da linha de comando e interagir com a AWS.
- username & password: é usado quando logamos na console
- gerar um .csv, caso haja muitos users.
- enviar email com as instruções

Só é possível ver esses detalhes uma vez, se perdemos, teremos que gerar de novo.

Vamos clicar em close. Podemos ver que nossos users aparecem na console:

![](images/users_console.png)

Vamos agora criar um novo grupo, e vamos colocar algum user nele:

No menu da esquerda, clicamos em grupos. Create New Group.

Damos um nome para o grupo:

![](images/grupo_bichas.png)

Dessa vez, só daremos permissões para lerem algo de um S3:

![](images/read_only_s3.png)

Crie o Grupo.

Agora, entre no grupo de Admins e remova os usuários do grupo:

![](images/remove_users.png)

Volte para o menu dos grupos.

Entre no novo grupo e adicione os usuários lá

![](images/grupo_bichas_users.png)

Podemos ver as Permissões do grupo, clicando na aba de Permissions do grupo.

Clicaremos em Users, e vamos em algum dos usuários que não são admins.

![](images/permissoes_user_grupo.png)

A permissao de ReadONly vem do grupo, agora, podemos adicionar permissoes exclusivas a algum user, clicando em Add Permissions.
Ao invés de adicionar a um grupo, vamos adicionar uma política.

Full Glacier.

Podemos ver que foi adicionada quando entramos novamente no user.

Podemos entrar na aba Security Credentials.

![](images/security_credentials_yago.png)

Podemos deixar inativo a access key, assim ele não acessará por aplicação a AWS.
Podemos gerar novamente as keys.

Voltamos a nossa DashBoard.

Agora só falta: Apply an IAM password policy. Abrimos a aba e clicamos em Manage Password Policy.

![](images/password_policy.png)

Vemos as seguintes informações:
- Tamanho mínimo da senha
- Algumas requisições para a senha, como letra maiuscula, minuscula, caractere especial
- Permitir que users troquem suas senhas.
- Senhas expirarem ou não(e seu período)

![](images/exemplo_password_policy.png)

Se voltarmos ao Dashboard, todos os status estão OK.

Agora falta um último detalhe: Roles.

No menu da esquerda, clicamos em Roles.

Roles são permissões a determinadas entidades. Pode ser uma aplicação, algum use, outro serviço.
Qualquer interação com a sua cloud, seja interna ou externa.

Clique em Create a Role:

Vamos criar uma Role que permite EC2 escrever files em algum S3.

![](images/create_role.png)

Role Type é dividido pelos serviços que a AWS oferece. Clicamos em EC2:

![](images/roles_ec2.png)

Selecione o Case:

![](images/roles_case.png)

Next:Permissions

Agora deveremos selecionar as policies para nossa Role, assim como foi com os users:

![](images/role_s3.png)

Next.

Agora devemos dar um nome para nossa Role.

![](images/role_review.png)

Create Role.

Quando criar um EC2 poderemos ativar essa role para ele.

Voltando ao DashBoard.
