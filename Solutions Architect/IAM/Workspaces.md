# Workspaces

É um VDI - Virtual Desktop Infrastructure.

É um desktop na cloud. Permite que o usuário faça tarefas diáras como se estivesse usando um desktop tradicional.

Pode se conectar usando qualquer tipo de device, como mobiles, tables, Kindles.

As credenciais são feitas por um administrador ou podem existir em um AD se a AWS estiver integrada com esse AD.

## Quick Facts

1. Usa Windows 7
2. Pode ser personalizado
3. Pode instalar suas aplicações, tem acesso adm local.
4. São persistentes - não pede os seus dados.
5. Todos os dados no disco D:\ tem backup de 12 em 12h - **muito importante lembrar disso**
6. Não precisa de conta AWS para ter um workspace.