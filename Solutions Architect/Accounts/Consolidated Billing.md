# Consolidated Billin

AWS Organization é um serviço de administração de contas AWS.

Funciona em dois sets:

1. Consolidated Billing
2. All Features

A organização tem uma conta que faz o pagamento. E é possível linkar as outras contas com essa, de forma que fique até mais barato.

É possível escrever Policies para organizar melhor os acessos das contas.

## How it Works
    
* Root Account
  * Dev
  * Prod
  * BackOffice

A Root vai receber a conta das outras três, e não pode acessar o recurso dessas contas.

**Existe um limite de 20 contas linkadas**

Para adicionar mais, tem que ser requisitado nesse ![link](https://aws-portal.amazon.com/gp/aws/html-forms-controller/contractus/aws-account-and-billing)

## Best Practices

1. Sempre habilitar MFA na conta root
2. Usar uma senha forte na conta root
3. Não usar recursos na conta de pagamento. Ela só serve para pagar.
4. Estabelecer Billing Alerts tanto na conta de pagamento, quanto nas individuais

## Exam Tips

1. Consolidated billing permite descontos
2. Unused reserved instances são aplicadas através do grupo
3. CloudTrail tem que ser ativado por conta e por região
4. Podemos centralizar as logs do CloudTrail de várias contas em um só bucket.