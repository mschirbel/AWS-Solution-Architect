# AWS Organizations

É um serviço que permite administrar múltiplas contas AWS de uma vez. Também é possível aplicar policies a esses grupos.

## Funções:

1. Centralizadamente administrar policies entre múltiplas contas
2. Controle de acesso aos serviços da AWS
3. Automação de criação de contas
4. Consolidate Billing entre múltiplas contas.

### Central Management

Permite criar grupos e colocar policies aos grupos.
Isso pode ser feito dentre múltiplas contas

### Controle de Acesso

Permitir ou negar serviços a certas contas.
Exemplo: Kinesis é proibido para o time de RH

E SCP(Service Control Policies) faz um override em IAM. Mesmo que o user tenha permissão, a policy não vai permitir.

### Automate AWS Account Creation

Criar contas e habilitar heranças entre grupos e usuários.

### Consolidated Billing

Organiza as contas da empresa. E ainda reduz o custo.

---

### Set up de AWS Organization

Logue no console da AWS.

Para entrar no serviço de Organization, não vai ser na tab de Services, mas sim, em Helpful Tips, no lado direito:

![](images/01.png)

Clique em Start Now abaixo de Create an Organization

Clique em Create Organization:

![](images/02.png)

Você recebrá um e-mail para verificar a conta mestre:

![](images/03.png)

Ao clicar em Adicionar Conta, vemos:

![](images/04.png)

Podemos convidar uma conta já existe na AWS, ou criar uma nova conta.

Após convidar algum e-mail, esse email deverá confirmar a conta.

Agora podemos criar nossa organização. Clique em Organize accounts:

![](images/05.png)

Clique em +New Organizational Unit:

![](images/06.png)

Assim podemos desenhar a nossa empresa, dentro da AWS.

E veja que ao lado, temos a estrutura:

![](images/07.png)

Podemos entrar dentro de uma unidade e criar novas:

![](images/08.png)

E a nossa estrutura:

![](images/09.png)

Ao clicarmos em algum usuário, podemos mover para alguma unidade:

![](images/10.png)

### Policies

Clique em Policies:

![](images/11.png)

Temos aqui a default policy, que é a de acesso total.

Vamos criar uma nova Policy:

![](images/12.png)

Aqui devemos declarar a regra principal, se vai ser de Deny ou Allow.

Com deny, tudo fica permitido, exceto aquilo que estiver preenchido.
Com allow, tudo fica proibido, exceto aquilo que estiver preenchido.

Embaixo, basta marcar os serviços que serão usados.

Por exemplo, caso eu queria negar EC2 para algum grupo:

![](images/13.png)

Para inserir uma policy a algum usuário, ou grupo, basta clicar em:

![](images/14.png)

Agora podemos colocar as policies:

![](images/15.png)

Assim, o meu time de Development não terá acesso ao EC2.