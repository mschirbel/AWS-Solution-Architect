# Cross Account Access

Muitos clientes usam diversas contas AWS para adminsistrar os departamentos internos.

Isso possibilita separar os recursos por área e também algum certo nível de segurança.

Cross Account Access facilita trabalhar num ambiente com múltiplas contas(ou multi-role) pois permite trocar as roles dentro do AWS Console.

É possível acessar outra conta, sem ao menos lembrar a senha do outro usuário.

## Steps

1. identificar os números de contas
2. criar um grupo no IAM
3. criar um user
4. logar em produção
5. criar uma policy para buckets
6. criar uma role
7. logar em dev
8. criar uma policy inline
9. aplicar essa policy no grupo de dev
10. logar como user de dev.

## Lab

### Criando um grupo na conta de Dev

Entre na sua conta de desenvolvimento. Vá até IAM e crie um grupo:

![](images/16.png)

Para as Policies desse grupo:

![](images/17.png)

### Criando um usuário

![](images/18.png)

Insira o nome de usuário, senha e desmarque o checkbox para que não seja necessário trocar a senha após o login.

Adicione esse usuário ao grupo de desenvolvedores:

![](images/19.png)

Agora temos um usuário dentro do grupo de developers. Esse usuário pode logar usando o seguinte link:

![](images/20.png)

Esse link é único, é composto de:

https://[seu id de usuário].signin.aws.amazon.com/console

Agora logue usando a sua conta destinada a Produção.

### Criando um bucket

Crie um novo bucket que será dividido entre as contas:

![](images/21.png)

A ideia é que o developer só pode adicionar recursos a esse bucket.

### Criando uma policy

Clique em Policy e Create Policy. Selecione para criar sua policy:

![](images/22.png)

Dê um nome, uma descrição e para o texto, copie do arquivo My-Prod-Policy.txt.

![](images/23.png)

Entretando, precisamos trocar a linha onde está:

```json
"Resource": "arn:aws:s3:::productionapp"
```

Para:

```json
"Resource": "arn:aws:s3:::[NOME DO SEU BUCKET]"
```

Isso funciona porque S3 é global.

Clique para validar sua policy, e espere aparecer:

![](images/24.png)

### Criando uma Role

Dentro de IAM ainda, clique em Roles e crie uma nova:

![](images/25.png)

Selectione o tipo de Role, aqui deve ser marcado que a role será Across Accounts:

![](images/26.png)

Agora entre com o ID da conta de Developer e se quiser, pode pedir MFA.

Agora temos que inserir a policy que criamos, nessa role.

![](images/27.png)

Agora logue de volta na conta de Developer.

### Acessando a conta de Produção

Agora vá até os grupos e acesse o grupo de desenvolvedores.

Vamos adicionar uma nova policy inline. Para isso, abra a aba Inline Policies

![](images/28.png)

Use uma custom policy:

![](images/29.png)

O código dessa policy está em anexo, só não se esqueça de trocar:

```json
    "Resource": "arn:aws:iam::[ID DE PRODUÇÃO]:role/[Nome da Role na conta de Produção]"
```

![](images/30.png)

Agora vamos logar como John.
Agora queremos trocar de Role, basta clicar no nome:

![](images/31.png)

Clique em Switch Role

![](images/32.png)

Agora temos que colocar o número de nossa conta de produção, o nome da Role(é case sensitive), um DisplayName e uma cor.

![](images/33.png)

Veja que fica marcado qual a Role e em qual conta está assumindo:

![](images/34.png)

Se clicarmos em S3, podemos ver todos os buckets de nossa conta de produção, podendo, até mesmo, fazer uploads:

![](images/35.png)

Mas, só temos permissão de entrar nos buckets, que nos foi dado permissão.

Se quiser voltar para a conta do usuário:

![](images/36.png)