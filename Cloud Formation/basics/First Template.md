# Criando uma Intância EC2

Então vamos colocar um Elastic IP e dois SG.

Logue na sua conta da AWS. Pesquise pelo serviço da CloudFormation.

Só para estar tudo claro, trabalhe na region de N.Virginia.

---

Ao entrar no serviço, clique em Create a New Stack.

Na próxima tela, podemos escrever nosso template, ou usar algum pronto ou até mesmo fazer upload, seja no seu PC ou em algum S3.

Escolha fazer um upload. E selecione o arquivo 1-introduction/0-just-ec2.yaml

Dentro do arquivo, podemos ver que somente estamos criando uma instância, dentro da region de N.Virginia e usando um t2-micro

Clicamos em Next e esperamos. O script está subindo para a AWS e sendo validado.

Agora, damos um nome para nossa stack. Stack é o conjunto de recursos que serão criados pelo CF.

Podemos dar Tags para nossos recursos. Essas Tags são para a stack, caso precisemos dentro dos recursos, podemos adicionar no yaml.

Não precisamos de IAM Roles. Basta criar.

---

## Abas

### Events

Agora aguardamos a stack terminar de criar. Podemos acompanhar a criação na aba de Events.

### Resources

Podemos ver aqui quais recursos foram criados em nossa stack.

Se formos em EC2, temos algumas outras tags dentro da nossa instância.
Temos a tag que colocamos na stack e algumas que o CF coloca automaticamente para identificar a qual stack pertence qual instância.

Note que, uma vez escolhido o nome da stack, não é possível trocar depois.

## Update Stack.

Caso desejemos mudar a nossa stack, podemos somente trazer um novo template para a AWS e ela automaticamente irá reconhecer.

Para isso, vá em Stack, e dentro de Action, selecione Update Stack.

Veja que o código está em code/1-introduction/1-ec2-with-sg-eip.yaml 
Nesse código tem um exemplo de SG e como atribuir um Elastic IP para um EC2.