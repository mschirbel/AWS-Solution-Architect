# Designer

É uma ferramenta que auxilia a visualizar o template, ou a criá-lo.

Basta clicar em Designer, na página inicial do CloudFormation.

E é basicamente um grab-and-drop.

## Documentation

Podemos ver a documentação ao clicar com o botão direito sobre um recurso.

## Links

Podemos linkar recursos, ao arrastar o círculo roxo de um recurso a outro.

Por exemplo:

![](../images/Anotação-2019-02-04-222045.jpg)

**Ao linkar objetos, lembre-se que: você deve encontrar o ponto correto. Cada ponto é de um link específico**

É possível também validar o template e depois fazer o upload da stack.

![](../images/Anotação-2019-02-04-222428.jpg)