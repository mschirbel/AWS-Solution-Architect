# Template options

Existem alguns parâmetros que são comuns a quase todos os CF:

1. Tags
2. Permissions
3. Notifications
4. Timeouts
5. Rollback
6. Stack Policy

## Tags

Permite que possa identificar os recursos

### Permissions

Atribuir roles para recursos

### Notifications

E-mail ou SNS Topics para pessoas corretas

### Timeouts

Garantir que CF falhe caso passe de um certo tempo

### Rollback

Permite que o CF volte a algum certo período em caso de falha

### Stack Policy

Proteger alguns recursos para que nunca sejam modificados ou deletados.

## Onde podemos ver isso?

Na aba de Options, na criação de uma stack. Alguns deles estão debaixo da aba Advanced.