# AWS

É um provedor de servidores em nuvem, criada pela Amazon.

AWS tem revolucionado a TI, por exemplo, a Netflix está lá.

Mas atualmente a AWS tem muitos serviços, por isso, foi criado um jeito de administrar tudo usando código

## CloudFormation

É um jeito declarativo de prover os recursos da AWS.

Por exemplo, será declarado os SG, EC2, RDS e por aí vai.

E tudo é criado na order em que foi declarado, com a configuração que foi declarada no script.

## Benefits

Mais controle. Pois tudo é feito pela própria AWS.

Podemos ter controle de versionamento com o git, por exemplo.

Todas as mudanças na infra serão feitas via código. Isso é claro e transparente a todos.

Cada recurso dentro da stack é taggeado, logo, sabemos quanto custará toda a stack.
Podemos estimar o custo por mês.

Podemos gerar diagramas da stack. É bom para mostrar para o chefe.

Existem diversos templates na web. Provavelmente vai ter algo já bem feito na web.

## CloudFormation vs Ansible vs Terraform

CF sempre vai ter as últimas features da AWS.

Ansible e Terraform usam APIs que devem ser atualizadas.

Sempre que for usar algo da AWS, prefira CF.