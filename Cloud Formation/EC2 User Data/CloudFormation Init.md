# cfn-init

Geralmente está nesse formato:

```
Resources:
    MyInstance:
        Type: "AWS::EC2::Instance"
        Metadata:
            AWS::CloudFormation::Init:
                config:
                    packages:
                    groups:
                    users:
                    sources:
                    files:
                    commands:
                    services:
        Properties:
```

Packages: instala uma lista de packages no SO
Groups: grupos de usuários
Users: cria users e insere nos grupos
Sources: baixa arquivos
Files: cria arquivos ou pega de alguma URL
Commands: roda algum comando
Services: sysvinit

## Packages

Podemos usar: rpm, yum/apt, python, rubygems

Podemos especificar uma versão, caso seja necessário.

Exemplo:

```
rpm:
    epel: "http://download.fedoraproject.org.pub/epel/5/epel-release-5.4-noarch.rpm"
yum:
    httpd: []
    php: []
    wordpress: []
rubygems:
    chef:
        - "0.10.2"
```

Quando existir um "[]" é porque queremos a última versão.

## Groups and Users

Quando queremos ter usuários e grupos distintos em nossa instância.

```
groups:
    group1: {}
    group2:
        gid: "45"
users:
    myUser:
        groups:
            - "group1"
            - "group2"
        uid: "50"
        homeDir: "/tmp"
```

## Sources

Serve para baixar arquivos, sejam grandes ou pequenos, para diretórios na sua instância.

```
sources:
    /etc/puppet: "https://github.com/user/cfn/demo/master"
    /etc/myapp: "https://s3.amazonaws.com/mybucket/myapp.tar"
```

## Files

Controle sobre qualquer conteúdo. Podemos criar arquivos ou pegá-los de alguma URL

```
files:
    /tmp/mysql:
        content: !Sub |
            CREATE DATABASE ${DBName};
            FLUSH PRIVILEGES;
        mode: "000644"
        owner: "root"
        group: "root"
```

Esse !Sub é uma função para fazer substituições, isso server para o ${DBName}, o !Sub vai avaliar isso, e vai substituir pelo parâmetro encontrado para DBName.

## Commands

Podemos usar os comandos um de cada vez, na ordem alfabética. Podemos também direcionar para algum diretório e enviar variáveis de ambiente.

É possível rodar testes para saber se os comandos foram executados ou não.

```
commands:
    test:
        command: "echo \"$MAGIC\" > test.txt"
        env: 
            MAGIC: "I come from the environment"
        cwd: "~"
        test: "test ! -e ~/test.txt"
        ignoreErrors: "false"
```

cwd é o diretório no qual queremos rodar o comando.

Use commands somente se o restante não funcionar, use somente em último caso.

## Services

Podemos iniciar e controlar os serviços ao iniciar uma instância:

```
services:
    sysvinit:
        httpd:
            enabled: "true"
            ensureRunning: "true"
        sendmail:
            enabled: "false"
            ensureRunning: "false"
```

