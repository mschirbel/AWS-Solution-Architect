# cfn-hup

É para avisar uma instância para ver os metadados e checar se houve alguma mudança. Se houver, aplicar as configurações novas.

Sempre tem um delay de 15min.

Para criar um arquivo de hup conf:

```
 "/etc/cfn/cfn-hup.conf":
              content: !Sub |
                [main]
                stack=${AWS::StackId}
                region=${AWS::Region}
              mode: "000400"
              owner: "root"
              group: "root"
```

Para criar um arquivo de reload hup:

```
"/etc/cfn/hooks.d/cfn-auto-reloader.conf":
              content: !Sub |
                [cfn-auto-reloader-hook]
                triggers=post.update
                path=Resources.WebServerHost.Metadata.AWS::CloudFormation::Init
                action=/opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --resource WebServerHost --region ${AWS::Region}
              mode: "000400"
              owner: "root"
              group: "root"
```