# cfn-signal

Usamos o init para iniciar as configurações;

Usamos o signal para saber quando terminou e avisar o template se deu tudo certo ou não.

Temos que usar juntos com uma CreationPolicy e um timeout para saber se a instância terminou o init ou não.

Se o update não funcionar, vai ter um rollback.


```
CreationPolicy:
    ResourceSignal:
        Timeout: PT5M
```



