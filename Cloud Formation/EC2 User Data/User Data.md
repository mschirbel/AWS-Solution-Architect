# User Data

Usamos o console para criar uma instância EC2.

No exemplo, vamos instalar PHP e MySQL em uma instância usando um user-data script.
Esse script é aquele usado na inicialização da instância.

Ao criar uma instância, na fase 3. Configure Instance, na aba Advanced Detais, podemos inserir nosso script.

Será algo mais ou menos assim:

```bash
#!/bin/bash
yum update -y
yum install -y httpd24 php56 mysql55-server php56-mysqlnd
service httpd start
chkconfig httpd on
groupadd www
usermod -a -G www ec2-user
chown -R root:www /var/www
chmod 2775 /var/www
find /var/www -type d -exec chmod 2775 {} +
find /var/www -type f -exec chmod 0664 {} +
echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php
```

Agora, vamos usar isso, dentro de um CF Template.

Para isso, dentro de um recurso, use a tag UserData

Desse modo:

```
 UserData:
        Fn::Base64: |
           #!/bin/bash
           yum update -y
           yum install -y httpd24 php56 mysql55-server php56-mysqlnd
           service httpd start
           chkconfig httpd on
           groupadd www
           usermod -a -G www ec2-user
           chown -R root:www /var/www
           chmod 2775 /var/www
           find /var/www -type d -exec chmod 2775 {} +
           find /var/www -type f -exec chmod 0664 {} +
           echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php
```

O caracter "|" determina que tudo que está depois será do jeito que está escrito, ou seja, com as quebras de linhas. Isso vai para o shell também.

Podemos ver o script em code/9-cfn-init/1-ec2-user-data.yml

## Problems with EC2 User Data

E se quiséssemos uma instância muito grande? Ou se quiséssemos mudar o script, sem terminar a instância?
Como modificar isso para ficar mais fácil de ler?

Para isso, temos o CF Helper Scripts:

1. cfn-init: instala packages, cria arquivos
2. cfn-signal: sincronizar recursos para atuarem em ordem
3. cfn-get-metadata: puxa os metadados de um recurso
4. cfn-hup: update nos metadas quando detecta mudanças.

Flow: cfn-init -> cfn-signal -> cfn-hup