# Troposphere

Troposphere permite usar Python para escrever um template no CF.

Pode ser visto ![aqui](https://github.com/cloudtools/troposphere)

Podemos gerar um CF válido e dinâmico.
E podemos ter condições complexas, o que é bom.

Mas temos que escrever um código Python, que vai gerar um JSON.

Temos um exemplo em code/10-advanced/1-troposphere-example.py