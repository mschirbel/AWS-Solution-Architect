# AWS CLI

Vamos usar a cli da aws para fazer um deploy de um template.

Podemos criar, deletar ou atualizar templates.

Também é muito bom para parameters.

## Instalar aws cli

Primeiro passo é instalar Python, seja 2.6.5+ ou 3.3+

Depois, basta rodar o comando:

```
pip install awscli --upgrade --user
```

Agora basta digitar

```
aws
```

Agora precisamos configurar um profile.
Para isso, vá no console da sua conta, clique no seu nome e depois em My Security Credentials

![](../images/Anotação-2019-02-07-103922.jpg)

Agora é clicar em Chaves de Acesso e em Criar nova:

![](../images/Anotação-2019-02-07-104037.jpg)

Faça o download. E guarde os valores de Access Key e de Secret Access Key.

Agora basta usar um

```
aws configure --profile [seu profile]
```

Insira suas keys e a region que deseja trabalhar.

Para criar uma stack:

```
aws cloudformation create-stack --stack name [nome da stack] --template-body [path para o template] --parameters [path para o arquivo de parameters] --profile [seu profile] --region [region q vc quer subir o template]
```

Esse arquivo de parametes, pode ser visto em code/10-advanced/0-parametes.json.

