# Best Practices

## Deletion Policy

Com DeletionPolicy temos uma prevenção de apagar recursos ou, em alguns casos, fazer até um backup antes.

Temos alguns casos:

1. Delete: AWS vai realmente deletar
2. Retain: AWS não vai deletar
3. Snapshot: Não são todos que podem, mas os que tem suporte a Snapshot, podemos apagar e manter um snap.

## Custom Resources with AWS Lambda

Custom Resources permite escrever o que quiser em um template. Por exemplo, recursos que não existem no CF ainda, mas podem ser acessíveis pelo Lambda.

## Best Practices

Temos dois tipos de arquiteturas:

1. arquitetura de camadas
2. arquitetura de serviços

Na primeira, temos cada template com as camadas, como por exemplo: um template com a VPC, DB, Webserver
Na segunda, temos cada template com os serviços, como por exemplo: um template para cada microserviço.

---

É sempre bom usar cross stack. Ajuda a deixar mais organizado.

Crie um template que possa ser usado para dev/test e prod; Ou em várias regions.

**Nunca use credenciais no código**

Nunca faça nada manual nas instâncias. Sempre use o CFN Init.

Use Deletion Policies em Databases ou em dados importantes.

## Cost Estimation

Basta fazer o upload da stack e clicar em Cost na aba Review.

![](../images/Anotação-2019-02-07-114438.jpg)