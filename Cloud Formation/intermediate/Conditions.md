# Conditions

São usadas para controlar a criação de recursos ou export de outputs.

Podem ser usadas de diversos jeitos, como por exemplo

1. environment
2. region
3. az
4. etc

```
Conditions:
    Logical ID:
        Intrisic function
```

Logical ID é o nome da condição
intrinsic function pode ser:

1. Fn::And
2. Fn::Equals
3. Fn::If
4. Fn::Not
5. Fn::Or

## Exemplo

Como criar uma condition:

```
Conditions:
  CreateProdResources: !Equals [ !Ref EnvType, prod ]
```

Como aplicar:

```
MountPoint:
    Type: "AWS::EC2::VolumeAttachment"
    Condition: CreateProdResources
```

Parameter de referência:

```
Parameters:
  EnvType:
    Description: Environment type.
    Default: test
    Type: String
    AllowedValues:
      - prod
      - test
    ConstraintDescription: must specify prod or test.
```

## Fn::GetAll

É uma função para pegar atributos de quaisquer recursos criados.

Sintaxe:

```
!GetAtt NomeDoRecurso.NomeDoAtributo
```

Exemplo:

```
!GetAtt EC2Instance.AvailabilityZone
``` 