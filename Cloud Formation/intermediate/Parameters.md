# Parameters

É um jeito de colocar entradas em um template.

É importante porque é possível reutilizar um template. Isso ajuda muito a empresa como um todo.

Parameters são tipados e controlados para evitar erros.

## Settings

### Tipos

1. String
2. Number
3. List
4. CommaDelimitedList
5. AWSParameter - como por exemplo um ID de um Security Group
6. Description
7. Constrains
8. ConstrainsDescription - para saber o erro de uma constrain
9. Min/Max Length
10. Min/Max Value
11. Defaults
12. Allowed Values
13. Allowed Pattern
14. NoEcho - não vai ser exibido

## Exemplo

Temos o arquivo code/3-parameters/0-parameters-hands-on.yaml para servir como base.