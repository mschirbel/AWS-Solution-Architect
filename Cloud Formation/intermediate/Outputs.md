# Outputs

É uma seção opcional que mostra valores que podemos importar para outras stacks.

Por exemplo, caso você crie um template somente para a VPC, você pode trazer o output como VPC ID ou Subnet ID para outras stacks.

É a melhor forma de relacionar stacks. E colaborar com outros times.

## Export

Para exportar o nome para outras stacks, use a sintaxe:

*Para que o output seja usado em outra stack, ele deve ser Exportado*

```
Outputs:
  StackSSHSecurityGroup:
    Description: The SSH Security Group for our Company
    Value: !Ref MyCompanyWideSSHSecurityGroup
    Export:
      Name: NomeDoRecurso
```

## Import

Para importar o nome de outras stacks, use a sintaxe:

```
  - !ImportValue NomeDoRecurso
```

Saiba que, o NomeDoRecurso deve ser globalmente única em sua conta.

## Remover

Não é possível remover uma referência sem que as dependências sejam deletadas antes.

Exemplo: Se uma instância EC2 depender de um SG, não é possível apagar o SG antes de apagar a EC2 instance.