# Mapping

Mappings são variáveis fixadas em um template do CF.

Podem ser usadas para diferenciar ambientes, como dev e prod, regions na AWS ou AMIs.

Todos os valores estão hardcoded.

Mappings são bons quando conhecemos os valores que serão tomados. Como regions, AZ, contas na AWS. Parameters são bons quando dependemos de usuários para inserção.

## Find in Map

Para encontrar um valor de um key:

```
!FindInMap [MapName, TopLevelKey, SecondLevelKey]
```

Exemplo:

```
Mappings:
  AWSRegionArch2AMI:
    us-east-1:
      HVM64: ami-6869aa05
```

Para referenciar esse mapa:

```
ImageId: !FindInMap [AWSRegionArch2AMI, us-east-1, HVM64]
```

## Pseudo Parameter

AWS oferece alguns pseudo parameters, que podem ser usados em qualquer lugar, em qualquer order. São habilitados por default.

1. AWS::AccountID
2. AWS::NotificationARNs
3. AWS::NoValue
4. AWS::Region
5. AWS::StackID
6. AWS::StackName