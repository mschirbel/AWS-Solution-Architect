# Resources

Representam os componentes da AWS que serão criados e configurados.

Podem referenciar outros recursos.

A AWS que vai daterminar a ordem de criação, exclusão e atualização dos recursos, baseado no seu template.

Sempre seguem os mesmos identificadores

```
AWS::aws-product-name::data-type-name
```

Podemos encontrar as docs nesse ![link](https://docs.aws.amazon.com/pt_br/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html)

---

## Atributos

Dentro do template, não importa a ordem dos recursos, pois, no final, a AWS que irá determinar a ordem de criação.

Mas existem atributos que podem alterar algumas ordens default:

- DependsOn: server para criar dependência entre dois recursos
- DeletionPolicy: não exclui o recurso mesmo que a stack seja deletada.
- CreationPolicy
- Metadata: inserir dados.

---

## FAQ

1. Posso criar dinamicamente?
   R: Não, tudo é criado de forma procedural. Sem lógica de programação.
2. Todos os Recursos Existem?
   R: Quase todos, se não existir, você pode usar Lambda.