# Metadata

São dados opcionais e arbitrários que podemos incluir para ter detalhes sobre algum recurso.

Temos algumas keys especiais:

1. AWS::CloudFormation::Designer - mostra pro Designer o (x,y) para o desenho
2. AWS::CloudFormation::Interface - grupos e ordem de entradas no console.
3. AWS::CloudFormation::Init - usado para CFN Init.

## Designer

![](../images/Anotação-2019-02-05-232517.jpg)

Podemos ver que ao usar o Designer, os metadados são inseridos nos recursos.

## Interface

Define a ordem de entrada ao usar uma stack.

Isso é bom para organizar os parameters.

```
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: "Network Configuration"
        Parameters:
          - VPCID
          - SubnetID
          - SecurityGroupID
      - Label:
          default: "Amazon EC2 Configuration"
        Parameters:
          - InstanceType
          - KeyName
    ParameterLabels:
      VPCID:
        default: "Which VPC should this be deployed to?"
```

E na hora do deploy seria algo mais ou menos assim:

![](../images/Anotação-2019-02-05-232852.jpg)