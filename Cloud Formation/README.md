# Curso para AWS CloudFormation

Link: https://www.udemy.com/aws-cloudformation-master-class/learn/v4/content

## Pré Requisitos

1. conhecimento basico da aws
2. conhecimento de JSON ou YAML
3. windows/mac/linux recentes

## Publico Alvo

1. Devs
2. DevOps Engineer
3. AWS Solutions Architects

---