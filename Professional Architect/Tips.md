# Tips for the Exam

O exame num nível profissional é muito mais de resistência do que de conhecimento técnico.
O conhecimento é importante, mas praticar questões é o que realmente interessa na estratégia.

### Estratégia:

- identificar os pontos mais importantes. Tempos, volumes, tipos de arquiteturas, máquinas, serviços utilizados e etc.
- escrever os pontos mais importantes
- identificar respostas similares, e realçar a diferenças.
- eliminar respostas totalmente erradas(sempre vai ter).
- eliminar as respostas que são confusas ou contra as melhores práticas da AWS.
- entre as respostas que sobrarem, pegar a que mais se adequa com os pontos mais importantes e com as melhores práticas da AWS.

Pode acontecer das duas estarem corretas, mas devemos escolher a mais apropriada.

---

Responder primeiro as questões mais fáceis, e por último as que são confusas. Deixar pra depois as que não sabemos mesmo a resposta.