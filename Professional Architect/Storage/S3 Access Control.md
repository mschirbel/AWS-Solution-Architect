# S3 Access Control

Quais elementos de segurança são aplicáveis ao S3?

Todos os buckets são privados por default. Somente o dono do recurso pode acessar. E o bucket confia no dono da conta(root/admin).

E só posso dar acesso por IAM de acesso ao bucket, para usuários dentro dessa conta, ou roles. Então se alguém de fora da conta quiser acessar, ele precisa de uma role interna.

## Bucket Policies

São aplicadas **somente ao bucket**. Não aos usuários do IAM. As permissões são aplicadas a **todos** os objetos do bucket.

Isso acontece porque a bucket policy não considera usuários do IAM, ou seja, todo mundo é não autorizado.
E a policy permite ou nega acesso a um usuário particular:
    - seja ele anônimo
    - a um *principal*, que pode executar PUT ou DELETE
    - a um IP
    - a horas do dia

Podemos inclusive, usar tags para restringir acessos a objetos. Pois podemos usar tags até em objetos.

## S3 Access Control List

É um legado de segurança(AWS não recomenda mais).

Permite acesso a usuários de outras contas AWS, ou ao público.
Tanto os bucket quanto os objetos possuel ACL.

E é com o ACL que podemos acessar um objeto usando uma URL. Assim, podemos ter interações com outras contas pra fazer, por exemplo, *log delivery*. Como centralizar logs em um único bucket em uma única conta.

## Pre Signed URL

Todas as vezes que acessamos um objeto, a AWS avalia:
    autorização
    autenticação

Mas podemos dar a um indivíduo permissões *temporárias*. Para não ser permanente.

Para fazer isso, podemos usar o comando:

```
aws s3 presign <BUCKET ARN>/<OBJECT>
```

Essa URL acessa o objeto com o security check da hora da criação da URL. E temos uma data de expiração também.

**A autenticação e autorização vem do usuário que criou a URL**.

Mas mesmo assim, se o usuário que gerou a URL, perder as permissões, a URL não será mais válida.