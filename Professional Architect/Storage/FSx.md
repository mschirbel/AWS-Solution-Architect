# FSx

EFS é um NFS. o FSx é parecido mas com ele podemos usar file system de outros fornecedores. Podemos acessar até mesmo de instâncias Windows.

Para isso, fazemos um AD Trust, usando o MicrosoftAD e é possível fazer um DFS Replication entre os file systems. 
O único problema é que não é HA por definição.

![](../media/fsx.png)

Temos quase a mesma arquitetura que o EFS, temos que conectar com uma VPC, e temos um SG. Mas os arquivos estão restritos a uma única subnet, logo, uma única AZ.

Podemos, também fazer o VSS(Shadow Copy) para o S3.

Temos a forma de segurança também usando KMS, criando uma CMK. Além do Security Group nas instâncias. Mas o mais importante é configurar o AD Trust no FSx.