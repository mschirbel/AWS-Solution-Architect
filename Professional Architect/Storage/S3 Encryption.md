# Encryption

Para fazer a criptografia do S3 podemos fazer de duas formas:
    - client side
    - server side

Se usarmos client side, somos totalmente responsáveis de fazer a criptografia **antes** do upload para o S3.

Se usarmos server side, podemos utilizar algumas formas:
    1. SSE-C: usando uma customer key
    2. SSE-KMS: usando o KMS
    3. SSE-S3

Mandamos um objeto não criptografado e recebemos um objeto descriptografado.

#### SSE-C

A key é usada para a criptografia e depois descartada. O customer é responsável por fazer a rotação de keys.
Não podemos usar esse método com CRR.

#### SSE-KMS

![](../media/s3-kms.png)

Funciona assim:
1. KMS cria uma chave master, chamada de CMK
2. Ao fazer o upload de um objeto a CMK gera uma key para a criptografia do objeto
3. KMS retona duas versões da key do objeto: uma cifrada e outra em plain text
4. S3 usa a chave em plain text para cifrar o objeto e guarda junto com a chave cifrada
5. Ao solicitar o download de um objeto(cifrado), pegamos junto a key(também cifrada)
6. Manda essa chave(cifrada) para o KMS que usa a chave CMK para fazer a descriptografia da chave
7. volta a chave em plain text para o S3
8. S3 descriptografa o objeto.

Sobre a CMK podemos:
    - definirmos rotacionamento
    - permissoes em usuário
    - permissoes entre contas
    - auditoria em cloudtrail

Podemos usar isso com CRR, mas precisamos de permissão.

#### SSE-S3

O S3 cuida da criptografia e a key(object) é guardada com o objeto. É o método default, no qual a key(master) é rotacionada regularmente, e tudo é controlado pelo S3.

---

Cuidado para não confundir isso com a criptografia do bucket, pois isso seta como default qualquer tipo de criptografia de objetos.
E podemos usar bucket policies para forçar upload criptografado.