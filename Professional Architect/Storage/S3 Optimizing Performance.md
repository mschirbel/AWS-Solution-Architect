# Optimizing Performance

## Standard vs Multipart Upload

Usando o upload standard, o S3 usa somente **uma stream** de dados. E isso implica em algumas limitações:
    - objetos só podem ter até 5GB
    - velocidade limitada
    - falhas de conexão podem fazer o upload falhar

Com o multipart, o S3 cria um ID. Depois podemos dividir o arquivo em algumas partes, e fazer o upload de cada uma delas, usando esse ID, o S3 se encarrega de juntar o arquivo. A limitação vai até 10000 partes, com até 5GB em cada uma, e um tamanho total de 5TB.

Com isso, temos mais velocidades e resiliência contra falhas de redes. E somos cobrados pelo upload de cada uma das partes.

## Transfer Acceleration

Precisamos habilitar em cada bucket. Então, ao invés de usar o endpoint regional, recebemos novos endpoints. Fazemos o upload para uma edge location e de lá, será feito a transferência para o bucket usando a rede da AWS, que é muito mais rápido.

Podemos fazer um teste de velocidade por ![aqui](http://s3-accelerate-speedtest.s3-accelerate.amazonaws.com/en/accelerate-speed-comparsion.html).

## Partitions and Object Naming

Podemos ter uma estrutura de pastas, dentro do S3, usando os `prefixes`. As partições são determinadas pelos `prefixes` de um objeto.

E cada partition, dentro do bucket, tem a mesma performance:
    - 3500 puts/sec
    - 5500 gets/sec

Só consideramos isso se precisarmos de uma peformance maior do que essas métricas.