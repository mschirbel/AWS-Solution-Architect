# Versioning and Locking

## Versioning

Versionamento é adcionar um ID **único** para cada objeto.
Nunca posso desabilitar o versioning, apenas suspender.

Com o versionamento, posso fazer download de versões específicas;

Quando for feito o upload de um arquivo com o mesmo nome os dois objetos ficarão no bucket, mas com um ID diferente(é um pra cada objeto), e o último arquivo vira a última versão. Quando for feito uma requisição ao arquivo, sempre será da última versão, por default.

Cada versão do objeto é única, e podem ocupar tiers diferentes de storage. E o delete de um objeto nunca ocorre realmente, apenas adcionamos uma marca de *deleted* na última versão e a última versão vai para o próximo ID.

Para fazer o delete completo do objeto, precisamos deletar todas as suas versões anteriores. E por manter versões anteriores, temos mais custo.

## Locking

Object locking tem dois métodos:
    - períodos de retenção: previne um tempo para o objeto não ser deletado com uma data de expiração
    - *legal holds*: previne que o objeto seja deletado indefinidamente.

E precisamos de versionamento habilitado.