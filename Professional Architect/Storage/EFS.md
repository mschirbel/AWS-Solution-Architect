# EFS

Com o EFS temos a capacidade de criar um file system dividido. Podemos acessá-lo de várias EC2 ou até mesmo do On-Prem.

Lembre que os Instance Store é um disco efêmero, e o EBS é um network storage; e com ambos não era possível fazer um sistema distribuído entre diversas AZ.

É aí que o EFS entra, pois é um sistema de arquivos distribuído e altamente performático.
Esse FS pode ser `mount` em diversas EC2 e até no On-Prem. É algo já bem comum no Linux, usando o NFS v4/v4.1

A diferença entre arquitetar com S3 ou EFS, é a habilidade de fazer o `mount` no SO. É ótimo para aplicações que tem processamento paralelo ou quando precisamos dividir *media* entre instâncias.

## Arquitetura

EFS vive dentro de uma VPC, por isso funciona em várias AZ ou até mesmo no On Prem.
O EFS usa o protocolo NFS para fazer a comunicação. Para boas práticas, criamos os *mount targets* em cada uma das AZ, para resiliência. Cada mount target terá um IP e um Security Group.

O custo é feito pelo tanto que estamos usando. E o tamanho é gigantesco.

##### Performance e Throughput

Temos o modo de performance:
    Só podemos escolher quando criamos um EFS. Temos dois modos, o General Purpose e o Max I/O. Em 99% dos casos usaremos o General. Max I/O usamos quando precisamos **muito** da leitura e escrita.

E temos o modo de Throughput:
    Podemos escolher mesmo depois de criado, temos o Bursting e o Provisioned. Geralmente usamos o Provisioned para pequenos dados saindo constantemente e o Bursting para saídas irregulares. Bursting é o mais comum.

## Segurança

Temos criptografia em duas formas:
    - in transit
    - at rest

Para os dados *at rest* temos que usar uma CMK do KMS.
Para os dados *at transit* temos que fazer a configuração quando criamos os mounts nas instâncias.

## Install

Para melhor controle do EFS, temos que instalar as ferramentas:

```
sudo yum install -y amazon-efs-utils
```

E com isso podemos ver os progressos lá na console da AWS. Se eu não instalar essas ferramentas nas instâncias, eu preciso fazer o mount usando o IP fornecido.

Tudo o que precisamos agora é o ID do FS. Para fazer o mount:

```
cd /
sudo mkdir /efs
sudo mount -t efs <EFS ID>:/ /efs
```

Esse último comando vai falhar, pois não temos comunicação entre as instâncias. Precisamos usar o security group correto nas instâncias. Rode o comando novamente para funcionar, após trocar os SG.

```
touch efs/efs.txt
```

Vá na outra instância e veja que o arquivo está lá.

Para usar a chave de criptografia, podemos usar o comando:

```
sudo mount -t efs -o tls <EFS ID>:/ /efs
```

## Backup

Temos o serviço da AWS Backup para fazer backups automáticos de todo o EFS.
E o AWS DataSync para trazer dados para o EFS.