# Cross Region Replication

Com essa feature podemos fazer os objetos de um bucket(origin) serem replicados em outro bucket(destination) em outra region.

CRR só funciona de uma única direção e não é retroativo, ou seja, só funciona a partir do momento que habilitamos o CRR.

A classe de storage e os donos são mantidos no destination bucket. Mas não as lifecycle rules. Nem são replicadas se o dono não tiver permissões no bucket destination.

A arquitetura é assim para o caso de uma única conta e múltiplas contas:

![](../media/s3-crr.png)
Caso for usar em outra conta, marque a opção de *Change object Ownership to destionation bucket* para trocar as permissões na outra conta.

--- 

Também podemos replicar objetos criptografados *desde que não seja uma criptografia custom*.