# S3 Glacier

Quando trocamos a classe de um objeto no S3, e selecionamos Glacier, ele vai para um produto específico, chamado de Glacier.

Nesse produto, temos a capacidade de criar *vaults*. Vaults são os *buckets* do Glacier. E colocamos nossos arquivos dentro desse vault. O nome do vault precisa ser único em cada região, por causa do DNS para upload.

Um Vault tem uma região específica, são criados em uma e listam os conteúdos de uma, diferente do S3.
Podemos ter até 1000 vaults por região por conta.

## Arquitetura

Os objetos no vault são chamados de *archives*. Cada um deles tem uma description, e um ID. É com esse ID que são localizados e listados por região.
É possível realizar uma operação chamada de *inventory* que é um reconhecimento de todos os archives de uma região, bem como sua data de upload, tamanho e ID.

Archives não tem nome, somente um ID e a descrição(dada quando são feitos o upload). E não podem ser editados, somente deletados.

## Download Archives

Para fazer o download, precisamos de um *job*.

Pode ser uma operação que demora, e temos 3 tipos de velocidade:
    - expedited: jobs que demoram de 1 a 5 min. É o mais caro.
    - standard: jobs que demoram de 3h a 5h
    - bulk: é mais econômico e demora de 5h a 12h

Por default, sempre que fazemos um download, fazemos o download do archive inteiro. Mas é possível requisitar somente um componente do archive.