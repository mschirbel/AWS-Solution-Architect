# File Gateway & Volume Gateway

Storage gateway tem 3 versões:
    - file gateway
    - volume gateway
    - tape gateway

É útil quando já temos um ambiente on-prem e podemos usar como:
    - forma de migrar os dados para a aws
    - extender a capacidade de storage do on-prem

### Tape Gateway

Serve para backups, quando temos uma *tape library* nos servidores on-prem. Mas isso é perigoso por que as tapes podem degradar, serem perdidas.

Com o tape gateway podemos integrar isso via um endpoint. Podemos usar o iSCSI como protocolo de comunicação com o S3/Glacier.
Os backups saem do tape gateway e são replicados para o S3, e depois rotacionados para o Glacier.

### File Gateway

Usamos quando temos um uso muito grande de storage e precisamos expandir. Podemos usar o NFS ou SMB como protocolos de comunicação, fazendo um Mapping 1:1 de um arquivo com um Objeto no S3.

### Volume Gateway

São volumes mapeados no on-prem com a AWS. Mas não pense que vai pro EBS, **vai para o S3**.
Podemos fazer snapshots de nossas instâncias e mandar para o S3, como snapshots e backups.