# S3 Lifecycle

O poder do S3 vem da possiblidade de criar regras de lifecycle. Assim podemos mover nossos arquivos entre os diferentes tier de storage para otimizar custos e controle de acesso.

Ao criarmos uma regra, definimos as regras para automação de transição.
Podemos fazer isso para as versões de nosso buckets, ou para versões passadas, o que pode ser muito útil.

Existem certas direções para as regras, não podemos ficar indo e voltando com os objetos, a hierarquia é a seguinte:

Standard -> Standard IA -> Intelligent Tiering -> One Zone IA -> Glacier -> Glacier Deep Archive

*Não podemos subir na hierarquia*.

---

Se os objetos forem criptografados, eles permaneceram assim durante toda a transição.