# S3 Architecture

Storage é uma das partes mais importantes do exame.
S3 é um sistema de storage de objetos. Não são arquivos, pois não são guardados em FS.

Um objeto é uma entidade isolada que carrega quaisquer metadados. Pode ser um texto, imagem, video, audio e etc.

Um bucket pode ter um número quase ilimitado de objeto, e existem diversas classes de buckets, para finalidades e custos diferentes.
Podemos controlar granularmente os objetos e seus acessos.

Bem como definir lifecycle de objetos, para gestão de objetos.

Podemos também sustentar sites e objetos estáticos. Assim distribuindo usando o cloudfront.
E também podemos usar com o Storage Gateway(conectar o on-prem com a aws)

---

O S3 é um serviço global, e isso é importante, porque o nome do bucket precisa ser único no mundo inteiro.
Entretanto, podemos colocar nossos objetos em alguma region para deixar mais próximo do cliente. E só podemos ter até 100 buckets por conta, isso pode ser aumentado via ticket.

O nome do bucket é muito importante, principalmente quando usamos para um site, pois o nome precisa ser igual ao domínio:
    - nao podemos ter upper-case
    - nao podemos ter "_"
    - nao podemos terminar em "/"
    - nomes entre 3 e 63 caracteres
    - tem que começar com uma letra minuscula ou numero
    - evitar espaços

---

S3 guarda os objetos em formatos de key-value. Sendo a key o nome do objeto. Podemos ter uma key simples como `file.txt` ou complexas como `files/file.txt`.
O value seria o conteúdo do objeto. A sequencia de bytes, indo de 0 até 5TB.
Ainda é possível guardar:
    - version: quando o bucket é versionado ele guarda um id da versao sendo exibida
    - metadata: outros key-values para saber owner e características de sistema
    - suboject: ACL ou informações de torrent
    - ACI: access control information - que são as permissões do objeto.

### Static Web Hosting

Podemos transformar um bucket em um webserver, no qual podemos usar:
    - html
    - css
    - javascript

Isso custa bem pouco e temos um endpoint único.

Um ponto importante disso é habilitar o CORS, para que outros domínios possam acessar recursos do nosso site.

### Policies

Podemos controlar os acessos de um bucket das seguintes maneiras:
    - IAM
    - bucket policies
    - ACL

Isso é imporante porque o S3 fica na zona pública da AWS, e precisamos proteger e também de dentro da nossa VPC.

### Events

Buckets podem ativar eventos, assim temos uma arquitetura de event driven, passando para SNS, SQS ou Lamdba.

### Biling

É imporante dependendo do tier do storage, mas existem alguns que são padrões:
    - colocar dados para **dentro** do S3 é de graça
    - **tirar** dados do S3 tem custo
    - GB/month dos objetos que estão lá
    - PUT/GET por arquivos

Claro que isso varia de acordo com o Tier.