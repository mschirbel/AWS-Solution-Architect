# S3 Tiers

Existem as seguintes classes de storage:
    - standard
    - intelligent-tiering
    - standard-ia
    - one zone-ia
    - glacier
    - glacier deep archive

Todos os objetos possuem uma storage class, podem ser ajustadas manualmente ou via lifecycle policies.
Elas determinam o custo, durabilidade, disponibilidade, byte-latency(é o tempo que demora pra fazer o donwload do primeiro byte)

Todos as classes possuem 99.9999999999% de durabilidade

## Diferenças nos tiers

|                        | Standard                         | Infrequent Access                          | One Zone IA                            | Glacier                                          | Glacier Deep Archive                                   |
|------------------------|----------------------------------|--------------------------------------------|----------------------------------------|--------------------------------------------------|--------------------------------------------------------|
| Propósito              | geral, acesso rápido e frequente | objetos importantes mas não tão frequentes | objetos não críticos mas reproduzíveis | arquivação de longo prazo não serve para backups | arquivação de longuíssimo prazo não serve para backups |
| Disponibilidade        | 99.99%                           | 99.9%                                      | 99.5%                                  | 99.5%                                            | 99.5%                                                  |
| Replicação             | 3+ AZ                            | 3+ AZ                                      | 1 AZ                                   | 3+ AZ                                            | 3+ AZ                                                  |
| Byte-latency           | milisegundos                     | milisegundos                               | minutos                                | minutos ou horas                                 | horas                                                  |
| Custo                  | 0,023 USD por GB                 | 0,0125 USD por GB                          | 0,01 USD por GB                        | 0,004 USD por GB                                 | 0,00099 USD por GB                                     |
| Tamanho mínimo         | não há                           | 128KB                                      | 128KB                                  | 40KB                                             | 40KB                                                   |
| Custo de pegar objetos | não há                           | há                                         | há                                     | há                                               | há                                                     |


---

Se uma AZ morrer e nossos dados estiverem no S3 One Zone IA, podemos perder os dados

## Intelligent Tier

É feito para acessos que não seguem padrões. Tem a possibilidade de mover os objetos para um tier que é otimizado para acesso frequente e outro que é otimizado para acesso não frequente.

Não pagamos pelo retrieval fee.