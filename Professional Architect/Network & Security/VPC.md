# VPC

Virtual Private Cloud.

Com isso temos domínios privados. Assim podemos separar de outras redes a nossa. E também podemos fazer conexões com outras contas.

Podemos ter uma rede privada seja na AWS ou com uma conexão com on-premisse.

Temos 3 regiões de redes:
    - a rede privada, dentro da aws
    - a rede pública, fora da aws
    - a rede pública da AWS - como S3, DynamoBD e CWLogs, no qual podemos ter acesso via internet pública, mas desde que tenhamos permissões.

A VPC atua na rede privada, dentro da AWS.
E são totalmente regionais, e por default não há comunicação entre elas.

## Default VPC

Só podemos ter uma default VPC, que é criada assim que criamos a conta. Muitos serviços da AWS dependem dessa VPC.

Se deletarmos, podemos configurar somente uma outra para ser default.
São sempre configuradas do mesmo jeito com o mesmo IPv4 CIDR.

## Basics

Toda VPC tem um IPv4 CIDR.
O maior range que podemos ter numa rede é */16* e o menor é */28*.

Se for usar IPv6, temos que aplicar para toda a VPC.

E podemos ter 2 tenancies:
    - default: podemos ter hosts dedicados para nossa vpc
    - dedicated: só temos hosts dedicados

Quando criamos uma VPC pode default são criados:
    - NACL
    - DHCP options(configuramos o que usamos de DNS, NBIOS e NTP)
    - route table

E agora precisamos criar uma subnet. É nela que colocaremos os nossos recursos. Uma subnet está localizada em uma AZ específica. E precisam ocupar um range menor do que o da VPC.

**Mas como saber quantas subnets eu precisarei?**
Primeiramente precisamos saber quantas camadas teremos na nossa aplicação. Sendo webservers, databases, dmzs e etc.

E sempre podemos criar com uma subnet a mais, sendo o nosso buffer. Pois as coisas mudam e podemos precisar dessa gordurinha depois.

E para definir o range do CIDR, sempre fazemos a divisão, como no exemplo abaixo:
10.10.0.0/16 tem um tamanho X
10.10.0.0/17 tem um tamanho X/2
10.10.0.0/18 tem um tamanho X/4
10.10.0.0/19 tem um tamanho X/8
10.10.0.0/20 tem um tamanho X/16

A regra de ouro é:
Usar um tamanho que seja X/2^n, sendo *n* o número de camadas de nossa aplicação, contando com o buffer.

## IP's Reservados

1. 10.0.0.0 - Network Addres
2. 10.0.0.1 - VPC Router
3. 10.0.0.2 - DNS
4. 10.0.0.3 - Para uso futuro
5. 10.0.0.255 - Broadcast

Esses IP's não podem ser usados dentro de uma VPC.