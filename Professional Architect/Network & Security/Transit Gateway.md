# Transit Gateway

VPC Peer não suporta Transiting Routing.
Mas para suprir isso, temos o Transit Gateway.

Vamos supor que uma empresa tem uma rede interna e precisa de conexões entre todas as suas VPCs bem como com a sua rede interna.

Com o TG podemos ter um em uma região e attacher as VPC's por meio de VPC Attachments e as VPN's por meio de VPN Attachments.

Permite transiting routing pois tem múltiplas route tables e também temos hierarquias entre elas.
E ainda pode ser controlado pelo RAM, ou seja, podemos controlar redes em até outras contas.

Podemos ter o suporte de transit routing em AZ, ou habilitar em todas as AZ's para termos em VPC inteiras.
Tem uma limitação: **é fechado para regions**. Não podemos conectar redes em regiões distintas.

Outro ponto bacana, é termos a integração com DNS Resolution, de acordo com a sua VPC.

Depois de criar um TG, precisamos criar um TG Attachment, tudo dentro do serviço da VPC. Nessa parte que selecionamos se vai ser uma VPC ou VPN.