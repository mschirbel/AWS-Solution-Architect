# HMS

É bem relacionado com o KMS. O HMS providencia um hardware que pode gerar keys para o seu storage.
Por baixo dos panos, o KMS usa o HSM.

O HMS tradicional é um device físico que você:
    - compra
    - tem que fazer a gestão
    - cuidar das falhas
    - HA

Isso serve se vc **realmente** precisa controlar o seu hardware físico.
Assim, não precisamos usar um serviço da AWS como o KMS. 
Podemos usar APIs normais, como:
    - PKCS
    - JCE
    - CNG

KMS não suporta nenhuma das APIs acima, por isso usamos o HSM. É um serviço de HSM na AWS.

A necessidade de ser físico ou CloudHSM, depende dos padrões de segurança que temos. KMS só chega até o FIPS 140-2, enquanto que o HSM vai até o FIPS 104-3.