# KMS

É um serviço que controla chaves de acesso e também criptografia e descriptografia.

KMS gera CMK's, que são Customer Master Keys, que são representações lógicas de uma chave. Como uma backing key. É essa chave que vai criptografar e descriptografar os dados. 

**CMK nunca podem sair de uma region**. E podem fazer criptografia até 4KB.

CMK nunca saem do Hardware da AWS e só podem acessadas via API.
Para usá-la, temos que ter Key Policies, com Key Admins responsáveis.

Os comandos para gerar uma CMK:

```
aws kms create-key --description "PROD" --region sa-east-1
```

Receberemos um JSON com um arn específico. E agora criamos um alias para nossa Key. São importantes porque podemos referenciá-la de outros serviço.

```
aws kms create-alias --target-key-id <KEY-ID> --alias-name "KEYPROD" --region sa-east-1
```

## Como funciona

Depende muito da aplicação, mas o que podemos fazer é:

    - pedir ao KMS para gerar uma chave a partir da nossa CMK
    - temos dois retornos:
        - uma cópia criptografada da chave gerada
        - uma cópia em texto da chave gerada
    - criptografamos os dados com o arquivo em texto da chave gerada
    - descartamos esse texto
    - guardamos o objeto criptografado e a chave criptografada juntos

Para fazer a descriptografia, passamos ao KMS a chave gerada criptografada e ele nos retornará o objeto.
Para poder fazer isso, usamos os comandos abaixo:

```
aws kms generate-data-key --key-id alias/KEYPROD --key-spec AES_256 --region sa-east-1
```

Temos o retorno de um JSON com o plai text da chave.

Podemos ver o contéudo:

```
echo <CHAVE EM PLAIN TEXT> | base64 --decode
```