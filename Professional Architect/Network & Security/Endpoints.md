# Endpoints

VPC Endpoint permitem acesso a zona pública da aws, vindo de recursos de uma VPC, sem que tenham acesso a internet.

Até agora vimos que precisamos de um IGW para acessar a internet. Mas isso não é verdade. Pois quando temos uma VPC podemos usar um *Gateway Endpoint*.

Pois podemos preferir que os recursos tenham acessos a rede publica da aws(s3, sqs, cwlogs, etc), mas não tenham acesso na internet.

Temos dois tipos de Endpoints:
    - Gateway Endpoints
    - Interface Endpoint

## Gateway Endpoint

Pertencem a VPC e são referenciáveis em uma route table. Ou seja, ao invés de apontarmos a um IGW, podemos apontar para um Gateway Endpoint.

Geralmente apontamos em uma route table de uma zona privada. Usamos um *Prefix* e apontamos para o gateway. Para pegar o prefix, usamos o comando `aws ec2 describe-prefix-lists`.

Assim não precisamos de um NAT Gateway para acessar os serviços públicos da AWS.

*São HA* e usamos Policies para fazer o controle.

## Interface Endpoint

Não usam entradas na route table. Eles são entidades físicas em uma subnet.
Ele ocupa uma AZ, pois é físico. Diferente do Gateway Endpoint que é em uma subnet.

*Não são HA*. E usamos security groups para fazer o controle de acesso.

Usa um produto chamado Private Link, assim podemos criar IE dentro da nossa VPC e se comunicam com outras redes por um canal seguro. No caso, pode ser uma rede externa ou os serviços da zona pública da aws.

E qualquer comunicação com a outra rede é feita por DNS. Temos um DNS para cada AZ que selecionamos e outro para um da region.