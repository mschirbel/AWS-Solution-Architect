# NACL

São firewalls dentro de uma subnet de uma VPC.
São *stateless* filtros de IP.

Toda VPC tem um NACL por default. E toda subnet que não tiver um custom NACL, usará o default da VPC.

Subnets só podem ter 1 NACL associado.

Por default, eles permitem todo o tráfego para fora e para dentro da subnet. Ou seja, tem um *implicity allow*. Isso acontece porque a AWS prefere que vc use SG.

*Um NACL pode afetar o tráfego entre recursos de diferentes subnets, mas nunca de recursos em uma mesma subnet.*

## Rules Set

Lembre-se que o NACL é *stateless*, ele não consegue definir se uma comunicação de resposta vem de dentro ou de fora. Por exemplo:

Se estivermos nos comunicando de fora para um EC2 dentro de uma subnet. O NACL precisa que informemos que tanto a comunicação de fora para dentro existe quanto a de dentro para fora. Elas são independentes.

Por isso geralmente usamos *1024-65535* para Port Ranges, pois são efêmeros.

### Inbound

Quando o tráfego vem de fora da subnet para dentro.

### Outbound

Quando o tráfego vem de dentro da subnet para fora.

## Process Order

Sempre começamos com o menor número do **Rule #**. E isso importa pois quando algo for liberado ou negado não importa se algo depois faz a operação contrária. Ele trava na primeira regra de ordem.

## Use cases

Podemos usar o NACL para fazer proteções a uma EC2 pública, permitindo somente alguns ranges de IP.
Mas nunca podemos fazer uma regra com um device lógico, somente com ranges de IP.