# DNS

O DNS sempre está no IP reservado Network+2. Que é chamado de R53 Resolver. E somente é acessível de dentro da VPC.

O principal problema que temos que enfrentar com o DNS é quando queremos fazer a ligação de um ambiente On-Premises para a Nuvem, com a mesma resolução de DNS.

Para isso, usamos o R53 Resolver.
E permite que configuremos nosso DNS entre as redes externas e uma VPC.

Ele faz isso via *Inbound Endpoints* e *Outbound Endpoints*.

Um Inbound Endpoint fica dentro da VPC e serve para que a rede externa acesse. Ele só server para conectar a rede externa com o R53 Resolver.
Vai receber dois IPs dentro da VPC e assim a rede externa que fechou uma VPN vai poder se conectar.

Um Outbound Endpoint fica dentro da VPC e serve para que acessemos a rede externa. Só serve para conectar os recursos que estão na VPC com a rede externa.
Depois de criado, podemos criar *rules* que vão fazer o roteamento de domínios internos para servidores de DNS externos.

Quando temos um outbound endpoint, a chamada de DNS que fazemos dentro da VPC vai pro DNS interno da vpc, que manda para o outbount endpoint, esse por sua vez, roteia a chamada para o servidor de DNS que está apontado em sua configuração. O retorno volta pro recurso que fez a chamada pelo mesmo caminho que veio.