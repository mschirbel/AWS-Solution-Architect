# WAF

É um Web Application Firewall. Monitora, filtra e toma ações baseados no tráfego que chega em um determinado serviço.

É uma proteção que funciona para:
    - CloudFront(global)
    - API Gateway(region/global)
    - ELB(region)

Para que o WAF funcione usamos webACLs. Assim temos regras de **allow**, **deny**, **count**, associadas com os serviços que o WAF se integra.
As regras tem certas condições, como verificar um IP, número de requisições por minuto, algum tipo de SQL Exploit e etc.

Com essas regras podemos criar um webACL e tomamos ações de **allow**, **deny**.

Temos alguns tipos de Condições:
    - IP Address
    - Cross Site Script
    - Geo Match
    - Size Constrains
    - SQL Injections
    - String and Regex Matching

Entretanto, o WAF não é muito bom para enormes aplicações, pois temos muito overhead por ser algo explícito na declaração das regras.

# Shield

É um serviço que impede ataques DDoS. Podemos usar o modelo Standard, que funciona bem, mas o melhor é o Advanced que funciona melhor para grandes corporações.

O Shield é imporante porque ele evita que o tráfego sequer chegue ao recurso. Assim não pagamos o tráfego que o atacante causa.