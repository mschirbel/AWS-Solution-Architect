# Routing

Toda VPC tem um Router e opera como um device lógico.
É praticamente totalmente controlado pela AWS, mas tem um IP address fixo que é o IP Network +1. Um daqueles 5 IPs que fica reservado quando criamos a VPC.

É reponsável por fazer o tráfego entre a VPC e outras redes, sejam públicas ou privadas.

O jeito que podemos controlar o VPC Router é via Route Table.
Toda VPC cria uma Route Table, que é associada com todas as subnets criadas dentro daquela VPC.
Podemos criar uma custom route table e começa totalmente zerada. Só podemos ter uma route table associada com cada subnet, seja a Main Route Table(criada pela VPC) ou uma custom.

Toda Route Table começa com uma regra:

```
<VPC CIDR>  local   active  No
```

Com essa route fazemos todo o tráfego interno da VPC. Não podemos apagá-la e nem modificar. É assim que os recursos dentro de uma VPC são comunicáveis.

## Ordem das routes

Quanto menor for o range de IPs, maior a ordem de prioridade sobre as route table.
Ou seja, 10.10.0.0/16 tem menor preferência que 10.10.0.0/32

## Tipos de routes

Temos as routes estáticas, nas quais nós mesmos configuramos.
E temos as routes propagadas, nas quais são routes vistas por virtual private gateways.