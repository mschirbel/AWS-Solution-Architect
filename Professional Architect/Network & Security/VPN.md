# VPN

VPN usam a internet como canal para comunicação entre a sua rede on-prem e a AWS.
Tem um canal totalmente criptografado.

## Customer Gateway - lado do cliente

O primeiro componente é o Customer Gateway, na aba da VPC. Fica do lado do cliente e é geralmente um *router*. É capaz de IPSec usando routing dinâmico ou estático.

Do lado da AWS é uma representação lógicado do device físico que está na rede do cliente.

*Static Routing* é a forma mais fácil de fazer. Pois só precisamos indicar as subnets que estão disponíveis.
Se usarmos *Dynamic Routing* podemos fazer essa troca de IP addressing dos dois lados dinamicamente. 
Geralmente usamos o modo dinâmico em produção, no qual temos que providenciar um ![BGP ASN](https://en.wikipedia.org/wiki/Autonomous_system_(Internet)).

Depois de configurado o Customer Gateway vamos para o Virtual Private Gateway

## Virtual Private Gateway - lado da AWS

Depois de criado, temos que fazer o *attach* a somente uma VPC.
Mas podemos ter vários Customer Gateways terminando em um Virtual Private Gateway.

## Site-to-Site VPN

É onde criamos a ligação entre o Virtual Private Gateway e o Customer Gateway.

Para criar um ambiente totalmente resiliente, devemos ter 2 Customer Gateways, sendo cada um conectados a no mínimo dois Tunnel Endpoints(cada Tunnel Endpoint aponta para uma AZ).
E para isso, precisamos de uma VPN dinâmica.

Depois de criada a VPN, podemos clicar em *Download Configuration*, alterando o formato para *generic*.
Nesse arquivo temos a **Pre-Shared-Key**, que é a senha para autenticação em ambas as pontas.

Depois disso, precisamos adicionar uma rota para o CIDR da rede do cliente com o target que é o Virtual Private Gateway. Inclusive, podemos marcar Route Propagation.

## Diferenças de VPN Direct Connect?

- VPN são rápidas de fazer o setup, DC demora semanas
- VPN são baratas, o custo é por hora e dados de saída
- Performance é limitada na VPN, porque tem criptografia de ponta a ponta e por causa do hardware do Customer Gateway
- Tem latência

## Arquiteturas de HA para VPN

1. NO HA

![](../media/vpnnoha.png)

2. AWS HA

![](../media/vpnawsha.png)

3. FULL HA

![](../media/vpnfullha.png)