# Flow Logs

Assim temos visibilidades sobre o que acontece. Pois temos acesso aos metadados sobre o tráfego. Como por exemplo:

    - version
    - account id
    - protocol
    - bytes
    - start/end
    - action
    - log status
    - source port, dest port
    - packets

Mas o que não é logado?

    - DHCP
    - AWS DNS
    - Metadata de recursos
    - Licenses requests

Podemos colocar flow logs em três camadas distintas:
    - vpc
    - subnet
    - interface de rede de um recurso

E as informações são interpoladas. Se tivermos flow logs na vpc, veremos todas as logs das subnets também.

E podemos exportar os dados para CWLogs ou para o S3.

Um flow log está depois que selecionamos um VPC, nos menus que aparecem.
E também precisamos de uma IAM Role para que possamos interagir com os recursos e salvar no local que exportamos.

A policy é parecida com essa:

```json
{ 
    "Version": "2019-20-01",
    "Statement": [
        {
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:DescribeLogGroup",
                "logs:DescribeLogStream",
                "logs:PutLogEvents",
            ],
            "Effect": "Allow",
            "Resource": "*"
            
        } 
    ]
}
```

Se adicionarmos em uma VPC, as subnets que estão nessa VPC herdarão o Flow Logs. Bem como os recursos que estiverem essa VPC.
Mas não se esqueça que os tráfegos não são em *realtime*.