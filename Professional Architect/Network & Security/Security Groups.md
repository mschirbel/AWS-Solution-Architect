# Security Groups

São filtros de IPs, assim como os NACL. Mas são aplicados para interfaces de redes dentro de um recurso.

SG aplicam-se quando o tráfego vem e vai de um recurso, e não de uma subnet.
E esse é a principal diferença entre um SG e um NACL.

Podemos ter um SG aplicado a múltiplas interfaces. Ou seja, podemos ter várias instâncias EC2 com um mesmo SG.

E os SG são *stateful*. Ou seja, não precisamos nos preocupar com portas efêmeras. Podemos declarar as portas específicas quando temos as regras inbounds e outbounds.

Em SG não temos regras de ordem, são todos avaliados ao mesmo tempo. SG tem um *implict deny*. Por essa razão não podemos fazer um *explicit deny*. Só podemos fazer um allow.

Outra diferença é que podemos fazer uma ligação com devices lógicos, como outros SGs. Podemos colocar um SG como origem de inbound rules de outro SG.