# Subnets & Internet Gateway

## Public and Private Subnets

São subnets com uma configuração diferente. 
Privates são aquelas que não podem se comunicar com a Internet ou com a AWS Public Zone.
Publics são aquelas que podem.

Por default, quando são criadas, todas as subnets são privadas.
Então para ser pública ela precisa:
    - a vpc precisa ter um IGW attachada
    - uma route table que tem uma default route apontando para um IGW
    - subnets associadas com essa route table

Podemos editar as subnets associations(dentro da route table) sobre as quais queremos que estejam na zona pública

É interessante deixar marcado o Auto Assign de IP Público, pois qualquer recurso que estiver nas subnets públicas receberão um IP Público. Isso pode ser feito nas subnets, selecionando qual você quer e clicando em *Modify auto-assign IP settings*.

## Bastion

É uma EC2 que está subnet pública e aceita conexões vindas da internet pública.
E aí podemos usar o bastion para somente se comunicar com private subnets.

Assim protegemos a entrada de informações em nossas privates subnets.
Podemos configurar o login usando algum IAM privado, como o AD da Microsoft.

## NAT Gateway

É quando fazemos a tradução de um IP para outro.
O IGW é um tipo de NAT Gateway que usa NAT estático. Ele traduz de uma IP privada para um **único** IP público.

Use o exemplo de um Bastion em uma subnet pública:
    Sempre que o IGW receber um tráfego de um recurso que tiver um IP público ele troca esse *internal from address* por um *external from address*. Então um Bastion pode usar um IP público, pois a route table joga sua saída para um IGW, mesmo que o Bastion em si não tenha um IP público, pois está numa VPC.

Mas para recursos que estão na rede privada, nós não queremos que eles recebam um IP público.
É aí que entra o NAT Gateway.

UM NAT Gateway tem um IP público **único**. Eles traduzem um IP privado para um público, mas não fazem o contrário. O IP dele é um *Elastic IP Address*, ele não muda.

Para criar um NAT Gateway precisamos:
    - de uma subnet, na qual teremos um nat gw
    - um elastic ip(precisamos criar um)

E para acessar a internet, criamos uma Route Table(serão as privadas) para cada NAT GW. Então associamos as subnets privadas com essa Route Table. Depois criamos uma rota nessa Route Table com o target do NAT GW.

*Se vc quiser muita disponibilidade, use um NAT GW para cada AZ*.

## Egress Only Gateway

Imagine uma empresa que usa um NAT GW como saída pra internet de seus serviços. Isso pode causar um overhead de rede, visto que o NatGW usa o IPv4.

O IPv6 não deveria ter esse problema de congestionamento.
E na AWS todos os IPv6 são públicos.

Mas como todos os IPv6 são públicos, qual seria a necessidade de um NatGW?
Por isso não podemos usar o NatGW com IPv6.

É aí que entra o Egress Only. Temos a possibilidade de que todas os recursos que tenham um IPv6 se comuniquem com a Internet Pública, sem que haja o caminho de volta.
Mesmo que o IPv6 seja público, podemos colocar a instância numa zona privada, que assim poderemos fazer a comunicação com a internet e estarmos protegidos.

Internet Gateway   -> Permite Inbound e Outbound da Internet Publica
EgressOnly Gateway -> Permete Outbound para a Internet Publica