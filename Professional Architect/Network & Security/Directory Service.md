# Directory Service

É um aglomerado de produtos:
    - Amazon Cloud Directory
    - Microsoft Active Directory
    - Simple AD
    - AD Connector
    - Amazon Cognito

Precisamos saber quando usar e quando nunca usar cada um deles.
*Em geral usamos a Role de EC2-SSM para as máquinas do AD*.

Nas máquinas de client do MSAD, temos que instalar:
    - AD DS and AD LDS Tools
    - DNS Server Tools

## Simple AD

##### Vantagens

Barato e serve tanto para a AWS como pra fora da AWS. Integra com SSO para serviços como EC2 e Workspaces

##### Desvantagens

Não funciona para apps mais complicados. E não suporta Trust Relationships.

---

## Microsoft AD

##### Vantagens

Bem conhecido e tem HA por default, pois funciona em múltiplas AZ

##### Desvantagens

Mais caro que o Simple AD e não é bom para aplicações grandes

---

## AD Connector

##### Vantagens

Funciona como um proxy entre a AWS e o on-prem. Podemos ter um AD no on-prem e controlar da AWS.

##### Desvantagens

Não tem autenticação e autorização, precisa fazer isso por fora

---

## Amazon Cognito

##### Vantagens

Tem ID Federation, bom para apps mobiles e web.

##### Desvantagens

Não é bom para controle de diretórios

---

## Amazon Cloud Directory

##### Vantagens

Controla informações de objetos e suas relações.

##### Desvantagens

Não é bom para usuários, somente para diretórios.