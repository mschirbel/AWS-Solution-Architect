# Direct Connect

Direct Connect é uma "conexão direta" e física com a AWS.
Diferente da VPN, que passamos por vários peers entre a Internet até chegar a AWS, com o Direct Connect temos um link **direto** com a AWS.

É mais seguro e muito mais estável que uma VPN.
Podemos usar algumas velocidades de conexão:
    - < 1 Gbps
    - 1 Gbps
    - 10 Gbps

Depois de solicitar o CD, receberemos uma DX porta exclusíva na AWS. Daí receberemos uma LOA-CFA Letter of Authorization and Connecting a Facility Assignment. É uma autorização para iniciar uma conexão com a porta do DC da AWS com um dos seus roteadores. Aí teremos um cabo entre o seu DC e o DC da AWS.

## Benefícios:

    - redução na banda, pois não trafega ISP pela internet
    - menos custo de transferência pra aws
    - menos latência
    - mais consistência
    - nao precisamos de bastions
    - podemos usar várioas VIF para conectar em várias VPC's

## Architecture

Primeiro passo é submeter um pedido por uma DX Port, depois, fazemos os procedimentos e obtemos um cross connect. 
Criamos então um VIF Público e um VIF Privado. O Privado associamos com o VGW e com a VPC.
O VIF publico nos dá acesso com a AWS Public Zone e o privado com a nossa VPC.
Depois configuramos o equipamento on-prem para acessar o VIF.

## Direct Connect Gateway

É um serviço global, no qual podemos associar vários VIF e podemos associar com quaisquer VPG em qualquer region. Assim só precisamos manter 1 VIF da on-prem, pois só precisamos conectar com 1 lugar da AWS. Mas a comunicação não é transitiva entre VPC conectadas em um DCGw.

Antigamente tinha que ter 1 VIF on-prem pra cada region da AWS.