# Guard Duty

Guard Duty ele ingere logs de outros serviços como:
    - Log Flow
    - Cloud Trail
    - R53 DNS Query Logs
    - 3rd Pt solutions

E faz recomendações sobre ataques, IPs maliciosos e domínios, usando ML para alertar ou mitigar.
Podemos tomar ações no CW baseado nesses eventos.

Podemos criar uma rede de master-member de contas AWS para serem analizadas no Guard Duty. Podemos tentar levar isso para as organizações, mas temos que fazer isso independentemente uma da outra.