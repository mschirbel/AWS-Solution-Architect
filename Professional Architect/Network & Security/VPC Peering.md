# VPC Peering

Permite comunicação entre VPC's isoladas. Podemos fazer a conexão entre VPC's em regiões e até contas diferentes.
São objetos lógicos, logo, precisamos configurar as route tables dos dois lados.

A arquitetura fica assim:

![](../media/vpcpeer.png)

Geralmente fazemos isso quando queremos que dois recursos se comuniquem.
VPC Peer precisam de alguns passos para funcionar:
    - criação do peer
    - aceitação do peer
    - criação de rotas nas duas VPC(veja qual a subnet que o recurso está)
    - verificar se o NACL está negando algum trânsito, **nas duas vpcs**
    - liberação de security groups para os recursos.

Um dos poucos cuidados que devemos ter é o *overlapping* de IP's nos CIDR. Não pode haver interseção de IP's, desse jeito não funciona.

*Uma dica importante é*:
Durante a liberação de regra no SG, poderiamos liberar para todo o CIDR da rede convidada. O que é mais interessante, é liberar lógicamente para o SG da rede convidada. Entretanto, **isso só funciona quando estivermos na mesma region**.

Dentro da mesma region --> Podemos usar a referência lógica
Fora da region --> Só podemos usar IP's e CIDR.

## Transitive Routing

Uma coisa que devemos ressaltar é que não temos um *transitive routing*. Isso significa que, por exemplo:

1. Temos um Peer da VPC1 para VPC2
2. Temos um Peer da VPC1 para VPC3

**Não podemos comunicar a VPC2 com a VPC3 diretamente**. Temos que fazer isso usando a VPC1.

## DNS Settings

Podemos entrar nas Opções de DNS do Peering que criamos e permitir que os DNS sejam acessíveis pelas VPC's. Todos serão resolvidos para o DNS Privado.

## Limitações e Considerações

|                                        | Mesma Região | Diferente Região |
|----------------------------------------|--------------|------------------|
| Não pode Overlap de CIDR               |       x      |         x        |
| Peer não transitivo                    |       x      |         x        |
| IPv6 não suportado por default         |       x      |         x        |
| SG podem ser referenciados logicamente |       x      |                  |
| DNS Privado resolve                    |       x      |                  |
| Devemos habilitar o DNS Privado        |              |         x        |