# Certificates

Temos um serviço da AWS que cria e controla certificados. É o ACM - Asymmetric Certificate Manager.

ACM é crítico para a segurança da AWS, pois podemos gerenciar certificados X509 v3 SSL/TLS.
Com certificados temos um canal criptografado garantido por uma certificadora que cria uma cadeia de confiança que garante integridade desde o SO até a outra ponta.

Com o ACM criamos os certificados e podemos usar nos recursos dentro da AWS. E tudo isso é feito por baixo dos panos usando KMS.

São integrados os seguintes serviços:
    - ELB
    - CloudFront
    - Elastic Beanstalk
    - API Gateway

E não temos custos na associação dos certificados. E são automaticamente renovados.
Temos um detalhe, o ACM é regional, ou seja, só pode ser aplicado aos recursos dentro da mesma região.

E fica tudo mais fácil quando usamos o domínio via R53, pois a automação vem da AWS.