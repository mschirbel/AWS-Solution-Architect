# RAM

Resource Access Management.
Podemos usar esse recurso dentro de organizações. Mas precisamos habilitar o *sharing* dentro da organização

![](../media/ram.png)

E temos que usar o modo de All Features na Organizations.

Depois, podemos usar o AWS RAM, e com isso temos o conceito de AZ-ID. Pois o nome de *sa-east-1a* pode apontar para um datacenter diferente do que outra conta vê. E para isso temos o AZ-ID, algo que canaliza o tráfego para uma AZ correspondente entre as contas.

E fazemos isso criando um Resource Share. Podemos dividir:
    - subnets
    - resolver rules
    - license configuration
    - transit gateways

Indicamos qual é a conta que será divida e esperamos a criação. Caso não haja uma organização para habilitar o share de recursos entre as contas, devemos ir na outra conta e aceitar o pedido para dividir.

Assim podemos ter todos esses recursos disponíveis nas duas contas. E se dividirmos subnets, por exemplo, dividiremos suas respectivas VPCs, também.

Se o OWNER da VPC modificá-la ou deletá-la, os PARTICIPANTES sofrerão os impactos.
PARTICIPANTES não podem modificar os recursos que foram divididos.

Existe um detalhe.
**Somente subnets** são restritas para serem divididas com contas de sua organização. Os outros recursos podem ser divididos com contas externas.