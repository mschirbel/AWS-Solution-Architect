# Roles & Temporary Credentials

Uma Role é uma entidade que tem permissões para interagir com outros recursos.
Roles são assumidas por qualquer um que precise daquelas permissões.

Roles tem dois componentes:

    - trust policy: verifica se vc pode assumir essa role
    - permission policy: concede as permissoes dessa rola a entidade que assumiu

Uma role não tem uma credential. O STS cria uma credencial temporária que pode durar por minutos ou horas, e podem ser renovadas.

Por exemplo, podemos colocar uma role em uma EC2 para acessar buckets no S3.
A qual podemos conferir em

```
curl http://169.254.169.254/latest/meta-data/iam/security-credentials/<ROLE NAME>
```

Veremos que temos Access Key e uma Secret Access Key. Temos o tempo de expiração e um token. Não duram para sempre, o que mantém mais seguro o acesso a recursos.
Quando alteramos as policies dentro de uma role. Todas as instâncias que usam a roles sofrerão update.

Roles também são úteis quando precisamos interagir com diversos recursos da AWS.

Para assumir uma role, podemos usar o STS

```
aws sts assume-role --role-arn <ARN> --role-session-name <NAME DA ROLE>
```

O que pode ser útil quando queremos acessar uma role de uma outra conta.

### Revoke Roles

Podemos fazer um *revoke session* de uma role, assim podemos excluir entidades de assumir aquela role sem necessariamente editar as policies nela envolvidas. Mas o *revoke sessions* só funciona para sessões para aquelas que foram dadas antes do *revoke*, ou seja, só podemos revogar por tempo.