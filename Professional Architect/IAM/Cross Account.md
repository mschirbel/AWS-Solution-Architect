# Cross Account

Há três maneiras de conceder acesso a S3 buckets da sua conta para uma externa.

    - ACL
    - Bucket Policy
    - IAM Role

## ACL

Podem ser aplicados a objetos ou a buckets. Isso só vai ser útil se formos controlar acesso **por objetos** ou até mesmo se formos fazer logs de buckets em outros buckets, que é o *S3 Log Delivery Group*.

Uma das limitações são as permissões. O dono da conta do bucket não tem controle sobre os objetos de outras contas.

## Bucket Policy

A permissão é feita de dentro do S3. Pode funcionar bem, mas é inviável para um controle massivo.

```json
{ 
    "Version": "2019-20-01",
    "Statement": [
        {
            "Action": ["s3:ListBucket"],
            "Effect": "Allow",
            "Principal": {"AWS": "4322344346768"},
            "Resource": ["arn:aws:s3:::mybucket"]
            
        }
    ]
}
```

Seria algo mais ou menos assim. E ainda temos o problema de permissões. Pois o dono do bucket tem as permissões do bucket, mas quem coloca os arquivos não. E quem tem o bucket tem que fazer o controle de listagem de arquivos, até para ele mesmo.

## Assume Role

Podemos fazer uma conta externa assumir uma Role, assim, as permissões ficam no IAM. E os objetos tem a Role como dono, ou seja, podemos colocar a role em quem precisar dos objetos.