# Policies

Podemos ter a situação de escolher uma policy ou até mesmo montar uma. Para isso precisamos entender como elas funcionam e como se relacionam com os recursos da AWS.

Uma IAM policy é um arquivo JSON e consiste de um ou mais statements. Cada parte temos no mínimo 3 chaves-valores.

```json
{ 
    "Version": "2012-23-12",
    "Statement": [
        { 
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
```

Effect pode ser allow ou deny.
Resource é um serviço da AWS.
Action são ações possíveis dentro de um recurso.

No exemplo acima, temos a **permissão** de executar **qualquer ação de s3** em todos os **recursos** da AWS.
Como somente o S3 tem as ações *s3:*, eu só posso fazer ações no S3.

Mas, se ao mesmo tempo houver um permissão assim:

```json
{ 
    "Version": "2012-23=-12",
    "Statement": [
        { 
            "Effect": "Deny",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
```

Nosso usuário não terá permissão. Pois o *deny* tem procedência sobre o *allow*. E lembrando que o deny sempre está implícito.

---

## Managed Policies

Existem policies que são criadas pela AWS e são separadas, não podemos integrar duas ou mais em um único documento.

Caso precisemos criar algo mais específico, temos que criar uma inline policy ou criar uma Custom Managed.

Policies podem ser aplicadas:

    - grupos
    - usuários
    - roles

---

## Conditions

```json
{ 
    "Version": "2019-20-01",
    "Statement": [
        {
            "Action": ["s3:ListBucket"],
            "Effect": "Allow",
            "Resource": ["arn:aws:s3:::mybucket"],
            "Condition": { "StringLike": { "s3:prefix": ["user/*"]}}
        },
        { 
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Effect": "Allow",
            "Resource": ["arn:aws:s3:::mybucket/user/*"]
        }
    ]
}
```

Nessa policy temos uma condição, no caso, só podemos listar o conteúdo da pasta *user*.
A condition vem antes da ação.

Isso pode acabar sendo um overhead para administração, entretanto, podemos usar variáveis:

```json
{ 
    "Version": "2019-20-01",
    "Statement": [
        {
            "Action": ["s3:ListBucket"],
            "Effect": "Allow",
            "Resource": ["arn:aws:s3:::mybucket"],
            "Condition": { "StringLike": { "s3:prefix": ["${aws:username}/*"]}}
        },
        { 
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Effect": "Allow",
            "Resource": ["arn:aws:s3:::mybucket/${aws:username}/*"]
        }
    ]
}
```

Podemos ver as variáveis possívels ![aqui](https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/reference_policies_variables.html#policy-vars-tags).

---

## Resources Policies

Existem alguns recursos que possuem policies próprias nos serviços. Como por exemplo o S3, que temos a parte de Permissions -> Bucket Policy:

![](../media/bucket-policy.png)

No caso, essa policy também é levada em conta quando um acesso é feito. Como há uma policy com *Allow* no IAM, tudo ocorre bem, mas, aqui temos também um implicity deny. É importante levar isso em conta.

Mas uma Resource Policy sempre tem um **Principal**. Se houver, é uma resource policy, caso não, vai ser uma IAM policy.

Geralmente usamos uma policy de recurso quando queremos aplicar a diferentes entidades. Públicas ou privadas em relação a conta.