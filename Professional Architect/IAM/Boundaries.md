# Boundaries

Boundaries seguem o mesmo princípio das SCP das Organizations.

Entretanto, podemos colocar somente em usuários e roles, dentro de uma conta AWS.
Assim a entidade não pode passar de uma certo limites de políticas de segurança.

## Exemplo

Caso tenhamos uma SCP

```json
{ 
    "Version": "2019-20-01",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:*",
                "cloudwatch:*",
                "ec2:*",
            ],
            "Resource": "*"
        }
    ]
}
```

E um Boundarie:

```json
{ 
    "Version": "2019-20-01",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:*",
                "rds:*",
                "sqs:*",
            ],
            "Resource": "*"
        }
    ]
}
```

No fim das contas, o usuário somente teria acesso ao **s3**, pois é a interseção entre as duas.