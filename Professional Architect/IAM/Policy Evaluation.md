# Policy Evaluation

A primeira coisa que devemos considerar é a regra de *deny-allow*.

Sempre temos um *explicit* deny isso prevalece sobre um *explicit* allow. Caso não haja nenhum dos dois, temos um *implicit* deny.
Deny -> Allow -> Deny

Depois disso, consideramos a seguinte ordem de hierarquia:

```
Organization Boundaries
        \/
User or Roles Boundaries
        \/
Role Policies
        \/
Permissions(Identity or Resource)
```

