# Federation

Esse é um tópico crítico para o exame.

Identity Federation é quando fazemos uma operabilidade entre diferentes domínios de gestão de acessos.

Um exemplo disso, é quando fazemos uma cross role account. Pois uma ação permite ações de outra conta via role.

Podemos ter um identity federation:
    - interno: entre contas da aws usando o IAM
    - externo: usando softwares de terceiros

Um exemplo de uso externo é quando fazemos o login em um site usando o Google ou Facebook.
Assim usamos um External IDP(External Identity Providers).

Assim temos um token que é enviado para o STS que verifica e envia as credenciais temporárias corretas(usando uma role).

Os usos do STS podem ser:
    - Identity Federation
    - Cross Account Roles
    - Service Roles

Lembre que: *nunca usamos o token que nos foi dado do External IDP, o STS faz um swap dele por um válido na AWS*.

Podemos ver como funciona um login via web usando o ![Playground](https://web-identity-federation-playground.s3.amazonaws.com/index.html).

Um exemplo de como seria a federation com SAML(exemplo do AD da Microsoft):

![](../media/saml.png)