# Organizations

Caso existam muitas contas, teremos muitas contas e uma administração envolvida.
Para isso, podemos concentrar todas as contas em uma só conta, com isso temos as *Linked Accounts*.

AWS Organizations é um serviço no qual podemos consolidar múltiplas contas em uma única organização.
Assim temos o Billing e as Permissões de forma mais clara e fácil de administrar.

Para isso, criamos uma conta que será a **Root**. Só pode existir uma única conta root.
E **tudo** o que for feito na conta root, será propagado para as contas membras da organização.

Dentro da root, podemos criar outras organizações, que são as *Organization Units*.

Organizações podem operar em dois modos

1. *Consolidated Billing* - no qual temos todas as contas consolidadas em uma só. E podemos quebrar ela pra ver o gasto de cada uma também. E assim podemos também alcançar descontos de volume mais rápido.

2. *All Features* - podemos usar as *SCP - Service Control Policies*, assim podemos aplicar restrições a toda uma conta. Como um IAM para as contas, tipo assim.

---

Dentro do AWS Organizations podemos criar contas diretamente dentro da organização. Lembrando que o e-mail deve ser único.
E quando criada, é usada uma Role chamada *OrganizationAccountAccessRole* que nos dá previlégios de admin e a habilidade de fazer um Switch Role.
E quando criamos uma conta assim, elas são criadas sem acesso ao root user. É possível recuperar a senha.

## Service Control Policies

São arquivos JSON que podem ser aplicados para:
    - contas
    - organization units
    - root users

Se for aplicada para um nível de hierarquia acima, será herdado.
*Só não funcionam para master accounts(a conta principal)*.

SCP tem uma particularidade. Elas não **permitem nada**. Elas dão a possibilidade de se garantir(via policies) algum acesso.
Em geral, elas fazem um deny implícito para permitir as permissões.
Resumindo, elas definem limites a recursos a nível de contas.

Por default, temos uma policy totalmente liberal:

```json
{ 
    "Version": "2019-20-01",
    "Statement": [
        {
            "Action": "*",
            "Effect": "Allow",
            "Resource": "*"
        }
    ]
}
```

Com isso temos um processo mais fácil para criar as organização. Isso está na root container, ou seja, é herdado para todas as outras. Mas ainda assim, podemos criar policies de deny para as contas.

```json
{ 
    "Version": "2019-20-01",
    "Statement": [
        {
            "Action": "s3:*",
            "Effect": "Deny",
            "Resource": "*"
        }
    ]
}
```

Aqui temos um exemplo de deny para ações no s3.
No fim das contas, as permissões em uma conta membro da organização será uma interseção do que:
    - O IAM da conta membro permite
    - SCP policy na master