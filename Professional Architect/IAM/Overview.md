# IAM Overview

Identity and Access Management. 
É o serviço que controla os acessos a conta da AWS. Seja de recursos ou de serviços. Podemos criar entidades para controlarmos as permissões de acessos.

Podemos usar:
    - users
    - groups
    - roles
    - policies
    - authentication

Sempre que logamos na conta da aws, usamos o IAM.
Primeiramente, podemos manter históricos de entidades, nessas entidades podemos autenticar para ter acesso a uma conta, se for autenticada, temos uma entidade autenticada. Ainda depois disso, podemos dar autorização a essa entidade, assim temos uma entidade autorizada.

Podemos nos autenticar de duas formas:

    - com usuário e senha
    - com access key e secret access key

Com a access key e secret access key podemos fazer um controle mais programático dos acessos. Lembre-se que a secret key somente é acessível uma única vez. São conhecida comos *long term access credentials* pois não expiram a não ser que sejam excluídas.

O IAM controla a autorização via policies, que podemo ser feitas pela AWS ou criamos as nossas via JSON.
Podemos colocar essas polices em grupos ou usuários, sendo que não poderemos logar em um grupo.

*Temos no IAM um credential report, no qual temos informações sobre os usuários e keys da conta*.

### Conceitos de Criação:

    - um usuário recém criado, não tem nenhuma permissão
    - deny tem mais prioridade que um allow
    - todas as policies tem um implicity deny. Tudo que não está autorizado, está negado.