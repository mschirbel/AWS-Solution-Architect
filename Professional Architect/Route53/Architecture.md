# Architecture

R53 provê acessos a serviços internos ou públicos. Podemos fazer o registro(comprando) de um domínio ou hostiar um domínio.

Podemos fazer um registro, criar um do zero. Nativamente e comprando de dentro do R53.
Ou podemos fazer a transferência de uma fonte externa.

Registrando um domínio é assim que o R53 interage com o DNS global. O que importa são os *name servers* que o domínio possui. É o link entre os *name servers* do domínio e da Hosted Zone que faz o domínio funcionar.

## Hosted Zone

São um container de informações sobre tráfegos de rotas da internet para um domínio. 
Podemos criar uma zona privada(sob uma vpc) ou uma pública(sob a internet);

Dentro da Hosted Zone criamos o Record Set. Que é um container de informações sob tráfegos de um domínio para um subdomínio(pode ser entre VPCs).

Existem diversos tipos de Record Sets, mas os mais importantes são:
    - A: usado para IPv4
    - AAAA: usado para IPv6
    - CNAME: usado para um DNS
    - MX: usado para e-mail

Dentro de um record set temos um TTL(para cacheamento de DNS) e o value, que vai ser o preenchimento do tipo.
Uma feature importante é o *health check*, ou seja, o R53 monitora o endpoint. Especificamos o protocolo, endpoint, porta e path para a URL.

Outra feature é usar o Alias, assim podemos referenciar produtos da AWS, como o DNS de um bucket, por exemplo. Usamos um A record, para fazer, nunca um CNAME. Podemos fazer record sets serem secundários, como um failover para o site primário(como uma página de erro, por exemplo).

O importante de health check é que não detecta falhas de instâncias atrás de ELB. Somente detectaria a falha do ELB(falha da region). Ou seja, serve bem para monitorar algo que seja bem grande.