# Advanced Concepts

Vamos falar de Routing, envolvendo o R53. No caso, podemos fazer policies para tratar nosso tráfego:
    - simple: não influencia em nada na query. e não podemos criar múltiplos record sets para o mesmo *Name*. Caso existam múltiplos values, serão retornados em ordem aleatória.
    - failover: definimos um primário, secundário e um health check. Quando o primário falhar, o tráfego vai ser direcionado para o secundário.
    - geolocation: especificamos um lugar no mundo para um determinado domínio.
    - latency: r53 monitora a latência no mundo e podemos enviar para onde tem a menor latência.
    - weighted: cada record tem um peso, e podemos enviar para determinadas plataformas. Podemos usar isso para testes ou migrações.
    - multivalue answer: é tipo o simple, mas temos health check para cada um dos values.

E podemos combinar essas policies, usando o Traffic Flow. É um editor visual para facilitar o controle.