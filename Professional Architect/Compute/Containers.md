# Containers

Essa é a aula introdutória de ECS.

Container é um ambiente isolado no qual podemos rodar nossa aplicação. Isso significa que containers tem sempre a mesma versão de libraries, SO e da app.

É totalmente portável. Pois podemos ter o mesmo ambiente de desenvolvimento em homologação e em produção.
E assim não teremos conflitos entre libraries.

A diferença entre virtualização e containerização:

![](../media/container101.png)

Temos um pouco menos de segurança quando usamos containers. Mas temos que considerar os benefícios da portabilidade.

A base do Docker, é o Dockerfile que descreve como construir uma imagem. No Dockerfile temos diversas camadas, que serão unificadas para, no fim, termos a imagem. Que podem ser armazenadas em Registries, podendo ser privados ou locais.

A partir de uma imagem, podemos construir um container. Com o container podemos ter um:
    - volume mapping - para expor o FS do container
    - port mapping - para expor a aplicação

Assim acessamos o container a partir de fora, do host. E assim temos portabilidade de containers.