# EC2 Instance Profiles and Roles

É a melhor prática de segurança usar Roles. Ponto passivo. Mas agora precisamos entender como uma Role chega no EC2.

Quando criamos uma Role de EC2, diga-se por exemplo, controlar S3, a trust relationship da role fica mais ou menos assim:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
```

Assim, temos que o serviço de EC2 pode assumir essa role. Não é somente uma instância, é todo o serviço. E no meio disso, está o EC2 Instance Profile.

Instance Profile permite que uma Role seja dada para uma ec2 instance e para que apps dentro dessa instância assumam também essa role.

Podemos ver isso nos metadados da instância.

```
curl http://169.254.169.254/latest/meta-data/iam/security-credentials/<ROLE NAME>
```

Instance Profile é o que faz o link entre a role e a instância.
Quando rodarmos esse comando, veremos que receberemos um *token* e uma data de expiração.
Essas credenciais temporárias são mais prioritárias do que a CLI. Se uma aplicação também precisar de conexão com a AWS é daqui que ela puxa as permissões.