# Stateless Architecture

## Monolithic

Nesse tipo de arquitetura, temos todo o código em um só lugar. Não é também tão escalável assim. Pois o estado é guardado no servidor, por exemplo, um carrinho de compras online.

Ou seja, a *session data* é unica no servidor e é guardada somente em um local. Se o servidor falhar, a session data é perdida, causando problema para o usuário.

Isso leva a problemas com o balanceamento da aplicação, pois precisamos manter a conexão em uma máquina específica. E se uma das máquinas falhar, o balacing também continua comprometido.

## Stateless

Quando as conexões chegarem, não importa qual servidor receber a conexão, pois o estado não está no servidor, mas em algum outro local, tipo um DynamoDB.
Assim a session data continua disponível.

## Escalar aplicações

#### Vertical Scaling

É aumentando o tamanho da máquina. Seja em CPU, Memória e Disco. Assim o sistema pode tratar melhor mais carga.
Isso é bom quando a arquitetura é monolítica.

O problema é que há sempre um tamanho máximo no qual a instância pode chegar. Outro problema é que todos os componentes estão em um único servidor, assim não podemos escalar somente um deles.

#### Horizontal Scaling

Temos a possibilidade de aumentar a quantidade de instâncias que o sistema demanda. Elasticidade é um dos pilares da cloud computing e o scaling pode ser até mesmo automático, usando ASG.