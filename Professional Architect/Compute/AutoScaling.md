# AutoScaling

Com esse serviço temos a possiblidade de escalar nossas instâncias de EC2:
    - scaling up
    - scaling down

Podemos tanto aumentar, quanto diminuir o número de instâncias baseado no workload, e métricas de nossas instâncias.

Existem alguns métodos que podemos usar para provisionar instâncias já prontas para produção:
    - ami baking: criamos uma imagem de uma instância que já configuramos, mas limitamos nossa **flexibilidade** para edições dentro da AMI. O ponto positivo é a velocidade.
    - bootstraping: usamos o user data da instância. o benefício disso é **flexibilidade** da configuração da nova instância, mas temos mais tempo de processo quando criamos uma nova instância.

Com o ASG temos a possiblidade de automatizar o processo de aumentar ou diminuir o número de instâncias EC2, em um tier específico. As métricas usadas para saber o scaling vem do CloudWatch.

O grande benefício é otimizar os custos e eficiências das aplicações.

### Launch

É uma configuração das instâncias que estão sendo criadas. E temos dois componentes aqui:
    - launch config
    - launch template

#### Launch Config

É uma configuração que define várias configurações das instâncias, como por exemplo:
    - ami
    - instance type
    - user data
    - storage
    - security grops
    - key pair

E é importante saber que a configuração é **imutável**. Não podemos alterá-la. Por isso **a AWS não recomenda usar o Launch Configuration**.

### Launch Template

São todas as regras que governam quando um EC2 é provisionado ou terminado.
Temos também:
    - o número máximo e mínimo de instâncias.
    - vpc
    - elb
    - scaling policy
    - sns

Para criar novos, podemos fazer cópias dos antigos e fazer as modificações.

#### Termination Policy

São políticas para fazer o scale down das instâncias. Pode ser:
    - multiple instance in one az
    - oldest launch config
    - billing
    - random

### ASG Capabilities

Com o ASG controlamos tudo que não está nas instâncias. Como por exemplo:
    - qual template usamos
    - vpc/subnets
    - tamanho do grupo de início
    - balancing

---

## Scaling

Se alterarmos a *desired capability*, o ASG tentará criar uma nova instância sempre na subnet no qual não temos nenhuma instância ainda.

Se o número desejado for maior que o número de subnets, aí o ASG tenta balancear entre cada uma das subnets.

E para facilitar o scaling, podemos usar métricas do CW para isso. Por exemplo, caso a CPU de uma das instâncias ultrapasse um valor, podemos aumentar o número de instâncias.

E para ajudar ainda mais, podemos usar o *step scaling*, no caso, definimos quantas instâncias podemos aumentar por vez. Isso dá mais granularidade para o processo.

E ainda mais, podemos definir uma janela do dia para aumentarmos o número de instâncias; bem como um tempo de *cooldown* para tomar a próxima ação.

#### Manteinence

Podemos fazer duas coisas para auxiliar em manutenções:
    - deixar uma instância em standby, ou seja, o tráfego não será levado para ela.
    - proteger uma instância de ser terminada automaticamente