# EC2 Concepts

## AMI

Toda instância é criada a partir de uma AMI. É uma imagem da amazon que usamos como base para o SO.
Toda AMI tem um root volume com um SO instalado.

Tem um certo tipo de virtualização e pode ser 32 ou 64 bits.

Temos alguns tipos:
    - as do quick start(oferecidas pela aws)
    - my amis - as que vc criou
    - aws marketplace - podemos comprar amis
    - community - feitas pela comunidade

## Instance Type

Existem muitos tipos de instâncias e todas elas tem algum tipo de família.
Podemos ver todas elas ![aqui](https://www.ec2instances.info/).

No exame, precisamos saber qual tipo de família usar para uma determinada aplicação.

## Initial Configuration

Precisamos indicar quantas intâncias e qual o tipo de Billing Model.
Depois configuramos a rede, vpc e subnets daquela instância, também as suas configurações de interfaces de redes, bem como a sua autorização dentro da AWS.

Por último, configuramos a parte de boot-up da máquina.

## Storage

Depois seleciona qual o tipo de storage, quantidade de disco e tipo de disco.
Dependendo da app, é uma das partes mais imporantes devido ao IOPS

## Tags

Como todo recurso da AWS podemos adicionar tags para controles futuros.

## Security Groups

Podemos adicionar SG as interfaces de redes, como vimos nas configurações iniciais, e isso pode ser bem discutido visto que podemos ter mais de uma interface de rede.

## Key Pair

Depois precisamo associar com uma KEY PAIR, se for Linux usamos para fazer SSH, se for Windows usamos para fazer a descriptografia da senha de Administrator.