# Lambda

Lambda é FaaS - Function as a Service. No qual especificamos uma linguagem para Runtime, algumas características superficiais do ambiente que vai rodar o código.

Podemos fazer com o Lambda seja construtivo ou responsivo a eventos.

---

Lambda Function é um pedaço do código, no qual é executado pelo serviço. Outro termo é o *Function ZIP*, no qual colocamos todos os arquivos de dependência para o código.

E por boas práticas, as funções devem ser simples e objetivas. Isso porque há um limite para o Lambda funcionar, que é 15 minutos.

Quando a função for invocada, ele acontece em uma Sandbox, totalmente isolada, rodando com o Runtime que você selecionou para o seu código.
Na primeira execução do Lambda, temos o que é conhecido como Cold Start. Pois é a primeira vez que estamos criando o Sandbox, o que pode demorar um pouco. Caso você esteja rodando as funções constantemente, temos o que é chamado de Warm Start, no qual o Sandbox já está criado.

## Architecture

A arquitetura do Lambda é mais ou menos assim:

![](../media/lambda-arch.png)

A parte em verde acontece na sua conta. A parte laranja é na AWS e pode ser dividido com múltiplos clientes.
Por baixo dos panos, usamos o ![Firecracker](https://github.com/firecracker-microvm/firecracker), criado pela AWS.

Todas as variáveis de ambiente do Lambda podem ser enviadas para a Sandbox usando criptografia do KMS. E contam com roles para acessos usando STS.

Lambda é restrita pela saída da VPC, ou seja, se precisarmos de acesso a internet, precisamos de um IGW na VPC. Mas a AWS está usando RemoteNAT, no qual podemos usar múltiplos Lambdas usando a mesma interface de rede da VPC.

Outro ponto importante, é que podemos fazer escritas em disco no Lambda, usando o diretório `/tmp` da sandbox.

## Performance Improvement

Tudo é que declarado dentro da função `lambda_handler`, a principal função do Lambda fica restrito a Sandbox que você está rodando naquele momento.

Entretanto, tudo que você cria **fora** pode ser usado novamente por outras função. Sempre que houver repetições rápidas, podemos ter a chance de usar a mesma sandbox, logo, o código já estaria pronto.

## Layers

Podemos fazer o código do Lambda de 3 jeitos:
    - editando o código diretamente no serviço, inline code
    - fazendo o upload de um zip - function zip
    - fazendo o upload do S3 - S3 code

Mas podemos usar Layers para facilitar um pouco o trabalho.
Podemos usar a Layers para adicionar novos runtimes, para qualquer linguagem. Ou podemos usar para libraries comuns a múltiplas funções.

Layers são imutáveis, uma vez criadas, não podemos mudar. Novas versões podem ser criadas, mas as que já existem são totalmente estáticas. E cada Layer só pode ter até 250MB.

Qualquer Lambda function pode usar até 5 Layers, que são extraídas no diretório `/opt` do sandbox.