# Spot or Reserved

## On-Demand

É o modelo default. Pagamos pelo que consumimos por hora e por GB transferido.

É ótimo para quando não conseguimos calcular o quanto vamos precisar. Tem um pequeno desconto para altos volumes, mas geralmente é o modelo mais caro.

E tem a prioridade média de *startup*.

## Reserved

É ótimo quando podemos calcular o quanto vamos gastar por 12 ou 36 meses. Geralmente desejamos cobir o *base cost*. Não queremos usar algo que seja variável, mas somente aquilo **que temos certeza que vamos gastar**.

Podemos usar em diversos serviços da AWS, como EC2, RDS, DynamoDB.
Exitem 3 tipos de pagamentos:
    - all upfront
    - partial upfront
    - no upfront

E tem a maior prioridade de *startup*.

## Spot

É o ideal para workloads esporádicos e quando queremos o menor preço. Podem ter até 90% do preço de instâncias On-Demand.

Mas a instância pode ser terminada a qualquer momento e sem avisos. Então temos que ter tolerância para interrupções.

E tem a menor prioridade de *startup*.