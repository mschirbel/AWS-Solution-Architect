# API Gateway

Com o API Gateway temos API's de uma forma totalmente serverless.
API, ou Application Programming Interface é um modo como podemos interagir com os componentes de um site ou produto.

Existem dois tipos de APIs:
    - REST - um client no qual requisita algo do servidor por meio de uma URL. Depois temos uma resposta.
    - WebSocket - temos interatividade entre clientes e a API.

De qualquer forma, temos uma troca de informações com a API para realizar uma ação. E para isso, precisamos de um servidor para providenciar essa URL da API publicamente.

E para isso, podemos usar uma arquitetura Serverless. Com o API GW podemos ter interações com os serviços de Backend, podendo ser banco de dados, S3, Lambda e etc.

Podemos usar o API GW de forma regional, usando as Edge Locations da AWS como forma de Endpoint da API.

## Update

Usamos stages para fazer o updade de uma API.
Chamamos de stage, no qual teremos o contexto de nossa API. O formato é sempre assim:

`https://<RANDOM STRING>.execute-api-<REGION>.amazonaws.com/<STAGE>/<METHOD>`

## Integration

Existem diversas formas de interagir com outros serviços. Por exemplo, podemos usar todo um backend como forma de teste(*mock*), ou por exemplo interagir com o Lambda, recebendo um *event*.

Tanto o API GW e o Lambda são fundamentais para o Serverless.

Assim como podemos fazer cache de aplicações, ou colocar um WAF para o acesso, CW para logging e outras diversas integrações.