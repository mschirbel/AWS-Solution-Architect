# Custom Origins

Assim, falamos apenas de EC2 ou On-prem server.

Para isso, as Edge Locations tem que ser capazes de acessarem esses recursos, logo, precisa de acessível via internet pública.

Quando usamos um S3, por exemplo, vamos que existem diversas limitações, pois só podemos alterar o Path e os Headers. E o protocolo de Viewer e o Fetch são o mesmo, se usarmos HTTPS, será assim em todo o caminho.

Mas se usarmos uma Custom Origin, podemos definir os protocolos de Origin Fetch, vários tipos de TLS/SSL. E depois podemos selecionar o Viewer Protocol. Podemos alterar as portas de comunicação e os TTL.