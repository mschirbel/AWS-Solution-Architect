# ECS

Elastic Container Service.

## Architecture

ECS tem tasks. Tasks são arquivos de configurações com determinações de containers, como eles interagem entre si e com o mundo afora.

As tasks podem usar:
    - EC2
    - Fargate

#### EC2

Se feitas no EC2, algumas instâncias serão criadas com ECS Agents que se comunicam de volta com o ECS. E além disso, temos os containers, por si.
Para ter uma ideia, uma task pode ser um *docker-compose*, mais ou menos.
Podemos ter containers em Linux ou em Windows.

Também podemos determinar o *Provisioning Model*, podendo ser On-Demand Instance ou Spot.

#### Fargate

Aqui não temos recursos controlados por nós. Do contrário, não temos que controlar os EC2 por trás. Cada uma das tasks declaradas, serão isoladas.
Acaba sendo mais seguro, do que usar EC2. E também mais barato.

No fim das contas, temos uma interface de rede que é criada pra fazer a comunicação entre a VPC e os containers da task.

Tanto é que se olharmos algum LB criado no ECS, o target group dele será um IP, esse é o IP da interface de rede.

### Task Definition

Podemos declarar em qual VPC, qual o tipo de rede e quanto de CPU e Memória usaremos para nossos containers.
Um exemplo segue abaixo

```json
{
    "family": "",
    "taskRoleArn": "",
    "executionRoleArn": "",
    "networkMode": "host",
    "containerDefinitions": [
        {
            "name": "",
            "image": "",
            "repositoryCredentials": {
                "credentialsParameter": ""
            },
            "cpu": 0,
            "memory": 0,
            "memoryReservation": 0,
            "links": [
                ""
            ],
            "portMappings": [
                {
                    "containerPort": 0,
                    "hostPort": 0,
                    "protocol": "udp"
                }
            ],
            "essential": true,
            "entryPoint": [
                ""
            ],
            "command": [
                ""
            ],
            "environment": [
                {
                    "name": "",
                    "value": ""
                }
            ],
            "mountPoints": [
                {
                    "sourceVolume": "",
                    "containerPath": "",
                    "readOnly": true
                }
            ],
            "volumesFrom": [
                {
                    "sourceContainer": "",
                    "readOnly": true
                }
            ],
            "linuxParameters": {
                "capabilities": {
                    "add": [
                        ""
                    ],
                    "drop": [
                        ""
                    ]
                },
                "devices": [
                    {
                        "hostPath": "",
                        "containerPath": "",
                        "permissions": [
                            "write"
                        ]
                    }
                ],
                "initProcessEnabled": true,
                "sharedMemorySize": 0,
                "tmpfs": [
                    {
                        "containerPath": "",
                        "size": 0,
                        "mountOptions": [
                            ""
                        ]
                    }
                ],
                "maxSwap": 0,
                "swappiness": 0
            },
            "secrets": [
                {
                    "name": "",
                    "valueFrom": ""
                }
            ],
            "dependsOn": [
                {
                    "containerName": "",
                    "condition": "HEALTHY"
                }
            ],
            "startTimeout": 0,
            "stopTimeout": 0,
            "hostname": "",
            "user": "",
            "workingDirectory": "",
            "disableNetworking": true,
            "privileged": true,
            "readonlyRootFilesystem": true,
            "dnsServers": [
                ""
            ],
            "dnsSearchDomains": [
                ""
            ],
            "extraHosts": [
                {
                    "hostname": "",
                    "ipAddress": ""
                }
            ],
            "dockerSecurityOptions": [
                ""
            ],
            "interactive": true,
            "pseudoTerminal": true,
            "dockerLabels": {
                "KeyName": ""
            },
            "ulimits": [
                {
                    "name": "cpu",
                    "softLimit": 0,
                    "hardLimit": 0
                }
            ],
            "logConfiguration": {
                "logDriver": "splunk",
                "options": {
                    "KeyName": ""
                },
                "secretOptions": [
                    {
                        "name": "",
                        "valueFrom": ""
                    }
                ]
            },
            "healthCheck": {
                "command": [
                    ""
                ],
                "interval": 0,
                "timeout": 0,
                "retries": 0,
                "startPeriod": 0
            },
            "systemControls": [
                {
                    "namespace": "",
                    "value": ""
                }
            ],
            "resourceRequirements": [
                {
                    "value": "",
                    "type": "InferenceAccelerator"
                }
            ],
            "firelensConfiguration": {
                "type": "fluentd",
                "options": {
                    "KeyName": ""
                }
            }
        }
    ],
    "volumes": [
        {
            "name": "",
            "host": {
                "sourcePath": ""
            },
            "dockerVolumeConfiguration": {
                "scope": "shared",
                "autoprovision": true,
                "driver": "",
                "driverOpts": {
                    "KeyName": ""
                },
                "labels": {
                    "KeyName": ""
                }
            },
            "efsVolumeConfiguration": {
                "fileSystemId": "",
                "rootDirectory": ""
            }
        }
    ],
    "placementConstraints": [
        {
            "type": "memberOf",
            "expression": ""
        }
    ],
    "requiresCompatibilities": [
        "FARGATE"
    ],
    "cpu": "",
    "memory": "",
    "tags": [
        {
            "key": "",
            "value": ""
        }
    ],
    "pidMode": "task",
    "ipcMode": "host",
    "proxyConfiguration": {
        "type": "APPMESH",
        "containerName": "",
        "properties": [
            {
                "name": "",
                "value": ""
            }
        ]
    },
    "inferenceAccelerators": [
        {
            "deviceName": "",
            "deviceType": ""
        }
    ]
}
```

Temos 4 partes:
    1. container definition - qual imagem, memoria, portas, storage e etc
    2. task definition - network, roles, launchtype
    3. service - LB entre as tasks e escalabilidade
    4. cluster - vai usar EC2 ou Fargate

##### Network Modes

O default é usando o *awsvpc*, no qual controlamos via VPC, por meio de uma interface de rede.
Mas temos outros:

    - none - no qual podemos usar para mapear volumes em outros containers, assim sabemos quais containers fazem processamento e quais expoem informações com o mundo
    - bridge - no qual fazemos comunicação com o host e outros containers
    - host - no qual fazemos comunicação direta com o host, não temos port mapping

No Fargate, só podemos usar o awsvpc.

## Security

Podemos ver as subnets de uma VPC criada pelo ECS. E veremos que ela tem somente 249 IP's disponíveis, ao contrário do normal, que é 250. 

Isso acontece pois é descontado um IP para cada LB associado com uma NI(network interface) usando o Modo Fargate.

Podemos ver essa NI no serviço de EC2, e nela podemos associas SG. E com isso fazemos as restrições de tráfego de acesso ao container.

Quando usamos o Modo EC2, temos instâncias comuns em nossa dashboard, e portanto, a segurança fica do mesmo jeito que vemos nos outros casos.

---

E como podemos dar permissões tanto para os hosts, tanto para os containers?
Se usarmos o Fargate, damos permissões somente nas tasks.
Se usarmos o EC2, damos permissões nas tasks e nos hosts. E para isso, usamos o Instance Profile.

No Instance profile ja virá uma Role por default atachada, que permite a comunicação com ECS, **devemos** manter essa role, mas podemos adicionar outras policies.

Entretanto, podemos adicionar Roles para Tasks. Usamos uma *task role* é como uma Instance Role, mas somente para um container, e fazemos isso diretamente do ECS.

Permissões para o host interagir com outros serviços ---> IAM Roles
Permissões para tasks ----> Tasks Roles

Mas com o Fargate, só podemos usar *tasks roles*.

---

Não podemos confundir a Task Role com a Task Execution Role

Task Role ---> permissão para tasks
Task Execution Role ---> permissões para o agente do ECS executar tarefas no host.