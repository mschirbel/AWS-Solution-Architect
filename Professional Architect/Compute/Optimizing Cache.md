# Optimizing Cache

Devemos saber como fazer a otimização de cache quando usamos o CloudFront.
Pois com o Cloudfront podemos trazer o conteúdo mais próximo ao usuário. Para um arquiteto, queremos que todas as Edge Locations tenham nosso conteúdo.

Caso um client queira um objeto e consiga de uma Edge Location ou Regional Cache, chamamos isso de um *Cache Hit*. Caso não consiga é um *Cache Miss*.

No mundo real, nunca teremos 100% de cache hit, mas podemos aumentar drasticamente essa proporção. Podemos ver isso na aba de Cloudfront Cache Statistics.

As formas são:
1. aumentar o TTL para cache de objetos.(sempre em segundos). O default é ser armazenado por 24h. Podemos aumentar caso nosso conteúdo seja muito estático, e diminuir caso contrário.
2. nao acessar objetos diretamente
3. fazer o forward de query strings(aqueles ?language=en no fim da URL), **se necessário**. Podemos fazer whitelist de certas query strings
4. fazer o forward de cookies, **se necessário**.
5. verificar a necessidade fazer cache dos headers. não fazer é o melhor.

## Lambda at Edge

Quando adicionamos as Lambda a um **behaviour** de uma distribuição, precisamos com antecedência criar essa funções.

Podemos ter os seguintes eventos:
    - viewer request
    - viewer response
    - origin request
    - origin respose

E assim podemos tratar os conteúdos dinâmicos de um site. Podemos ver ![aqui](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-event-structure.html) os conteúdos retornados.
Assim podemos fazer nosso site responsivo, ou até mesmo diferente para diferentes países.