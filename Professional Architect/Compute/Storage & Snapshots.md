# Storage & Snapshots

## Instance Store Volumes

São storages físicos que são associados com uma instância. Mas não são todas os tipos de instâncias que aceitam Instance Store.

Esses storages são conhecidos como *ephemerals*, isso acontece porque quando a máquina desliga, ou para, o hardware vai parar e o volume se perde.
Nunca devemos usar os dados como persistentes, **sempre como efêmeros**. 

Podemos usar isso para aplicações que só fazem processamento de dados. Ou quando precisamos de altos I/O & Throughput, pois os devices são todos NVMe SSD.

Um ponto importante é que não podemos usar *shared storage* pois o storage é dependente do host físico, e não da instância.

Também não tem elasticidade para ser aumentado, pois depende da máquina física.

## EBS

São volumes em rede. Eles ocupam uma AZ. Isso melhora o tráfego de dados pois não está no mesmo tráfego de rede.
São ótimos com persistência de dados ou quando precisamos de dados duráveis - como snapshots e resiliência.

Também podemos aumentar o tamanho o disco, pois eles são associados com a instância, não com o host físico. E existem alguns tipos de EBS:

1. gp2(ssd) - general purpose - é bom para a maioria de tarefas. só vai até 16.000 IOPS/vol
2. io1(ssd) - provisioned IOPS - tarefas que precisam de muito IOPS
3. st1(hdd) - Troughput Optimized - custa pouco e é bom para dados pouco acessados(não serve como boot)
4. sc1(hdd) - Cold HDD - custo menor, serve para dados que não serão acessados

O máximo que podemos alcançar com EBS é 1.750MiB/s e 80.000 IOPS por instância.

Mas temos algumas desvantagens:
    - o custo da persistência de dados deve ser considerada
    - s3 é melhor para distribuir conteúdo
    - nao podemos distribuir entre instâncias

## Snapshots

São regionais, e podemos usar para criar outros EBS volumes em outras AZ.
E podemos copiar os snapshots para outras regions, criando EBS volumes em outras regiões mas nunca em outra contas.

São guardados no S3, por isso não ficam presos a uma região. E acabam sendo incrementais se forem tirados do mesmo disco. 
Se você deletar algum snapshot que tenha dependência de outros, o espaço ocupado ainda será considerado no gasto.

Algo a se considerar quando criando um snapshot é que são *fotos* de um disco em um ponto no tempo. Caso seja um volume com um grande trânsito de informação, é ideal para a instância para tirar um snapshot.