# HPC & Placement Groups

Não são todos os tipos de instâncias que suportam Placement Groups.
É um aglomerado de instâncias em uma determinada AZ.

## Cluster Placement Group - performance física

Um Placement Group é um grupo de instância em uma **única** AZ. Geralmente quando queremos uma alta performance entre as instâncias EC2.

Localiza todas essas instâncias em uma distância bem pequena entre os datacenters. Assim temos a menor latência na AWS.

E não podemos modificar um PG, caso precisemos adicionar mais instâncias. E sempre temos melhor performance se usarmos o mesmo tipo de instância.

## Partition Placement Group - HA e FT

Quando precisamos do controle físico de onde as instâncias estão, mas estamos usando duas AZ, logo, podemos ter HA na nossa arquitetura.
Só podemos criar esse tipo de PG usando a CLI.

## Spread Placement Group - crítico para nao cair

Podem operar em múltiplas AZ. Mas é recomendado quando a aplicação usa poucas instâncias, mas são **muito críticas**, na qual espalhamos as instâncias estão em hardware *completamente diferentes*.

O número é limitado para 7 instâncias por AZ.