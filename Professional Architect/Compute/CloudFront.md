# CloudFront

Cloudfront opera em redor de uma *distribuição*, que pode conter informações sobre:
    - como o conteúdo vai ser distribuido
    - quais protocolos
    - segurança
    - formas de distribuição

A distribuição segue a seguinte arquitetura:

![](../media/cloudfront.png)

Quando criamos uma distribuição escolhemos dois tipos:
    - web: serve para conteúdos estáticos e dinâmicos ou streaming
    - rtmp: somente serve para aplicações com Adobe Flash(obrigatório o uso do S3)

Se for uma distribuição web, podemos escolher a **origem** dos arquivos(Origin Protocol) em:
    - s3
    - elb
    - mediastore
    - mediapackage

Podendo ser uma Lambda, EC2, Containers, Buckets e etc. E é assim que o cloudfront envia os arquivos para as Edge Locations(Origin Fetch).

Depois definimios o **Behavior**(Viewer Protocol), que é o protocolo, métodos de HTTP, criptografia e cache.
Isso é algo necessário não importa qual a distribuição.

Por último, fazemos o deploy das **configurações** da distribuição para as Edge Locations. Aqui escolhemos as classes(tipos de performance) da distribuição, mas isso aumenta o custo também(pois estamos levando para mais Edge Locations). E para proteção, podemos conectar com um WAF e habilitar logs.

Depois, se quisermos usar isso no R53, podemos criar um Record Set ALIAS e apontar para o DNS da distriuição.

---

## Como funciona?

O client faz uma requisição as Edge Locations, e caso esse seja o primeiro acesso, será feito um *Origin Fetch* da Origin. Assim, aquela Edge Location específica tem o conteúdo. E os próximos clients que fizerem requisição, só fazem um *Cache Hit*.

E para isso que servem os valores de TTL, assim sabemos quanto tempo os arquivos ficarão naquela Edge Location.

Mas também existe o *Regional Cache*. Serve para caso os acessos venham de uma área específica. Então, ao invés de solicitar o arquivo da Origin, ele olha as Edge Locations daquela Region e vê se alguma já tem os arquivos para assim fazer um Cache Hit.

Se não tiver o arquivo nem no Edge Cache, nem na Region Cache, aí ele faz um Origin Fetch.

---

## Invalidations

Podemos invalidar certas URI de uma distribuição. E para todas as modificações feitas em uma distribuição podem levar até 45 minutos.

E assim podemos enviar os arquivos para mais próxima do cliente, assim dando a impressão de mais performance.

## Security

#### Distribution Security

O primeiro ponto é claro, usar HTTPS na conexão do client com a Edge Location. Mas perceba, que o client só pode fazer HTTPS para a Edge Location.

O segundo ponto é usar um WAF, para contar contra ataques mal intencionados. Podemos definir filtros e números de requisições por segundo.

O terceiro ponto é usar um custom SSL. Assim podemos proteger a comunicação entre as Edge Locations e a Origin. Precisamos de um certificado confiado publicamente, tipo DigiCert ou GoDaddy. Isso alteramos no campo **SSL Certificate** nas configurações da distribuição.

O quarto ponto, depende do tipo de Origin que estamos usando. Se usarmos um S3, o certificado vem da AWS. Mas se usarmos um ELB, precisamos de um certificado, ou até mesmo se usarmos On-prem, precisamos de um certificado assinado.

O quinto ponto seria proteger, com policies, o acesso a sua origin. Se for um S3, temos que restringir o acesso de outras fontes se não o Cloudfront. 
No caso do S3, podemos usar o OAI - Origin Access Identity. Usamos uma bucket policy para permitir OAI e negar os outros acessos. Para isso, editamos a Origin de nossa distribuição e restringimos o acesso aos buckets:

![](../media/editorigin.png)

Assim, criamos uma nova identidado, com um nome genérico, pois podemos usar para vários acessos. E no bucket teremos a seguinte policy:

```json
{ 
    "Version": "2012-10-17",
    "Id": "PolicyForCloudFrontPrivateContent",
    "Statement": {
        "Sid": "2",
        "Effect": "Allow",
        "Pincipal": {
            "AWS": "<ARN DO OAI>"
        },
        "Action": "s3:GetObject",
        "Resource": "<ARN DO BUCKET>"
    }
}
```

*É possível que exista outra ação, permitindo o acesso público, é necessário remover essa parte, deixando somente o acesso do OAI*.

O sexto ponto seria usar uma Signed URL ou Cookies. Permitimos assim o uso de permissões mais restritivas nos objetos. Com os Cookies podemos dar acesso aos objetos da distribuição.

![](../media/cloudfrontcookie.png)

Para habilitar isso, temos que editar o **Behavior** da nossa distribuição:

![](../media/cloudfrontbehaviour.png)

Assim podemos ter uma API interagindo com um CloudFront gerando uma URL para acesso aos objetos privados.

O sétimo ponto seria *restrictions*. Podemos fazer dois tipos de geo-restrictions:
    - a nível de distribuição: com whitelist/blacklist. Quem faz a pesquisa de geoIP é a Edge Location
    - 3rd party geo location: quem faz a pesquisa é uma aplicação

A primeira, só faz no IP da requisição. A segunda é mais específica.

O oitavo ponto seria o Field-Level Encryption. Permite definir uma public key para uma distribution. Assim, quando a informação vem para a Edge Location, podemos criptografá-la e assim segue até a Origin. Podemos ver mais ![aqui](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/field-level-encryption.html).
Assim podemos ter uma criptografia de ponta a ponta.