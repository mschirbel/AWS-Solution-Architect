# Serverless

Devemos saber os conceitos e diferenças entre Serverless e Event-Driven-Architecture.

Mesmo que chamados de Serverless, ainda sim usamos servers, por baixo dos panos. Entretanto, a administração deles fica invisível para nós.

Um ponto importante do Serverless, é que só pagamos que consumimos, no tempo em que estamos consumindo.
Isso traz uma eficiência muito grande para o negócio.

Um exemplo de site serverless é o https://web-identity-federation-playground.s3.amazonaws.com/index.html

A intenção da arquitetura é somente usar plataformas como serviço, um exemplo são:
    - s3
    - lambda
    - dynamodb
    - api gw

e outros. Mas o mais importante é que somente usamos plataformas como serviço.
Mas o que faz realmente possível é o Lambda. Um serviço no qual podemos escrever nossas funções baseadas em reações a eventos.

Por exemplo, podemos configurar um evento em um S3 para interagir com notificações para uma Lambda Function:

![](../media/s3events.png)

E podemos fazer eventos baseados em objetos, o que é ótimo para fazer a programação de funções Lambdas.