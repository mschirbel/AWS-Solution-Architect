# AMI

AMI são objetos contendo todas as informações para criar uma instância. Como por exemplo:
    - o owner
    - permissões de inicialização(quem pode usar)
    - arquitetura(64 x86 ou 64 arm)
    - SO
    - block devices

![](../media/ami.png)

## Instance with EBS - mais novas

Quando criamos uma AMI a partir de uma instância com EBS, na verdade o que ocorre é um snapshot dos volumes associados com a instância.
A AMI criada referencia esses snapshots e é por eles que somos cobrados.

E podemos copiar uma AMI para uma nova region. Desde que dentro da mesma conta. Mas o que acontece é que são copiados os snapshots dos EBS que são referenciados.

Por default, o criador da AMI é o dono da AMI, mas podemos dar permissões para outros ou torná-la pública.
E assim temos um mercado emm torno de AMI, no qual podemos vender nossas imagens.

## Instance Store AMI - legacy

São AMI que não consomem volumes EBS. Essas AMIs terão limitações de tipos de máquinas. Depois de configurada, fazemos um *bundle* para um S3, e criamos uma AMI referenciando um bucket.