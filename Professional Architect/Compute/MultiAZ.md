# MultiAZ

Como deveríamos selecionar quantas instâncias devemos colocar em cada AZ?

Com aplicações em multi az, temos HA e FT em nossa arquitetura. Entretanto, precisamos saber quantas instâncias em cada AZ devemos colocar.

Por exemplo:
    Se uma app precisar de 9 instâncias para operar com 100% e, por acaso, estiver em 3 AZ, colocamos 3 instâncias em cada AZ. Mas e se uma AZ cair e não tiver capacidade em uma AZ para as instâncias que você precisa.
    Para isso podemos, por exemplo, comprar reserved instances para uma base em todas as AZ.
    Outra estratégia é rodar em *overcapacity*, com mais AZ disponíveis.

A dica é: qaunto mais AZs você usar, menor o número de instâncias como buffer de instâncias.