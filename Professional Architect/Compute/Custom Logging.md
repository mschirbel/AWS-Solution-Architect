# Custom Logging

Vamos ver como EC2 se integra com CW.
CW recolhe informações básicas(telemetria) das instâncias, como podemos ver na aba Monitoring:
    - cpu
    - disk read
    - disk write
    - network in
    - network out
    - packets
    - status check

Essa informação também está no CW. Mas o CW não consegue ver o que tem dentro da instância. E para isso temos o CW Agent, para termos informações do que acontece dentro da instancia, como Memória, por exemplo.

Caso precisemos de interação com CW, precisamos de uma Role específica para CW e SSM, no caso, usamos as policies:
    - AmazonEC2RoleforSSM
    - CloudWatchAgentServerPolicy

Na policy do **AmazonEC2RoleforSSM** devemos incluir uma linha: *"ssm:GetParameter"*. Note que na policy podemos usar o *"ssm:GetParameters"*, no plural. Isso pode ser um problema.

Assim temos as permissões de usar SSM na máquina e colocar os dados no CW.

*Podemos ver as métricas coletadas do EC2, no painél do CW, menu de Metrics*.

Depois, podemos fazer de alguns jeitos, talvez o mais tranquilo seja instalando o agente do CW na máquina.

## Jeito Manual

```
wget htttps://s3.amazonaws.com/amazoncloudwatch-agent/amazon_linux/amd64/latest/amazon-cloudwatch-agent.rpm
rpm -ivh amazon-cloudwatch-agent.rpm
ls /opt/aws/amazon-cloudwatch-agent/etc/
sudo touch /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
sudo vim /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
```

```json
   {
      "agent": {
        "metrics_collection_interval": 10,
        "logfile": "/opt/aws/amazon-cloudwatch-agent/logs/amazon-cloudwatch-agent.log"
      },
      "metrics": {
        "metrics_collected": {
          "cpu": {
            "resources": [
              "*"
            ],
            "measurement": [
              {"name": "cpu_usage_idle", "rename": "CPU_USAGE_IDLE", "unit": "Percent"},
              {"name": "cpu_usage_nice", "unit": "Percent"},
              "cpu_usage_guest"
            ],
            "totalcpu": false,
            "metrics_collection_interval": 10,
            "append_dimensions": {
              "customized_dimension_key_1": "customized_dimension_value_1",
              "customized_dimension_key_2": "customized_dimension_value_2"
            }
          },
          "disk": {
            "resources": [
              "/",
              "/tmp"
            ],
            "measurement": [
              {"name": "free", "rename": "DISK_FREE", "unit": "Gigabytes"},
              "total",
              "used"
            ],
             "ignore_file_system_types": [
              "sysfs", "devtmpfs"
            ],
            "metrics_collection_interval": 60,
            "append_dimensions": {
              "customized_dimension_key_3": "customized_dimension_value_3",
              "customized_dimension_key_4": "customized_dimension_value_4"
            }
          },
          "diskio": {
            "resources": [
              "*"
            ],
            "measurement": [
              "reads",
              "writes",
              "read_time",
              "write_time",
              "io_time"
            ],
            "metrics_collection_interval": 60
          },
          "swap": {
            "measurement": [
              "swap_used",
              "swap_free",
              "swap_used_percent"
            ]
          },
          "mem": {
            "measurement": [
              "mem_used",
              "mem_cached",
              "mem_total"
            ],
            "metrics_collection_interval": 1
          },
          "net": {
            "resources": [
              "eth0"
            ],
            "measurement": [
              "bytes_sent",
              "bytes_recv",
              "drop_in",
              "drop_out"
            ]
          },
          "netstat": {
            "measurement": [
              "tcp_established",
              "tcp_syn_sent",
              "tcp_close"
            ],
            "metrics_collection_interval": 60
          },
          "processes": {
            "measurement": [
              "running",
              "sleeping",
              "dead"
            ]
          }
        },
        "append_dimensions": {
          "ImageId": "${aws:ImageId}",
          "InstanceId": "${aws:InstanceId}",
          "InstanceType": "${aws:InstanceType}",
          "AutoScalingGroupName": "${aws:AutoScalingGroupName}"
        },
        "aggregation_dimensions" : [["ImageId"], ["InstanceId", "InstanceType"], ["d1"],[]],
        "force_flush_interval" : 30
      },
      "logs": {
        "logs_collected": {
          "files": {
            "collect_list": [
              {
                "file_path": "/opt/aws/amazon-cloudwatch-agent/logs/amazon-cloudwatch-agent.log",
                "log_group_name": "amazon-cloudwatch-agent.log",
                "log_stream_name": "amazon-cloudwatch-agent.log",
                "timezone": "UTC"
              },
              {
                "file_path": "/opt/aws/amazon-cloudwatch-agent/logs/test.log",
                "log_group_name": "test.log",
                "log_stream_name": "test.log",
                "timezone": "Local"
              }
            ]
          }
        },
        "log_stream_name": "my_log_stream_name",
        "force_flush_interval" : 15
      }
    }
```

Esse é um exemplo de como seria a exposição para o CW e como o SSM Agent deve guardar as informações e enviar para o CW.

Depois que os arquivos estiverem nos lugares:

```
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a -fetch-config -m ec2 -c file:/op/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json -s 
```

Podemos ver mais sobre essas configurações ![aqui](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/CloudWatch-Agent-Configuration-File-Details.html).

Agora teremos uma nova Log Stream, que foi criada pelo Agent na instância. Esse é o jeito manual de fazer.

## Using SSM

Dentro do serviço do SSM, temos no menu Run Command a instrução *AWS-Configure-AWSPackage*.

![](../media/cw-ssm.png)

Depois marque como *Install*, e com a versão *latest*. Depois selecione qual instância deseja instalar.

Para configurar, usamos o *Parameter Store*. No value, durante a criação de um novo parameter, use o JSON acima.

Assim temos as mesmas métricas, criadas em dois jeitos distintos.