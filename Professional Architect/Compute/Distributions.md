# Distributions

Se usarmos uma Web ou RTMP distribuition, as configurações são quase as mesmas. Mas na RTMP temos muito menos configurações.

Lembrando que na RTMP só usamos quando nosso site tem Adobe Flash.

## Origins

Podemos definir múltiplas origins ou criar um origin group. Assim podemos ter um FT caso a sua origin fique fora do ar.

## Special Config

Quando criamos uma distribuition, por default usamos o domain name default. Que é aquele que usa *.cloudfront.net*.
Ou seja, só precisamos nos preocupar com Certificados, se usarmos um domínio customizado. Mas ainda assim, podemos importar o certificado do ACM.

---

Se precisarmos de SSL, podemos usar o SNI(mas é muito recente), ou pagar $600 para alocar IPs específicos para as Edge Locations, assim o SSL funciona.
A versão recomendada é a **TLS1.1_2016** para a Security Policy.