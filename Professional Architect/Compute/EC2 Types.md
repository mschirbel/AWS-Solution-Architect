# EC2 Types

Antigamente os SO eram todos em Bare Metal, depois tivemos o advento da virtualização. Hoje, temos diversas técnicas para otimizar tanto a virtualização que parece que estamos no bare metal.

![](../media/virtualization.png)

Em 2017 temos o AWS Nitro 2017. Tenta usar virtualização *across the board*. No exame podemos ter perguntas que vão perguntar virtualização próxima ao bare metal, no caso é o Nitro.

Claro que a AWS oferece soluções em Bare Metal, mas o Nitro oferece uma performance com perda de 0.5% em relação ao bare metal.

Quando escolher os tipos de famílias, temos que saber qual o propósito de cada uma delas. Se não tivermos qualquer requisitos, usamos o General Purpose.

1. General Purpose
    * A, T, M
2. Compute Optimized
    * C
3. Memory Optimized
    * R, X, Z
4. Accelerated Optimized
    * P, G, Inf, F
5. Storage Optimized
    * I3, D, H

Para ver com detalhe, clique ![aqui](https://aws.amazon.com/pt/ec2/instance-types/)

conforme o tipo de produtos, podemos ter créditos. Caso usemos menos CPU do que o treshold, ganhamos créditos que podem ser usados caso usemos mais CPU do que o treshold.

E temos que lembrar que as limitações de qualquer instâncias são:
    - cpu
    - memória
    - storage
    - network

Caso precise de um site para comparar, use o site ![ec2info](https://www.ec2instances.info/).