# Logging and Monitoring

Como parte do cloudfront, temos a parte de Report & Analytics.
Aqui podemos ver várias estatísticas de nossas distribuições, como quantidade de quests, dados enviados, códigos de HTTP, download e etc.

Podemos quebrar as informações por data ou até mesmo por países. Ou por tipos de devices, OS, browsers.

Podemos até criar CW Alarms baseado nas métricas do cloudfront. Assim podemos fazer uma série de Event Triggers.

E dentro da distribuição, podemos habilitar as logs e colocá-las em um bucket. Temos informações detalhadas dos requests feitas aos objetos. Podemos também fazer logs dos cookies do site.