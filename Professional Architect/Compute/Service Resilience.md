# Service Resilience

Esse é um tópico muito crítico para a arquitetura da AWS.
Precisamos saber o quanto são os produtos que a AWS oferece.

## IAM

É o produto que faz gestão de acesso. É global, portanto não precisamos selecionar alguma region para interagir, isso porque os dados são replicados globalmente.

Digamos que o us-east-1 falhe. Ainda assim podemos interagir com o IAM.

## EC2/EBS

O serviço de compute da AWS são localizados em regiões e até AZ. Caso uma AZ ou uma region falhe, teremos uma interrupção no serviço.

Com isso podemos até perder dados nos nossos EBS. Devemos ter a ideia que uma AZ é um ponto de falha crítico para nossa arquitetura, por exemplo, para que um EC2 converse com um EBS, precisam estar na mesma AZ.

Para adicionar resiliência, podemos tirar um snapshot e movê-lo para outra region.

## S3

É resiliente regionalmente. Apesar de acessarmos globalmente o serviço, o objeto é salvo em uma region e replicado para múltiplas AZ's.

## R53

Opera de dentro de Edge Locations, que são globais.
Assim são resistentes até caso aconteça algo na AWS Region

## ELB/ALB

São regionais e específicos em uma AZ.

Quando colocamos um ELB, temos uma NI atralada a esse LB, pois estão em uma subnet. Para aumentar a resiliência, podemos fazer uma arquitetura para replicação em múltiplas AZ.

## VPC

Também são regionais, e podem operar em AZ. São FT caso falhe uma region, mas não as subnets, pois são linkadas com uma AZ.

Existem produtos que são HA, como IGW, VPC Peer e etc, **mas as subnets não**.

## NAT GW

Estão em uma subnet específica, logo, precisamos de rendundância em cada subnet para ser HA. Para aumentar a resiliência, podemos colocar um NAT GW em cada AZ.

## ASG

ASG são regionais, mas não presos a uma AZ, ou seja, podemos deixar o ASG para ser anti falha em uma AZ. Porque ele pode subir instâncias em diversas AZ.

## VPN

São configuradas com um Virtual Private Gateway. E temos os Endpoints dele que são associados com AZs. Assim, podemos associar a múltiplas AZ, para termos FT.