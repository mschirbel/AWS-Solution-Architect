# ELB

## Arquitetura

Esse ![link](https://aws.amazon.com/pt/elasticloadbalancing/features/) pode ajudar muito.

Um dos produtos mais importantes para a arquitetura na AWS. Elastic Load Balancer tem três formas:
    - classic
    - application
    - network

Um LB aceita conexões de um client e repassa entre um ou mais serviços. São desenhados para se integrar com outros recursos. Um ELB tem o próprio DNS, gerenciado pela AWS, que permite o acesso externo ou interno. Se for externo, chamamos de *public-facing*, se for *internal* ele somente é acessível de uma vpc.

Ele tem HA e FT definidos pela AWS, assim como faz cross-zone balancing entre as AZs.

A AWS cria um node em cada AZ que você selecionou, esse node é quem faz o balancing. Mas isso é totalmente gerenciado pela AWS.
Isso ajuda inclusive com certificados, pois podemos deixar o certificado no LB(CLB ou ALB) e assim aliviamos o tráfego dos EC2, chamamos isso de *SSL Offload*.

## CLB

CLB foi deprecado pela AWS, então **não** devemos usar.
Quando criamos, podemos selecionar se vai ser interno ou externo, assim como criar os *Listeners*.
Um Listener contém duas partes essenciais:
    - um protocolo e uma porta do node do CLB
    - um protocolo e uma porta da instância EC2

Uma boa prática é deixar o acesso público somente liberado no SG do LB, e restringir os SG das instâncias para somente receber do LB.

No fim, configuramos também o *Health Check*, que é o que determina para qual instância o CLB pode enviar o tráfego. Se uma instância não estiver OK, ele não recebe tráfego.

Para evitar perda de sessão, podemos habilitar a *stickness*, isso é feito diretamente no LB.
Uma coisa importante para entender é que: um CLB não está na camada 7, isso significa que cada Listener só pode ter 1 target. Não há um *granular routing*. Não podemos um routing especial para paths específicos.

## ALB

É um LB que fica na camada 7, de aplicação. ALB são mais performáticos, mais baratos e podem tomar decisões de balancing baseado em especificações da camada 7, por exemplo, URI's.

ALB pode usar IPv4 ou IPv6, e do mesmo jeito que um CLB pode ser interno ou externo.

Mas a principal diferença entre eles são os Listeners. Um CLB pode ter diversos protocolos, mas um ALB, como está na camada 7, só pode ter HTTP e HTTPS. E não temos a possibilidade de definir as portas de instâncias.

A partir disso, podemos criar Rules, baseado nos Listeners que temos. Existem dois tipos de rules:
    - host-based rules: baseados no header do http
    - path-based rules: baseados na URI do header

Com isso criamos os target groups que serão os alvos dessas rules. Os alvos podem ser:
    - instâncias
    - IP
    - Lambda

Por isso, o ALB é suportado pelos seguintes serviços:
    - ec2
    - ecs
    - eks
    - https e https/2
    - websockets

Por ter essa possibilidade de múltiplos targets para múltiplas rules podemos ter também múltiplos certificados, usando ![SNI](https://docs.aws.amazon.com/pt_br/AmazonCloudFront/latest/DeveloperGuide/cnames-https-dedicated-ip-or-sni.html)(Server Name Indication).

Podemos ter diversas conexões em um ALB, e tudo é escalado pela AWS.

## NLB

Operam na camada 4 de networ, ou seja, na camada de TCP. E por isso, tem uma performance **incrível**. Isso porque a camada 4 demanda menos processamento. Principalmente bom quando temos tráfego imprevisível. **Não suporta UDP**.

NLB tem duas coisas importantes: IP estático e baixíssima latência. O IP fica em uma AZ específica, e pode ter um EIP, isso ajuda muito com proteção de firewall.

O NLB não cuida de criptografia, ele somente vê a parte de TCP do packet, não tem nada de criptografia. Quem tem que fazer a criptografia são as pontas finais.

Mas tem as mesmas configurações do que um ALB, eles tem Listeners, Target Groups e Targets.