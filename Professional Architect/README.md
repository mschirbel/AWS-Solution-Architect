# Professional Course AWS - Architect

## Accounts

    - [x] Accounts basics
    - [x] Regions and Azs
    - [x] Reliability
    - [x] Data Persistence
    - [x] OSI
    - [x] Limits
    - [x] Support Tiers
    - [x] Config
    - [x] Service Catalog
    - [x] Billing
    - [x] Migrating Data to AWS

## IAM

    - [x] Overview
    - [x] Policies
    - [x] Roles & Temporary Credentials
    - [x] Cross Account
    - [x] Organizations
    - [x] Service Control Policies
    - [x] Federation
    - [x] IAM Boundaries
    - [x] Policy Evaluation

## Network

    - [x] VPC
    - [x] RAM
    - [x] Routing
    - [x] NACL
    - [x] Security Groups
    - [x] Subnets & Internet Gateway
    - [x] DNS
    - [x] Flow Logs
    - [x] Endpoints
    - [x] VPC Peering
    - [x] VPN
    - [x] Direct Connect
    - [x] Transit Gateway
    - [x] KMS
    - [x] HSM
    - [x] Certificate
    - [x] Directory Service
    - [x] WAF
    - [x] Guard Duty

## Compute

    - [x] Concepts
    - [x] AMI
    - [x] EC2 Types
    - [x] Storage & Snapshots
    - [x] EC2 Roles
    - [x] HPC & Placement Groups
    - [x] Custom Logging
    - [x] Containers
    - [x] ECS
    - [x] Serverless
    - [x] Lambda
    - [x] API Gateway
    - [x] Service Resilience
    - [x] Stateless Architeture
    - [x] Spot or Reserved
    - [x] AutoScaling
    - [x] Multi AZ
    - [x] ELB
    Cloudfront:
        - [x] CloudFront
        - [x] Distributions
        - [x] Custom Origins
        - [x] Optimizing Cache
        - [x] Logging & Monitoring

## Route53

    - [x] Architecture
    - [x] Advanced Concept

## Storage

    - [x] Architecture
    - [x] Tiers
    - [x] Lifecycle
    - [x] Versioning & Locking
    - [x] Access Control
    - [x] Cross Region Replication
    - [x] Encryption
    - [x] Optimizing Performance
    - [x] Glacier
    - [x] EFS
    - [x] FSx
    - [x] File Gateway & Volume Gateway

## Data

    - [x] Self-Managed Databases
    - [x] Data Models & Engines
    - [x] RDS
    - [x] Aurora
    - [x] Athena
    - [x] DynamoDB
    - [x] Neptune
    - [x] QLDB
    - [x] DocumentDB & MongoDB
    - [x] ElastiCache
    - [x] MapReduce
    - [x] EMR
    - [x] Kinesis
    - [x] Redshift
    - [x] IoT
    - [x] QuickSight
    - [x] ElasticSearch

## Logs

    - [x] CloudWatch
    - [x] CloudTrail
    - [x] Route53 Logging
    - [x] S3 Logging

## Deploy

    - [x] SSM
    - [x] CloudFormation
    - [x] Elastic Beanstalk
    - [x] Ops Works
    - [x] Data Pipeline

## Integrations

    - [x] SQS
    - [x] SNS
    - [x] MQ
    - [x] Workflow