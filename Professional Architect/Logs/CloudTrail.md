# CloudTrail

CloudTrail é um serviço que guarda logs sobre as mudanças em uma conta da AWS, como alterar tipos de máquinas, criação de serviços e etc.

Sem qualquer configuração, ele já faz o log da API da AWS. Por default, os eventos ficam guardados por 90d. Os eventos somente são guardados quando fazem invocação da API da AWS.

Se houver qualquer automação, podemos fazer um *parse* do evento usando JSON.
As ações podem ser iniciadas por:
    - usuários
    - roles
    - serviços monitorados pelo CloudTrail(Lambda, por exemplo)

## Trail

É configurar o CloudTrail para se integrar com:
    - S3 buckets
    - CW Logs, caso haja algum problema com organizations, veja ![aqui](https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-required-policy-for-cloudwatch-logs.html)
    - CW Events

Um Trail pode ser configurado para todas as regions, ou somente para a region do serviço. Isso é bom, pois podemos fazer o log de serviços globais, como IAM. As boas práticas mandam adotar o CloudTrail para todas as regiões.

Depois podemos direcionar o trail para algum local, isso pode ou não incluir a criptografia.

Assim podemos fazer integrações com outros serviços da AWS, e criar automações.
Há diversas configurações, como por exemplo:
    - controlar tipos de eventos
    - eventos de dados
    - criptografia
    - sns topic

Um exemplo de como seria o flow de informações com storage direcionado:

![](../media/cloudtrail.png)

#### Data Events

Data events fazem o log de objetos em serviços. Por exemplo, no S3 ele faz a log de quando um objeto é inserido ou apagado.