# S3 Logging

S3 faz as logs é muito simples. Assim temos logs de todos os acessos a um bucket.

Podemos configurar um bucket para receber as logs de um outro bucket. As únicas condições são:
    - acessos de escrita nos buckets
    - estar na mesma region

Para permitir, basta ir em *Properties* -> *Server Access Logging*.

Depois entramos com o nome do bucket de destino, e um prefix, se necessário.