# Cloudwatch

## Architecture

É um sistema de monitoração, que recebe dados de outros sitemas.
O conceito fundamento é a *metric* que é uma pequena quantidade de dados, em um curto intervalo de tempo fornecido para o CW.

Esse dado em um ponto do tempo é conhecido como *data-point*. Cada data-point tem uma data relacionada, e uma unidade de medida. Por default, usamos um tempo de 5 minutos, mas podemos alterar isso, conforme a necessidade do serviço.

Se publicarmos os data-points a cada 60s, eles ficam disponíveis por 3h. E são inversamente proporcionais, para storage da AWS.

As métricas podem ser agrupadas em *namespaces*. Podemos ver os serviços que publicam nas métricas ![aqui](https://github.com/awsdocs/amazon-cloudwatch-user-guide/blob/master/doc_source/aws-services-cloudwatch-metrics.md).

Podemos criar as nossas próprias namespaces, caso os dados sejam inseridos de forma própria. Com essas métricas, podemos criar os nossos próprios Dashboards. Mas podemos fazer muito mais com os dados, assim que estão no CW.

Podemos fazer isso, inclusive de forma automática, criando um *Alarm*. Com isso, selecionamos uma namespace e depois algumas métricas específicas. E podemos criar uso de data-conforme a nossa necessidade.

##### Alarm

Podem estar em 3 estados:
    - OK: quando tem dados, mas não atingiram o treshold
    - ALARM: quando tem dados, e atingiu o treshold
    - INSUFFICIEN_DATA: quando não tem dados para o nível exigido.

Podemos fazer integrações, dependendo dos estados, seja para enviar notificações para um SNS Topic, e depois um Lambda. Ou até mesmo ativar um AutoScaling Group. Ou até mesmo ações em EC2.
*Para isso, é necessário instalar o agente do CW nas máquinas*.

## CloudWatch Logs

CW Logs tem alguns objetos fundamentais:
1. log group: é um container para streams de logs que tem a mesma retenção, acesso e monitoração.
    Geralmente é no nível do log group que realizamos as configurações.

2. log stream: são log events de uma mesma origem. Quando colocamos log events em sequencia, temos uma stream.

3. metric filter: definimos as métricas aplicadas para um log group. Podemos criar filtragens nas streams para pegar dados específicos.

4. log event: é um timestamp + a mensagem do evento de log