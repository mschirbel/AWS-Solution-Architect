# Route53 Logging

R53 é o serviço de DNS da AWS, usando tanto de forma pública, como privada nas VPC's da AWS.

Entretanto, o logging só funciona para a parte pública.
Dentro de Hosted Zones, podemos selecionar a parte de *Configure Query Logging*.

Isso se integra com o CW Logs, e será criado um novo Log Group. Mas independente de como escolhermos, todas as logs globais serão colocadas em um único bucket. O nome deve ser no formato:

`/aws/route53/meudomínio.com`.

Devemos criar uma role para o acesso do R53 colocar os dados de logs no CW.

As *Query Logging* guardam as seguintes informações:
    - timestamp
    - dns zone
    - query type
    - respose code
    - layer 4 protocol
    - r53 edge location
    - resolver ip

Cada log stream no CW representa uma edge location. A aws usa o código de aeroportos para referenciar as edge locations.