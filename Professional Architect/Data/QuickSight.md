# QuickSight

É um serviço de visualização de dados, BI. Pode ser integrado com outros serviços para obter dados e criar dashboards interativos.

Pode receber dados de:
    - athena
    - aurora
    - redshift
    - s3
    - spark
    - mariadb
    - mssql
    - mysql

Existem dois tipos de Billing, o Standard e o Enterprise. Mas o interessante é que temos dois tipos de usuários:
    - autores
    - leitores

Um pode construir os dashboards, os outros somente acesso de leitura.