# DynamoDB

DynamoDB é um DBaaS. E é totalmente regional, ou seja, um dado em uma region não sai dela.
O ponto fundamental do DynamoDB é ser um Key-Value Store Collection. E ele junta tudo em uma tabela. Essa tabela não tem um *schema* fixo e o tamanho da tabela é determinada conforme os dados.

Cada conjunto de Key-Value é chamado de Item, que funciona como uma linha, em um SGBD SQL. Mas isso se resume em uma *Primary Key for an Item*. Essa PK é única na tabela toda. E é obrigatória.

Todos os outros dados, além da PK, são atributos. E podemos ter de 0 até quantos quisermos. Mas eles tem um tamanho máximo de 400KB. Geralmente, é o tamanho do item que determina o preço cobrado pelo serviço.

A PK pode ser:
    - uma partition/hash key
    - uma partition/hash key + sort key(SK)

Cada partition, pode ter até 10GB.

Cada vez que adicionamos um Item, podemos adicionar diferentes atributos, mas sempre com a mesma PK e SK.
Podemos ter a mesma PK ou SK, mas **nunca** as duas iguais.

## Capacity Unit

Podemos determinar Read Capacity Units(4KB) e Write Capacity Unity(1KB). Cada vez que lemos ou escrevemos usamos um item completo ou mais. Sempre que fazemos essa operação sempre consumimos uma Capacity Unit, no mínimo.

## Reads

Existem dois tipos de leituras:
    - scanning
    - querying

Scanning é ruim e ineficiente, pois sempre usamos todas as Capacity Units.
Querying pode ser usado para filtrar PK e SK. Depois podemos adicionar LSI(Local Secondary Indexes) para fazer queries com mesma PK e SK diferentes.
Quando queremos filtrar entre PK e SK diferentes usamos GSI(Global Secondary Indexes).

Só usamos Scanning quando precisamos ver mais de uma PK.

## Archictecture

Por baixo de cada table, existe uma *partition*.
Cada partition tem 10GB e tem 1000WCU e 3000RCU.

Se vc extrapolar cada um dos valores, uma nova partition é adicionada **e não podem ser desalocadas**.

Isso é imporante porque quando adicionamos um item, ele escolhe a partition baseado numa função hash da PK.
Entretanto, isso pode piorar muito a performance caso vc não calcule bem as units. Pra isso, podemos usar Indexes.

Podemos nos precaver e reduzir custos, podemos comprar Reserved Capacity Units, assim economizamos um pouco, assim como no EC2.

##### GSI
São globais e só podem ser criados na criação da tabela ou depois dela. Com ele podemos restringir nossas pesquisas com scanning.

E vai ter a própria contagem de Capacity Units. Com isso podemos usar o método de querying, ao invés de scanning.

##### LSI
Podem ser criados somente durante a criação da tabela. Eles servem como uma alternativa para a SK.

### Consistency

DynamoDB tem Consistência Eventual ou Consistência Imediata.
Quando usamos a eventual, usamos metade da capacidade reservada.
Se usarmos a imediata, usamos toda a capacidade.

### Backup

O backup do DyDB guarda não só os dados, mas a tabela e a capacity unit também. Por isso um restore volta tudo mesmo, até os indexes.

Assim como temos os *point in time restore*. Precisa ser habilitado por tabela explicitamente.

### Encryption

Por default não usa, mas podemos habilitar usando:
    - KMS
    - a nossa própria chave importada no KMS


## Advanced

Sempre lembrar que:
    - read pode ir até 4KB de informação
    - write pode ir até 1KB de informação

### Auto Scalling

Só funciona para o modo Provisioned de Billing.

Mas agora podemos configurar Auto Scalling para read e write. Ajusta-se a capacidade baseado no load. Pois definimos:
    - minimo
    - maximo 
    - % de uso

E precisamos de uma role pra fazer isso.

### Adaptive Capacity

Se temos uma tabela com uma grande quantidade mas os acessos consumindo menos RCU/WCU de uma partition do que outra, elas podem se "emprestar" essas capacidades sobrando.

### Streams

Um Item consiste de:
    - uma PK
    - uma SK(opcional)
    - 0 ou mais atributos

Streams listam partes de um Item que foram mudadas. Como se criássemos uma *view*. E assim podemos agir sobre isso, seja:
    - com alterações em keys(objetos pesados)
    - new images
    - old images
    - new and old images(nao tem overload para o banco)

São usadas principalmente para usar com triggers. Quando temos uma mudança na tabela, podemos fazer um trigger pra uma Lambda.

### TTL

Podemos fazer o expire de dados em uma tabela. Cada Item pode ter o seu TTL, ele acaba sendo um atributo no item.

### Global Tables

Podemos criar replicas em diversas regiões da AWS. Cuja replicação é feita de forma assíncrona master-master.
Ou seja, podemos ter read/write em todas as tabelas.
Depois que adicionamos dados a tabela, nao podemos mais adicionar regions. E se ainda formos para a region, a tabela continuará lá. Elas somente é retirada da replicação. Precisa apagar a tabela manualmente.

### Federation

Podemos usar o DynamoDB para fazer federation de aplicações, principalmente com apps mobile e apps web.
Para isso, podemos usar uma *IAM Policy Conditions for Fine Grained Access Control* aí restringimos o uso para somente atributos/items em uma tabela.

### DAX - DynamoDB Accelerator

Podemos fazer um *append* de um cache, como um ElastiCache na frente do DyDB. É um cluster de cache de memória integrado com as tabelas.