# DocumentDB & MongoDB

AWS DocumentDB é relacionado com o MongoDB, pois há compatibilidade. Nada  mais é que um cluster de Mongo, por baixo dos panos. Tanto é que usamos o *mongoshell* para fazer a conexão com o DocumentDB.

Há uma certa correlação entre as relações e os documentos:

Relações <-> Coleções
Linhas <-> Documentos
Colunas <-> Campos
PK <-> ObjectID
Index <-> Index
View <-> View
Array <-> Array

---

Os documentos geralmente são arquivos JSON que seguem formatos específicos com Key-Valye.

A arquitetura do serviço é assim:

![](../media/documentdb.png)

---

## Cluster

Cluster é a entidade base do DocumentDB, sempre funciona em cluster.
Selecionamos o tipo de instância e o número de instâncias. Sendo que precisamos de no mínimo 1, que será a Master, e as outras serão todas replicas.

Podemos depois escolher as opções de rede, criptografia(com rotatividade de chave), *point in time recovery*, logging e manutenções.

Sempre que criamos um cluster, temos que usar um SG, se não selecionarmos, criar-se-á um novo.

Por default, o cluster tenta alocar as instâncias nas AZ diferentes da Master, mas só se possível.
E as replicas só podem ser usadas para *read*.

### Scaling

Podemos fazer o Scale-Out(para ter mais leitura), criando mais replicas;
Podemos fazer o Scale-Up(para ter mais escrita), aumentando o tipo de instância primária.

### Cluster Storage Volume

Aqui também é usado um CSV, pois todas as replicas podem ler, mas somente a primária pode escrever.

### Backup

Todo o backup é feito em S3, com até 35 dias para o *point in time recovery*.

### Failover

Caso uma das instâncias de read falhar, o DocumentDB faz o scaling.

Se a master falhar, é feito o chaveamento de master para uma instância de replica, e depois feito o scaling de replicas.