# Redshift

Redshift é um Data Warehouse para Petabytes de dados. Redshift se integra com diversos outros serviços.
Aguenta até 2 Petabytes de dados, e veio de um fork do PostgreSQL.

Como modificação, é uma DB baseado em Colunas. Ou seja, os dados são guardados e disco como *column arrangement*. Se fizermos uma query, vamos ler somente os dados agregados a uma coluna. No exemplo abaixo, os dados em amarelo serão lidos:

![](../media/columndata.png)

E assim temos a arquitetura dentro do Redshift:

![](../media/redshift.png)

Quando criamos o cluster de redshift, o próprio serviço indica o tipo de máquina que vamos usar, e quantos nodes. Isso baseado na quantidade de dados que colocamos.

Os Nodes podem ser de dois tipos:
    - Leader Node
    - Compute Node

Leader são aqueles que controlam as tasks e distribuição. E os Computes que executam as queries nos dados.

Redshift permite escolhermos tipos de máquinas:
    - dc2 type: vem com SSD e serve para tasks de poder computacional
    - ds2 type: vem com discos magnéticos e mais storage, e serve para tasks que usam muito storages.

Os nodes podem ter ou 2 ou 16 slices. Cada slice o componente resposável por realizar as pequenas partes de uma task.
Quando o Leader manda uma query pro Compute, na verdade, ele manda pequenas partes, ainda menores, para os Slices. Os slices podem dividir os dados de acordo com a necessidade e tipo de dados.

#### Encryption

Os dados podem ser criptografados *at rest* ou *at transit*, o que é muito importante para a empresa, e tudo isso controlado via KMS.

## Load / Unload

É a parte que insere ou retira dados do Redshift. E para otimizar os processos de load, **temos que usar múltiplos arquivos**. Um único arquivo será dado para somente um Slice.

O Redshift é feito para ser usado de forma paralela. Então queremos que o nosso Load seja feito como um múltiplo do número de slices.

## Optimizing

Assim como as Reserverd Instances, podemos fazer com que os dados ainda sejam persitidos, além de diminuir o custo, usando os Reserved Nodes

---

Podemos então fazer o Resize do Cluster, usando dois modos:
    - classic resize: coloca o cluster em read-only e clona o cluster e os dados para um novo. assim podemos trocar os tipos das máquinas e aumentar os números de nodes.
    - elastic resize: não podemos trocar o tipo de máquinas, mas podemos trocar a quantidade, e isso é muito mais rápido;

---

Podemos fazer snapshots, podendo ser automáticos ou manuais.
Eles tem um período de retenção, que deve ser ajustado.

## Disaster Recovery

Redshift usa os snapshots automáticos, e se quisermos que eles durem, usamos uma retenção de **-1** dias.
E o Redshift usa o S3 para o DR.

E é com os snapshots que ele guarda lá que é possível superar um disastre. Mesmo que o bucket esteja em outra região, podemos fazer um cross replication para lá.

Para o restore, podemos fazer:
    - de todo o warehouse
    - somente uma tabela

Nos dois selecionamos um snapshot, só que na tabela temos que selecionar o DB e a tabela também.

Para os backups, os nodes podem ser restaurados usando outros computes. Ou seja, para estratégias de DR, precisamos usar múltiplos nodes.