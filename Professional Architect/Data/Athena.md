# Athena 

Athena é um serviço serverless que permite fazer queries SQL em dados de um S3. Isso inclui diversos formatos:
    - XML
    - JSON
    - CSV
    - TSV
    - AVRO
    - ORC
    - PARQUET

Athena permite criar um *schema* sob os dados do S3. Ele faz um overlay sobre os dados, chamado de *schema on read*. E aí podemos fazer queries com os dados. Não precisamos fazer processo de ETL(Extract-Transform-Load), operamos nos dados brutos.

Não precisamos usar servidores com o Athena, apenas pagamos pelo que usamos.

As tabelas definidas são guardadas num *catalog*. Por causa disso, a performance é bem rápida e não modificamos os dados do S3. Nenhum dado é guardado no Athena.

Podemos criar uma tabela como segue:

```sql
CREATE EXTERNAL TABLE animal (
    id BIGINT,
    type STRING,
    race STRING
)
STORED AS ORCFILE
LOCATION 's3://<MY BUCKET>';
```

E depois podemos fazer uma query normal:

```sql
SELECT * FROM animal
WHERE type = 'cachorro' AND race = 'rotweiller';
```

Mesmo que seja uma quantidade enorme de dados, o Athena faz um processamento muito rápido.
E podemos integrar isso com outros serviços, como CW, Cloudtrail, elb e vpc.