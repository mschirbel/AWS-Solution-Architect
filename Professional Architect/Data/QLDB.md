# QLDB

Amazon Quantum Ledger Database.
É um DB imutável que usa criptografia *ledger* pra fazer um *backtrack* das mudanças nos dados. Isso evita que seja manipulado, diferente dos outros SGBD.

QLDB usa o modelo de documentos, assim temos dados complexos em estruturas fácieis de fazer uma query.
Ainda temos o modelo ACID para consistência transacional. Isso significa que quando uma transação termina, ou ela termina com sucesso ou com falha(e rollback).

QLDB é totalmente serverless. Ou seja, não temos provisionamento de I/O.

## Hash Chain

Quando um dado é inicialmente adicionado é criado uma HASH para esse dado. Quando uma nova versão é adicionado, uma nova hash é criada somando-se com a hash antiga, formando uma *chain*. Quanto mais dados, mais difícil de quebrar essa *chain*.

Por causa disso, QLDB é ótimo para sistemas que necessitamd e extrema confiabilidade nos dados.

```
cat file.json | shasum -a 256 > hash1
cat file.json hash1 | shasum -a 256 > hash2
cat file.json hash2 | shasum -a 256 > hash3
```

Assim temos uma chain de hashes.