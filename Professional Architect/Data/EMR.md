# EMR

## Map Reduce

É um processo de arquitetura para análise de enormes quantidades de dados de forma paralela. É ótimo para dados modularizados no quais executamos operações básicas em escala.

MapReduce tem dois principais processos:
    - map
    - reduce

O processo é o seguinte:
    primeiro fazemos reconhecimento de padrões, assim podemos quebrar os dados entre *splits*, baseado no tamanho. Depois, cada *split* sofre um processo paralelo, de acordo com a necessidade, assim temos um map:

![](../media/emr1.png)

Esse processo de criação de nodes(cada um com um map) é feito de forma paralela. Mesmo quando temos enormes quantidades de dados, o Map Reduce é uma das melhores estratégias.

Geralmente os frameworks que fazem MapReduce, cuidam da automação entre os processos de map e os processos de reduce.

E depois de gerar os *maps* fazemos o **reduce**. Para isso, agrupamos os dados com correlação.:

![](../media/emr2.png)

E agora fazemos o reduce, para compilar os grupos em dados correlacionados:

![](../media/emr3.png)

## EMR Architecture

É um ecossistema de softwares que é viabilizado para grandes quantidades de dados. Alguns dos softwares temos:
    - apache hadoop
    - apache spark
    - hive
    - presto
    - ganglia
    - hue
    - mahout
    - hbase
    - pig
    - outros

Podemos ver todos ![aqui](https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-release-components.html).

Com o EMR temos um cluster para fazer esse processo de BigData. Mas podemos criar esse cluster de algumas formas:
    - série de processos escolhidos arbitrariamente(como uma parte de outro serviço da aws)
    - um cluster com todas as funcionalidades, gerenciado pela AWS
    - um cluster para você fazer SSH e fazer as atividades

*Por default, todas as instâncias estão na mesma AZ, pela baixa latência na comunicação.*

### Master Node

Sempre temos que criar primeiramente um Master Node(Hadoop Distributed File System Name Node - HDFS Name Node). O Master gerencia o cluster, ele faz a distribuição de workloads e monitora os Core Nodes.

Se precisarmos fazer SSH, é sempre no Master.

Não podemos trocar o tipo de billing na instância do Master, uma vez escolhido, é isso.

### Core Nodes

EMR pode ter de 0 até vários Nodes(HDFS Data Nodes), que são gerenciados pelo Master.
Eles processam as tasks e executam os processos de Map e Reduce.
Se o os Core Nodes falhar, podemos perder dados, mesmo havendo uma replicação entre eles.

Podemos usar inclusive instâncias Spots - mas temos o risco de perder dados.

### Task Nodes

São opicionais, e podem ser usados para tasks que não tem envolvimento com o storage do HDFS. Com esses sim, é ótimo usarmos Spots.

### Storage

Pode ser feito de duas formas:
    - HDFS
    - EMRFS

HDFS é o default do Hadoop. Usamos os discos das instâncias para guardar os dados que são processandos dentro do cluster.
EMRFS é usar o S3 como backend, assim temos persistência além da vida do cluster.

## EMR Performance Optimization

Há poucas coisas que podemos fazer para garantiar a melhor performance. Podemos ver todas as recomendações da AWS ![aqui](https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-plan-instances-guidelines.html).

1. Temos que fazer o deploy do cluster em uma region que seja próximo a origem e ao local de destino dos dados.
2. Temos que considerar os tipos de instâncias, bem como o billing de cada uma delas.
3. Os melhores softwares para o nosso tipo de aplicação.
4. Previnir job failure quando uma spot terminar.