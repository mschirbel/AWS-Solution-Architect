# Aurora

É um produto desenhado pela AWS para fornecer extra performance, com um custo adicional.

No MultiAZ convencional temos uma replicação do master para o slave, mas nunca acessamos o slave quando não estiver sendo usado. Mas no master podemos fazer o acesso, com write e read.

No Aurora só temos compatibilidade com MySQL e PostgreSQL. Sendo que a performance é 5x e 3x, respectivamente.
Mas a diferença principal é que temos endpoint para as instâncias para as replicas, diferente de um MultiAZ. Isso acontece porque por baixo dos panos, no Aurora eles dividem um *Cluster Storage Volume*, que é acessível por todas as instâncias. Assim não temos que fazer replicação entre as instâncias.

#### DB engine

Podemos ter:
    - provisioned: que é o comum, no qual temos um servidor
    - provisioned with paralel query enabled: tem melhor performance para query analítica
    - serverless: aurora faz o scale baseado em um minimo e máximo para o cluster

Note que aqui, temos a possibilidade de escolher MultiAZ, ou não, mas é um pouco diferente do MultiAZ convencional.

Ainda precisamos de um subnet group, e precisamos configurar o SG para acesso.

### Failover

Aqui é uma das diferenças, na qual podemos escolher o tier das replicas para qual queremos escolher como master.

### Backtrack

Podemos manter um tempo para o qual gostaríamos de ter o backup pronto para restore. Podemos escolher um ponto específico no tempo para restaurar o banco.Tudo isso é feito pelo *Cluster Storage Volume*, ou seja, as instâncias não precisam se preocupar

### Cloning

Podemos fazer o cloning usando o Aurora, pois é mais rápido, pois os clones são criados baseados no Cluster Storage Volume. É um clone independente do database, ou seja, qualquer mudança não afeta o original.

---
*o restante das configs são iguais ao RDS convencional*

---

## Architecture

Temos uma *Primary Instance*, que é a instância que pode ler e escrever. As outras são somente replicas, com capacidades de read, somente.

Podemos ter até 50 replicas em um cluster. Mas essas replicas acessam o mesmo storage volume, diferente de um RDS convencional. O tamanho desse storage é de 64TiB e ele aumenta de tamanho conforme dados são adcionados.

Temos alguns endpoints para o Aurora. Temos o principal, que é o Writer, mas temos o endpoint de Reader, que faz o mapeamento para todas as replicas. Quanto mais replicas, mais HA. Por isso, fazemos o deploy no mínimo, uma replica em cada AZ.

![](../media/aurora.png)

**News** Agora podemos ter multimaster no aurora: https://aws.amazon.com/pt/about-aws/whats-new/2019/08/amazon-aurora-multimaster-now-generally-available/

### Auto Scaling Replicas

Nas *actions* podemos adicionar um Auto Scaling Policy, baseado:
    - cpu
    - numero de conexões

Aí definimos o mínimo e o máximo de instâncias.

### Aurora Global Database

Temos duas infras de Aurora, em duas regiões distintas, e a replicação acontece no storage delas. A primary instance além de escrever no cluster regional, envia o sinal para um servidor de replicação que faz a comunicação com a região remota e faz a escrita no storage remoto. Tem baixa latência entre a replicação remota e se recupera muito rápido de uma falha regional.

![](../media/aurora-global.png)

## Serverless

Uma das limitações era estar na mesma arquitetura do que o RDS convencional. Imagine que você está invocando milhares de Lambdas que fazem conexões com o banco, mas com o Aurora Serverless temos:
    - acesso ao BD como serviço
    - acesso aos dados via API convencionais, sem usar clients de SGBD.

Ao selecionar o DB Engine como Serverless, apenas vamos configurar o nome do DB e o usuário root. Nenhuma configuração de storage ou de tamanho da instância importa.

O que vamos definir é o Aurora Capacity Units(ACU): que são formas do Aurora calcular o uso/custo e também o scaling, baseado no número de conexões.
Temos também a capacidade de definir um *pause*. No qual o cluster é "desligado" após um snapshot.

AWS mantém uma *Warm Instance Pool* de diversos tamanhos de ACU prontas para acessar os dados diretamente do Cluster Storage Volume, assim não precisamos nos preocupar, pois todos os schemas são *migrated* para nossa instância alocada, via uma *proxy layer*.

Na perspectiva do usuário, não há drop de conexão, no máximo uma latência.

Um problema que ocorre é que não há MultiAZ, portanto, caso haja uma falha, tem que acontecer um failover para outra AZ com uma nova ACU(warm).

##### Data API

Por default isso não vem habilitado, temos que modificar o DB pra isso.
Agora, podemos nos conectar com o DB Serverless via console da AWS. Isso só é possível porque usamos Data API para acesso ao banco, que podemos fazer de forma programáticas também.