# IoT

Podemos ter até milhões de devices de IoT, comunidando e enviando dados. Para ess serviço temos alguns componentes essenciais:

### IoT Devices (Thing)

*Things* se comunicam usando um *IoT Gateway*(broker de mensagens) usando JSON com mensagens criptogradas.
Dentro do serviço, registramos uma thing e aí temos acesso ao gateway.

Essa comunicação apesar de segura é esporádica. E para isso temos o conceito de *IoT Shadows*. 

### IoT Shadows

Que é uma representação da entidade física, que tem o estado desse device. Mesmo com uma comunicação com o gateway, todas as entidades podem saber qual é o status de um device específico.

### IoT Gateway

Permite uma comunicação assíncrona entre os devices.

### Topics

São específicos de uma conta e região, e servem para a gente separar os devices em tipos específicos, como por exemplo:
    - sensores
    - devices da cozinha
    - carro

e etc.

Para um topic, temos os subscribers, que recebem as mensagens do topic. Podendo ou não ser outros devices de IoT.

### IoT Rules

Podemos reagir a dados publicados a um tópico. E podemos fazer um integração com:
    - cw
    - s3
    - dynamodb
    - republish - para outro topic
    - lambda
    - step function

---

Existe uma função, chamada *basic ingest*, que faz com que uma thing fale diretamente com uma rule. E assim evita um tópic, o que diminui o custo.