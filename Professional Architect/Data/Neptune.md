# Neptune

Nepture é um NoSQL Graph DB. 
É altamente disponível e temos uma API disponível pelo TinkerPop Gremlim.

Podemos escalar até 15 Read Replicas com storage até 64TB.

Podemos fazer criptografia dos dados *at rest*. E os dados ficam distribuídos em até 3AZ.

Podemos usar para:
    - redes sociais
    - grafos de conhecimento
    - detecção de fraude
    - sistemas de recomendação

Sempre com relação entre dados.