# RDS

Relational Database Service. É um serviço da AWS que entrega um SGBD pronto para uso, com a gestão inteira sob a custódia da AWS.

Depois de provisionado, temos um endpoint para a conexão.

A primeira coisa a ser criada é uma *subnet group*. Que é um conjunto de subnets na qual poderão estar nossos componentes do DB. É a entidade onde as instâncias do DB vão parar. Por default, RDS é sempre privado, mas podemos expor para a internet.

Depois podemos criar o RDS com uma engine de diversos bancos de dados, sejam opensource ou comerciais.
Podemos usar o Aurora, mas veremos mais pra frente uma aula só dele.

A seguir, escolhemos qual o caso de uso:
    - aurora
    - production
    - dev/test

Cada um de acordo com a sua necessidade. Com o production, temos o MultiAZ, no qual temos uma replicação sincrona entre duas instâncias em duas AZ distâncias. Uma das instâncias será a Master, para onde o endpoint aponta.
Caso o master falhe, é feito uma troca do CNAME automaticamente para o slave. Mas ainda assim, nunca acessaremos o slave.

Podemos criar também Read Replicas, mas lembre-se que elas não melhoram a performance de write, somente de read. Para cada read replica, temos um endpoint. Para criar uma, basta ter um snaphost de uma instância. A comunicação entre elas fica a cargo da AWS, mesmo que não tenha VPC peering entre as instâncias.

A partir daqui, mudam para cada engine de SGBD. Como modelo de licença, tipo de disco, storage, tamanho da instância e etc.
Sim, os tipos de instância imitam os de EC2, só temos o *db.* na frente pra indicar que é dedicada para RDS.

Por último, escolhemos qual vai ser o login e senha do usuário root do banco. E vamos para as configurações avançadas:
    - redes e segurança
    - settings do SGBC
    - criptografia(difere dos tipos de engines) - criptografamos o disco das instâncias usando KMS
    - backup - tem o automático(default de 7d) e podemos fazer manualmente depois.
    - monitoração
    - export de logs
    - manutenção(pode ser automático)
    - proteção contra deletes

Pode demorar até 45 minutos para fazer o deploy do serviço. No caso do MultiAZ, primeiro ele faz o deploy da Master, tira um snapshot, faz o deploy do slave e aí configura a replicação.

Algo legal que podemos fazer é editar o SG do banco, para permitir que devices que tenham o SG do banco possam acessá-lo. Basta adicionar uma regra e referenciar o próprio SG.

## Parameter / Option Groups

São formas de modificarmos as instâncias usando opções que necessariamente precisam de root level access.
Assim podemos fazer tudo isso via console.

## Backups

Ao deletar uma instância, os backups permanecem, **mas só pelo período de retenção**.