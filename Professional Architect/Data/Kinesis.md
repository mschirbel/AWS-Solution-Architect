# Kinesis

Kinesis é um serviço que permite receber **grandes** quantidades de dados(arquivos ou streams) em tempo real.

## Architecture

![](../media/kinesis.png)

A arquitetura aceita mensagens em tempo real de um conjunto enorme de *producer*. Um producer é algo que coloca dados em uma *stream* do Kinesis. Por exemplo:
    - iot
    - mobile app
    - apps
    - ec2
    - on prem
    - apis

Um producer coloca os dados em uma stream fornecendo:
    - o nome da stream
    - uma partition key
    - dados a serem adicionados

Uma PK permite que o Kinesis selecione um *shard* para ingerir os dados.

Depois de colocado os dados, os *consumers* consomem. Um consumer é uma entidade que consome dados de ums stream, por exemplo:
    - ec2 com KCL - Kinesis Consumer Library
    - Lmabda
    - Kinesis Firehose

Podemos ter vários consumers ou apenas 1, mas o ideal é que possamos ter vários consumers para a mesma stream. 
O uso de *shards* é o que permite a escalabilidade do Kinesis. Existe no mínimo 1 shard dentro de cada stream. E podemos aumentar ou diminuir os shards automaticamente.

A ideia de boa prática pra uma arquitetura é sempre ter mais consumidores do que produtores.

### Stream

Stream são usados para coletar, processar e analisar grandes quantidades de dados. Uma stream é acesível de dentro de uma VPC ou pela internet. E podem escalar muito usando a arquitetura de multi-shard.

Não precisamos nos preocupar com storage, pois uma stream escala isso, de forma a guardar todos os dados recebidos por 24h e acessíveis até 7 dias(com custo).

Existem dois tipos:
    - data
    - video

E o processamento não acontece nas streams, é nos shards. Por isso, quando escalamos, é sempre o número de shards.

Podemos usar criptografia na nossa stream *at rest*, bem como configurar monitoração usando o CloudWatch, seja no nível do shard, ou na stream.

##### Enhanced Fan-out

Podemos definir consumidores para receber mais *throughput* de um shard. Assim definimos os consumidores que tem mais privilégios, até 5, mas podemos solicitar mais.

### Data Record

É uma entidade básica, que pode ser escrita ou lida por uma strea. Um data record consiste de:
    - um número sequencial
    - PK
    - data blob

Um blob pode ter até 1MB e é inalterável. E o número de sequencia é único dentro do shard.

### Shard

Uma stream tem no mínimo 1 shard. Que pode ingerir até 1MB de consumir até 2MB de dados. Para calcular quantos shards precisamos usamos a seguinte fórmula:
    req = max(ingestion/1024KB, read/2048KB)

Sabar o quanto precisaremos de shards é algo fundamental.

1 shard consegue:
    - escrever 1 MB/sec
    - ler 2 MB/sec
    - escrever 1000 records/sec

## Kinesis Firehose

É um serviço que pode entregar dados em tempo real para destinos. Podemos pegar dados de uma stream do Kinesis ou até mesmo de producers mais tradicionais.

Existem dois tipos de origens:
    - tradicionais
    - kinesis stream

Podemos fazer a entrega para:
    - S3
    - Redshift
    - AWS ElasticSearch
    - Splunk

Ainda assim, podemos manipular os dados enquanto são entregues. Podemos por exemplo, usar uma Lambda que faz isso. Ou até mesmo converter os dados apra outro formato, usando o AWS Glue.

## Kinesis Data Analytics

Esse serviço permite usarmos aplicações SQL para analisar os dados em tempo real. Podemos usar para:
    - análises em tempo real
    - dashboards em tempo real
    - métricas em tempo real

Mas o SQL opera em relações, e não tem dados em tempo real. Então, primeiro criamos um *Data Source* que é um link entre uma Stream e uma *In-Application Input Stream* que é uma tabela virtual que representa o conteúdo de uma Stream(por isso o uso de uma PK lá).

Como a Stream tem uma retenção de 24h podemos fazer essas queries quase como *point in time*. E com isso, podemos fornecer dados para um dashboard em tempo real.

Todos os dados que foram usados pelo SQL, são colocados em uma *In-Application Output Stream* que é uma tabela virtual que mapeia para um output do Analytics, por exemplo o Firehose.