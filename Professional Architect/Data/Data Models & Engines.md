# Data Models and Engines

Precisamos saber qual o tipo certo de DB pra responder as questões do exame. Principalmente os tipos de transações e os modelos de dados que serão aglomerados.

Existem dois tipos de Data Models:  
    - relational
    - non relational

## Relational

São quando os dados tem relações fixas. Data é guardada em colunas, assim como numa tabela.
Todas as relações tem um *schema* que tem um layout fixo com todas as colunas e linhas.
E todas as linhas precisam ter todas as colunas preenchidas(mesmo que seja com valor nulo).

A função principal é a confiabilidade dos dados, mas para isso, abrimos mão de um pouco da performance. Para isso temos o princípio ACID:
    - A: Atomicity - todas as operações tem somente dois retornos, ou sucesso da operação toda ou erro.
    - C: Consistency - quando as transações são completadas, o DB ta em um estado *válido*
    - I: Isolation - todas as transações acessam os dados uma de cada vez, nunca em conjunto
    - D: Durability - o resultado de uma transação é permanente.

Para a extração de dados usamos a linguagem SQL. É a linguagem usada para relações.

## Non Relational

São quando os dados não tem relações fixas, e não temos um *schema*, pois a estrutura dos dados pode variar.

A função principal é performance, para isso, temos o princípio BASE:
    - BA: Basic Available - o sistema tem um certo nível de disponibilidade
    - S: Soft state - o sistema pode mudar com o tempo, diferente da durabilidade 
    - E: Eventual consistency - o sistema pode reconhecer inputs, mas não garante distribuir isso ao mesmo tempo

Com o tempo, as replicas acabam migrando para um estado de consistência. Mas com isso, podemos ter máxima performance e escalabilidade.

---

## Types of DB

#### Key-Value

Por exemplo o DynamoDB

Temos um atributo de chave, e associamos outro valor, chamado de valor. Não temos relacionamentos ou schemas.
Temos queries muito rápidas e alta escalabilidade. Usamos para dados que são efêmeros, como tokens de sessões e etc.

#### Document DB

Por exemplo o MongoDB

Os dados são documentos, coleções de Key-Value. Temos aqui um certo tipo de relações, no qual podemos fazer interações entre documentos.
Mas ainda não temos relacionamentos entre os documentos. O mais importante, é entender a estrutura dos documentos.

#### Column Based DB

Por exemplo o Redshift

Eles guardam os dados em colunas, ao contrário dos SQL que guardam em linhas. Isso acontece para salvar tempo e ser mais eficiente quando buscamos por uma quantidade grande de dados.
Acaba sendo mais lento para consultas individuais, mas para Big Data, é ótimo.

##### Graph Based DB

Por exemplo o Neo4J

Desenhado para mostrar os relacionamento entre os dados em formato de grafos. Assim os dados são guardados em nodes e os relacionamentos acontecem entre os nodes.
É ótimo para redes socias ou qualquer relacionamento entre humanos. Geralmente tem a própria custom query.