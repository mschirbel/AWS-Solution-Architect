# ElasticSearch

AWS Elasticsearch é um serviço de Elasticsearch gerenciado pela AWS, que vem com Kibana e Logstash. Ou seja, toda a ELK Stack.
ES é um NoSQL DB, e tem uma capacidade de fazer queries quase em real time.

Kibana é uma ferramenta de visualização de dados;
Elasticsearch é uma ferramenta de guardar, procurar e analisar dados;
Beats/Logstash são ferramentas para coletar dados e transformá-las.

Um possível uso, de integração com outros serviços, é usar o CloudWatch como fonte dos dados.

## Architecture

![](../media/escluster.png)

Se quisermos usar o serviço por muito tempo, uma boa ideia é usar Reserved Instances, porque o ES usa muitos dados persistentes.

ES é ótimo para guardar dados de diferentes formatos, pois é um DB sem *schema*, e por isso, podemos escalar o sistema de forma mais fácil.
Por default, o estilo Production de deploy é feito em MultiAZ, e o de desenvolvimento é feito em uma única AZ.

Sempre quando possível, use MultiAZ, pois o cluster sempre cuida das integrações entre os Nodes, é totalmente gerenciado pela AWS. E a replicação entre os Nodes também é automática, quanto mais AZ, melhor a resiliência.

Existem dois tipos de criptografia:
    - node-to-node: só podemos habilitar isso quando criamos o cluster
    - at rest: usando KMS

E os backups são feitos com snapshots, automáticos em uma hora específica do dia.

---

Por boas práticas, devemos ter no mínimo 3 masters, para a replicação dos shards e no mínimo 3 data nodes. Sempre preferindo máquinas com múltiplos cores, pois o ES é feito para ser multithread.

#### Instances

1. Data Instances

É quem faz o trabalho de guardar e procurar os dados. São eles que fazem o trabalho pesado usando CPU, Memória e Disco. Dependendo do tipo da máquina, podemos até usar Instance Store. Se usarmos EBS, como default, podemos usar SSD ou até magnético.

2. Master Instances

É quem faz o gerenciamento das tarefas nos Nodes, bem como a replicação e *sharding*, o trabalho é mais leve em CPU, Memória e Disco.
Se houver problemas de performance, geralmente o problema está nas Data Instances.

### DocumentDB

ES é baseado em documentos, e temos aqui o conceito de *indexes* que são coleções de documentos relacionados.
Podemos dividir esses indexes em *shards*. Shards são divididos entre os nodes, com replicação.
Portanto, existe o *primary node* de um determinado shard, e os outros nodes são apenas réplicas.

Isso implica em alta resiliência, ou seja, o cluster pode se recuperar quando falhar.
E ótima saída de dados, pois podemos pegar os dados de múltiplas fontes.