# Self Managed Databases

Podemos instalar os servidores de databases em um EC2, com um EBS attachado.
Assim temos acesso ao SO, com acesso root e podemos editar o SO a nossa disposição.

## Riscos
1. admin overhead: performance, install, config, backup, DR
2. az fail: se a AZ falhar, podemos perder dados.
3. backups: o admin tem que fazer os backups para o S3 dos snapshots.
4. HA: precisa fazer uma sync manual além de configuração adicional no DB

Seria o mesmo princípio de usar um DB on-prem.