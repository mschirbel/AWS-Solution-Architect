# ElastiCache

É um serviço de caching da AWS, por baixo dos panos é um Redis ou Memcached.

A arquitetura do serviço é a seguinte:

![](../media/elasticache.png)

É totalmente controlado pela AWS, e server para melhorar o tempo de *querying* dos bancos de dados. Por com o EC temos um cluster, altamente escalável.

As aplicações podem fazer o check, se o dado está no cache, caso contrário, salva no cache e retorna. Por exemplo, podemos guardar *web sessions*.

Podemos ver a diferenca entre o Memcached e o Redis, clique ![aqui](https://aws.amazon.com/pt/elasticache/redis-vs-memcached/).

### Memcached

É usado para simplicidade nos dados, como key-value. Não tem criptografia e tem poucas features.
Um dos benefícios aqui, é que podemos usar *multi-thread*, **o que o redis não suporta**.

Aqui não tem failover, pois não tem MultiAZ.

### Redis

É usado para dados complexos, aqui podemos ter listas, arrays, sets e etc(diversos tipos de dados). Há criptografia e diversas features. E podemos usar o modo *node* ou *cluster*, porque **não suporta multi-thread**.

Aqui tem AZ e tem failover automático.