# SNS

Simple Notification Service.

Os componentes são: 
- Topic: que é o objeto no qual publicamos as mensagens
- Subscriber: um endpoint para o qual a mensagem é enviada, podendo ser:
    - http
    - https
    - email
    - email-json
    - sqs
    - apps
    - lambda
    - sms
- Publisher: que é a entidade que envia a mensagem , podendo ser:
    - apps
    - events
    - alarms do cw

---

O sns coordena as mensagens baseadas nos eventos de envio e envia para quem está inscrito naquele canal de mensagens.

## Fanout Pattern

Criamos um tópico no SNS, e consideramos a arquitetura no qual sempre que um evento acontecer, podemos levar os eventos para filas do SQS, no qual uma aplicação consome essa fila. Para cada fila, teremos uma subscrição no mesmo SNS Topic. Sempre que uma mensagem for publicada no tópico, ela seria processada por várias filas.

![](../media/fanout.png)

E cada um dos processamentos pode fazer as necessárias alterações.