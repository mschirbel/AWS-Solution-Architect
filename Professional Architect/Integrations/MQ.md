# MQ

SQS geralmente é baseado em 1-1 componentes, como um producer e um consumer.
Com o Amazon Messaging Queue podemos ter 1-n, ou até n-n.

É um message broker baseado no ApacheMQ, com os protoclos:
    - JMS
    - AMQP
    - MQTT
    - OpenWire
    - STOMP

Pode funcionar como uma fila ou como um tópico.
E é um serviço interno de VPC, e podemos ter um broker único ou em HA(active/standby).

Como o serviço é gerenciado pela AWS, temos a web console disponível em um endpoint próprio.

Apesar de suportar tópicos, podemos ter um *virtual tópic*, que é basicamente a arquitetura de fanout já integrada no serviço.

## Network Broker

Podemos desenhar topologias entre regiões, usando brokers via rede, podendo chegar até um nível global.