# Workflow & Step Functions

Simple Worflow Service e Step Functions são orquestradores de ações, tasks conectadas para um processo em larga escala.

SWF define um workflow, que é conjunto de atividades. Workflows vivem em *domínios*, que não podem ser violados por workflows externos ao domínio.

As atividades podem ser distribuídas e até assíncronas. Não há duplicadas e o SWF garante que as tasks serão executadas em ordem.

## Componentes do SWF

1. Starter: inicia o workflow
2. Workflow: sequencia de passos para fazer uma task
3. Activities: unidade de um workflow(passos)
4. Tasks: interage com o workflow e com os workers
    - activity task: manda o worker fazer uma tarefa
    - decision task: verifica o status do workflow e decide a proxima tarefa
5. Worker: quem faz a tarefa acontecer

Os workers eram instâncias EC2, que precisavam rodar 24/7. Por isso a AWS não recomenda mais esse serviço. Agora podemos usar o Step Functions.

Ainda temos uma orquestração de workflos, mas agora podemos usar Lambda, de forma totalmente serverless. Aqui temos alguns tipos de states:
1. action state
2. decision state
3. fail/succeed state
4. pass state
5. wait state
6. parallel state

Um action state pode ser:
- lambda
- ecs function
- ec2
- qualquer recurso que faz processamento de dados

As tasks podem durar até 1 ano.