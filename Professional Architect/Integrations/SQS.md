# SQS

Simple Queue Service. É um serviço que providencia uma fila de mensagens.
Com isso podemos colocar mensagens e consumi-las de outras formas.

O SQS nos permite desacoplar partes de uma aplicação, com isso o processamento pode ficar mais leve para o cliente.

As mensagens são retiradas por um processo chamado *polling*. Existem dois tipos:
    - *short polling*(default):  tem mais API request porque retorna subconjuntos de mensagens
    - *long polling*: espera até uma mensagem estar disponível para enviar uma resposta

Cada mensagem pode ter até 256KB de texto, em qualquer formato, e isso nos dá dois tipos de filas de mensagens:
    1. Standard: não garante ordem mas garante entrega. Pode escalar até um tamanho enorme
    2. FIFO: garante a ordem, mas não podemos escalar até o céu.

Uma das partes mais importantes das queues é que a mensagem está visível para o consumidor. Que é uma das formas de acelermos os consumos.
Depois de receber a mensagem, **devemos** deletar a mensagem com o *body* da mensagem, *ReceiptHandle*. Esse é o objeto necessário para remover a mensagem da fila por completo.

## Workflow

Podemos usar instâncias para consumir a fila, ou Lambdas.
Ou até mesmo fazer um scaling de instâncias baseadas em mensagens em fila.

Com o uso de filas, podemos escalar cada componente do workflow, porque o sistema de fila será escalável na AWS.

## Encryption

Podemos ter o KMS criptografando as mensagens, mas isso implica na descriptografia do lado da aplicação, também.