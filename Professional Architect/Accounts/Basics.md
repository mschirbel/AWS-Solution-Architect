# Account Basics

Uma conta é feita de 3 principais elementos:

    - a autenticação
    - a conta
    - a autorização

Dentro disso, temos os limites de isolamento da conta. Isso é chamado de *blast radius* pela Amazon.

Quando criamos uma conta, só temos o *root user*. Ele não tem habilidades de logar em outras contas e é o princípio de autorização e autorização da sua conta.

O *root* é o único pricípio que a conta autentica e autoriza a realizar ações. No caso, acessar os serviços da AWS.

E tudo isso é controlado pelo IAM. O acesso pode ser feito pode diversas formas, desde usando softwares de terceiros, até um simples *username* e *password*.

Quando um usuário tenta usar um serviço ele usa o *permission store* do IAM, e o *root* não tem limites para seus poderes. Inclusive, criar outros usuários, que começam com zero permissões.

## Introduction

Temos que entender que as contas da AWS são partes importantes do exame de arquiteto profissional. Tanto como parte de um AWS Organizations e as integrações com serviços, billings e principalmente IAM. Que provavelmente é um dos principais serviços do exame.