# Service Catalog

Esse serviço nos permite descrever tudo o que os departamentos podem ofereçer.
Assim podemos distribuir essa informação por toda a empresa.

Com isso temos um controle de permissões, preços e tudo mais, totalmente divido para a empresa.

Um administrador pode criar **produtos**, que são templates de cloudformations. Pode concentrar dois ou mais produtos em um **portifólio**.

Em um portifólio podemos definir acessos e permissões para diferentes grupos de pessoas.

Os usuários podem ver os produtos e criá-los(assim como stacks roles no cloudformation).

O Service Catalog é como uma web UI para o cloudformation para usuários leigos.
Assim podemos retirar as permissões dos usuários para alterar recursos. Pois podemos criar uma role para o portifólio, assim o usuário só escolhe o portifólio e não mexe nos recursos.

![](../media/servicecatalog.png)

Para garantir que o usuário final consiga fazer o provisionamento de recursos via Service Catalog, essa deve ser a policy na role dele:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "servicecatalog:ProvisionProduct"
            ],
            "Resource": "*"
        }
    ]
}
```