# Data Persistence

Existem três tipos de dados:

1. Ephemeral
São aqueles que ficam localmente e são perdidos quando o sistema é desligado.
Ex: ElastiCache, Instance Store Volume

É bom para altos IOPS ou altos troughtput

2. Transient
São aqueles dados que existem em um local temporariamente e são enviados para outros
Ex: SQS

É bom para manter dados e relações entre serviços, como circuit breakers

3. Persistent
São aqueles que são duráveis e sobrevivem mesmo quando são desligados.
EBS, EFS

É bom para guardar dados por grandes quantidades de tempo