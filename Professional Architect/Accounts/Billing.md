# Billing

Temos algums diferenças entre os modelos de cobrança entre *On-Demand*, *Reserved* e *Spot*.

## On-Demand

É o modelo default. Pagamos pelo:
    - que usamos por hora
    - GB transferido
    - GB guardado por mês

Não tem descontos e é bem preditivo.
Mas é o que tem prioridade neutra de ser iniciada.

## Reserved 

Temos um contrato com a AWS para comprarmos máquinas por um determinado tempo:
    1. 12 meses
    2. 36 meses

E temos 3 tipos de contrato:
    1. All Upfront
    2. Partial Upfront
    3. No Upfront

Cada qual com sua % de pagamento antes de começar a usar e com seus descontos.
Reservation pode ser usado para EC2, RDS, DynamoDB e outros serviços.
É ideal quando sabemos o quanto vamos usar e podemos prever isso.

Temos descontos e sabemos que será estável
Tem a maior prioridade de ser iniciada.

## Spot

É o qual toleramos interrupções para pagar menos. Fazemos uma *bid* do que queremos pagar e a instância ficará disponível até não atingirmos o limite do preço.
É ideal para workloads temporários.

Tem a menor prioridade de ser iniciada.