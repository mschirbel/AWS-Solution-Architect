# Reliability

## HA

*High Avaliable*

É ter duplicadas de recursos. E com isso conseguimos sobreviver a um desastre. Mesmo que haja uma indisponibilidade, mas conseguimos seguir com os recursos.

Um exemplo é um *AutoScaling Group* para várias AZ's.

## FT

*Fault Tolerance*

É termos várias redundâncias e o sistema pode rodar com qualquer uma delas e pode fazer a troca sem impacto para o usuário.

Um exemplo é um LoadBalancer com um Cache para não deixar dados sensíveis no webserver que pode falhar.

## DR

*Disaster Recovery*

É um processo que é desenhado para ocorrer. É um sistema que seria a recuperação de um sistema que falhou.
DR é feito para proteger o sistema principal em caso de um desastre.
Nunca devemos usar arquiteturas de MultiAZ **para DR**, mas sim uma solução própria de Backup.

Existem duas medidas para um DR:

#### RPO

RPO: Recover Point Objective
É o tempo entre um desastre e a última cópia de dados do negócio.
Isso pode ser acordado por horas, exemplo: 24h, 6h, 30min e etc.
Para isso, podemos minimizar o tempo com backups, snapshots e logs, para evitar perda de dados.

#### RTO

RTO: Recover Time Object
É o tempo entre um desastre e a hora que o sistema volta para ser testado pelo negócio.
Isso também pode ser acordado por horas, ou minutos até.
Precisamos localizar o backup, os dados e restaurar tudo, nem tudo é técnico, as vezes podemos precisar de certas pessoas para isso.

---

Quando é indicado o tempo de falha, lembramos que corrupção de dados podem ter sido replicados. Então arquiteturas de replicação síncronas podem ter um problema.