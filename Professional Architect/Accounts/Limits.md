## Account and Support Limits

Existem certos limites para as contas da AWS.
Inclusive para valores específicos de serviços.

Não precisamos saber dos valores especificamente, mas saber quais são os limites e como isso pode afetar a arquitetura da sua empresa ou solução.

![Aqui](https://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html) podemos ver todos os limites.