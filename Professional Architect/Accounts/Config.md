# Account Configuration

AWS Config guarda um estado de configurações de uma conta da AWS.
É um serviço bem flexível e podemos configurar triggers, notifications e etc.

Tem duas funções principais:

    - monitorar todos os recursos com o tempo
    - consegue seguir padrões de compliance dentro de recursos

O Compliance pode ser regras de security groups, saídas de IP e etc.
Para isso, podemos criar *rules*, como por exemplo a *restricted-common-ports* que verifica um conjunto de portas bloqueadas em um security group. Essa avaliação dos recursos pode demorar um pouco, por isso podemos clicar em *Reavaluate*.

Podemos ver também a relaçao com outros serviços da AWS.

Podemos fazer a arquitetura do serviço do seguinte modo:

![](../media/config.png)

Precisamos de algumas coisas para funcionar:

    - uma region para guardar as configurações
    - uma role para acesso nos serviços
    - ativar o serviço

Existem algumas palavras chaves para esse serviço:
    1. recurso = são serviços de uma conta aws
    2. configuration item = a partir do momento que habilitamos guardar as configurações de um recurso
    3. config stream = como o config comunica as mudanças para o administrador da conta(como um sns).

Pode demorar alguns minutos para o AWS Config pegar as informações.