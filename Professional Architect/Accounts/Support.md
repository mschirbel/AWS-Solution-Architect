# Support Tiers

Precisamos saber o que diferencia os planos de suporte.
Existem 4 tipos de suporte:

    - basic - é o que vêm quando vc cria a conta
    - developer - algo que vai um pouco além do default
    - business - se temos algo pequeno em produção
    - enterprise -  se temos algo grande em produção

Mas precisamos saber alguns pontos chaves para saber qual escolher.

Podemos ver quais são as diferenças vendo ![aqui](https://aws.amazon.com/pt/premiumsupport/plans/).

Os elementos mais importantes são:

    - Enhanced Technical Support
    - Architectural Guidance
    - Programmatic Case Management
    - Proactive Programs
    - Technical Account Management
    - Pricing