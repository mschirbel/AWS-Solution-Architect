# Migrating Data to AWS

Existem algumas formas de trazermos dados para a AWS. Entretanto, devemos considerar o tamanho dos dados quando fizermos as migrações. Para dados sigilosos, ou em grande volume, talvez alguma solução em hardware possa ser mais apropriada.

Temos as seguintes:

1. Snowball

É uma caixa que pode carregar até 80TB. Podemos levar dados do S3 para nosso On-Prem ou vice-versa. Eles são levados para o cliente de volta para uma localidade da AWS.

No On-prem usamos um client do Snowball para fazer a transferência. E temos criptografia de ponta-a-ponta feita pelo KMS, seja *at-rest* ou *at-transit*.

2. Snowball Edge

É parecido com o Snowball e pode carregar até 100TB. A diferença é que podemos fazer processamento nos dados durante o upload, usando Lambda ou EC2.
Por isso, existem três tipos:
    - storage optimized
    - compute optimized
    - compute optimized with cpu

3. Snowmobile

É um datacenter móvel. Que pode carregar até 10PB, mas é necessário um contato direto com a AWS. Vem um caminhão pra levar os dados até a AWS.
É possível usar de forma paralela, para maiores quantidades de dados.