# Region and AZs

*Regions* são um aglomerado de *Availability Zones* isolados de outras *regions*. *Availability Zones* são um ou mais datacentes dentro de uma *region*. De forma que não são impactados por um evento catastrófico, mas ainda assim, baixa latência de rede.

Não necessariamente temos uma única *region* em um país, podemos ter mais de uma, como nos EUA.

Devemos pensar nisso quando arquitetamos soluções globais ou que necessitam de um *failover*. Ou para nos proteger de informações de influências políticas, pois um dado de uma *region* não sairá dela.

*Edge Locations* são pequenos datacenters que são distribuídos globalmente em cidades grandes. São usados para distribuir conteúdo e dados entre serviços.
Não podemos fazer deploy de serviços em *edge locations*.