# OSI

As 7 camadas do modelo OSI de network são:

![](../media/osi.png)

O modelo divide a comunicação em rede em camadas isoladas com comunicação por protocolos.
Protocolos são conjuntos de regras com um propósito de entregar informações em um formato específico.

A regra do modelo é que as camadas inferiores entregam informações para as camadas superiores. A internet funciona assim hoje em dia.

A primeira camada é a camada física é o hardware. É onde são comunicados os bits, via energia elétrica.
A segunda camada é a camada de Data Link, onde estão o MacAddress que associa o hardware.
A terceira camada é a camada de Rede, onde temos o IP's. Não temos ordem.
A quarta camada é a camada de transporte, onde temos os protocolos de TCP e UDP. Aqui podemos ou não ter ordem
A quinta camada é a camada de sessão, onde temos as portas, nas quais podemos ter várias comunicações ao mesmo tempo
A sexta camada é a camada de apresentação, onde temos padrões de dados, como criptografia, por exemplo, no HTTPS(quem coloca a criptografia é a camada 6).
A sétima camada é a camada de aplicação, onde temos os softwares ou os dados como vemos e conhecemos.