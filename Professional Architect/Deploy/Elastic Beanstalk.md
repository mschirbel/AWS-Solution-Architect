# Elastic Beanstalk

Existem outros dois serviços que fazem deploy de recursos na AWS. Um deles é o Elastic Beanstalk.

O Elastic Beanstalk é um serviço que fornece uma plataforma para uma aplicação.
Existem alguns conceitos importantes:
    - application: que é uma composição de environments, versions e configurations
    - environments: um ambiente isolado de um tier(web ou worker)
    - version: uma disinta versão de um código que pode ser deployada em um environment

Os tiers, que podem ser web e worker desempenham funções distintas. O web aceita web requests e encaminham para alguma fila ou tópico. Worker recebem as requests e tratam na aplicação.

Quando criamos uma aplicação, temos que selecionar qual o tier. Existem alguns environments próprios, que estão nas docs da AWS.

Após fazer o deploy, o Elastic Beanstalk nos providencia uma URL para acesso da aplicação. O interessante, é podemos fazer o swap de environments, o que nos permite estabilizar uma estratégia de blue-green deployment.

O scaling é feito de forma automática, desde que exista um ELB. Somente marcamos o máximo de instâncias e o treshold de scaling.

Para restores e backups, podemos salvar os environments em configurations, que são arquivos YAML.