# Cloudformation

Cloudformation é o serviço que permite criarmos recursos na AWS via códigos. O CF funciona por meio de Stacks.
Uma stack é a aplicação de um Template, que pode ser escrito em YAML ou JSON.

No mínimo, tem um recurso que será criado pela Stack. A stack tem um ciclo de vida, e quando deletamos uma stack, todos os recursos associados também são deletados.

Sempre que criamos uma stack, será gerado uma event stream. Podemos ver isso:
    - da console
    - da linha de comando
    - por integração com o SNS

E sempre que temos um recurso, vamos ter um *Logical ID*. O que é diferente do *Physical ID*. O logical é o ID definido dentro do template do CF. O CF pega esses logical IDs e cria um physical ID relacionado, como o ID de uma instância EC2, por exemplo.

O nome da stack precisa ser único na região.

## Update Stack

Quando formos realizar um update, podemos:
    - alterar o template
    - alterar os parâmetros e o template

Quando alteramos o template, podemos adicionar recursos, remover recursos ou alterar os recursos que já existe. E cada um demanda um risco distinto.

Quando fazemos o update, clicando em *Actions -> Update Stack*, o CF nos permite ajustar os parâmetros. Quando selecionamos os templates, o CF compara os arquivos e vê quais são as diferenças nos recursos lógicos.

Após a comparação, ele cria/remove os recursos físicos.

Mas quando queremos alterar aquilo que já existe, ou seja, quando alteramos a configuração de um recurso lógico, temos mais risco envolvido. Existem 3 tipos de riscos:
    - update wo/ interruption: quando a alteração não causa downtime nos serviços
    - update w/ some interruption: quando tem um pequeno downtime
    - replacement: o maior tempo de downtime possível. geralmente quando um recurso é destruido e criado.

##### Change Set

Podemos criar um change set, que é uma forma de **propor** uma mudança em uma stack. O mais importante, é que ele mostra qual é o tipo de update. Assim, podemos controlar permissões de usuários para ter a autorização de aceitar ou não o change set.

## Portability & Reuse

Podemos reutilizar nossos templates em outras regiões e em qualquer quantidade desejada. Portabilidade é aplicar o template em diversas contas e regiões, sem limitações de uso.

Para usar isso precisamos alterar o nosso código para carregar padrões de design que são replicáveis entre regiões e contas, por exemplo:
    - reduzir o número de parâmetro
    - usar o SSM Parameter Store para pegar os valores em nossos parametros do CF
    - usar pseudo parameters(são parâmetros que sempre existem no CF)
    - não usar valores arbitrários, como nomes. Aí o CF cria nome usando um sufixo aleatório.

Pseudo parameters podem ser:

```json
{ "Ref" : "AWS::Region"}
```

Existem diversos pseudo parameters que podem ser usados.

## Stack Roles

Sem usarmos uma stack role, o CF vai usar as permissões da identidade que está criando a stack. Entretanto, podemos diferenciar isso, usando Stack Roles. Assim, cada stack vai ter as permissões para criar os recursos.

No serviço do IAM, podemos criar uma Role, uma de AWS Service, basta marcar Cloud Formation e criar as policies ali dentro.
Uma vez criada, podemos ver que o trust relatioships é assumido pelo CF. E ao criar uma stack, podemos selecionar uma Role específica nas opções.

## StackSets

StackSets permitem definir uma conta de administração e uma conta de *target*. Assim podemos fazer o deploy de CF em múltiplas contas e múltiplas regiões, partindo da conta de administração. Para isso, precisamos de duas roles:
    - uma role na conta de adm: `AWSCloudFormationstackSetAdministationRole`.
    - uma role nas contas de *target*: `AWSCloudFormationstackSetExecutionRole`.

Na conta de administração temos um stackset, que vai orquestrar o deploy nas stacks em outras contas.

## CF for Disaster Recovery

Isso está baseado no ![whitepaper sobre DR](https://aws.amazon.com/pt/blogs/aws/new-whitepaper-use-aws-for-disaster-recovery/). Aqui temos uma seção sobre backup e restores, uma delas é usando CloudFormation.

Existem diversas estratégias, sejam Pilot Lights(poucos recursos críticos), Warm Standby(mais recursos) ou Multi AZ(todos os serviços).

E tudo isso pode ser feito usando CF. Pois podemos guadar os arquivos em S3, e rodar de forma automatizada.

## Custom Resources

Sabemos que o CF possui recursos próprios, que podem ser usados de acordo com a documentação.
Entretanto, podemos querer integrar com serviços que estão fora da AWS.

Para isso, existem os Custom Resources. Eles podem ser:
    - baseados em um sns
    - baseados em um lambda

Eles enviam os dados para esses serviços com a esperança que será interceptado, e eventos ocorrerão a partir disso.