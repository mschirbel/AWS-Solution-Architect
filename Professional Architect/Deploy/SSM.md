# SSM

AWS Systems Manager é um componenente crucial da AWS, com ele podemos fazer a gestão de instâncias, seja na AWS ou On-Prem.

Ele faz uma série de atividades, devido aos componentes internos deles.
SSM faz o gerenciamento de recursos dentro da AWS, sejam Windows ou Linux. E faz isso usando um agente, que já vem nas novas AMIs da AWS.
Entretanto, precisamos de permissão para conversar com o SSM.

SSM transforma uma instância em uma SSM Managed Instance, que é um recurso controlável pelo SSM.
Para recursos da AWS, usamos o IAM.
Para recursos fora da AWS, precisamos fazer uma ativação do agente.

Um dos requerimentos do SSM é acesso a internet, pois o endpoint do SSM é na zona pública da AWS.
As muitas atividades que podemos fazer com o SSM são:


### Inventory

SSM coleta informações das instâncias que são SSM Managed Instance. Podemos ser informações como:
    - configurações
    - OS
    - serviços
    - arquivos
    - network
    - aplicações

E outras informações, que podemos fazer de forma customizada.

### Documents

É o que define que o SSM vai fazer nas instâncias. Podemos fazer:
    - estados das maquinas
    - rodar um comando
    - automações

Podem ser escritos em YAML ou JSON.


### Automation

É uma função que permite criar automações, como um workflow, dentro de várias instâncias.
Fora que podemos controlar as automações via roles do IAM, muito mais seguro.

### Run Command

Executamos um documento em uma instância. E podemos fazer isso em escala, garantindo um estado padrão para todas.

### Patch Manager

Conseguimos definir estados para os sistemas operacionais das instâncias, definimos janelas de manutenção, regularidade de updates.

---

Todos os serviços do SSM tem correlação, é como um aglomerado de ferramentas.

## SSM Parameter Store

Temos uma maneira segura de guardar dados e segredos das aplicações. Esses dados podem ser guardados como *plaintext* ou criptografados pelo KMS.
Com isso, separamos os dados dos códigos, quem podem ser versionados, controlados e auditados.
Com o parameter store podemos acessar instâncias/serviços:
    - ec2
    - ecs 
    - lambda
    - codebuild
    - opsworks

É totalmente serverless, resiliente e seguro.
E assim podemos pegar os secrets, dentro de um arquivo no FS, por exemplo.

Podemos, pegar esse secret numa Lambda, em python, assim:

```python
c = boto3.client('ssm')
env = os.environ['ENV']
app-config = os.environ['APP_CONFIG_PATH']
full-path = '/' + env + '/' + app-config
param_details = c.get_parameters_by_path(
    Path=full-path,
    Recursive=True,
    WithDecryption=True
)
print(param_details)
```

E adicionamos as variáveis de ambiente na Lambda.
Podemos usar `ENV` como prod, ou hmg. Assim teremos um path como `/prod/secret/`.