# Ops Works

É um produto de plataforma para deploy. É como se fosse um Chef/Puppet as a Service.

Tem uma plataforma de infraestrutura, mas sem perder tanta flexibilidade para o administrador.
O que é importante é a *Stack* que é o conceito mais importante. Uma stack pode representar um ambiente, seja prod, hmg ou dev ou até mesmo uma aplicação.

Uma stack é um único objeto de configuração. E cada stack é consistida em *layers*, assim como montamos uma arquitetura:

![](../media/opsworks.png)

Se formos usar nossos cookbooks, podemos apontar para um bucket ou para um repositório(com user e senha).
As Layers podem ser integradas com outros serviços, como RDS, ECS, ELB e etc. Uma coisa interessante, é que podemos configurar SG por *layers*, bem como logging no CW.

### Instances

Existem três tipos:
    - 24/7: instâncias que estão online o tempo todo
    - timebased: instâncias nas quais selecionamos o período do dia que estão online
    - loadbased: instâncias que iniciam baseados em resposta a CPU, memória ou métricas de instâncias em uma layer

### IAM

Podemos dar acessos a partes das layers, ou até mesmo a nível de recursos, sejam com permissões de deploy, leitura ou até mesmo admin.