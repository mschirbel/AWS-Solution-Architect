# Data Pipeline

É um serviço serverless que permite criar workflows de migrações/workflows de dados.
Por exemplo, podemos fazer a transposição de dados em db diferentes, considerando novas formas de armazenar os dados, como trocar o *schema*.

O Data Pipeline funciona baseado em tasks, com hierarquia ou ordem, nas quais podemos escolher diversos modos.

Baseado nos modos, escolhemos os parâmetros necessários. Inclusive, o schedule do pipeline e o logging(em um bucket).

Data Pipeline pode usar outros serviços da AWS por baixo dos panos, como EMR, por exemplo. O que pode ser muito bem utilizado para acelerar os processos. Quando usado, ele é usado como um cluster efêmero, ou seja, será destruido após o uso.